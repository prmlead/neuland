﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;
using PR = NeulandSAPIntegration.Neuland.PROD.PR;
using MAT = NeulandSAPIntegration.Neuland.Material1;
using VD = NeulandSAPIntegration.Neuland.Vendor1;
using INVOICE_REJECTION = NeulandSAPIntegration.INVOICE_REJECTION;
using GRN = NeulandSAPIntegration.NeulandSAPIntegration.GRN;
using PO = NeulandSAPIntegration.Neuland.PO;
using CURR = NeulandSAPIntegration.Neuland.Currency;
using CONTRACT = NeulandSAPIntegration.Neuland.Contracts;
using System.Runtime.Serialization;
using System.Net.Mail;
using PRMServices.SQLHelper;
using System.IO;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System.Text;
using OfficeOpenXml;
using PAYMENT = NeulandSAPIntegration.Payment;

namespace NeulandSAPIntegration
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static bool writeToDB = true;
        private static string hostURL = ConfigurationManager.AppSettings["Host"];
        private static string fileName = ConfigurationManager.AppSettings["PR_PROCESSING_FILE_NAME"];
        private static string filePath = ConfigurationManager.AppSettings["QCS_FILE_PATH_TO_READ"];
        private static string stagingFolder = ConfigurationManager.AppSettings["StagingFolder"];
        private static string processingFolder = ConfigurationManager.AppSettings["ProcessingFolder"];
        private static string processedFolder = ConfigurationManager.AppSettings["ProcessedFolder"];
        private static string errFolder = ConfigurationManager.AppSettings["ErrorFolder"];

        static void Main(string[] args)
        {
            bool isConnected = false;

            string jobType = ConfigurationManager.AppSettings["JOB_TYPE"].ToString();
            logger.Info("JOB_TYPE:" + jobType);

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
               jobType.Contains("MAT_JOB"))
            {
                GetMaterailDetails();
            }

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                jobType.Contains("VENDOR_JOB"))
            {
                GetVendorDetails();
            }

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                jobType.Contains("PR_JOB"))
            {
                GetPRDetails();
            }

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                jobType.Contains("GRN_JOB"))
            {
                GetGRNDetails();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "PO_JOB")
            {
                GetPOScheduleDetails();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "CURRENCY_JOB")
            {
                GetCurrencyDetails();
            }
            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "CONTRACT_JOB")
            {
                GetContractDetails();
            }
            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "PO_EMAIL")
            {
                GetPOData(ConfigurationManager.AppSettings["PO_EMAIL_JOB_ID"]);
            }
            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "GRN_EMAIL")
            {
                GetGRNData(ConfigurationManager.AppSettings["PO_EMAIL_JOB_ID"]);
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "INVOICE_REJECTION")
            {
                string hostURL = ConfigurationManager.AppSettings["INVOICE_REJECT_URL"].ToString();
                ICredentials credentials = null;
                credentials = GetConnection(hostURL, isConnected);
                if (credentials != null)
                {
                    GetInvoiceRejectionData(hostURL, credentials);
                } else
                {
                    logger.Error("Connection Failed");
                }
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "PAYMENT")
            {
                string hostURL = ConfigurationManager.AppSettings["PAYMENT_URL"].ToString();
                ICredentials credentials = null;
                credentials = GetConnection(hostURL, isConnected);
                if (credentials != null)
                {
                    GetPaymentData(hostURL, credentials);
                }
                else
                {
                    logger.Error("Connection Failed");
                }
            }

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                jobType.Contains("PR_EXCEL_JOB"))
            {
                ReadPRFiles();
            }
        }

        private static ICredentials GetConnection(string hostURL, bool isConnected)
        {
            ICredentials credentials = null;
            try
            {
                string newURL = hostURL;
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                credentials = netCredential.GetCredential(uri, "Basic");
                isConnected = true;
            }
            catch (Exception Ex)
            {
                isConnected = false;
                logger.Error("Connection Failure >>" + Ex.Message);
            }
            return credentials;
        }


        private static void GetInvoiceRejectionData(string hostURL, ICredentials credentials)
        {
            try
            {
                INVOICE_REJECTION.ZFM_INVOICE_REJECT test = new INVOICE_REJECTION.ZFM_INVOICE_REJECT();
                test.Url = hostURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
                string dateFieds = ConfigurationManager.AppSettings["INVOICE_REJECT_DATE"].ToString();
                INVOICE_REJECTION.ZFM_INVOICE_REJECT1 invoiceRejectObject = new INVOICE_REJECTION.ZFM_INVOICE_REJECT1();
                if (string.IsNullOrEmpty(dateFieds))
                {
                    dateFieds = DateTime.Now.ToString("yyyy-MM-dd");
                }
                invoiceRejectObject.IM_DATE = dateFieds;
                List<INVOICE_REJECTION.ZINVOICEREJECT> itemsArr = new List<INVOICE_REJECTION.ZINVOICEREJECT>();
                INVOICE_REJECTION.ZINVOICEREJECT singleValue = new INVOICE_REJECTION.ZINVOICEREJECT();
                singleValue.BELNR = null;
                singleValue.GJAHR = null;
                singleValue.LIFNR = null;
                singleValue.COMMENT = null;
                singleValue.USERNAME = null;
                singleValue.XBLNR = null;
                singleValue.DUMMY1 = null;
                singleValue.DUMMY2 = null;
                singleValue.DUMMY3 = null;
                singleValue.DUMMY4 = null;
                singleValue.DUMMY5 = null;
                
                itemsArr.Add(singleValue);

                invoiceRejectObject.IT_INV = itemsArr.ToArray();
                var response = test.CallZFM_INVOICE_REJECT(invoiceRejectObject);

                var invoiceRejections = response.IT_INV;

                logger.Debug("TOTAL Invoice Rejection Details: " + (invoiceRejections != null ? invoiceRejections.Count().ToString() : "0"));
                logger.Debug("END GetInvoiceRejectionData: " + DateTime.Now.ToString());

                List<string> columns = @"BELNR;GJAHR;LIFNR;COMMENT;USERNAME;XBLNR;DUMMY1;DUMMY2;DUMMY3;DUMMY4;DUMMY5".Split(';').ToList();

                if (invoiceRejections != null && invoiceRejections.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = invoiceRejections.AsEnumerable().ToList().Chunk(500);
                    logger.Debug("GetInvoiceRejection GUID: " + guid);
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        
                        prepareRows(chunk,dt, guid1, columns, columns[0]);
                        
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_INVOICE_REJECTION_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (invoiceRejections != null && invoiceRejections.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_invoice_rejection_details", sd);

                        string sendEmail = ConfigurationManager.AppSettings["SEND_INVOICE_EMAIL"].ToString();
                        if (sendEmail.Equals("TRUE"))
                        {
                            try 
                            {
                                GetInvoiceData(guid);
                            }
                            catch (Exception ex) 
                            {
                                throw new Exception($@"Email not being sent to the JOB_ID : {guid} for : {ex.Message}");
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GetInvoiceRejectionData ERROR");
            }
        }

        private static void GetPaymentData(string hostURL, ICredentials credentials)
        {
            try
            {
                PAYMENT.ZFM_PAYMENT_DETAILS_SRV test = new PAYMENT.ZFM_PAYMENT_DETAILS_SRV();
                test.Url = hostURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
                string dateFieds = ConfigurationManager.AppSettings["PAYMENT_DATE"].ToString();
                PAYMENT.ZFM_PAYMENT_DETAILS paymentObject = new PAYMENT.ZFM_PAYMENT_DETAILS();
                if (string.IsNullOrEmpty(dateFieds))
                {
                    dateFieds = DateTime.Now.ToString("yyyy-MM-dd");
                }
                paymentObject.IM_DATE = dateFieds;
                List<PAYMENT.ZSPAYDETAILS> itemsArr = new List<PAYMENT.ZSPAYDETAILS>();
                PAYMENT.ZSPAYDETAILS singleValue = new PAYMENT.ZSPAYDETAILS();
                singleValue.BELNR = null;
                singleValue.GJAHR = null;
                singleValue.BLART = null;
                singleValue.BLDAT = null;
                singleValue.BUDAT = null;
                singleValue.XBLNR = null;
                singleValue.LIFNR = null;
                singleValue.RMWWR = 0;
                singleValue.WAERS = null;
                singleValue.RBSTAT = null;
                singleValue.KIDNO = null;
                singleValue.ADDITIONAL1 = null;
                singleValue.ADDITIONAL2 = null;
                singleValue.ADDITIONAL3 = null;
                singleValue.ADDITIONAL4 = null;
                singleValue.ADDITIONAL5 = null;
                

                itemsArr.Add(singleValue);

                paymentObject.IT_PAYMENT = itemsArr.ToArray();
                var response = test.ZFM_PAYMENT_DETAILS(paymentObject);

                var payments = response.IT_PAYMENT;

                logger.Debug("TOTAL Payment Details: " + (payments != null ? payments.Count().ToString() : "0"));
                logger.Debug("END GetPaymentData: " + DateTime.Now.ToString());

                List<string> columns = @"BELNR;GJAHR;BLART;BLDAT;BUDAT;XBLNR;LIFNR;RMWWR;WAERS;RBSTAT;KIDNO;ADDITIONAL1;ADDITIONAL2;ADDITIONAL3;ADDITIONAL4;ADDITIONAL5".Split(';').ToList();

                if (payments != null && payments.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = payments.AsEnumerable().ToList().Chunk(500);
                    logger.Debug("GetPaymentData GUID: " + guid);
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");

                        prepareRows(chunk, dt, guid1, columns, columns[0]);

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_PAYMENT_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (payments != null && payments.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_payment_details", sd);

                        string sendEmail = ConfigurationManager.AppSettings["SEND_PAYMENT_EMAIL"].ToString();
                        if (sendEmail.Equals("TRUE"))
                        {
                            try
                            {
                                GetPaymentData(guid);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception($@"Email not being sent to the JOB_ID : {guid} for : {ex.Message}");
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GetPaymentData ERROR");
            }
        }


        static void prepareRows(dynamic chunk, DataTable dt, Guid guid1, List<string> columns, string mandateColumnName) 
        {
            foreach (var obj in chunk)
            {
                bool isValid = false;
                var row = dt.NewRow();
                row["JOB_ID"] = guid1;
                if (columns != null && columns.Count > 0)
                {
                    foreach (string column in columns)
                    {
                        try
                        {
                            PropertyInfo property = obj.GetType().GetProperty(column);


                            if (property.Name == mandateColumnName)
                            {
                                if (string.IsNullOrEmpty(property.GetValue(obj)))
                                {
                                    break;
                                }
                            }

                            if (property != null)
                            {
                                isValid = true;
                                row[column] = property.GetValue(obj) ?? DBNull.Value;

                            }
                            else
                            {
                                isValid = true;
                                row[column] = DBNull.Value; // Example: set it to DBNull.Value
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error($@"issue while processing the column Name {row[column]} and issue is {ex.Message} for job id {guid1}");
                            break;
                        }
                    }
                }
                
                if (isValid) 
                { 
                    dt.Rows.Add(row);
                }
            }
        }

        private static void GetVendorDetails()
        {
            try
            {
                string currentIndFlag = ConfigurationManager.AppSettings["IND_FLAG"].ToString();
                string VendorParams = ConfigurationManager.AppSettings["VendorParams"];
                logger.Info("VendorParams:" + VendorParams);
                string dates = VendorParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                string plants = VendorParams.Split(';')[1];
                string fromPlant = "";
                string toPlant = "";

                logger.Info("VendorParams:" + VendorParams);
                logger.Debug("START GetVendorDetails: " + DateTime.Now.ToString());
                string newURL = hostURL + "zvendor_srv";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                VD.zvendor_srv test = new VD.zvendor_srv();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
                VD.ZFM_VENDOR_DATA vENDOR_DATA = new VD.ZFM_VENDOR_DATA();
                List<VD.ZDATE_STR> S_ZDATE_STR = new List<VD.ZDATE_STR>();
                if (string.IsNullOrEmpty(currentIndFlag))
                {
                    S_ZDATE_STR.Add(new VD.ZDATE_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    }); ;
                }
                else
                {
                    vENDOR_DATA.CURENT_IND = currentIndFlag;
                }

                vENDOR_DATA.R_DATE = S_ZDATE_STR.ToArray();
                vENDOR_DATA.T_VENDOR_DATA = new List<VD.ZVENDOR_DATA_STR>().ToArray();
                
                //test.SoapVersion = System.Web.Services.Protocols.SoapProtocolVersion.Default;
                var vendorsResponse = test.ZFM_VENDOR_DATA(vENDOR_DATA);
                var vendors = vendorsResponse.T_VENDOR_DATA;
                logger.Debug("TOTAL COUNT GetVendorDetails: " + (vendors != null ? vendors.Count().ToString() : "0"));
                logger.Debug("END GetVendorDetails: " + DateTime.Now.ToString());
                List<string> columns = @"NAMEV;NAME1;SMTP_ADDR;TELF1;NAME2;WAERS;SMTP_ADDR1;SMTP_ADDR2;SMTP_ADDR3;SMTP_ADDR4;SMTP_ADDR5;SMTP_ADDR6;SMTP_ADDR7;SMTP_ADDR8;SMTP_ADDR9;SMTP_ADDR10;TEL_NUMBER;VEN_ALT_FIR;VEN_ALT_LAS;LIFNR;MATNR;WERKS;STCD3;CITY1;LAND1;ZTERM;TEXT1;ERDAT;VENDOR_ADDRESS".Split(';').ToList();

                if (vendors != null && vendors.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = vendors.AsEnumerable().ToList().Chunk(500);
                    logger.Debug("GetVendorDetails GUID: " + guid);
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        foreach (var vendor in chunk)
                        {
                            var row = dt.NewRow();
                            row["JOB_ID"] = guid1;
                            row["NAMEV"] = $"{vendor.VENDOR_FIRSTNAME} {vendor.VENDOR_LASTNAME}";
                            row["NAME1"] = $"{vendor.VENDOR_LASTNAME}";
                            row["SMTP_ADDR"] = $"{vendor.VENDOR_EMAIL}";
                            row["TELF1"] = $"{vendor.VENDOR_PHONENUMBER}";
                            row["NAME2"] = $"{vendor.VENDOR_FIRSTNAME} {vendor.VENDOR_LASTNAME}"; // $"{vendor.VENDOR_COMPANY_NAME}";
                            row["WAERS"] = $"{vendor.VENDOR_CURRENCY}";
                            row["SMTP_ADDR1"] = $"{vendor.VENDOR_ALT_EMAIL1}";
                            row["SMTP_ADDR2"] = $"{vendor.VENDOR_ALT_EMAIL2}";
                            row["SMTP_ADDR3"] = $"{vendor.VENDOR_ALT_EMAIL3}";
                            row["SMTP_ADDR4"] = $"{vendor.VENDOR_ALT_EMAIL4}";
                            row["SMTP_ADDR5"] = $"{vendor.VENDOR_ALT_EMAIL5}";
                            row["SMTP_ADDR6"] = $"{vendor.VENDOR_ALT_EMAIL6}";
                            row["SMTP_ADDR7"] = $"{vendor.VENDOR_ALT_EMAIL7}";
                            row["SMTP_ADDR8"] = $"{vendor.VENDOR_ALT_EMAIL8}";
                            row["SMTP_ADDR9"] = $"{vendor.VENDOR_ALT_EMAIL9}";
                            row["SMTP_ADDR10"] = $"{vendor.VENDOR_ALT_EMAIL10}";
                            row["TEL_NUMBER"] = $"{vendor.VENDOR_ALT_PHONENO}";
                            row["VEN_ALT_FIR"] = $"{vendor.VENDOR_ALT_POC_FIRSTNAME}";
                            row["VEN_ALT_LAS"] = $"{vendor.VENDOR_ALT_CONTACT_LASTNAME}";
                            row["LIFNR"] = $"{vendor.VENDOR_VENDOR_ERPCODE}";
                            row["MATNR"] = $"{vendor.VENDOR_MATERIAL_CODE}";
                            row["WERKS"] = $"{vendor.PLANT}";
                            row["STCD3"] = $"{vendor.VENDOR_GST_NUMBER}";
                            row["CITY1"] = $"{vendor.VENDOR_LOCATION}";
                            row["LAND1"] = $"{vendor.VENDOR_COUNTRY_DETAILS}";
                            row["ZTERM"] = $"{vendor.PAYMENT_TERMS_CODE}";
                            row["TEXT1"] = $"{vendor.PAYMENT_TERMS_DESC}";
                            row["VENDOR_ADDRESS"] = $"{vendor.ADDRESS}";
                            row["ERDAT"] = $"{vendor.VENDOR_CREATION_DATE}";
                            row["DATE_CREATED"] = DateTime.UtcNow;
                            dt.Rows.Add(row);
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_VENDOR_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (vendors != null && vendors.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_vendor_details", sd);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETVENDORDETAILS ERROR");
            }
        }

        private static void GetMaterailDetails()
        {
            try
            {
                string currentIndFlag = ConfigurationManager.AppSettings["IND_FLAG"].ToString();
                string getMATParams = ConfigurationManager.AppSettings["MATParams"];

                string dates = getMATParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);


                logger.Info("getMATParams:" + getMATParams);
                string materials = getMATParams.Split(';')[1];
                List<string> materialLis = materials.Split(',').ToList();
                
                logger.Debug("START GetMaterailDetails: " + DateTime.Now.ToString());
                string newURL = hostURL + "zmaterial_srv";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                MAT.zmaterial_srv test = new MAT.zmaterial_srv();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);

                List<MAT.ZDATE_STR> S_ZDATE_STR = new List<MAT.ZDATE_STR>();
                MAT.ZMATERIAL_DATA zMATERIAL_DATA = new MAT.ZMATERIAL_DATA();

                if (string.IsNullOrEmpty(currentIndFlag))
                {
                    S_ZDATE_STR.Add(new MAT.ZDATE_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    });
                }
                else
                {
                    zMATERIAL_DATA.CURENT_IND = currentIndFlag;
                }

                List<MAT.ZMTART_STR> S_MTART = new List<MAT.ZMTART_STR>();
                foreach (var mat in materialLis)
                {
                    S_MTART.Add(new MAT.ZMTART_STR()
                    {
                        SIGN = "I",
                        OPTION = "EQ",
                        LOW = mat,
                        HIGH = ""
                    });
                }
                
                zMATERIAL_DATA.R_DATE_RANGE = S_ZDATE_STR.ToArray();
                zMATERIAL_DATA.R_MATERIAL_TYPE = S_MTART.ToArray();
                zMATERIAL_DATA.T_MATERIAL_DATA = new List<MAT.ZMATERIAL_DATA_STR>().ToArray();
                var materailsresponse = test.ZMATERIAL_DATA(zMATERIAL_DATA);
                var materails = materailsresponse.T_MATERIAL_DATA;
                logger.Debug($"TOTAL COUNT GetMaterailDetails: {(materails != null ? materails.Count().ToString() : "0")}");
                logger.Debug("END GetMaterailDetails: " + DateTime.Now.ToString());
                List<string> columns = @"MATKL;WGBEZ;MATNR;MTART;STEUC;MAKTX;PO_TEXT;MEINS;CASNR;GPNUM;ASNUM;ASKTX;MEINS1;MATKL1;MEINH;MEINH1;MEINH2;MEINH3;MEINH4;MEINH5;MEINH6;MEINH7;UMREN;UMREN1;UMREN2;UMREN3;UMREN4;UMREN5;UMREN6;UMREN7;UMREZ;UMREZ1;UMREZ2;UMREZ3;UMREZ4;UMREZ5;UMREZ6;UMREZ7;TAXIM;LVORM;MMSTA;MSTAE;ERSDA;LVORM1;ERDAT1;AEDAT1;LAEDA;WERKS".Split(';').ToList();

                if (materails != null && materails.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = materails.AsEnumerable().ToList().Chunk(500);
                    logger.Debug("GetMaterailDetails GUID: " + guid);
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        foreach (var material in chunk)
                        {
                            var row = dt.NewRow();
                            row["JOB_ID"] = guid1;
                            row["MATKL"] = $"{material.MATERIAL_GROUP_CODE}";
                            row["WGBEZ"] = $"{material.MATERIAL_GROUP_DESC}";
                            row["MATNR"] = $"{material.MATERIAL_CODE}";
                            row["MTART"] = $"{material.MATERIAL_DESCRIPTION}";
                            row["STEUC"] = $"{material.MATERIAL_HSN_CODE}";
                            row["MAKTX"] = $"{material.MATERIAL_CODE}";
                            row["PO_TEXT"] = $"{material.PO_TEXT}";
                            //row["MEINS"] = $"{material.ALTERNATE_QTY2}";
                            row["CASNR"] = $"{material.CAS_NUMBER}";
                            row["GPNUM"] = $"{material.MFCD_CODE}";
                            row["ASNUM"] = $"{""}";
                            row["ASKTX"] = $"{""}";
                            //row["MEINS1"] = $"{material.ALTERNATE_QTY2}";
                            row["MATKL1"] = $"";
                            row["MEINS"] = $"{material.UOM}";
                            row["MEINH1"] = $"{material.ALTERNATE_UOM1}";
                            row["MEINH2"] = $"{material.ALTERNATE_UOM2}";
                            row["MEINH3"] = $"{material.ALTERNATE_UOM3}";
                            row["MEINH4"] = $"{material.ALTERNATE_UOM4}";
                            row["MEINH5"] = $"{material.ALTERNATE_UOM5}";
                            row["MEINH6"] = $"{material.ALTERNATE_UOM6}";
                            row["MEINH7"] = $"{material.ALTERNATE_UOM7}";
                            row["UMREN"] = $"{0}";
                            row["UMREN1"] = $"{0}";
                            row["UMREN2"] = $"{0}";
                            row["UMREN3"] = $"{0}";
                            row["UMREN4"] = $"{0}";
                            row["UMREN5"] = $"{0}";
                            row["UMREN6"] = $"{0}";
                            row["UMREN7"] = $"{0}";
                            row["UMREZ"] = $"{0}";
                            row["UMREZ1"] = $"{0}";
                            row["UMREZ2"] = $"{0}";
                            row["UMREZ3"] = $"{0}";
                            row["UMREZ4"] = $"{0}";
                            row["UMREZ5"] = $"{0}";
                            row["UMREZ6"] = $"{0}";
                            row["UMREZ7"] = $"{0}";
                            row["TAXIM"] = $"0";
                            row["LVORM"] = $"";
                            row["MMSTA"] = $"";
                            row["MSTAE"] = $"";
                            row["ERSDA"] = $"{material.MATERIAL_CREATED_DATE}";
                            row["LVORM1"] = $"{""}";
                            row["ERDAT1"] = $"{material.MATERIAL_CREATED_DATE}";
                            row["AEDAT1"] = "1970-01-01";
                            row["LAEDA"] = "1970-01-01";
                            row["WERKS"] = $"{material.PLANT}";
                            row["DATE_CREATED"] = DateTime.UtcNow;
                            dt.Rows.Add(row);
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_MATERIAL_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (materails != null && materails.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_material_details", sd);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETMATERIALDETAILS ERROR");
            }
        }

        private static void GetPRDetails()
        {
            try
            {
                string currentIndFlag = ConfigurationManager.AppSettings["IND_FLAG"].ToString();
                string getPRParams = ConfigurationManager.AppSettings["PRParams"];
                logger.Info("getPRParams:" + getPRParams);
                string dates = getPRParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                string plants = getPRParams.Split(';')[1];
                string fromPlant = "";
                string toPlant = "";
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    plants = "ALL";
                }
                else if (plants.Contains(":"))
                {
                    fromPlant = plants.Split(':')[0];
                    toPlant = plants.Split(':')[1];
                }

                logger.Debug("START GetPRDetails: " + DateTime.Now.ToString());
                string newURL = ConfigurationManager.AppSettings["PR_URL"].ToString();
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                PR.ZPREQ_SRV test = new PR.ZPREQ_SRV();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
                PR.ZFM_PR_DATA zFM_PR_DATA = new PR.ZFM_PR_DATA();
                List<PR.ZDATE_STR> S_ZDATE_STR = new List<PR.ZDATE_STR>();

                if (string.IsNullOrEmpty(currentIndFlag))
                {
                    S_ZDATE_STR.Add(new PR.ZDATE_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    });
                }
                else
                {
                    zFM_PR_DATA.CURENT_IND = currentIndFlag;
                }
                

                zFM_PR_DATA.R_DATE = S_ZDATE_STR.ToArray();
                zFM_PR_DATA.T_PR_DATA = new List<PR.ZPR_DATA_STR>().ToArray();
                var prresponse = test.ZFM_PR_DATA(zFM_PR_DATA);
                var prs = prresponse.T_PR_DATA;
                logger.Debug("TOTAL COUNT GetPRDetails: " + (prs != null ? prs.Count().ToString() : "0"));
                logger.Debug("END GetPRDetails: " + DateTime.Now.ToString());
                List<string> columns = @"PLANT;PLANT_CODE;GMP;PURCHASE_GROUP_CODE;PURCHASE_GROUP_NAME;REQUESITION_DATE;PR_RELEASE_DATE;PR_CHANGE_DATE;PR_NUMBER;PR_CREATOR_NAME;REQUISITIONER_NAME;REQUISITIONER_EMAIL;ITEM_OF_REQUESITION;MATERIAL_GROUP_CODE;MATERIAL_GROUP_DESC;MATERIAL_CODE;MATERIAL_TYPE;MATERIAL_HSN_CODE;MATERIAL_DESCRIPTION;SHORT_TEXT;ITEM_TEXT;UOM;QTY_REQUIRED;MATERIAL_DELIVERY_DATE;LEAD_TIME;PR_TYPE;PR_TYPE_DESC;PR_NOTE;CASNR;WBS_CODE;MFCD_NUMBER;PROJECT_DESCRIPTION;PROFIT_CENTER;ALTERNATE_UOM;ALTERNATE_QTY;SECTION_HEAD;SERVICE_CODE;SERVICE_CODE_DESCRIPTION;SERVICE_QTY;SERVICE_UNIT_OF_MEASURE;ITEM_CATEGORY;ACCOUNT_ASSIGNMENT_CATEGORY;PROJECT_TYPE;SUB_VERTICAL;LOEKZ;ERDAT;EBAKZ".Split(';').ToList();

                if (prs != null && prs.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = prs.AsEnumerable().ToList().Chunk(500);
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        int processedRows = 0;
                        foreach (var pr in chunk)
                        {
                            var row = dt.NewRow();
                            row["JOB_ID"] = guid1;
                            row["PLANT"] = $"{pr.PLANT}";
                            row["PLANT_CODE"] = $"{pr.PLANT}";
                            row["GMP"] = $"";
                            row["PURCHASE_GROUP_CODE"] = $"{pr.PUR_GROUP}";
                            row["PURCHASE_GROUP_NAME"] = $"{pr.PUR_GROUP}";
                            row["REQUESITION_DATE"] = $"{pr.PREQ_DATE}";
                            row["PR_RELEASE_DATE"] = $"{pr.REL_DATE}";
                            row["PR_CHANGE_DATE"] = $"{pr.CHANGED_ON_DATE}";
                            row["PR_NUMBER"] = $"{pr.PR_NO}";
                            row["PR_CREATOR_NAME"] = $"{pr.PR_CRE_NAME}";
                            row["REQUISITIONER_NAME"] = $"{pr.PREQ_NAME}";
                            row["REQUISITIONER_EMAIL"] = $"{pr.E_MAIL}";
                            row["ITEM_OF_REQUESITION"] = $"{pr.ITEM_OF_PR}";
                            row["MATERIAL_GROUP_CODE"] = $"{pr.MAT_GRP_CODE}";
                            row["MATERIAL_GROUP_DESC"] = $"{pr.MAT_GRP_DESC}";
                            row["MATERIAL_CODE"] = $"{pr.MATERIAL}";
                            row["MATERIAL_TYPE"] = $"{pr.MAT_TYPE}";
                            row["MATERIAL_HSN_CODE"] = $"{pr.HSN_CODE}";
                            row["MATERIAL_DESCRIPTION"] = $"{pr.MAT_DESC}";
                            row["SHORT_TEXT"] = $"{pr.SHORT_TEXT}";
                            row["ITEM_TEXT"] = $"{pr.ITEM_TEXT}";
                            row["UOM"] = $"{pr.UOM}";
                            row["QTY_REQUIRED"] = $"{pr.QTY_REQD}";
                            row["MATERIAL_DELIVERY_DATE"] = $"{pr.MAT_DEL_DATE}";
                            row["LEAD_TIME"] = $"{""}";
                            row["PR_TYPE"] = $"{pr.PR_TYPE}";
                            row["PR_TYPE_DESC"] = $"{pr.PR_TYPE_DESC}";
                            row["PR_NOTE"] = $"{pr.PR_NOTE}";
                            row["CASNR"] = $"{""}";
                            row["WBS_CODE"] = $"{""}";
                            row["MFCD_NUMBER"] = $"{""}";
                            row["PROJECT_DESCRIPTION"] = $"{pr.PROJECT_DESCRIPTION}";
                            row["PROFIT_CENTER"] = $"{pr.PROFIT_CENTER}";
                            row["ALTERNATE_UOM"] = $"{pr.ALTERNATE_UOM}";
                            row["ALTERNATE_QTY"] = $"{pr.ALTERNATE_QTY}";
                            row["SECTION_HEAD"] = $"{pr.SERVICE_CODE}";
                            row["SERVICE_CODE"] = $"{pr.SERVICE_CODE}";
                            row["SERVICE_CODE_DESCRIPTION"] = $"{pr.SERVICE_CODE_DESC}";
                            row["SERVICE_QTY"] = $"{pr.SERVICE_QTY}";
                            row["SERVICE_UNIT_OF_MEASURE"] = $"{pr.SERVICE_UOM}";
                            row["ITEM_CATEGORY"] = $"{pr.ITEM_CATEGORY}";
                            row["ACCOUNT_ASSIGNMENT_CATEGORY"] = $"{pr.ACC_ASSIGNMENT_CAT}";
                            row["PROJECT_TYPE"] = $"{pr.PROJECT_TYPE}";
                            row["SUB_VERTICAL"] = $"{""}";
                            row["LOEKZ"] = $"{""}";
                            row["ERDAT"] = $"{pr.CHANGED_ON_DATE}";
                            row["EBAKZ"] = $"{pr.PR_STATUS}";
                            row["DATE_CREATED"] = DateTime.UtcNow;
                            dt.Rows.Add(row);
                            processedRows++;
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_PR_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (prs != null && prs.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_pr_details", sd);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETPRDETAILS ERROR");
            }
        }

        private static void GetGRNDetails()
        {
            try
            {
                string currentIndFlag = ConfigurationManager.AppSettings["IND_FLAG"].ToString();
                string getGRNParams = ConfigurationManager.AppSettings["GRNParams"];
                logger.Info("getGRNParams:" + getGRNParams);
                string dates = getGRNParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                string plants = getGRNParams.Split(';')[1];
                string fromPlant = "";
                string toPlant = "";
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    plants = "ALL";
                }
                else if (plants.Contains(":"))
                {
                    fromPlant = plants.Split(':')[0];
                    toPlant = plants.Split(':')[1];
                }

                logger.Debug("START GetGRNDetails: " + DateTime.Now.ToString());
                string newURL = ConfigurationManager.AppSettings["GRN_URL"].ToString();
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                GRN.ZGRN_SRV_WS test = new GRN.ZGRN_SRV_WS();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
                GRN.ZFM_GRN_DATA zFM_GRN_DATA = new GRN.ZFM_GRN_DATA();
                List<GRN.ZDATE_STR> ZBSART_STR = new List<GRN.ZDATE_STR>();

                if (string.IsNullOrEmpty(currentIndFlag))
                {
                    ZBSART_STR.Add(new GRN.ZDATE_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    });
                }
                else
                {
                    ZBSART_STR.Add(new GRN.ZDATE_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = DateTime.Now.ToString("yyyy-MM-dd"),
                        HIGH = DateTime.Now.ToString("yyyy-MM-dd")
                    });
                }

                //zFM_GRN_DATA.R_BLDAT = ZBSART_STR.ToArray();
                zFM_GRN_DATA.R_BUDAT = ZBSART_STR.ToArray();
                zFM_GRN_DATA.T_GRN_DATA = new List<GRN.ZGRN_DATA_STR>().ToArray();
                var grnresponse = test.ZFM_GRN_DATA(zFM_GRN_DATA);
                var grns = grnresponse.T_GRN_DATA;
                logger.Debug("TOTAL COUNT GetGRNDetails: " + (grns != null ? grns.Count().ToString() : "0"));
                logger.Debug("END GetGRNDetails: " + DateTime.Now.ToString());
                List<string> columns = @"BUDAT;ZZSBILL_ENTRY;ZZBILL_ENTRY;ZZDATE_SBILL_ENTRY;SGTXT;WAERS;BLDAT;VFDAT;WRBTR;WRBTRSpecified;ZZGATE_ENT_DATE;ZZGATE_ENTRY;CHARG;LFBNR;BUDAT1;ZEILE;MBLNR;MJAHR;MENGE;MENGESpecified;WRBTR1;ZZBILL_DOC;ZZLR_DATE;ZZLR_NUMBER;ZZLUT_NUM;HSDAT;MAKTX;MATNR;BWART;WERKS;MENGE1;RDATE;SRVPOS;LFBNR1;LFPOS;STATUS;LGORT;MEINS;CHARG1;LIFNR;NAME1;LAND1;PS_PSP_PNR;EBELN;EBELP;CPUDT_MKPF;CPUTM_MKPF;HEADER_TEXT;DELIVERY_NOTE".Split(';').ToList();

                if (grns != null && grns.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = grns.AsEnumerable().ToList().Chunk(500);
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        int processedRows = 0;
                        foreach (var pr in chunk)
                        {
                            var newRow = dt.NewRow();
                            newRow["JOB_ID"] = guid1;
                            newRow["BUDAT"] = pr.ACUTAL_DELIVERY_DATE == "0000-00-00" ? null : pr.ACUTAL_DELIVERY_DATE;
                            newRow["WAERS"] = pr.CURRENCY;
                            newRow["BLDAT"] = pr.DOCUMENT_DT;
                            newRow["VFDAT"] = pr.EXPIRY_DATE == "0000-00-00" ? null : pr.EXPIRY_DATE; ;
                            newRow["WRBTR"] = pr.FRIEGHT_VALUE;
                            newRow["CHARG"] = pr.GR_BATCH_NO;
                            newRow["LFBNR"] = pr.GR_REFERENCE;
                            newRow["BUDAT1"] = pr.GRN_DT;
                            newRow["ZEILE"] = pr.GRN_LINE_ITEM;
                            newRow["MBLNR"] = pr.GRN_NO;
                            newRow["MJAHR"] = pr.GR_YEAR;
                            newRow["MENGE"] = pr.GRN_QTY;
                            newRow["WRBTR1"] = pr.GRN_VALUE;
                            newRow["HSDAT"] = pr.MANUFACTURING_DATE == "0000-00-00" ? null : pr.MANUFACTURING_DATE;
                            newRow["MAKTX"] = pr.MAT_DESCRIPTION;
                            newRow["MATNR"] = pr.MATERIAL;
                            newRow["BWART"] = pr.MV_TYPE;
                            newRow["WERKS"] = pr.PLANT;
                            newRow["MENGE1"] = pr.PO_QUANTITY;
                            newRow["RDATE"] = pr.RETEST_DATE == "0000-00-00" ? null : pr.RETEST_DATE;
                            newRow["SRVPOS"] = pr.SERVICE_CODE;
                            newRow["LFBNR1"] = pr.SERVICE_ENTRY_SHEET;
                            newRow["LFPOS"] = pr.SERVICE_LN_ITM;
                            newRow["STATUS"] = pr.STATUS;
                            newRow["LGORT"] = pr.STORAGE_LOCATION;
                            newRow["MEINS"] = pr.UOM;
                            newRow["CHARG1"] = pr.VENDOR_BATCH_NO;
                            newRow["LIFNR"] = pr.VENDOR_CODE;
                            newRow["NAME1"] = pr.VENDOR_NAME;
                            newRow["LAND1"] = pr.VENDORS_COUNTRY;
                            newRow["PS_PSP_PNR"] = "";
                            newRow["EBELN"] = pr.PO_NUMBER;
                            newRow["EBELP"] = pr.PO_LINE_ITEM;
                            newRow["CPUDT_MKPF"] = pr.GR_CREATION_DATE == "0000-00-00" ? null : pr.GR_CREATION_DATE;
                            newRow["CPUTM_MKPF"] = "";
                            newRow["DATE_CREATED"] = DateTime.UtcNow;
                            newRow["HEADER_TEXT"] = pr.HEADER_TEXT;
                            newRow["DELIVERY_NOTE"] = pr.DELIVERY_NOTE;

                            dt.Rows.Add(newRow);
                            processedRows++;
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_GRN_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (grns != null && grns.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_grn_details", sd);

                        string sendEmail = ConfigurationManager.AppSettings["SEND_PO_EMAIL"].ToString();
                        if (sendEmail.Equals("TRUE"))
                        {
                            GetGRNData(guid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETGRNDETAILS ERROR");
            }
        }

        private static void GetPOScheduleDetails()
        {
            try
            {
                string currentIndFlag = ConfigurationManager.AppSettings["IND_FLAG"].ToString();
                string getPOSCHParams = ConfigurationManager.AppSettings["POParams"];
                logger.Info("getPOSCHParams:" + getPOSCHParams);
                string dates = getPOSCHParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                string plants = getPOSCHParams.Split(';')[1];
                string fromPlant = "";
                string toPlant = "";
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    plants = "ALL";
                }
                else if (plants.Contains(":"))
                {
                    fromPlant = plants.Split(':')[0];
                    toPlant = plants.Split(':')[1];
                }

                logger.Debug("START GetPOScheduleDetails: " + DateTime.Now.ToString());
                string newURL = ConfigurationManager.AppSettings["PO_URL"].ToString();
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                PO.ZPO_PRM_SRV test = new PO.ZPO_PRM_SRV();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
                PO.ZFM_PO_DATA zFM_PO_DATA = new PO.ZFM_PO_DATA();
                List<PO.ZDATE_STR> ZDATE_STR = new List<PO.ZDATE_STR>();
                if (string.IsNullOrEmpty(currentIndFlag))
                {
                    ZDATE_STR.Add(new PO.ZDATE_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    });
                }
                else
                {
                    //zFM_PO_DATA.CURENT_IND = currentIndFlag;
                    ZDATE_STR.Add(new PO.ZDATE_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = DateTime.Now.ToString("yyyy-MM-dd"),
                        HIGH = DateTime.Now.ToString("yyyy-MM-dd")
                    });
                }

                zFM_PO_DATA.R_BUKRS = new List<PO.ZBUKRS_STR>().ToArray();
                zFM_PO_DATA.R_EBELN = new List<PO.ZEBELN_STR>().ToArray();
                zFM_PO_DATA.R_ERDAT = ZDATE_STR.ToArray();
                zFM_PO_DATA.R_MATNR = new List<PO.ZMATERIAL_STR>().ToArray();
                zFM_PO_DATA.R_MTART = new List<PO.ZMTART_STR>().ToArray();
                zFM_PO_DATA.R_WERKS = new List<PO.ZWERKS_STR>().ToArray();
                zFM_PO_DATA.T_PO_DATA = new List<PO.ZPO_DATA_STR>().ToArray();

                var poresponse = test.ZFM_PO_DATA(zFM_PO_DATA);
                var POs = poresponse.T_PO_DATA;
                logger.Debug("TOTAL COUNT GetPOScheduleDetails: " + (POs != null ? POs.Count().ToString() : "0"));
                logger.Debug("END GetPOScheduleDetails: " + DateTime.Now.ToString());
                List<string> columns = @"WERKS;MATNR;TXZ01;MATKL;LIFNR;NAME1;TELF1;SMTP_ADDR;EBELN;EBELP;AEDAT;ERNAM;ZTERM;MWSKZ;TEXT1;UDATE;EINDT;MTART;ORT01;REGIO;J_1BNBM;ORD_QTY;MEINS;MEINS1;MENGE1;MENGE1Specified;EFFWR;EFFWRSpecified;NETWR1;NETWR1Specified;WAERS;KNUMV;KNUMVSpecified;MENGE2;MENGE2Specified;BANFN;BNFPO;MENGE3;MENGE3Specified;UDATE1;LTEXT1;LFDAT;LTEXT2;FRGKZ;AFNAM;BSART;SRVPOS;ASKTX;KNUMV1;KNUMV1Specified;KNUMV2;KNUMV2Specified;BSTYP;KDATB;KDATE;AEDAT1;ELIKZ;LOEKZ;CGST;SGST;IGST;CESS;TCS;PO_ITEM_CHANGE_DATE;PO_MATERIAL_DESC;ITEM_MAT_TEXT;ITEM_GROSS_PRICE;ITEM_DISCOUNT_VALUE;ITEM_DISCOUNT_PERCENTAGE;INCO_TERMS;HEADER_TEXT;BUYER_EMAIL;ZZ_USER_NAME;ZZ_MAIL1;PEINH".Split(';').ToList();

                if (POs != null && POs.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = POs.AsEnumerable().ToList().Chunk(500);
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        int processedRows = 0;
                        foreach (var pr in chunk)
                        {
                            var newRow = dt.NewRow();
                            newRow["JOB_ID"] = guid1;

                            try { var temp = Convert.ToDateTime(pr.PO_DATE); } catch { pr.PO_DATE = null; }
                            try { var temp = Convert.ToDateTime(pr.PO_RELEASE_DATE); } catch { pr.PO_RELEASE_DATE = null; }
                            try { var temp = Convert.ToDateTime(pr.DELV_DATE); } catch { pr.DELV_DATE = null; }
                            try { var temp = Convert.ToDateTime(pr.PR_RELEASE_DATE); } catch { pr.PR_RELEASE_DATE = null; }
                            try { var temp = Convert.ToDateTime(pr.PR_DELV_DATE); } catch { pr.PR_DELV_DATE = null; }
                            try { var temp = Convert.ToDateTime(pr.VALID_FROM); } catch { pr.VALID_FROM = null; }
                            try { var temp = Convert.ToDateTime(pr.VALID_TO); } catch { pr.VALID_TO = null; }
                            try { var temp = Convert.ToDateTime(pr.PO_ITEM_CHANGE_DATE); } catch { pr.PO_ITEM_CHANGE_DATE = null; }                            
                            try { var temp = Convert.ToDecimal(pr.CGST); } catch { pr.CGST = 0; }
                            try { var temp = Convert.ToDecimal(pr.IGST); } catch { pr.IGST = 0; }
                            try { var temp = Convert.ToDecimal(pr.SGST); } catch { pr.SGST = 0; }
                            try { var temp = Convert.ToDecimal(pr.CESS); } catch { pr.CESS = 0; }

                            newRow["WERKS"] = pr.PLANT;
                            newRow["MATNR"] = pr.MATERIAL;
                            newRow["TXZ01"] = pr.DESCRIPTION;
                            newRow["MATKL"] = pr.MAT_GROUP;
                            newRow["LIFNR"] = pr.VENDOR_CODE; //GetDataRowVaue(row, "VENDOR");
                            newRow["NAME1"] = pr.VENDOR_NAME;
                            newRow["TELF1"] = pr.VENDOR_PRIMARY_PHNO;
                            newRow["SMTP_ADDR"] = pr.VENDOR_PRIMARY_EMAIL;
                            newRow["EBELN"] = pr.PO_NUMBER;
                            newRow["EBELP"] = pr.PO_LINE_ITEM;
                            newRow["AEDAT"] = string.IsNullOrEmpty(pr.PO_DATE) || pr.PO_DATE == "0000-00-00" ? null : pr.PO_DATE;
                            newRow["ERNAM"] = pr.PO_CREATOR;
                            newRow["ZTERM"] = pr.PAYMENT_TERMS;
                            newRow["MWSKZ"] = pr.TAX_CODE;
                            newRow["TEXT1"] = pr.TAX_CODE_DESC;
                            newRow["UDATE"] = string.IsNullOrEmpty(pr.PO_RELEASE_DATE) || pr.PO_RELEASE_DATE == "0000-00-00" ? null : pr.PO_RELEASE_DATE;
                            newRow["EINDT"] = string.IsNullOrEmpty(pr.DELV_DATE) || pr.DELV_DATE == "0000-00-00" ? null : pr.DELV_DATE;
                            newRow["MTART"] = pr.MAT_TYPE;
                            newRow["ORT01"] = pr.CITY;
                            newRow["REGIO"] = pr.REGION_DESC;
                            newRow["J_1BNBM"] = pr.HSN_CODE;
                            newRow["ORD_QTY"] = pr.ORD_QTY;
                            newRow["MEINS"] = pr.UOM;
                            newRow["MEINS1"] = pr.ALTERNATIVE_UOM;
                            newRow["MENGE1"] = pr.QTY_ALTERNATE_UOM;
                            newRow["EFFWR"] = pr.NET_PRICE;
                            newRow["NETWR1"] = pr.VALUE_INR;
                            newRow["WAERS"] = pr.CURRENCY;
                            newRow["KNUMV"] = pr.ITEM_FREIGHT_CHARGE;
                            newRow["MENGE2"] = pr.PEND_QTY;
                            newRow["BANFN"] = pr.PR_LINE;
                            newRow["BNFPO"] = pr.PR_NUM;
                            newRow["MENGE3"] = pr.PR_QTY;
                            newRow["UDATE1"] = string.IsNullOrEmpty(pr.PR_RELEASE_DATE) ||  pr.PR_RELEASE_DATE == "0000-00-00" ? null : pr.PR_RELEASE_DATE;
                            newRow["LTEXT1"] = pr.PR_LINE_TEXT;
                            newRow["LFDAT"] = string.IsNullOrEmpty(pr.PR_DELV_DATE) || pr.PR_DELV_DATE == "0000-00-00" ? null : pr.PR_DELV_DATE;
                            newRow["LTEXT2"] = pr.ITEM_TEXT_PO;
                            newRow["FRGKZ"] = pr.REL_IND;
                            newRow["AFNAM"] = pr.PR_REQUISITIONER;
                            newRow["BSART"] = pr.DOC_TYPE;
                            newRow["SRVPOS"] = pr.SERVICE_CODE;
                            newRow["ASKTX"] = pr.SERVICE_DESCRIPTION;
                            newRow["KNUMV1"] = pr.MISC_CHARGES == "" ? null : pr.MISC_CHARGES;
                            newRow["KNUMV2"] = pr.PACKING_CHARGES == "" ? null : pr.PACKING_CHARGES;
                            newRow["BSTYP"] = pr.PO_CONTRACT;
                            newRow["KDATB"] = string.IsNullOrEmpty(pr.VALID_FROM) || pr.VALID_FROM == "0000-00-00" ? null : pr.VALID_FROM;
                            newRow["KDATE"] = string.IsNullOrEmpty(pr.VALID_TO) || pr.VALID_TO == "0000-00-00" ? null : pr.VALID_TO;
                            newRow["ELIKZ"] = pr.DELIVERY_COMPLETION_IND;
                            newRow["LOEKZ"] = pr.ITEM_DEL_IND; //pr.DEL_IND
                            newRow["CGST"] = pr.CGST;
                            newRow["SGST"] = pr.SGST;
                            newRow["IGST"] = pr.IGST;
                            newRow["CESS"] = pr.CESS;
                            newRow["TCS"] = pr.TCS;
                            newRow["PO_ITEM_CHANGE_DATE"] = string.IsNullOrEmpty(pr.PO_ITEM_CHANGE_DATE) || pr.VALID_TO == "0000-00-00" ? null : pr.PO_ITEM_CHANGE_DATE;
                            newRow["PO_MATERIAL_DESC"] = pr.PO_MATERIAL_DESC;
                            newRow["ITEM_MAT_TEXT"] = pr.ITEM_MAT_TEXT;
                            newRow["ITEM_GROSS_PRICE"] = pr.ITEM_GROSS_PRICE;
                            newRow["ITEM_DISCOUNT_VALUE"] = pr.ITEM_DISCOUNT_VALUE;
                            newRow["ITEM_DISCOUNT_PERCENTAGE"] = pr.ITEM_DISCOUNT_PERCENTAGE;
                            newRow["INCO_TERMS"] = pr.INCO_TERMS;
                            newRow["HEADER_TEXT"] = pr.HEADER_TEXT;
                            newRow["BUYER_EMAIL"] = pr.BUYER_EMAIL;
                            newRow["ZZ_USER_NAME"] = pr.ZZUSER_NAME;
                            newRow["ZZ_MAIL1"] = pr.ZZMAIL1;
                            newRow["DATE_CREATED"] = DateTime.UtcNow;
                            newRow["PEINH"] = pr.PEINH;

                            dt.Rows.Add(newRow);
                            processedRows++;
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_PO_SCHEDULE_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (POs != null && POs.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_po_schedule_details", sd);

                        string sendEmail = ConfigurationManager.AppSettings["SEND_PO_EMAIL"].ToString();
                        if (sendEmail.Equals("TRUE"))
                        {
                            GetPOData(guid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETPOSCHDETAILS ERROR");
            }
        }

        private static void GetCurrencyDetails()
        {
            try
            {
                List<CURR.ZEXCH_RATE> rates = new List<CURR.ZEXCH_RATE>();
                string currentIndFlag = ConfigurationManager.AppSettings["IND_FLAG"].ToString();
                string getCurrencyParams = ConfigurationManager.AppSettings["CurrencyParams"];
                logger.Info("getCurrencyParams:" + getCurrencyParams);
                string dates = getCurrencyParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                logger.Debug("START GetCurrencyDetails: " + DateTime.Now.ToString());
                string newURL = "http://NLLCOSAPDEV01.NEULANDLABS.COM/sap/bc/srt/wsdl/flv_10002p111ad1/sdef_url/zsrv_erate"; //"https://portal.neulandlabs.com:44318/sap/bc/srt/wsdl/flv_10002p111ad1/sdef_url/zsrv_erate";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                CURR.ZSRV_ERATE test = new CURR.ZSRV_ERATE();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
                CURR.ZEXCHANGE_RATE ZEXCHANGE_RATE_DATA = new CURR.ZEXCHANGE_RATE();
                ZEXCHANGE_RATE_DATA.IT_TCURR = rates.ToArray();
                var poresponse = test.ZEXCHANGE_RATE(ZEXCHANGE_RATE_DATA);
                var history = poresponse.IT_HISTORY;
                var current = poresponse.IT_TCURR;
                logger.Debug("TOTAL COUNT GetCurrencyDetails: " + (current != null ? current.Count().ToString() : "0"));
                logger.Debug("END GetCurrencyDetails: " + DateTime.Now.ToString());
                List<string> columns = @"KURST;FCURR;TCURR;GDATU;UKURS".Split(';').ToList();

                if (current != null && current.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = current.AsEnumerable().ToList().Chunk(500);
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        int processedRows = 0;
                        foreach (var pr in chunk)
                        {
                            var newRow = dt.NewRow();
                            newRow["JOB_ID"] = guid1;
                            newRow["KURST"] = pr.KURST;
                            newRow["FCURR"] = pr.FCURR;
                            newRow["TCURR"] = pr.TCURR;
                            newRow["GDATU"] = pr.GDATU;
                            newRow["UKURS"] = pr.UKURS;
                            newRow["DATE_CREATED"] = DateTime.UtcNow;

                            dt.Rows.Add(newRow);
                            processedRows++;
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_CURRENCY_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (current != null && current.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_currency_details", sd);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GetCurrencyDetails ERROR");
            }
        }
        private static void GetContractDetails()
        {
            try
            {
                List<CURR.ZEXCH_RATE> rates = new List<CURR.ZEXCH_RATE>();
                string currentIndFlag = ConfigurationManager.AppSettings["IND_FLAG"].ToString();
                string getContractParams = ConfigurationManager.AppSettings["ContractParams"];
                logger.Info("ContractParams:" + getContractParams);
                string dates = getContractParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                logger.Debug("START GetContractDetails: " + DateTime.Now.ToString());
                string newURL = hostURL + "zcontract_read_data_srv";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                CONTRACT.ZCONTRACT_READ_DATA_SRV test = new CONTRACT.ZCONTRACT_READ_DATA_SRV();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
                List<CONTRACT.ZDATE_STR> ZDATE_STR = new List<CONTRACT.ZDATE_STR>();
                if (string.IsNullOrEmpty(currentIndFlag))
                {
                    ZDATE_STR.Add(new CONTRACT.ZDATE_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    });
                }
                else
                {
                    ZDATE_STR.Add(new CONTRACT.ZDATE_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = DateTime.Now.ToString("yyyy-MM-dd"),
                        HIGH = DateTime.Now.ToString("yyyy-MM-dd")
                    });
                }
               
                CONTRACT.ZFM_CONTRACT_READ_DATA inputdata = new CONTRACT.ZFM_CONTRACT_READ_DATA();
                inputdata.T_CONTRACT_DATA = new List<CONTRACT.ZCONTRACT_DATA_STR>().ToArray();
                inputdata.R_DATE = ZDATE_STR.ToArray();
                //inputdata.R_CONTRACT = ZCONTRACT_DATA_STR.ToArray();
                var prresponse = test.ZFM_CONTRACT_READ_DATA(inputdata);
                var contracts = prresponse.T_CONTRACT_DATA;
                logger.Debug("TOTAL COUNT GetContractDetails: " + (contracts != null ? contracts.Count().ToString() : "0"));
                logger.Debug("END GetContractDetails: " + DateTime.Now.ToString());
                //List<string> columns = @"BUDAT;ZZSBILL_ENTRY;ZZBILL_ENTRY;ZZDATE_SBILL_ENTRY;SGTXT;WAERS;BLDAT;VFDAT;WRBTR;WRBTRSpecified;ZZGATE_ENT_DATE;ZZGATE_ENTRY;CHARG;LFBNR;BUDAT1;ZEILE;MBLNR;MJAHR;MENGE;MENGESpecified;WRBTR1;ZZBILL_DOC;ZZLR_DATE;ZZLR_NUMBER;ZZLUT_NUM;HSDAT;MAKTX;MATNR;BWART;WERKS;MENGE1;RDATE;SRVPOS;LFBNR1;LFPOS;STATUS;LGORT;MEINS;CHARG1;LIFNR;NAME1;LAND1;PS_PSP_PNR;EBELN;EBELP;CPUDT_MKPF;CPUTM_MKPF".Split(';').ToList();
                List<string> columns = @"CONTRACT;CONTRACT_DATE;VENDOR;VENDOR_NAME;ADDRESS;PERSON;HEADER_TEXT;TARGET_VALUE;ITEM;MATNR;TEXT;UOM;QUANTITY;CURRENCY;UNIT_PRICE;AMOUNT;TOTAL;CURENCY_WORDS;PAYMENT_TERMS;INCO_TERMS;MANUFACTURER;DEF_TEXT;CONTRACT_VALIDITY;ITEM_TEXT;DELIVERY_ADDRESS;AGREEMENT_TYPE;MATERIAL_GROUP;PLANT;MAT_PO_TEXT;TAX_CODE;PRICE_DATE;QTY_CONV;UNDER_TOLERENCE;OVER_TOLERENCE;EXCH_RATE".Split(';').ToList();

                if (contracts != null && contracts.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = contracts.AsEnumerable().ToList().Chunk(500);
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        int processedRows = 0;
                        foreach (var cntr in chunk)
                        {
                            var newRow = dt.NewRow();
                            newRow["JOB_ID"] = guid1;
                            newRow["CONTRACT"] = cntr.CONTRACT;
                            newRow["CONTRACT_DATE"] = cntr.CONTRACT_DATE == "0000-00-00" ? null : cntr.CONTRACT_DATE;
                            newRow["VENDOR"] = cntr.VENDOR;
                            newRow["VENDOR_NAME"] = cntr.VENDOR_NAME;
                            newRow["ADDRESS"] = cntr.ADDRESS;
                            newRow["PERSON"] = cntr.PERSON;
                            newRow["HEADER_TEXT"] = cntr.HEADER_TEXT;
                            newRow["TARGET_VALUE"] = cntr.TARGET_VALUE;
                            newRow["ITEM"] = cntr.ITEM;
                            newRow["MATNR"] = cntr.MATNR;
                            newRow["TEXT"] = cntr.TEXT;
                            newRow["UOM"] = cntr.UOM;
                            newRow["QUANTITY"] = cntr.QUANTITY;
                            newRow["CURRENCY"] = cntr.CURRENCY;
                            newRow["UNIT_PRICE"] = cntr.UNIT_PRICE;
                            newRow["AMOUNT"] = cntr.AMOUNT;
                            newRow["TOTAL"] = cntr.TOTAL;
                            newRow["CURENCY_WORDS"] = cntr.CURENCY_WORDS;
                            newRow["PAYMENT_TERMS"] = cntr.PAYMENT_TERMS;
                            newRow["INCO_TERMS"] = cntr.INCO_TERMS;
                            newRow["MANUFACTURER"] = cntr.MANUFACTURER;
                            newRow["DEF_TEXT"] = cntr.DEF_TEXT;
                            newRow["CONTRACT_VALIDITY"] = cntr.CONTRACT_VALIDITY;
                            newRow["ITEM_TEXT"] = cntr.ITEM_TEXT;
                            newRow["DELIVERY_ADDRESS"] = cntr.DELIVERY_ADDRESS;
                            newRow["AGREEMENT_TYPE"] = cntr.AGREEMENT_TYPE;
                            newRow["MATERIAL_GROUP"] = cntr.MATERIAL_GROUP;
                            newRow["PLANT"] = cntr.PLANT;
                            newRow["MAT_PO_TEXT"] = cntr.MAT_PO_TEXT;
                            newRow["TAX_CODE"] = cntr.TAX_CODE;
                            newRow["PRICE_DATE"] = cntr.PRICE_DATE == "0000-00-00" ? null : cntr.PRICE_DATE;
                            newRow["QTY_CONV"] = cntr.QTY_CONV;
                            newRow["UNDER_TOLERENCE"] = cntr.UNDER_TOLERENCE;
                            newRow["OVER_TOLERENCE"] = cntr.OVER_TOLERENCE;
                            newRow["EXCH_RATE"] = cntr.EXCH_RATE;
                            newRow["DATE_CREATED"] = DateTime.UtcNow;

                            dt.Rows.Add(newRow);
                            processedRows++;
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_CONTRACT_DETAILS]", columnMappings);
                        }
                        dt.Rows.Clear();
                        count++;
                    }

                    //if (contracts != null && contracts.Length > 0)
                    //{
                    //    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    //    sd.Add("P_JOB_ID", guid);
                    //    bizClass.SelectList("erp_process_sap_contract_details", sd);
                    //}
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GetContractDetails ERROR");
            }
        }

        public static void GetGRNData(string job_id)
        {
            List<POScheduleEmails> details = new List<POScheduleEmails>();
            MSSQLBizClass bizClass = new MSSQLBizClass();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_JOB_ID", job_id);
            DataSet ds = bizClass.SelectList("erp_send_grn_emails_vendors", sd);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                string sendAltEmails = ConfigurationManager.AppSettings["SEND_TO_ALTERNATE_EMAIL"].ToString();

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    POScheduleEmails PO = new POScheduleEmails();
                    PO.PO_NUMBER = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                    PO.PO_LINE_ITEM = row["PO_LINE_ITEM"] != DBNull.Value ? Convert.ToString(row["PO_LINE_ITEM"]) : string.Empty;
                    PO.GRN_NUMBER = row["GRN_NUMBER"] != DBNull.Value ? Convert.ToString(row["GRN_NUMBER"]) : string.Empty;
                    PO.GRN_LINE_ITEM = row["GRN_LINE_ITEM"] != DBNull.Value ? Convert.ToString(row["GRN_LINE_ITEM"]) : string.Empty;
                    PO.VENDOR_NAME = row["FULL_NAME"] != DBNull.Value ? Convert.ToString(row["FULL_NAME"]) : string.Empty;
                    PO.VENDOR_PRIMARY_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    PO.VENDOR_ALT_EMAIL = row["U_ALTEMAIL"] != DBNull.Value ? Convert.ToString(row["U_ALTEMAIL"]) : string.Empty;
                    PO.VENDOR_IS_PRIMARY = row["IS_PRIMARY"] != DBNull.Value ? Convert.ToString(row["IS_PRIMARY"]) : string.Empty;
                    PO.QUANTITY = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
                    PO.BUYER_EMAIL = row["BUYER_EMAIL"] != DBNull.Value ? Convert.ToString(row["BUYER_EMAIL"]) : string.Empty;
                    PO.PRODUCT_NAME = row["PRODUCT_NAME"] != DBNull.Value ? Convert.ToString(row["PRODUCT_NAME"]) : string.Empty;
                    PO.EMAIL_IDS = new List<string>();
                    PO.BUYER_EMAIL_IDS = new List<string>();
                    if (!string.IsNullOrEmpty(PO.VENDOR_PRIMARY_EMAIL))
                    {
                        PO.EMAIL_IDS.Add(PO.VENDOR_PRIMARY_EMAIL);
                    }

                    if (sendAltEmails.Equals("TRUE") && !string.IsNullOrEmpty(PO.VENDOR_ALT_EMAIL))
                    {
                        PO.EMAIL_IDS.Add(PO.VENDOR_ALT_EMAIL);
                    }

                    if (!string.IsNullOrEmpty(PO.BUYER_EMAIL))
                    {
                        PO.BUYER_EMAIL_IDS.Add(PO.BUYER_EMAIL);
                    }

                    details.Add(PO);
                }

                if (details != null && details.Count > 0)
                {
                    List<string> uniquePOs = details.Select(p => p.GRN_NUMBER).Distinct().ToList();
                    string htmlBody = ConfigurationManager.AppSettings["GRN_BODY_CONTENT"].ToString();
                    htmlBody = htmlBody.Replace("PRM_Link", $"<a href='{ConfigurationManager.AppSettings["GRN_PRM_URL"]}'>Link</a>");
                    string poBody = "";
                    List<string> successPO = new List<string>();
                    List<string> failedPO = new List<string>();
                    foreach (var po in uniquePOs)
                    {
                        poBody = htmlBody;
                        string subject = ConfigurationManager.AppSettings["GRN_SUBJECT"].ToString().Replace("PO_NUMBER", po);
                        List<POScheduleEmails> filteredPOs = details.Where(p => p.GRN_NUMBER.Equals(po)).ToList();
                        string tableContent = $@"<table style='width:100%;border: 1px solid black;border-collapse: collapse;'>
                                               <tr><td style='border:1px solid black;border-collapse:collapse;'>GRN Number</td>
                                                   <td style='border:1px solid black;border-collapse:collapse;'>GRN Line Item</td>
                                                   <td style='border:1px solid black;border-collapse:collapse;'>PO Number</td>
                                                   <td style='border:1px solid black;border-collapse:collapse;'>PO Line Item</td>
                                                   <td style='border:1px solid black;border-collapse:collapse;'>Material Description</td>
                                                   <td style='border:1px solid black;border-collapse:collapse;'>Quantity</td>
                                                </tr>";
                        foreach (var grnItem in filteredPOs) 
                        {
                            tableContent += $@"<tr><td style='border:1px solid black;border-collapse:collapse;'>{grnItem.GRN_NUMBER}</td>
                                             <td style='border:1px solid black;border-collapse:collapse;'>{grnItem.GRN_LINE_ITEM}</td>
                                             <td style='border:1px solid black;border-collapse:collapse;'>{grnItem.PO_NUMBER}</td>
                                             <td style='border:1px solid black;border-collapse:collapse;'>{grnItem.PO_LINE_ITEM}</td>
                                             <td style='border:1px solid black;border-collapse:collapse;'>{grnItem.PRODUCT_NAME}</td>
                                             <td style='border:1px solid black;border-collapse:collapse;'>{grnItem.QUANTITY}</td>
                                        </tr>";
                        }
                        tableContent += $@"</table>";
                        poBody = poBody.Replace("TABLE_CONTENT", $"{tableContent}");

                        var status = SendEmail(filteredPOs.First().EMAIL_IDS, filteredPOs.ToList().First().BUYER_EMAIL_IDS, subject, poBody);
                        if (status)
                        {
                            successPO.Add(po);
                        }
                        else
                        {
                            failedPO.Add(po);
                        }
                    }

                    if (successPO.Count > 0)
                    {
                        string pos = string.Join(",", successPO.Select(p => "'" + p + "'"));

                        string query = $"UPDATE GRN_DETAILS SET SENT_VENDOR_GRN_EMAIL = 1, GRN_EMAIL_SENT_DATE = GETUTCDATE() WHERE MBLNR IN ({pos})";
                        logger.Info("SUCCESS:" + query);
                        bizClass.SelectQuery(query);
                    }
                    if (failedPO.Count > 0)
                    {
                        string pos = string.Join(",", failedPO.Select(p => "'" + p + "'"));

                        string query = $"UPDATE GRN_DETAILS SET SENT_VENDOR_GRN_EMAIL = 0, GRN_EMAIL_SENT_DATE = GETUTCDATE() WHERE MBLNR IN ({pos})";
                        logger.Info("FAILED:" + query);
                        bizClass.SelectQuery(query);
                    }
                }
            }
        }

        public static void GetInvoiceData(string job_id)
        {
            MSSQLBizClass bizClass = new MSSQLBizClass();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_JOB_ID", job_id);
            DataSet ds = bizClass.SelectList("erp_send_invoice_rejection_emails", sd);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                var groupedRows = ds.Tables[0].AsEnumerable().GroupBy(row => row.Field<string>("UNIQUE_NUMBER"));

                string subject = ConfigurationManager.AppSettings["INVOICE_REJECTION_SUBJECT"].ToString();
                
                foreach (var group in groupedRows) 
                {
                    string body = ConfigurationManager.AppSettings["INVOICE_REJECTION_BODY_CONTENT"].ToString();

                    prepareEmailFormat(group, ds, body, subject);

                }
            }
        }

        public static void GetPaymentData(string job_id)
        {
            MSSQLBizClass bizClass = new MSSQLBizClass();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_JOB_ID", "7DD996D1-D6AB-4867-AB7E-2D695B652BB9");
            DataSet ds = bizClass.SelectList("erp_send_payment_emails", sd);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                var groupedRows = ds.Tables[0].AsEnumerable().GroupBy(row => row.Field<string>("UNIQUE_NUMBER"));

                string subject = ConfigurationManager.AppSettings["PAYMENT_SUBJECT"].ToString();

                foreach (var group in groupedRows)
                {
                    string body = ConfigurationManager.AppSettings["PAYMENT_BODY_CONTENT"].ToString();

                    prepareEmailFormat(group, ds, body, subject);

                }
            }
        }


        static void prepareEmailFormat(IGrouping<string,DataRow> group, DataSet ds, string body, string subject)
        {
            string email = group.Key;

            var columnsToExclude = new HashSet<string> { "UNIQUE_NUMBER" };

            List<string> emailAddress = new List<string>();
            StringBuilder htmlBuilder = new StringBuilder();
            htmlBuilder.AppendLine($@"<table style='width:100%;border: 1px solid black;border-collapse:collapse'>");
            htmlBuilder.AppendLine("<tr style='border:1px solid black;'>");
            foreach (DataColumn column in ds.Tables[0].Columns)
            {
                //htmlBuilder.AppendLine($@"<th style='border:1px solid black;border-collapse:collapse;'>{column.ColumnName}</th>");
                if (!columnsToExclude.Contains(column.ColumnName))
                {
                    htmlBuilder.AppendLine($@"<th style='border:1px solid black;'>{column.ColumnName}</th>");
                }
            }
            htmlBuilder.AppendLine("</tr>");


            foreach (DataRow row in group)
            {
                htmlBuilder.AppendLine("<tr style='border:1px solid black;'>");
                foreach (DataColumn column in ds.Tables[0].Columns)
                {
                    //htmlBuilder.AppendLine($"<td style='border:1px solid black;border-collapse:collapse;'>{row[column.ColumnName]}</td>");
                    if (!columnsToExclude.Contains(column.ColumnName))
                    {
                        htmlBuilder.AppendLine($"<td style='border:1px solid black;'>{row[column.ColumnName]}</td>");
                    }
                }
                htmlBuilder.AppendLine("</tr>");
            }
            htmlBuilder.AppendLine("</table>");

            body = body.Replace("TABLE_CONTENT", $"{htmlBuilder}");

            emailAddress.Add(email);

            SendEmail(emailAddress, null, subject, body);

        }

        public static void GetPOData(string job_id)
        {
            List<POScheduleEmails> details = new List<POScheduleEmails>();
            MSSQLBizClass bizClass = new MSSQLBizClass();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_JOB_ID", job_id);
            DataSet ds = bizClass.SelectList("erp_send_po_emails_vendors", sd);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                string sendAltEmails = ConfigurationManager.AppSettings["SEND_TO_ALTERNATE_EMAIL"].ToString();

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    POScheduleEmails PO = new POScheduleEmails();
                    PO.PO_NUMBER = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                    PO.PO_LINE_ITEM = row["PO_LINE_ITEM"] != DBNull.Value ? Convert.ToString(row["PO_LINE_ITEM"]) : string.Empty;
                    PO.PO_CREATOR = row["PO_CREATOR"] != DBNull.Value ? Convert.ToString(row["PO_CREATOR"]) : string.Empty;
                    PO.PO_DATE = row["PO_DATE"] != DBNull.Value ? Convert.ToString(row["PO_DATE"]) : string.Empty;
                    PO.VENDOR_NAME = row["FULL_NAME"] != DBNull.Value ? Convert.ToString(row["FULL_NAME"]) : string.Empty;
                    PO.VENDOR_PRIMARY_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    PO.VENDOR_ALT_EMAIL = row["U_ALTEMAIL"] != DBNull.Value ? Convert.ToString(row["U_ALTEMAIL"]) : string.Empty;
                    PO.VENDOR_IS_PRIMARY = row["IS_PRIMARY"] != DBNull.Value ? Convert.ToString(row["IS_PRIMARY"]) : string.Empty;
                    PO.BUYER_EMAIL = row["BUYER_EMAIL"] != DBNull.Value ? Convert.ToString(row["BUYER_EMAIL"]) : string.Empty;
                    PO.EMAIL_IDS = new List<string>();
                    PO.BUYER_EMAIL_IDS = new List<string>();
                    if (!string.IsNullOrEmpty(PO.VENDOR_PRIMARY_EMAIL))
                    {
                        PO.EMAIL_IDS.Add(PO.VENDOR_PRIMARY_EMAIL);
                    }
                    
                    if (sendAltEmails.Equals("TRUE") && !string.IsNullOrEmpty(PO.VENDOR_ALT_EMAIL))
                    {
                        PO.EMAIL_IDS.Add(PO.VENDOR_ALT_EMAIL);
                    }

                    if (!string.IsNullOrEmpty(PO.BUYER_EMAIL))
                    {
                        PO.BUYER_EMAIL_IDS.Add(PO.BUYER_EMAIL);
                    }

                    details.Add(PO);
                }

                if (details != null && details.Count > 0)
                {
                    List<string> uniquePOs = details.Select(p => p.PO_NUMBER).Distinct().ToList();
                    string htmlBody = ConfigurationManager.AppSettings["BODY_CONTENT"].ToString();
                    htmlBody = htmlBody.Replace("PRM_Link", $"<a href='{ConfigurationManager.AppSettings["PRM_URL"]}'>Link</a>");
                    string poBody = "";
                    List<string> successPO = new List<string>();
                    List<string> failedPO = new List<string>();
                    foreach (var po in uniquePOs)
                    {
                        poBody = htmlBody;
                        string subject = ConfigurationManager.AppSettings["SUBJECT"].ToString().Replace("PO_NUMBER", po);
                        List<POScheduleEmails> filteredPOs = details.Where(p => p.PO_NUMBER.Equals(po)).ToList();
                        string poCreatedBy = filteredPOs.First().PO_CREATOR;
                        string poDate = filteredPOs.First().PO_DATE;
                        poBody = poBody.Replace("PO_NUMBER", po);
                        string items = string.Join(",", filteredPOs.Select(p => p.PO_LINE_ITEM).Distinct());
                        //poBody += $@"<table style='width:100%;border: 1px solid black;border-collapse: collapse;'> 
                        //            <tr><td style='border:1px solid black;border-collapse:collapse;'>PO Number</td><td style='border:1px solid black;border-collapse:collapse;'>{po}</td></tr>
                        //            <tr><td style='border:1px solid black;border-collapse:collapse;'>PO Line Items</td>
                        //            <td style='border:1px solid black;border-collapse:collapse;'>{items}</td></tr> 
                        //            <tr><td style='border:1px solid black;border-collapse:collapse;'>PO Created By</td>
                        //                <td style='border:1px solid black;border-collapse:collapse;'>{poCreatedBy}</td></tr> 
                        //            <tr><td style='border:1px solid black;border-collapse:collapse;'>PO Date</td>
                        //                <td style='border:1px solid black;border-collapse:collapse;'>{poDate}</td></tr> 
                        //            </table>";

                        var status = SendEmail(filteredPOs.First().EMAIL_IDS, filteredPOs.ToList().First().BUYER_EMAIL_IDS, subject, poBody);
                        if (status)
                        {
                            successPO.Add(po);
                        }
                        else
                        {
                            failedPO.Add(po);
                        }
                    }

                    if (successPO.Count > 0)
                    {
                        string pos = string.Join(",", successPO.Select(p => "'" + p + "'"));
                        
                        string query = $"UPDATE POScheduleDetails SET SENT_VENDOR_PO_EMAIL = 1, EMAIL_SENT_DATE = GETUTCDATE() WHERE PO_NUMBER IN ({pos})";
                        logger.Info("SUCCESS:" + query);
                        bizClass.SelectQuery(query);
                    }
                    if (failedPO.Count > 0)
                    {
                        string pos = string.Join(",", failedPO.Select(p => "'" + p + "'"));
                        
                        string query = $"UPDATE POScheduleDetails SET SENT_VENDOR_PO_EMAIL = 0, EMAIL_SENT_DATE = GETUTCDATE() WHERE PO_NUMBER IN ({pos})";
                        logger.Info("FAILED:" + query);
                        bizClass.SelectQuery(query);
                    }
                }
            }
        }

        private static void ReadPRFiles() 
        {
            logger.Info("reading ReadPRFiles()");

            try
            {
                var host = ConfigurationManager.AppSettings["FTP_HOST"].ToString();
                var port = Convert.ToInt32(ConfigurationManager.AppSettings["FTP_PORT"].ToString());
                var ftpUser = ConfigurationManager.AppSettings["FTP_USERNAME"].ToString();
                var ftpPass = ConfigurationManager.AppSettings["FTP_PASSWORD"].ToString();
                var filePath = ConfigurationManager.AppSettings["QCS_FILE_PATH_TO_READ"].ToString();

                string ftp = host;


                using (SftpClient sftpClient = new SftpClient(ftp, port, ftpUser, ftpPass))
                {
                    sftpClient.Connect();
                    string targetDirectory = filePath.ToString();
                    sftpClient.ChangeDirectory(targetDirectory);
                    string currentDirectory = sftpClient.WorkingDirectory;
                    var files = sftpClient.ListDirectory(currentDirectory);
                    logger.Info("Total Files>>>" + files.Count());
                    foreach (var file in files)
                    {
                        using (var fileStream = File.OpenWrite(Path.Combine(stagingFolder, file.Name)))
                        {
                            sftpClient.DownloadFile(Path.Combine(filePath, file.Name), fileStream);
                        }
                        string fileContents = File.ReadAllText(Path.Combine(stagingFolder, file.Name));
                    }
                }

                ProcessPRExcelFiles();
            }
            catch (Exception ex)
            {
                string exceptionDet = $@"{ex.Message + "inner stack >>" + ex.StackTrace}";
                logger.Error("exception is>>>" + exceptionDet);
            }
        }

        static void ProcessPRExcelFiles()
        {
            string[] filePaths = Directory.GetFiles(stagingFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(fileName))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = processingFolder + "\\" + Path.GetFileName(filepath);
                        //processFolder = processFolder.Replace(".xlsx", ".csv").Replace(".txt", ".csv");

                        string archiveFolder = processedFolder + "\\" + Path.GetFileName(filepath);
                        //archiveFolder = archiveFolder.Replace(".xlsx", ".csv").Replace(".txt", ".csv");

                        string errorFolder = errFolder + "\\" + Path.GetFileName(filepath);
                        //errorFolder = errorFolder.Replace(".xlsx", ".csv").Replace(".txt", ".csv");

                        try
                        {
                            DataTable currentData = new DataTable();
                            ExcelPackage xlPackage = new ExcelPackage(new FileInfo(filepath));
                            currentData = WorksheetToDataTable(xlPackage.Workbook.Worksheets[1]);
                            if (currentData.Rows.Count > 0)
                            {
                                File.Move(filepath, processFolder);
                                MSSQLBizClass bizClass = new MSSQLBizClass();
                                DataTable dt = new DataTable();
                                dt.Clear();
                                dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                dt.Columns.AddRange
                                    (new DataColumn[]
                                    {
                                        new DataColumn("PR_NUMBER", typeof(string)),
                                        new DataColumn("ITEM_OF_REQUESITION", typeof(string)),
                                        new DataColumn("PR_TYPE", typeof(string)),
                                        new DataColumn("PR_CREATOR_NAME", typeof(string)),
                                        new DataColumn("REQUESITION_DATE", typeof(string)),
                                        new DataColumn("PURCHASE_GROUP_CODE", typeof(string)),
                                        new DataColumn("PURCHASE_GROUP_NAME", typeof(string)),
                                        new DataColumn("PLANT", typeof(string)),
                                        new DataColumn("PLANT_CODE", typeof(string)),
                                        new DataColumn("TOSS_QTY4", typeof(Decimal)),
                                        new DataColumn("QTY_REQUIRED", typeof(Decimal)),
                                        new DataColumn("MATERIAL_CODE", typeof(string)),
                                        new DataColumn("MATERIAL_DESCRIPTION", typeof(string)),
                                        new DataColumn("SHORT_TEXT", typeof(string)),
                                        new DataColumn("UOM", typeof(string)),
                                        new DataColumn("MATERIAL_DELIVERY_DATE", typeof(string)),
                                        new DataColumn("PR_NOTE", typeof(string)),
                                        new DataColumn("PR_RELEASE_DATE", typeof(string)),
                                        new DataColumn("PR_CHANGE_DATE", typeof(string)),
                                        new DataColumn("MATERIAL_GROUP_CODE", typeof(string)),
                                        new DataColumn("MATERIAL_GROUP_DESC", typeof(string)),
                                        new DataColumn("MATERIAL_TYPE", typeof(string)),
                                        new DataColumn("DATE_CREATED", System.Type.GetType("System.DateTime")),
                                    }
                                    );
                                int processedRows = 0;
                                foreach (DataRow pr in currentData.Rows) 
                                {
                                    string manDt = (pr.IsNull("MANDT") || pr["MANDT"] == DBNull.Value || string.IsNullOrEmpty(pr["MANDT"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["MANDT"].ToString().Trim());
                                    string prNumber = (pr.IsNull("BANFN") || pr["BANFN"] == DBNull.Value || string.IsNullOrEmpty(pr["BANFN"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["BANFN"].ToString().Trim());
                                    string prItemNumber = (pr.IsNull("BNFPO") || pr["BNFPO"] == DBNull.Value || string.IsNullOrEmpty(pr["BNFPO"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["BNFPO"].ToString().Trim());
                                    string prType = (pr.IsNull("BSART") || pr["BSART"] == DBNull.Value || string.IsNullOrEmpty(pr["BSART"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["BSART"].ToString().Trim());
                                    string prCreatorName = (pr.IsNull("ERNAM") || pr["ERNAM"] == DBNull.Value || string.IsNullOrEmpty(pr["ERNAM"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["ERNAM"].ToString().Trim());
                                    string requisitionDate = (pr.IsNull("BADAT") || pr["BADAT"] == DBNull.Value || string.IsNullOrEmpty(pr["BADAT"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["BADAT"].ToString().Trim());
                                    string purchaseGrpCode = (pr.IsNull("EKGRP") || pr["EKGRP"] == DBNull.Value || string.IsNullOrEmpty(pr["EKGRP"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["EKGRP"].ToString().Trim());
                                    string ekOrg = (pr.IsNull("EKORG") || pr["EKORG"] == DBNull.Value || string.IsNullOrEmpty(pr["EKORG"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["EKORG"].ToString().Trim());
                                    string plant = (pr.IsNull("WERKS") || pr["WERKS"] == DBNull.Value || string.IsNullOrEmpty(pr["WERKS"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["WERKS"].ToString().Trim());
                                    string tossQty4 = (pr.IsNull("TOSS_MENGE") || pr["TOSS_MENGE"] == DBNull.Value || string.IsNullOrEmpty(pr["TOSS_MENGE"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["TOSS_MENGE"].ToString().Trim());
                                    string prmQty = (pr.IsNull("PRM_MENGE") || pr["PRM_MENGE"] == DBNull.Value || string.IsNullOrEmpty(pr["PRM_MENGE"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["PRM_MENGE"].ToString().Trim());
                                    string materialCode = (pr.IsNull("MATNR") || pr["MATNR"] == DBNull.Value || string.IsNullOrEmpty(pr["MATNR"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["MATNR"].ToString().Trim());
                                    string materialDescription = (pr.IsNull("TXZ01") || pr["TXZ01"] == DBNull.Value || string.IsNullOrEmpty(pr["TXZ01"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["TXZ01"].ToString().Trim());
                                    string uom = (pr.IsNull("MEINS") || pr["MEINS"] == DBNull.Value || string.IsNullOrEmpty(pr["MEINS"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["MEINS"].ToString().Trim());
                                    string materialDelvDate = (pr.IsNull("LFDAT") || pr["LFDAT"] == DBNull.Value || string.IsNullOrEmpty(pr["LFDAT"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["LFDAT"].ToString().Trim());
                                    string prNote = (pr.IsNull("MESS") || pr["MESS"] == DBNull.Value || string.IsNullOrEmpty(pr["MESS"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["MESS"].ToString().Trim());
                                    string category = (pr.IsNull("MTART") || pr["MTART"] == DBNull.Value || string.IsNullOrEmpty(pr["MTART"].ToString().Trim())) ? string.Empty : Convert.ToString(pr["MTART"].ToString().Trim());

                                    decimal fortussQty = 0;
                                    if (!string.IsNullOrEmpty(tossQty4))
                                    {
                                        fortussQty = Convert.ToDecimal(tossQty4);
                                    }


                                    var row = dt.NewRow();
                                    row["JOB_ID"] = guid1;
                                    //row["MANDT"] = $"{}";
                                    //row["PR_NUMBER"] = $"{prNumber}";
                                    
                                    //if (!string.IsNullOrEmpty(requisitionDate)) 
                                    //{
                                    //    requisitionDate = Convert.ToDateTime(requisitionDate).ToString("yyyy-MM-dd");
                                    //}

                                    //if (!string.IsNullOrEmpty(materialDelvDate))
                                    //{
                                    //    materialDelvDate = Convert.ToDateTime(materialDelvDate).ToString("yyyy-MM-dd");
                                    //}

                                    row["PR_NUMBER"] = $"{prNumber}";
                                    row["ITEM_OF_REQUESITION"] = $"{prItemNumber}";
                                    row["PR_TYPE"] = $"{prType}";
                                    row["PR_CREATOR_NAME"] = $"{prCreatorName}";
                                    row["REQUESITION_DATE"] = (!string.IsNullOrEmpty(requisitionDate) ? Convert.ToDateTime(requisitionDate).ToString("yyyy-MM-dd") : null);
                                    row["PURCHASE_GROUP_CODE"] = $"{purchaseGrpCode}";
                                    row["PURCHASE_GROUP_NAME"] = $"{purchaseGrpCode}";
                                    row["PLANT"] = $"{plant}";
                                    row["PLANT_CODE"] = $"{plant}";
                                    row["TOSS_QTY4"] = $"{tossQty4}";
                                    row["QTY_REQUIRED"] = $"{prmQty}";
                                    row["MATERIAL_CODE"] = $"{materialCode}";
                                    row["MATERIAL_DESCRIPTION"] = $"{materialDescription}";
                                    row["SHORT_TEXT"] = $"{materialDescription}";
                                    row["MATERIAL_GROUP_CODE"] = $"{category}";
                                    row["MATERIAL_GROUP_DESC"] = $"{materialDescription}";
                                    row["MATERIAL_TYPE"] = $"{prType}";
                                    row["UOM"] = $"{uom}";
                                    row["MATERIAL_DELIVERY_DATE"] = (!string.IsNullOrEmpty(materialDelvDate) ? Convert.ToDateTime(materialDelvDate).ToString("yyyy-MM-dd") : null);
                                    row["PR_NOTE"] = $"{prNote}";
                                    row["PR_RELEASE_DATE"] = (!string.IsNullOrEmpty(requisitionDate) ? Convert.ToDateTime(requisitionDate).ToString("yyyy-MM-dd") : null);
                                    row["PR_CHANGE_DATE"] = (!string.IsNullOrEmpty(requisitionDate) ? Convert.ToDateTime(requisitionDate).ToString("yyyy-MM-dd") : null);
                                    row["DATE_CREATED"] = $"{DateTime.UtcNow}";

                                    /*row["GMP"] = $"";
                                    row["PR_RELEASE_DATE"] = !string.IsNullOrEmpty(requisitionDate) ? Convert.ToDateTime(requisitionDate).ToString("yyyy-MM-dd") : null;
                                    row["PR_CHANGE_DATE"] = !string.IsNullOrEmpty(requisitionDate) ? Convert.ToDateTime(requisitionDate).ToString("yyyy-MM-dd") : null;
                                    row["REQUISITIONER_NAME"] = $"";
                                    row["REQUISITIONER_EMAIL"] = $"";
                                    row["MATERIAL_GROUP_CODE"] = $"";
                                    row["MATERIAL_GROUP_DESC"] = $"";
                                    row["MATERIAL_TYPE"] = $"";
                                    row["MATERIAL_HSN_CODE"] = $"";
                                    row["SHORT_TEXT"] = $"";
                                    row["ITEM_TEXT"] = $"";
                                    row["LEAD_TIME"] = $"";
                                    row["PR_TYPE_DESC"] = $"";
                                    row["CASNR"] = $"{""}";
                                    row["WBS_CODE"] = $"{""}";
                                    row["MFCD_NUMBER"] = $"{""}";
                                    row["PROJECT_DESCRIPTION"] = $"";
                                    row["PROFIT_CENTER"] = $"";
                                    row["ALTERNATE_UOM"] = $"";
                                    row["ALTERNATE_QTY"] = $"";
                                    row["SECTION_HEAD"] = $"";
                                    row["SERVICE_CODE"] = $"";
                                    row["SERVICE_CODE_DESCRIPTION"] = $"";
                                    row["SERVICE_QTY"] = $"";
                                    row["SERVICE_UNIT_OF_MEASURE"] = $"";
                                    row["ITEM_CATEGORY"] = $"";
                                    row["ACCOUNT_ASSIGNMENT_CATEGORY"] = $"";
                                    row["PROJECT_TYPE"] = $"";
                                    row["SUB_VERTICAL"] = $"";
                                    row["LOEKZ"] = $"";
                                    row["ERDAT"] = $"";
                                    row["EBAKZ"] = $"";*/
                                    dt.Rows.Add(row);
                                    processedRows++;
                                }

                                List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                foreach (DataColumn col in dt.Columns)
                                {
                                    columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                }

                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    bizClass.BulkInsert(dt, "[dbo].[SAP_PR_DETAILS]", columnMappings);
                                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                                    sd.Add("P_JOB_ID", guid1);
                                    bizClass.SelectList("erp_process_sap_pr_details", sd);
                                }

                                dt.Rows.Clear();
                            }

                            archiveFolder = archiveFolder.Replace(".xlsx", "_" + ticks + ".xlsx");
                            File.Move(processFolder, archiveFolder);

                        }
                        catch (Exception ex) 
                        {
                            string exceptDet = ex.Message + "inner stack >" + ex.StackTrace;
                            logger.Error("exception while processing data"+ exceptDet);
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }


        static void ListDirectory(SftpClient client, String dirName, ref List<String> files)
        {
            try
            {
                foreach (var entry in client.ListDirectory(dirName))
                {

                    if (entry.IsDirectory)
                    {
                        ListDirectory(client, entry.FullName, ref files);
                    }
                    else
                    {
                        logger.Info("file name not a directory>>>"+ entry.FullName);
                        files.Add(entry.FullName);
                    }
                }
            }
            catch (Exception ex) 
            {
                throw ex;
            }
        }

        public static bool SendEmail(List<string> ToAddresses, List<string> ccAddresses, string subject, string body)
        {
            bool isSuccess = false;
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(RemoteServerCertificateValidationCallback);
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
                SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString(), Convert.ToInt16(ConfigurationManager.AppSettings["MAILHOST_PORT"]));
                MailMessage mail = new MailMessage();


                if (ToAddresses != null && ToAddresses.Count > 0)
                {
                    foreach (string address in ToAddresses)
                    {
                        if (!string.IsNullOrEmpty(address))
                        {
                            mail.To.Add(new MailAddress("sreevani@prm360.com"));
                        }
                    }
                }

                if (ccAddresses != null && ccAddresses.Count > 0)
                {
                    foreach (string address in ccAddresses)
                    {
                        if (!string.IsNullOrEmpty(address))
                        {
                            mail.CC.Add(new MailAddress(address));
                        }
                    }
                }

                //if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["PO_TEST_EMAIL"]))
                //{
                //    mail.To.Clear();
                //    mail.CC.Clear();
                //    var testEmails = ConfigurationManager.AppSettings["PO_TEST_EMAIL"].Split(';');
                //    foreach (var email in testEmails)
                //    {
                //        mail.To.Add(email);
                //    }
                //}

                string emails = "";
                
                foreach (var emailAdd in mail.To)
                {
                    emails += ";" + emailAdd.Address;
                }
                
                logger.Info(subject + "----" + emails);
                mail.From = new MailAddress(ConfigurationManager.AppSettings["FROMADDRESS"], ConfigurationManager.AppSettings["FROMDISPLAYNAME"]);
                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = body;
                smtpClient.Send(mail);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                isSuccess = false;
                logger.Error(ex, ex.Message);
            }

            return isSuccess;
        }

        private static void UploadFileToFtp(string url, NetworkCredential credentials, string localPath)
        {
            FtpWebRequest listRequest = (FtpWebRequest)WebRequest.Create(url);
            listRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            listRequest.Credentials = credentials;

            List<string> lines = new List<string>();

            using (FtpWebResponse listResponse = (FtpWebResponse)listRequest.GetResponse())
            using (Stream listStream = listResponse.GetResponseStream())
            using (StreamReader listReader = new StreamReader(listStream))
            {
                while (!listReader.EndOfStream)
                {
                    lines.Add(listReader.ReadLine());
                }
            }

            foreach (string line in lines)
            {
                try
                {
                    string[] tokens = line.Split(new[] { ' ' }, 9, StringSplitOptions.RemoveEmptyEntries);
                    string name = tokens[3];

                    string localFilePath = Path.Combine(localPath, name);
                    string fileUrl = url + "/" + name;

                    if (tokens[2] == "<DIR>" && false)
                    {
                        if (!Directory.Exists(localFilePath))
                        {
                            Directory.CreateDirectory(localFilePath);
                        }

                        //DownloadFtpDirectory(fileUrl + "/", credentials, localFilePath, jobType);
                    }
                    else if (tokens[2] != "<DIR>" && !string.IsNullOrWhiteSpace(name) && name.ToLower().Contains(".csv"))
                    {
                        logger.Info("FILEURL:" + fileUrl);
                        FtpWebRequest downloadRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                        downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                        downloadRequest.Credentials = credentials;
                        using (FtpWebResponse downloadResponse = (FtpWebResponse)downloadRequest.GetResponse())
                        using (Stream sourceStream = downloadResponse.GetResponseStream())
                        using (Stream targetStream = File.Create(localFilePath))
                        {
                            byte[] buffer = new byte[10240];
                            int read;
                            while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                targetStream.Write(buffer, 0, read);
                            }
                        }

                        FtpWebRequest fileMoveRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                        fileMoveRequest.UseBinary = true;
                        fileMoveRequest.Method = WebRequestMethods.Ftp.Rename;
                        fileMoveRequest.Credentials = credentials;
                        fileMoveRequest.RenameTo = $"{ConfigurationManager.AppSettings["FTPArchiveFolder"]}{name}";
                        var response = (FtpWebResponse)fileMoveRequest.GetResponse();
                        bool Success = response.StatusCode == FtpStatusCode.CommandOK || response.StatusCode == FtpStatusCode.FileActionOK;
                    }
                }
                catch (Exception ex)
                {
                    logger.Error($"DOWNLOAD FTP FILE: {line} with ERROR:{ex.Message}, DETAILS: {ex.StackTrace}");
                }
            }
        }

        private static bool RemoteServerCertificateValidationCallback(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            //Console.WriteLine(certificate);
            return true;
        }

        public static DataTable WorksheetToDataTable(ExcelWorksheet oSheet)
        {
            int totalRows = oSheet.Dimension.End.Row;
            int totalCols = oSheet.Dimension.End.Column;
            DataTable dt = new DataTable(oSheet.Name);
            DataRow dr = null;
            for (int i = 1; i <= totalRows; i++)
            {
                if (i > 1) dr = dt.Rows.Add();
                for (int j = 1; j <= totalCols; j++)
                {
                    if (i == 1)
                    {
                        dt.Columns.Add(oSheet.Cells[i, j].Value.ToString());
                    }
                    else
                    {
                        if (oSheet.Cells[i, j] != null && oSheet.Cells[i, j].Value != null)
                        {
                            dr[j - 1] = oSheet.Cells[i, j].Value.ToString();
                        }
                        else
                        {
                            dr[j - 1] = string.Empty;
                        }
                    }
                }
            }
            return dt;
        }


    }

    public static class ObjExtensions
    {
        public static Object GetPropValue(this Object obj, String name)
        {
            foreach (String part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(part);
                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }

        public static T GetPropValue<T>(this Object obj, String name)
        {
            Object retval = GetPropValue(obj, name);
            if (retval == null) { return default(T); }

            // throws InvalidCastException if types are incompatible
            return (T)retval;
        }

        public static void ToCSV(this DataTable dtDataTable, string strFilePath)
        {
            StreamWriter sw = new StreamWriter(strFilePath, false);
            //headers    
            for (int i = 0; i < dtDataTable.Columns.Count; i++)
            {
                sw.Write(dtDataTable.Columns[i]);
                if (i < dtDataTable.Columns.Count - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
            foreach (DataRow dr in dtDataTable.Rows)
            {
                for (int i = 0; i < dtDataTable.Columns.Count; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        string value = dr[i].ToString();
                        if (value.Contains(','))
                        {
                            value = String.Format("\"{0}\"", value);
                            sw.Write(value);
                        }
                        else
                        {
                            sw.Write(dr[i].ToString());
                        }
                    }
                    if (i < dtDataTable.Columns.Count - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();
        }
    }    

    public class POScheduleEmails
    {
        public string GRN_NUMBER { get; set; }
        public string GRN_LINE_ITEM { get; set; }
        public string PO_NUMBER { get; set; }
        public string PO_LINE_ITEM { get; set; }
        public string PO_CREATOR { get; set; }
        public string PO_DATE { get; set; }
        public string VENDOR_NAME { get; set; }
        public string VENDOR_PRIMARY_EMAIL { get; set; }
        public string VENDOR_ALT_EMAIL { get; set; }
        public string VENDOR_IS_PRIMARY { get; set; }
        public double QUANTITY { get; set; }
        public List<string> EMAIL_IDS { get; set; }
        public List<string> BUYER_EMAIL_IDS { get; set; }
        public string BUYER_EMAIL { get; set; }
        public string PRODUCT_NAME { get; set; }

    }
}
