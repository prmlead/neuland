﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeulandSAPIntegration
{
    public static class Helper
    {
        public static DataTable ReadCsvFile(string path, char csvDelimiter, string fileName)
        {
            DataTable dtCsv = new DataTable();
            string Fulltext;
            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                    string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                    for (int i = 0; i < rows.Count() - 1; i++)
                    {
                        string[] rowValues = rows[i].Split(csvDelimiter); //split each row with comma to get individual values  
                        {
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    string columnName = string.Empty;
                                    if (rowValues[j] != null)
                                    {
                                        columnName = rowValues[j].Trim();
                                        if (columnName.Contains("("))
                                        {
                                            columnName = columnName.Split('(')[0];
                                        }

                                            columnName = columnName.Replace(" ", "_");
                                            if (columnName.Equals("REQUESITION_DATE", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                columnName = "REQUESITION_DATE";
                                            }
                                            if (columnName.Equals("ITEM_OF_REQUESITION", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                columnName = "REQUISITION_ITEM";
                                            }
                                    }

                                    columnName = columnName.ToUpper();
                                    if (columnName.ToLower().Contains("date"))
                                    {
                                        dtCsv.Columns.Add(columnName, typeof(DateTime));
                                    }
                                    else
                                    {
                                        dtCsv.Columns.Add(columnName, typeof(String));
                                    }
                                }

                                dtCsv.Columns.Add("FILE_NAME", typeof(String));
                            }
                            else
                            {
                                DataRow dr = dtCsv.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    string columnValue = rowValues[k]?.ToString().Trim();
                                    try
                                    {
                                        DateTime dt;
                                        DateTime.TryParseExact(columnValue,
                                                               "dd.MM.yyyy",
                                                               CultureInfo.InvariantCulture,
                                                               DateTimeStyles.None,
                                                               out dt);
                                        if(dt.Year != 0001)
                                        {
                                            dr[k] = dt;
                                        }
                                        else
                                        {
                                            dr[k] = columnValue;
                                        }
                                    }
                                    catch {
                                        try
                                        {
                                            dr[k] = columnValue;
                                        }
                                        catch
                                        {
                                            //dr[k] = columnValue;
                                        }
                                    }
                                    
                                }

                                dr["FILE_NAME"] = fileName;
                                dtCsv.Rows.Add(dr); //add other rows  
                            }
                        }
                    }
                }
            }
            return dtCsv;
        }

        public static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> source, int chunksize)
        {
            while (source.Any())
            {
                yield return source.Take(chunksize);
                source = source.Skip(chunksize);
            }
        }

        public static DataTable ToDataTable<T>(this IEnumerable<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
