﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace NeulandSAPIntegration.Payment {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    // CODEGEN: The optional WSDL extension element 'Policy' from namespace 'http://schemas.xmlsoap.org/ws/2004/09/policy' was not handled.
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.9037.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="zfm_payment_details_srv", Namespace="urn:sap-com:document:sap:rfc:functions")]
    public partial class ZFM_PAYMENT_DETAILS_SRV : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback ZFM_PAYMENT_DETAILSOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public ZFM_PAYMENT_DETAILS_SRV() {
            this.Url = global::NeulandSAPIntegration.Properties.Settings.Default.NeulandSAPIntegration_Payment_ZFM_PAYMENT_DETAILS_SRV;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event ZFM_PAYMENT_DETAILSCompletedEventHandler ZFM_PAYMENT_DETAILSCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("urn:sap-com:document:sap:rfc:functions:zfm_payment_details_srv:ZFM_PAYMENT_DETAIL" +
            "SRequest", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("ZFM_PAYMENT_DETAILSResponse", Namespace="urn:sap-com:document:sap:rfc:functions")]
        public ZFM_PAYMENT_DETAILSResponse ZFM_PAYMENT_DETAILS([System.Xml.Serialization.XmlElementAttribute("ZFM_PAYMENT_DETAILS", Namespace="urn:sap-com:document:sap:rfc:functions")] ZFM_PAYMENT_DETAILS ZFM_PAYMENT_DETAILS1) {
            object[] results = this.Invoke("ZFM_PAYMENT_DETAILS", new object[] {
                        ZFM_PAYMENT_DETAILS1});
            return ((ZFM_PAYMENT_DETAILSResponse)(results[0]));
        }
        
        /// <remarks/>
        public void ZFM_PAYMENT_DETAILSAsync(ZFM_PAYMENT_DETAILS ZFM_PAYMENT_DETAILS1) {
            this.ZFM_PAYMENT_DETAILSAsync(ZFM_PAYMENT_DETAILS1, null);
        }
        
        /// <remarks/>
        public void ZFM_PAYMENT_DETAILSAsync(ZFM_PAYMENT_DETAILS ZFM_PAYMENT_DETAILS1, object userState) {
            if ((this.ZFM_PAYMENT_DETAILSOperationCompleted == null)) {
                this.ZFM_PAYMENT_DETAILSOperationCompleted = new System.Threading.SendOrPostCallback(this.OnZFM_PAYMENT_DETAILSOperationCompleted);
            }
            this.InvokeAsync("ZFM_PAYMENT_DETAILS", new object[] {
                        ZFM_PAYMENT_DETAILS1}, this.ZFM_PAYMENT_DETAILSOperationCompleted, userState);
        }
        
        private void OnZFM_PAYMENT_DETAILSOperationCompleted(object arg) {
            if ((this.ZFM_PAYMENT_DETAILSCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.ZFM_PAYMENT_DETAILSCompleted(this, new ZFM_PAYMENT_DETAILSCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.9037.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:sap-com:document:sap:rfc:functions")]
    public partial class ZFM_PAYMENT_DETAILS {
        
        private string iM_DATEField;
        
        private ZSPAYDETAILS[] iT_PAYMENTField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string IM_DATE {
            get {
                return this.iM_DATEField;
            }
            set {
                this.iM_DATEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public ZSPAYDETAILS[] IT_PAYMENT {
            get {
                return this.iT_PAYMENTField;
            }
            set {
                this.iT_PAYMENTField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.9037.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:sap-com:document:sap:rfc:functions")]
    public partial class ZSPAYDETAILS {
        
        private string bELNRField;
        
        private string gJAHRField;
        
        private string bLARTField;
        
        private string bLDATField;
        
        private string bUDATField;
        
        private string xBLNRField;
        
        private string lIFNRField;
        
        private decimal rMWWRField;
        
        private string wAERSField;
        
        private string rBSTATField;
        
        private string kIDNOField;
        
        private string aDDITIONAL1Field;
        
        private string aDDITIONAL2Field;
        
        private string aDDITIONAL3Field;
        
        private string aDDITIONAL4Field;
        
        private string aDDITIONAL5Field;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string BELNR {
            get {
                return this.bELNRField;
            }
            set {
                this.bELNRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string GJAHR {
            get {
                return this.gJAHRField;
            }
            set {
                this.gJAHRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string BLART {
            get {
                return this.bLARTField;
            }
            set {
                this.bLARTField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string BLDAT {
            get {
                return this.bLDATField;
            }
            set {
                this.bLDATField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string BUDAT {
            get {
                return this.bUDATField;
            }
            set {
                this.bUDATField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string XBLNR {
            get {
                return this.xBLNRField;
            }
            set {
                this.xBLNRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string LIFNR {
            get {
                return this.lIFNRField;
            }
            set {
                this.lIFNRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public decimal RMWWR {
            get {
                return this.rMWWRField;
            }
            set {
                this.rMWWRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string WAERS {
            get {
                return this.wAERSField;
            }
            set {
                this.wAERSField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string RBSTAT {
            get {
                return this.rBSTATField;
            }
            set {
                this.rBSTATField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string KIDNO {
            get {
                return this.kIDNOField;
            }
            set {
                this.kIDNOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ADDITIONAL1 {
            get {
                return this.aDDITIONAL1Field;
            }
            set {
                this.aDDITIONAL1Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ADDITIONAL2 {
            get {
                return this.aDDITIONAL2Field;
            }
            set {
                this.aDDITIONAL2Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ADDITIONAL3 {
            get {
                return this.aDDITIONAL3Field;
            }
            set {
                this.aDDITIONAL3Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ADDITIONAL4 {
            get {
                return this.aDDITIONAL4Field;
            }
            set {
                this.aDDITIONAL4Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ADDITIONAL5 {
            get {
                return this.aDDITIONAL5Field;
            }
            set {
                this.aDDITIONAL5Field = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.9037.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:sap-com:document:sap:rfc:functions")]
    public partial class ZFM_PAYMENT_DETAILSResponse {
        
        private string eX_MESSField;
        
        private ZSPAYDETAILS[] iT_PAYMENTField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string EX_MESS {
            get {
                return this.eX_MESSField;
            }
            set {
                this.eX_MESSField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public ZSPAYDETAILS[] IT_PAYMENT {
            get {
                return this.iT_PAYMENTField;
            }
            set {
                this.iT_PAYMENTField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.9037.0")]
    public delegate void ZFM_PAYMENT_DETAILSCompletedEventHandler(object sender, ZFM_PAYMENT_DETAILSCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.9037.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class ZFM_PAYMENT_DETAILSCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal ZFM_PAYMENT_DETAILSCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ZFM_PAYMENT_DETAILSResponse Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ZFM_PAYMENT_DETAILSResponse)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591