﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Models;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMSurrogateBid : IPRMSurrogateBid
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();

        #region Services

        public List<Surrogate> GetSurrogateUsers(int reqid, int vendorid, int deptid, string sessionid, bool onlyvalid = true)
        {
            List<Surrogate> details = new List<Surrogate>();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<Surrogate> mapper = new CORE.DataNamesMapper<Surrogate>();
                string query = string.Empty;
                if (reqid > 0)
                {
                    query = $@"SELECT SB.*, CONCAT(U.U_FNAME, ' ', U.U_LNAME) AS CUSTOMER_NAME, CONCAT(U1.U_FNAME, ' ', U1.U_LNAME) AS VENDOR_NAME FROM SurrogateBidding SB 
                                    INNER JOIN [User] U ON U.U_ID = SB.CUSTOMER_ID
                                    INNER JOIN [User] U1 ON U1.U_ID = SB.VENDOR_ID
                                    WHERE REQ_ID = {reqid}";
                    if (vendorid > 0)
                    {
                        query += $@" AND SB.VENDOR_ID = {vendorid}";
                    }

                    if (onlyvalid)
                    {
                        query += " AND IS_VALID = 1";
                    }
                }

                var dt = sqlHelper.SelectQuery(query);
                details = mapper.Map(dt).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response SaveSurrogateUsers(List<Surrogate> surrogates, int reqid, string comments, int user, string sessionid)
        {

            Response details = new Response();
           
            try
            {
                Utilities.ValidateSession(sessionid);
                if (surrogates != null && surrogates.Count > 0)
                {
                    List<Surrogate> currentSurrogates = GetSurrogateUsers(reqid, surrogates[0].VENDOR_ID, 0, sessionid, false);
                    foreach (var surrogate in surrogates)
                    {
                        string query = "";
                        if (currentSurrogates != null && currentSurrogates.Count > 0 && currentSurrogates.Any(c => c.CUSTOMER_ID == surrogate.CUSTOMER_ID))
                        {
                            query = $@"UPDATE surrogatebidding SET COMMENTS = '{surrogate.COMMENTS}', IS_VALID = {surrogate.IS_VALID} , DATE_MODIFIED = UTC_TIMESTAMP(), MODIFIED_BY = {user} 
                                    WHERE REQ_ID = {reqid} AND VENDOR_ID = {surrogate.VENDOR_ID} AND CUSTOMER_ID = {surrogate.CUSTOMER_ID}";
                        }
                        else
                        {
                            query = $@"INSERT INTO surrogatebidding (REQ_ID, VENDOR_ID, CUSTOMER_ID, COMMENTS, DATE_CREATED, DATE_MODIFIED, CREATED_BY, MODIFIED_BY) VALUES 
                            ({reqid}, {surrogate.VENDOR_ID}, {surrogate.CUSTOMER_ID}, '{surrogate.COMMENTS}', UTC_TIMESTAMP(), UTC_TIMESTAMP(), {user}, {user})";
                        }

                        if (!string.IsNullOrEmpty(query))
                        {
                            sqlHelper.ExecuteQuery(query);
                        }
                    }
                }
                
                if (false)
                {
                    //Utilities.SendEmail(vendor.AltEmail, subject, body, 0, vendor.UserID, "COMMUNICATIONS", sessionID).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }
        #endregion Services

    }

}