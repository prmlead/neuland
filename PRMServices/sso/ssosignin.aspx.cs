﻿using System;
using System.Configuration;
using System.Web;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;

namespace PRMServices
{
    public partial class ssosignin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext.Current.GetOwinContext().Authentication.Challenge(
                   //new AuthenticationProperties { RedirectUri = "https://localhost:44389/sso/prmlogin.aspx" },
                   new AuthenticationProperties { RedirectUri = ConfigurationManager.AppSettings["SSO_SIGNIN"] },
                   OpenIdConnectAuthenticationDefaults.AuthenticationType);
        }
    }
}