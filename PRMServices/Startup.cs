﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Owin;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Newtonsoft.Json;


using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using Microsoft.Owin.Extensions;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.Owin.Security.Notifications;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System.IdentityModel.Claims;
using System.IO;

[assembly: OwinStartup(typeof(PRMServices.SignalR.Startup))]

namespace PRMServices.SignalR
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);
            app.UseCookieAuthentication(new CookieAuthenticationOptions()
            {
                AuthenticationType = "Cookies",
                CookieManager = new Microsoft.Owin.Host.SystemWeb.SystemWebChunkingCookieManager(),
                ExpireTimeSpan = TimeSpan.FromHours(1),
                Provider = new CookieAuthenticationProvider
                {
                    OnResponseSignIn = OnCustomResponseSignIn,
                    OnValidateIdentity = OnMyCustomValidateIdentity
                }
            });
            app.UseOpenIdConnectAuthentication(
                new OpenIdConnectAuthenticationOptions
                {

                    //SignInAsAuthenticationType = "",
                    ClientSecret = ConfigurationManager.AppSettings["SSO_SECRET"],
                    RedirectUri = ConfigurationManager.AppSettings["SSO_SIGNIN"],
                    ClientId = ConfigurationManager.AppSettings["SSO_CLIENTID"],
                    Authority = Path.Combine("https://login.microsoftonline.com/", ConfigurationManager.AppSettings["SSO_AUTH"]),
                    PostLogoutRedirectUri = ConfigurationManager.AppSettings["SSO_LOGOUT"],
                    TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        // If the app needs access to the entire organization, then add the logic
                        // of validating the Issuer here.
                        // IssuerValidator
                    },
                    Notifications = new OpenIdConnectAuthenticationNotifications()
                    {
                        SecurityTokenValidated = (context) =>
                        {
                            // If your authentication logic is based on users
                            return Task.FromResult(0);
                        },
                        AuthenticationFailed = (context) =>
                        {
                            // Pass in the context back to the app
                            context.HandleResponse();
                            // Suppress the exception
                            return Task.FromResult(0);
                        }
                    }
                });

            // This makes any middleware defined above this line run before the Authorization rule is applied in web.config
            app.UseStageMarker(PipelineStage.Authenticate);

            //var config = new HttpConfiguration();
            //config.Routes.MapHubs(new HubConfiguration() { });
            //// For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            // Branch the pipeline here for requests that start with "/signalr"
            /*app.Map("/signalr", map =>
            {
                // Setup the CORS middleware to run before SignalR.
                // By default this will allow all origins. You can 
                // configure the set of origins and/or http verbs by
                // providing a cors options with a different policy.
                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration()
                {
                    EnableDetailedErrors = true,
                    EnableJSONP = true,
                    EnableJavaScriptProxies = true
                };
                // Run the SignalR pipeline. We're not using MapSignalR
                // since this branch already runs under the "/signalr"
                // path.
                map.RunSignalR(hubConfiguration);
            });*/
            //app.MapSignalR("/~/signalr", new HubConfiguration()
            //{
            //    EnableDetailedErrors = true,
            //    EnableJSONP = true
            //});

            app.Map("/~/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration()
                {
                    EnableDetailedErrors = true,
                    EnableJSONP = true
                };
                map.RunSignalR(hubConfiguration);
            });

            var jsonSerializer = new JsonSerializer();
            jsonSerializer.DateFormatHandling = DateFormatHandling.MicrosoftDateFormat;
            jsonSerializer.DateTimeZoneHandling = DateTimeZoneHandling.Local;
            jsonSerializer.NullValueHandling = NullValueHandling.Ignore;
            GlobalHost.DependencyResolver.Register(typeof(JsonSerializer), () => jsonSerializer);



        }

        private void OnCustomResponseSignIn(CookieResponseSignInContext context)
        {
            //context.Properties.AllowRefresh = true;
            //context.Properties.ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(2);

            var ticks = context.Options.SystemClock.UtcNow.AddHours(10).UtcTicks;
            context.Properties.Dictionary.Add("absolute", ticks.ToString());
        }

        private Task OnMyCustomValidateIdentity(CookieValidateIdentityContext context)
        {
            bool reject = true;
            string value;
            if (context.Properties.Dictionary.TryGetValue("absolute", out value))
            {
                long ticks;
                if (Int64.TryParse(value, out ticks))
                {
                    reject = context.Options.SystemClock.UtcNow.UtcTicks > ticks;
                }
            }
            if (reject)
            {
                context.RejectIdentity();
                // optionally clear cookie
                //ctx.OwinContext.Authentication.SignOut(ctx.Options.AuthenticationType);
            }

            return Task.FromResult(0);
        }
    }
}
