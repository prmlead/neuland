﻿
prmApp
    .controller('reqFwdVendorListCtrl', function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService,
        fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, PRMFwdReqServiceService) {

        var loginUserData = userService.getUserObj();

        $scope.isOTPVerified = loginUserData.isOTPVerified;
        $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;


        $scope.formRequest = {};
        $scope.formRequest.isForwardBidding = false;
        $scope.myActiveLeads = [];
        $scope.currentPage = 1;
        $scope.currentPage2 = 1;
        $scope.itemsPerPage = 8;
        $scope.itemsPerPage2 = 8;
        $scope.maxSize = 8;
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            
        };
        $scope.quotationAttachment = null;

        $scope.maxSize = 8;
        $scope.totalLeads = 0;
        $scope.itemsPerPage = 8;
        $scope.myAuctionsLoaded = false;
        $scope.myAuctions = [];
        $scope.myActiveLeads = [];
        $scope.myAuctionsMessage = '';

        $scope.getAuctions = function () {
            PRMFwdReqServiceService.getmyAuctions({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.myAuctions = response;
                        if ($scope.myAuctions.length > 0) {
                            $scope.myAuctionsLoaded = true;
                            $scope.totalItems = $scope.myAuctions.length;
                        } else {
                            $scope.myAuctionsLoaded = false;
                            $scope.totalItems = 0;
                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                        }
                    });
        };

        $scope.GetRequirementsReport = function () {

            auctionsService.getrequirementsreport({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.reqReport = response;

                    //alasql('SELECT requirementID as [RequirementID],title as [Title],vendorID as [VendorID],companyName as [Company],userPhone as [Phone],userEmail as [SEmail],itemID as [ItemID],productBrand as [Brand],productQuantity as [Quantity], productQuantityIn as [Units],costPrice as [InitialCostPrice],revCostPrice as [FinalCostPrice],netPrice as [NetPrice],marginAmount as [InitialMarginAmount],unitDiscount as [InitialMargin],revUnitDiscount as [FinalMargin],unitMRP as [MRP],gst as [GST],maxInitMargin as [MaximumInitialMargin],maxFinalMargin as [MaximumFinalMargin],savings as [Savings] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xslx", $scope.reqReport]);
                    alasql.fn.handleDate = function (date) {
                        return new moment(date).format("MM/DD/YYYY");
                    }
                    alasql('SELECT requirementID as RequirementID, title as RequirementTitle, handleDate(postedDate) as [PostedDate], status as Status, vendorID as VendorID, companyName as CompanyName, itemID as [ItemID], productBrand as Brand, productQuantity as Quantity, productQuantityIn as [Units], costPrice as [InitialCostPrice], revCostPrice as [FinalCostPrice], netPrice as [InitialNetPrice], marginAmount as [InitialMarginAmount], unitDiscount as [MarginPercentage],revUnitDiscount as [RevisedMarginPercentage],unitMRP as [MRP],gst as [GST] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xlsx", $scope.reqReport]);

                });


        }

        $scope.getLeads = function () {


            PRMFwdReqServiceService.getactiveleads({ "userid": userService.getUserId(), "page": ($scope.currentPage - 1) * $scope.itemsPerPage, "limit": $scope.itemsPerPage, "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.myActiveLeads1 = response;
                    $scope.myActiveLeads = response;
                    
                    if ($scope.myActiveLeads.length > 0) {
                        $scope.myAuctionsLoaded = true;
                        $scope.totalLeads = $scope.myActiveLeads.length;
                    } else {
                        $scope.totalLeads = 0;
                        $scope.myAuctionsLoaded = false;
                        $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                    }
                });
        }

        if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
            $scope.getLeads();
        }

        $scope.downloadTemplate = function (name) {
            reportingService.downloadTemplate(name, userService.getUserId(), 0);
        };

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {

                    if (id == "excelquotation") {
                        var bytearray = new Uint8Array(result);
                        $scope.quotationAttachment = $.makeArray(bytearray);
                        $scope.uploadquotationsfromexcel();
                    }
                });
        }

        $scope.uploadquotationsfromexcel = function (status) {
            var params = {
                reqID: 0,
                userID: userService.getUserId(),
                sessionID: userService.getUserToken(),
                quotationAttachment: $scope.quotationAttachment
            };
            auctionsService.uploadquotationsfromexcel(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        swal("Success", "Uploaded Successfully!", 'success');
                        location.reload();
                    } else {
                        swal("Error", response.errorMessage, 'error');
                    }

                })
        }
    });
