@echo off
set /p ENV="Enter ENV:-"
@echo Entered EVN IS:- %ENV%
if /I "%ENV%"=="DEV" (
	@echo DEV DEPLOY START
	echo user prmftpuser> ftpcmd.dat
	echo Wegr0w360@c@ds>> ftpcmd.dat
	echo cd httpdocs>> ftpcmd.dat
	echo cd WEBDEV>> ftpcmd.dat
	echo put dist\prm360.html>> ftpcmd.dat
	echo cd dist>> ftpcmd.dat
	echo prompt>> ftpcmd.dat
	echo mdelete *>> ftpcmd.dat
	echo bin>> ftpcmd.dat
	echo mput dist\*.js>> ftpcmd.dat
	echo mput dist\*.css>> ftpcmd.dat
	echo cd ..>> ftpcmd.dat
	echo cd views>> ftpcmd.dat
	echo mput views\*.html>> ftpcmd.dat
	echo cd ..>> ftpcmd.dat
	echo cd bin>> ftpcmd.dat

	echo quit>> ftpcmd.dat
	ftp -n -s:ftpcmd.dat 182.18.169.32
	del ftpcmd.dat
	@echo DEV DEPLOY DONE
)

if /I "%ENV%"=="QA" (
	echo user prmftpuser> ftpcmd.dat
	echo Wegr0w360@c@ds>> ftpcmd.dat
	echo cd httpdocs>> ftpcmd.dat
	echo cd WEBQA>> ftpcmd.dat
	echo put dist\prm360.html>> ftpcmd.dat
	echo cd dist>> ftpcmd.dat
	echo prompt>> ftpcmd.dat
	echo mdelete *>> ftpcmd.dat
	echo bin>> ftpcmd.dat
	echo mput dist\*.js>> ftpcmd.dat
	echo mput dist\*.css>> ftpcmd.dat

	echo quit>> ftpcmd.dat
	ftp -n -s:ftpcmd.dat 182.18.169.32
	del ftpcmd.dat
)

if /I "%ENV%"=="PROD" (
	echo user prmftpuser> ftpcmd.dat
	echo Wegr0w360@c@ds>> ftpcmd.dat
	echo cd httpdocs>> ftpcmd.dat
	echo cd WEB>> ftpcmd.dat
	echo put dist\prm360.html>> ftpcmd.dat
	echo cd dist>> ftpcmd.dat
	echo prompt>> ftpcmd.dat
	echo mdelete *>> ftpcmd.dat
	echo bin>> ftpcmd.dat
	echo mput dist\*.js>> ftpcmd.dat
	echo mput dist\*.css>> ftpcmd.dat

	echo quit>> ftpcmd.dat
	ftp -n -s:ftpcmd.dat 182.18.169.32
	del ftpcmd.dat
)