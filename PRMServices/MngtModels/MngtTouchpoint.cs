﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class MngtTouchpoint : Entity
    {
        [DataMember(Name="touchpoinID")]
        public int TouchpoinID { get; set; }

        [DataMember(Name = "companyID")]
        public int CompanyID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "touchpoinType")]
        public string TouchpoinType { get; set; }

        [DataMember(Name = "contactName")]
        public string ContactName { get; set; }

        [DataMember(Name = "contactDesignation")]
        public string ContactDesignation { get; set; }

        [DataMember(Name = "contactPhoneEmail")]
        public string ContactPhoneEmail { get; set; }

        [DataMember(Name = "touchpointDate")]
        public DateTime? TouchpointDate { get; set; }

        [DataMember(Name = "spentTime")]
        public string SpentTime { get; set; }

        [DataMember(Name = "outCome")]
        public string OutCome { get; set; }

        [DataMember(Name = "comments")]
        public string Comments { get; set; }


        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "meetingLocation")]
        public string meetingLocation { get; set; }

        [DataMember(Name = "marketingUser")]
        public int marketingUser { get; set; }

        [DataMember(Name = "marketingUserName")]
        public string marketingUserName { get; set; }

        [DataMember(Name = "companyWebsite")]
        public string CompanyWebsite { get; set; }

    }
}