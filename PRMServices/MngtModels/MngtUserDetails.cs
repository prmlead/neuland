﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class MngtUserDetails :  Entity
    {

        [DataMember(Name = "userCredentials")]
        public List<Credentials> UserCredentials { get; set; }

        [DataMember(Name = "userDashboardStats")]
        public DashboardStats UserDashboardStats { get; set; }

        [DataMember(Name = "userListRequirement")]
        public List<Requirement> UserListRequirement { get; set; }

        [DataMember(Name = "userId")]
        public int UserId { get; set; }     

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "emailId")]
        public string EmailId { get; set; }

        [DataMember(Name = "createdDate")]
        public DateTime? CreatedDate { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "isSuperUser")]
        public bool IsSuperUser { get; set; }

        [DataMember(Name = "superUserId")]
        public int SuperUserId { get; set; }

        [DataMember(Name = "isMobileVerified")]
        public bool IsMobileVerified { get; set; }

        [DataMember(Name = "isEmailVerified")]
        public bool IsEmailVerified { get; set; }

        [DataMember(Name = "isDocsVerified")]
        public bool IsDocsVerified { get; set; }

        [DataMember(Name = "companyID")]
        public int CompanyID { get; set; }

        [DataMember(Name = "companycontactID")]
        public int CompanyContactID { get; set; }

        [DataMember(Name = "contactDesignation")]
        public string ContactDesignation { get; set; }

        [DataMember(Name = "meetingsSetToday")]
        public int MeetingsSetToday { get; set; }

        [DataMember(Name = "callsMadeToday")]
        public int CallsMadeToday { get; set; }

        [DataMember(Name = "companyPhone")]
        public string CompanyPhone { get; set; }

        [DataMember(Name = "companyTurnover")]
        public string CompanyTurnover { get; set; }

        [DataMember(Name = "companyIndustry")]
        public string CompanyIndustry { get; set; }

        [DataMember(Name = "mtngSetCompanyList")]
        public List<MngtCompanies> MtngSetCompanyList { get; set; }

        [DataMember(Name = "followUpCompanyList")]
        public List<MngtCompanies> FollowUpCompanyList { get; set; }





        [DataMember(Name = "meetingsCount")]
        public int MeetingsCount { get; set; }

        [DataMember(Name = "positives")]
        public int Positives { get; set; }

        [DataMember(Name = "followUps")]
        public int FollowUps { get; set; }

        [DataMember(Name = "trail")]
        public int Trail { get; set; }

        [DataMember(Name = "closure")]
        public int Closure { get; set; }
    }
}