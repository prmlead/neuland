@echo off
except vendor.CACHE.js prm360.min.CACHE.css del /q "..\dist\*"
REM DEL ..\dist\fonts /q
for /f "tokens=1-4 delims=/ " %%i in ("%date%") do (
     set dow=%%i
     set month=%%j
     set day=%%k
     set year=%%l
)

for /f "tokens=1-3 delims=: " %%i in ("%time%") do (
     set H=%%i
     set M=%%j
     set S=%%k
)

for /f "tokens=1-2 delims=. " %%i in ("%S%") do (
     set S1=%%i
     set T=%%j
)

set datestr=%year%%month%%day%
set mytime=%H%%M%%S1%
set cachetoken=%datestr%%mytime%
echo newtoken is %cachetoken%

COPY app.js ..\dist\app.CACHE.js /y /b
COPY config.js ..\dist\config.CACHE.js /y /b
COPY modulesRouting.js ..\dist\modulesRouting.CACHE.js /y /b
COPY catalogMgmtRouting.js ..\dist\catalogMgmtRouting.CACHE.js /y /b
COPY cijIndentRouting.js ..\dist\cijIndentRouting.CACHE.js /y /b
COPY templates.js ..\dist\templates.CACHE.js /y /b
COPY custom.js ..\dist\custom.CACHE.js /y /b
COPY user\user.controller.js ..\dist\user.controller.CACHE.js /y /b
copy controllers\*.js+controllers\storecontrollers\*.js+controllers\techevalcontrollers\*.js+controllers\reportingcontrollers\*.js+controllers\pocontrollers\*.js+controllers\fwdrequirement\*.js+controllers\workflows\*.js+controllers\requestIndent\*.js+controllers\profilecontrollers\*.js+controllers\auction\*.js+controllers\common\*.js+controllers\settings\*.js+controllers\postpo\*.js+controllers\logistics\*.js+controllers\APMC\*.js+controllers\configuration\* +controllers\realtimeprice\* +controllers\catalogMgnt\*.js+controllers\pr\*.js+controllers\CounterBid\*.js+controllers\CatalogReports\*.js ..\dist\Controllers.CACHE.js /y /b
copy modules\*.js ..\dist\prmModules.CACHE.js /y /b
copy services\*Services.js ..\dist\prmServices.CACHE.js /y /b

COPY ..\json-requirement\js\routing\*.js + ..\json-requirement\js\controllers\*.js + ..\json-requirement\js\services\*.js ..\dist\json-requirement.CACHE.js /y /b

COPY ..\forward\js\routing\*.js + ..\forward\js\controllers\*.js + ..\forward\js\services\*.js ..\dist\forward.CACHE.js /y /b

COPY ..\audit\js\routing\*.js + ..\audit\js\controllers\*.js + ..\audit\js\services\*.js ..\dist\audit.CACHE.js /y /b

COPY ..\pr\js\routing\*.js + ..\pr\js\controllers\*.js + ..\pr\js\services\*.js ..\dist\pr.CACHE.js /y /b

COPY ..\chat\js\routing\*.js + ..\chat\js\controllers\*.js + ..\chat\js\services\*.js ..\dist\chat.CACHE.js /y /b

COPY ..\lot-bidding\js\routing\*.js + ..\lot-bidding\js\controllers\*.js + ..\lot-bidding\js\services\*.js ..\dist\lot-bidding.CACHE.js /y /b

COPY ..\tender\js\routing\*.js + ..\tender\js\controllers\*.js + ..\tender\js\services\*.js ..\dist\tender.CACHE.js /y /b

COPY ..\communication\js\routing\*.js + ..\communication\js\controllers\*.js + ..\communication\js\services\*.js ..\dist\communication.CACHE.js /y /b

COPY ..\custom-fields\js\routing\*.js + ..\custom-fields\js\controllers\*.js + ..\custom-fields\js\services\*.js ..\dist\custom-fields.CACHE.js /y /b

COPY ..\surrogate-bidding\js\routing\*.js + ..\surrogate-bidding\js\controllers\*.js + ..\surrogate-bidding\js\services\*.js ..\dist\surrogate-bidding.CACHE.js /y /b

COPY ..\Reports\js\routing\*.js + ..\Reports\js\controllers\*.js + ..\Reports\js\services\*.js ..\dist\Reports.CACHE.js /y /b

COPY ..\po\js\routing\*.js + ..\po\js\controllers\*.js + ..\po\js\services\*.js ..\dist\po.CACHE.js /y /b

COPY ..\PendingPO\js\routing\*.js + ..\PendingPO\js\controllers\*.js + ..\PendingPO\js\services\*.js ..\dist\PendingPO.CACHE.js /y /b

COPY ..\GRN\js\routing\*.js + ..\GRN\js\controllers\*.js + ..\GRN\js\services\*.js ..\dist\GRN.CACHE.js /y /b

xcopy resources\fonts ..\fonts /E /y
xcopy resources\css\icons\material-design-iconic-font\fonts ..\fonts /E /y
REM copy controllers\profileCtrl.js ..\dist\profileCtrl.CACHE.js /y /b

copy controllers\auction\itemCtrl.js ..\dist\itemCtrl.CACHE.js /y /b

copy controllers\common\loginCtrl.js ..\dist\loginCtrl.CACHE.js /y /b

COPY ..\prpo\js\routing\*.js + ..\prpo\js\controllers\*.js + ..\prpo\js\services\*.js ..\dist\prpo.CACHE.js /y /b
REM copy ..\css.min\prm360.min.css ..\dist\prm360.min.CACHE.css /y /b

copy ..\prm360.html ..\dist\prm360.html /y /b