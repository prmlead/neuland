var prmApp = angular.module("prmApp", [
    'prm.user',
	'ngAnimate',
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
    'oc.lazyLoad',
    'nouislider',
    'ngTable',
    'timer',
    'SignalR',
    'ngSanitize',
    'angularjs-datetime-picker',
    'angular-input-stars',
    'moment-picker',
    "isteven-multi-select",
    'ngDialog', 'ui.sortable', "ngMessages", "ngFileUpload", 'smart-table', 'doubleScrollBars', 'ui.tree', 'TreeWidget',
    'angular.filter', 'ui.multiselect', 'ngPatternRestrict', 'ngCookies'])
prmApp.directive('xdStSearch', ['stConfig', '$timeout', function (stConfig, $timeout) {
    return {
        require: { table: '^stTable', model: 'ngModel' },
        link: function (scope, element, attr, ctrl) {
            var tableCtrl = ctrl.table;
            var promise = null;
            var throttle = attr.stDelay || stConfig.search.delay;
            var event = attr.stInputEvent || stConfig.search.inputEvent;
            var trimSearch = attr.trimSearch || stConfig.search.trimSearch;

            attr.$observe('xdStSearch', function (newValue, oldValue) {
                var input = ctrl.model.$viewValue;
                if (newValue !== oldValue && input) {
                    tableCtrl.tableState().search = {};
                    input = angular.isString(input) && trimSearch ? input.trim() : input;
                    tableCtrl.search(input, newValue);
                }
            });

            // view -> table state
            ctrl.model.$parsers.push(throttleSearch);
            ctrl.model.$formatters.push(throttleSearch)

            function throttleSearch(value) {
                if (promise !== null) {
                    $timeout.cancel(promise);
                }
                promise = $timeout(function () {
                    var input = angular.isString(value) && trimSearch ? value.trim() : value;
                    tableCtrl.search(input, attr.xdStSearch || '');
                    promise = null;
                }, throttle);
                return value;
            }
        }
    };
}])

prmApp.directive('stFilteredCollection', function () {
    return {
        require: '^stTable',
        link: function (scope, element, attr, ctrl) {
            scope.$watch(ctrl.getFilteredCollection, function (val) {
                scope.filteredCollection = val;
            })
        }
    }
});

/*
app.run(function($rootScope, $templateCache) {
   $rootScope.$on('$viewContentLoaded', function() {
      $templateCache.removeAll();
    });
});*/
