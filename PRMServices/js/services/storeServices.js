prmApp

    .service('storeService', ["store", "storeDomain", "userService", "httpServices", function (store, storeDomain, userService, httpServices) {
        //var domain = 'http://182.18.169.32/services/';
        var storeService = this;
        
        storeService.savestore = function (params) {
            let url = storeDomain + 'savestore';
            return httpServices.post(url, params);
        };

        storeService.deletestore = function (params) {
            let url = storeDomain + 'deletestore';
            return httpServices.post(url, params);
        };

        storeService.deleteStoreItemDetails = function (params) {
            let url = storeDomain + 'deletestoreitemdetails';
            return httpServices.post(url, params);
        };

        storeService.getcompanystores = function (compID, parentStoreID) {
            let url = storeDomain + 'companystores?compid=' + compID + '&parentstoreid=' + parentStoreID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.getstores = function (storeID, compID) {
            let url = storeDomain + 'stores?storeid=' + storeID + '&compid' + compID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.savestoreitem = function (params) {
            let url = storeDomain + 'savestoreitem';
            return httpServices.post(url, params);
        };

        storeService.deletestoreitem = function (params) {
            let url = storeDomain + 'deletestoreitem';
            return httpServices.post(url, params);
        };

        storeService.getuniqueitems = function (compID, itemName) {
            let url = storeDomain + 'uniqueitems?compid=' + compID + '&itemname=' + itemName + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.getcompanystoreitems = function (storeID, itemID) {
            let url = storeDomain + 'storeitems?storeid=' + storeID + '&itemid=' + itemID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.importentity = function (params) {
            let url = storeDomain + 'importentity';
            return httpServices.post(url, params);
        };
        
        storeService.storesitempropvalues = function (itemID) {
            let url = storeDomain + 'companypropvalues?itemid=' + itemID + '&module=STORE&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.savecompanyprop = function (params) {
            let url = storeDomain + 'savecompanyprop';
            return httpServices.post(url, params);
        };

        storeService.savecompanypropvalue = function (params) {
            let url = storeDomain + 'savecompanypropvalue';
            return httpServices.post(url, params);
        };

        storeService.companypropvalues = function (itemID) {
            let url = storeDomain + 'companypropvalues?entityid=' + itemID + '&module=STORE&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.getStoreItemDetails = function (itemID, notDispatched) {
            let url = storeDomain + 'getstoreitemdetails?itemid=' + itemID + '&notdispatched=' + notDispatched + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.saveStoreItemDetails = function (params) {
            let url = storeDomain + 'savestoreitemdetails';
            return httpServices.post(url, params);
        };

        storeService.saveStoreItemGIN = function (params) {
            let url = storeDomain + 'savestoreitemissue';
            return httpServices.post(url, params);
        };

        storeService.saveStoreItemGRN = function (params) {
            let url = storeDomain + 'savestoreitemreceive';
            return httpServices.post(url, params);
        };

        storeService.getStoreItemGIN = function (ginCode, storeID) {
            let url = storeDomain + 'storeitemissuedetails?gincode=' + ginCode + '&storeid=' + storeID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.getStoreItemGRN = function (grnCode, storeID) {
            let url = storeDomain + 'storeitemreceivedetails?grncode=' + grnCode + '&storeid=' + storeID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.getIndentItemDetails = function (storeID, ginCode, grnCode) {
            let url = storeDomain + 'indentitemdetails?storeid=' + storeID + '&gincode=' + ginCode + '&grncode=' + grnCode + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };
        
        return storeService;
    }]);