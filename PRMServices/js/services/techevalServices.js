prmApp
    .service('techevalService', ["techevalDomain", "userService", "httpServices",
        function (techevalDomain, userService, httpServices) {
            //var domain = 'http://182.18.169.32/services/';
            var techevalService = this;

            techevalService.getquestionnairelist = function (evalID) {
                var url = techevalDomain + 'getquestionnairelist?userid=' + userService.getUserId() + '&evalid=' + evalID + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.savequestionnaire = function (params) {
                let url = techevalDomain + 'savequestionnaire';
                return httpServices.post(url, params);
            };

            techevalService.assignquestionnaire = function (params) {
                let url = techevalDomain + 'assignquestionnaire';
                return httpServices.post(url, params);
            };

            techevalService.savequestion = function (params) {
                let url = techevalDomain + 'savequestion';
                return httpServices.post(url, params);
            };

            techevalService.deletequestion = function (params) {
                let url = techevalDomain + 'deletequestion';
                return httpServices.post(url, params);
            };

            techevalService.importentity = function (params) {
                let url = techevalDomain + 'importentity';
                return httpServices.post(url, params);
            };

            techevalService.getquestionnaire = function (evalid, loadquestions) {
                var url = techevalDomain + 'getquestionnaire?evalid=' + evalid + '&loadquestions=' + loadquestions + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.getreqquestionnaire = function (reqid, loadquestions) {
                var url = techevalDomain + 'gettechevalforrequirement?reqid=' + reqid + '&loadquestions=' + loadquestions + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.saveresponse = function (params) {
                let url = techevalDomain + 'saveresponse';
                return httpServices.post(url, params);
            };

            techevalService.saveTechApproval = function (params) {
                let url = techevalDomain + 'saveresponse';
                return httpServices.post(url, params);
            };

            techevalService.getresponses = function (reqid, evalid, userid) {
                var url = techevalDomain + 'getresponses?evalid=' + evalid + '&userid=' + userid + '&reqid=' + reqid + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.GetTechEvalution = function (evalid, reqid, userid) {
                let url = techevalDomain + 'gettechevalution?evalid=' + evalid + '&reqid=' + reqid + '&userid=' + userid + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.GetVendorsForTechEval = function (evalid) {
                let url = techevalDomain + 'getvendorsfortecheval?evalid=' + evalid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.saveTechApproval = function (params) {
                let url = techevalDomain + 'savetechevaluation';
                return httpServices.post(url, params);
            };

            techevalService.saveVendorsForQuestionnaire = function (params) {
                let url = techevalDomain + 'savevendorforquestionnaire';
                return httpServices.post(url, params);
            };

            techevalService.deletequestionnaire = function (params) {
                let url = techevalDomain + 'deletequestionnaire';
                return httpServices.post(url, params);
            };

            techevalService.sendquestionnairetovendors = function (params) {
                let url = techevalDomain + 'sendquestionnairetovendors';
                return httpServices.post(url, params);
            };

            return techevalService;
        }]);