﻿prmApp
    .controller('paymentTrackCtrl', ["$scope", "$state", "$stateParams", "$window", "userService", "growlService", "fileReader", "$log", "poService",
        function ($scope, $state, $stateParams, $window, userService, growlService, fileReader, $log, poService) {
            $scope.reqID = $stateParams.reqID;
            $scope.vendorID = $stateParams.vendorID;
            $scope.poID = $stateParams.poID;

            $scope.paymentTrack = {};


            $scope.sessionID = userService.getUserToken();
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;


            $scope.totalPrice = 0;


            $scope.GetPaymentTrack = function () {
                poService.GetPaymentTrack($scope.vendorID, $scope.poID)
                    .then(function (response) {
                        $scope.paymentTrack = response;

                        if ($scope.paymentTrack) {
                            $scope.paymentTrack.dispatchObject.forEach(function (item, index) {

                                $scope.totalPrice += item.receivedQtyPrice;

                                item.dispatchDate = new moment(item.dispatchDate).format("DD-MM-YYYY");
                                item.recivedDate = new moment(item.recivedDate).format("DD-MM-YYYY");
                                if (item.dispatchDate.indexOf('-9999') > -1) {
                                    item.dispatchDate = "";
                                }
                                if (item.recivedDate.indexOf('-9999') > -1) {
                                    item.recivedDate = "";
                                }
                            });

                            $scope.paymentTrack.paymentInfoObject.forEach(function (item, index) {
                                item.paymentDate = new moment(item.paymentDate).format("DD-MM-YYYY");
                                if (item.paymentDate.indexOf('-9999') > -1) {
                                    item.paymentDate = "";
                                }
                            });
                        }
                    });
            }

            $scope.GetPaymentTrack();

            $scope.paymentObj = {
                sessionID: $scope.sessionID,
                poID: $scope.poID,
                pdID: 0,

            };


            $scope.SavePaymentInfo = function (paymentObj) {

                paymentObj.sessionID = $scope.sessionID;
                paymentObj.poID = $scope.poID;


                var ts = moment(paymentObj.paymentDate, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                paymentObj.paymentDate = "/Date(" + milliseconds + "000+0530)/";


                params = {
                    paymentinfo: paymentObj
                }

                poService.SavePaymentInfo(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            location.reload();
                        }
                    });
            }

            $scope.editPayment = function (data, edit, isCustomer) {

                if (isCustomer == true) {
                    data.isACK = false;
                } else if (isCustomer == false) {
                    data.isACK = true;
                }

                if (edit == 0 && $scope.paymentObj.pdID > 0) {
                    $scope.paymentObj = {
                        sessionID: $scope.sessionID,
                        poID: $scope.poID,
                        pdID: 0,

                    };
                }

                if (edit == 1) {
                    $scope.paymentObj = data;
                    
                }

            }



            $scope.cancelPO = function () {
                $state.go("po-list", { "reqID": $stateParams.reqID, "vendorID": $stateParams.vendorID, "poID": 0 });
            }


        }]);