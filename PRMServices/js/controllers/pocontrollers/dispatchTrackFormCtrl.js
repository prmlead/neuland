prmApp
    .controller('dispatchTrackFormCtrl', ["$scope", "$state", "$window", "$stateParams", "userService", "auctionsService", "fileReader", "poService", "PRMPOService","$http",
        function ($scope, $state, $window, $stateParams, userService, auctionsService, fileReader, poService, PRMPOService, $http) {
            $scope.reqID = $stateParams.reqID;
            $scope.poOrderId = $stateParams.poOrderId;
            $scope.dCode = $stateParams.dCode;
            $scope.vendorCode = $stateParams.vendorCode;
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.sessionID = userService.getUserToken();
            $scope.companyId = userService.getUserCompanyId();
            $scope.userID = userService.getUserId();
            $scope.currentUserId = userService.getUserId();
            $scope.dispatchTrackObject = {};
            $scope.vendors = [];

            $scope.dtobject = [];

            $scope.dispatchFile = 0;
            $scope.recivedFile = 0;
            $scope.totalAttachmentMaxSize = 6291456;
            $scope.totalASNSize = 0;
            $scope.totalASNItemSize = 0;
            $scope.asnForm = false;
            $scope.serviceForm = false;
            $scope.CUSTOMER_BATCH = 'ASN';
         //   $scope.customerBatch = 'ASN';
            //$scope.dispatchObject.CUSTOMER_BATCH = '';

            $scope.setMandatoryFields = function (type) {
                if (type == 'Actual') {
                    $scope.mandatoryField = true;
                } else {
                    $scope.mandatoryField = false;
                }

            }

            $scope.getCustomerBatch = function (type) {
                if (type == 'ASN') {
                    $scope.asnForm = true
                    $scope.serviceForm = false

                } else {
                    $scope.serviceForm = true
                    $scope.asnForm = false

                }
                if (!$scope.isCustomer && $scope.dCode == 0) {
                    var params1 = {
                        "ponumber": $scope.poOrderId,
                        "moredetails": 0,
                        'forasn': true
                    };
                    PRMPOService.getPOScheduleItems(params1)
                        .then(function (response) {
                            //response = response.filter(function (obj) {
                            //    return (obj.VENDOR_ACK_STATUS == "EDIT") || (obj.VENDOR_ACK_STATUS== 'ACKNOWLEDGE');
                            //});
                            $scope.dispatchObject = response;
                            //When we edit PO Line Item and save we need to create asn for ACK_QTY
                            //Added same in DB for REMAINING_QTY = ACK_QTY
                            $scope.dispatchObject.forEach(function (item) {
                                item.ORDER_QTY = item.ACK_QTY;
                                //item.NET_WT = item.ACK_QTY;
                                item.REQUESTED_DELIVERY_DATE_TEMP = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            })
                            if ($scope.dCode == 0 && $scope.dispatchObject.length>0) {
                                $scope.dispatchObject[0].SHIP_TO_LOCATION = '';
                                $scope.dispatchObject[0].SHIP_FROM_LOCATION = '';
                                $scope.dispatchObject[0].DELIVERY_DATE_TEMP = moment().format('DD-MM-YYYY');
                                $scope.dispatchObject[0].SHIPMENT_DATE_TEMP = moment().format('DD-MM-YYYY');
                                //$scope.dispatchObject[0].REQUESTED_DELIVERY_DATE_TEMP =  moment().format('DD-MM-YYYY');
                                $scope.dispatchObject[0].TRACKING_DATE_TEMP = moment().format('DD-MM-YYYY');
                                $scope.mandatoryField = false;
                            }
                            if ($scope.dispatchObject.length == 0 && $scope.dCode == 0) {
                                $scope.asnForm = false;
                                swal("Warning!", "There are no line items available for Acknowledgement.", "warning");
                            }
                            if ($scope.dispatchObject.length > 0) {
                                var params = {
                                    "compid": $scope.isCustomer ? $scope.compID : 0,
                                    "uid": $scope.isCustomer ? 0 : +$scope.userID,
                                    "search": $scope.poOrderId,
                                    "categoryid": '',
                                    "productid": '',
                                    "supplier": '',
                                    "postatus": '',
                                    "deliverystatus": '',
                                    "plant": '',
                                    "fromdate": '1970-01-01',
                                    "todate": '2100-01-01',
                                    "page": 0,
                                    "pagesize": 10,
                                    "ackStatus": '',
                                    "buyer": '',
                                    "purchaseGroup": '',
                                    "sessionid": userService.getUserToken()

                                };
                                PRMPOService.getPOScheduleList(params)
                                    .then(function (response) {
                                        $scope.dispatchObject[0].INCOTERM = response[0].INCO_TERMS;
                                    });

                            }
                        });

                    
                }
            }

            $scope.getDispatchTrack = function () {
               
                $scope.params = {
                    "compid": $scope.isCustomer ? $scope.companyId : 0,
                    "asnid": 0,
                    "ponumber": $scope.poOrderId,
                    "grncode": 0,
                    "asncode": $scope.dCode ? $scope.dCode : 0,
                    "vendorid": $scope.isCustomer ? 0 : $scope.currentUserId,
                    "senssionid": userService.getUserToken()
                };

                PRMPOService.getASNDetails($scope.params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            if ($scope.dCode == 0) {
                                response = response.filter(function (obj) {
                                    return (obj.REMAINING_NET_QTY !== 0);
                                });
                            } 
                            $scope.dispatchObject = response;
                            $scope.dispatchObject.forEach(function (item) {
                                item.isErrorWeightValidation = false;
                                item.isErrorGrossWeightValidation = false;
                                item.isErrorWeightValidationRemaining = false;
                                item.REQUESTED_DELIVERY_DATE_TEMP = userService.toLocalDateOnly(item.REQUESTED_DELIVERY_DATE);
                            })
                            if ($scope.dispatchObject.length > 0) {
                                $scope.dispatchObject[0].SHIP_TO_LOCATION = response[0].SHIP_TO_LOCATION;
                                $scope.dispatchObject[0].SHIP_FROM_LOCATION = response[0].SHIP_FROM_LOCATION;
                                $scope.CUSTOMER_BATCH = response[0].CUSTOMER_BATCH;
                                $scope.dispatchObject[0].DELIVERY_DATE_TEMP = userService.toLocalDateOnly($scope.dispatchObject[0].DELIVERY_DATE);
                                $scope.dispatchObject[0].SHIPMENT_DATE_TEMP = userService.toLocalDateOnly($scope.dispatchObject[0].SHIPMENT_DATE);
                                $scope.dispatchObject[0].TRACKING_DATE_TEMP = userService.toLocalDateOnly($scope.dispatchObject[0].SHIPMENT_DATE);
                                //if get ASN data having Actual or Estimate Notice Type
                                if (response[0].SHIP_NOTICE_TYPE == 'Actual') {
                                    $scope.mandatoryField = true;
                                } else {
                                    $scope.mandatoryField = false;
                                }
                            }
                            if ($scope.isCustomer) {
                                //$scope.dispatchObject.DOCUMENT_DATE = userService.toLocalDateOnly($scope.dispatchObject.DOCUMENT_DATE);
                                //$scope.dispatchObject[0].MANUFACTURED_DATE = userService.toLocalDateOnly($scope.dispatchObject[0].MANUFACTURED_DATE);
                                //$scope.dispatchObject[0].BEST_BEFORE_DATE = userService.toLocalDateOnly($scope.dispatchObject[0].BEST_BEFORE_DATE);
                                //$scope.dispatchObject[0].RECEIVED_DATE = userService.toLocalDateOnly($scope.dispatchObject[0].RECEIVED_DATE);
                            } else {
                                //$scope.dispatchObject[0].REQUESTED_DELIVERY_DATE_TEMP = userService.toLocalDateOnly($scope.dispatchObject[0].REQUESTED_DELIVERY_DATE);
                                //$scope.dispatchObject[0].TRACKING_DATE_TEMP = userService.toLocalDateOnly($scope.dispatchObject[0].SHIPMENT_DATE);

                            }
                           


                        }
                        $scope.getCustomerBatch($scope.CUSTOMER_BATCH);

                    });
            };



            $scope.getDispatchTrack();

            $scope.formatDate = function (date) {
                return moment(date).format('MM/DD/YYYY');
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.totalASNSize = 0;
                if ($scope.filesTemp && $scope.filesTemp.length > 0) {
                    $scope.filesTemp.forEach(function (item, index) {
                        $scope.totalASNSize = $scope.totalASNSize + item.size;
                    });
                }
                if (($scope.totalASNSize + $scope.totalASNItemSize) > $scope.totalAttachmentMaxSize) {
                    swal({
                        title: "Attachment size!",
                        text: "Total Attachments size cannot exceed 6MB",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return;
                        });
                    return;
                }

                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0,
                                fileSize: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileSize = result.byteLength;
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;
                            if (!$scope.dispatchObject[0].attachmentsArray) {
                                $scope.dispatchObject[0].attachmentsArray = [];
                            }

                            var ifExists = _.findIndex($scope.dispatchObject[0].attachmentsArray, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                            if (ifExists <= -1) {
                                $scope.dispatchObject[0].attachmentsArray.push(fileUpload);
                            }

                        });
                })
            }


            $scope.SaveDispatchTrack = function (type) {
                $scope.isError = false;
                $scope.isErrorWeightValidation = false;
                $scope.isErrorWeightValidationRemaining = false;
                $scope.isErrorGrossWeightValidation = false;

                if (type.DELIVERY_DATE_TEMP == undefined || type.SHIPMENT_DATE_TEMP == undefined || type.SHIP_NOTICE_TYPE == undefined || type.SHIP_FROM_LOCATION == undefined || type.SHIP_FROM_LOCATION == "" || type.SHIP_TO_LOCATION == undefined || type.SHIP_TO_LOCATION == "" ||
                    type.TRANSPOTER_NAME == undefined || type.TRANSPOTER_NAME == "" || (type.SHIP_NOTICE_TYPE == 'Actual' && (type.TRACKING_DATE_TEMP == undefined || type.VECHICLE_NO == undefined || type.VECHICLE_NO == ""))) {
                    $scope.isError = true;
                    return
                }
                //$scope.dispatchObject.forEach(function (item) {
                //    if ($scope.dCode == 0) {
                //        if (item.REMAINING_NET_QTY !== 0 && ((parseFloat(item.NET_WT) > parseFloat(item.REMAINING_NET_QTY)) || item.NET_WT == undefined)) {
                //            item.isErrorWeightValidationRemaining = true;
                //            $scope.isErrorWeightValidationRemaining = true;
                //        }
                //    } else {
                //        if (item.REMAINING_NET_QTY !== 0 && ((parseFloat(item.NET_WT) > parseFloat(item.ORDER_QTY)) || item.NET_WT == 0)) {
                //            item.isErrorWeightValidation = true;
                //            $scope.isErrorWeightValidation = true;
                //        }
                //    }
                //    if (item.GROSS_WT == undefined || item.GROSS_WT == 0) {
                //        item.isErrorGrossWeightValidation = true;
                //        $scope.isErrorGrossWeightValidation = true;
                //    }
                //})

                var arr = [];

                $scope.dispatchObject.forEach(function (item) {
                    if ($scope.dCode == 0) {
                        if (item.NET_WT > 0) {
                            arr.push(item);
                            if (item.REMAINING_NET_QTY !== 0 && ((parseFloat(item.NET_WT) > parseFloat(item.ORDER_QTY)))) {
                                item.isErrorWeightValidationRemaining = true;
                                $scope.isErrorWeightValidationRemaining = true;
                            }
                            if (item.GROSS_WT == undefined || item.GROSS_WT == 0) {
                                item.isErrorGrossWeightValidation = true;
                                $scope.isErrorGrossWeightValidation = true;
                            }
                        }
                    } else {
                        if (item.NET_WT > 0) {
                            arr.push(item);
                            if (item.REMAINING_NET_QTY !== 0 && ((parseFloat(item.NET_WT) > parseFloat(item.ORDER_QTY)))) {
                                item.isErrorWeightValidation = true;
                                $scope.isErrorWeightValidation = true;
                            }
                            if (item.GROSS_WT == undefined || item.GROSS_WT == 0) {
                                item.isErrorGrossWeightValidation = true;
                                $scope.isErrorGrossWeightValidation = true;
                            }
                        }

                    }


                });
                if (arr.length == 0) {
                    $scope.isErrorWeightValidation = true;
                    $scope.isErrorWeightValidationRemaining = true;
                    $scope.isErrorGrossWeightValidation = true;
                }

                if ($scope.isErrorWeightValidation || $scope.isErrorWeightValidationRemaining || $scope.isErrorGrossWeightValidation) {
                    return;
                }

                if ($scope.CUSTOMER_BATCH == 'ASN') {
                    $scope.dispatchObject.forEach(function (item) {
                        item.sessionID = userService.getUserToken();
                        item.COMP_ID = $scope.companyId;
                        item.PO_NUMBER = $scope.poOrderId;
                        item.VENDOR_ID = $scope.currentUserId;
                        item.VENDOR_CODE = $scope.vendorCode;
                        item.SHIP_NOTICE_TYPE = $scope.dispatchObject[0].SHIP_NOTICE_TYPE;
                        item.CUSTOMER_BATCH = $scope.CUSTOMER_BATCH;
                        item.SHIP_FROM_LOCATION = $scope.dispatchObject[0].SHIP_FROM_LOCATION;
                        item.SHIP_TO_LOCATION = $scope.dispatchObject[0].SHIP_TO_LOCATION;
                        item.TRANSPOTER_NAME = $scope.dispatchObject[0].TRANSPOTER_NAME;
                        item.VECHICLE_NO = $scope.dispatchObject[0].VECHICLE_NO;
                        item.SHIPPED_THROUGH = $scope.dispatchObject[0].SHIPPED_THROUGH;
                        item.INCOTERM = $scope.dispatchObject[0].INCOTERM;
                        item.PAYMENT_METHOD = $scope.dispatchObject[0].PAYMENT_METHOD;
                        item.INVOICE_NUMBER = $scope.dispatchObject[0].INVOICE_NUMBER;
                        item.attachmentsArray = $scope.dispatchObject[0].attachmentsArray;

                        var ts = moment($scope.dispatchObject[0].DELIVERY_DATE_TEMP, "DD-MM-YYYY").valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";

                        var ts = moment($scope.dispatchObject[0].SHIPMENT_DATE_TEMP, "DD-MM-YYYY").valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.SHIPMENT_DATE = "/Date(" + milliseconds + "000+0530)/";

                        var today = moment().format('DD-MM-YYYY');
                        var ts = moment(today,'DD-MM-YYYY').valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.MANUFACTURED_DATE = "/Date(" + milliseconds + "000+0530)/";

                        var today = moment().format('DD-MM-YYYY');
                        var ts = moment(today, 'DD-MM-YYYY').valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.BEST_BEFORE_DATE = "/Date(" + milliseconds + "000+0530)/";
                       

                        var ts = moment($scope.dispatchObject[0].REQUESTED_DELIVERY_DATE_TEMP, "DD-MM-YYYY").valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.REQUESTED_DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";

                        var ts = moment($scope.dispatchObject[0].TRACKING_DATE_TEMP, "DD-MM-YYYY").valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.TRACKING_DATE = "/Date(" + milliseconds + "000+0530)/";

                        var today = moment().format('DD-MM-YYYY');
                        var ts = moment(today, 'DD-MM-YYYY').valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.RECEIVED_DATE = "/Date(" + milliseconds + "000+0530)/";

                        var today = moment().format('DD-MM-YYYY');
                        var ts = moment(today, 'DD-MM-YYYY').valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.SERVICE_COMPLETED_DATE = "/Date(" + milliseconds + "000+0530)/";

                        var today = moment().format('DD-MM-YYYY');
                        var ts = moment(today, 'DD-MM-YYYY').valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.SERVICE_COMPLETION_DATE = "/Date(" + milliseconds + "000+0530)/";

                        var today = moment().format('DD-MM-YYYY');
                        var ts = moment(today, 'DD-MM-YYYY').valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.SERVICE_DOCUMENT_DATE = "/Date(" + milliseconds + "000+0530)/";

                       return

                    })

                } else {
                    $scope.dispatchObject.forEach(function (item) {
                        item.sessionID = userService.getUserToken();
                        item.COMP_ID = $scope.companyId;
                        item.PO_NUMBER = $scope.poOrderId;
                        item.VENDOR_ID = $scope.currentUserId;
                        item.VENDOR_CODE = $scope.vendorCode;
                        item.CUSTOMER_BATCH = $scope.CUSTOMER_BATCH;
                        item.SERVICE_CUSTOMER_LOCATION = $scope.dispatchObject[0].SERVICE_CUSTOMER_LOCATION;
                        item.SERVICE_LOCATION = $scope.dispatchObject[0].SERVICE_LOCATION;
                        item.SERVICE_BY = $scope.dispatchObject[0].SERVICE_BY;
                        item.INVOICE_NUMBER = $scope.dispatchObject[0].INVOICE_NUMBER;
                        item.INVOICE_AMOUNT = $scope.dispatchObject[0].INVOICE_AMOUNT;
                        item.COMMENTS = $scope.dispatchObject[0].COMMENTS;


                        var ts = moment($scope.dispatchObject[0].RECEIVED_DATE, "DD-MM-YYYY").valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.RECEIVED_DATE = "/Date(" + milliseconds + "000+0530)/";


                        var ts = moment($scope.dispatchObject[0].SERVICE_COMPLETED_DATE, "DD-MM-YYYY").valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.SERVICE_COMPLETED_DATE = "/Date(" + milliseconds + "000+0530)/";

                        var ts = moment($scope.dispatchObject[0].SERVICE_COMPLETION_DATE, "DD-MM-YYYY").valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.SERVICE_COMPLETION_DATE = "/Date(" + milliseconds + "000+0530)/";

                        var ts = moment($scope.dispatchObject[0].SERVICE_DOCUMENT_DATE, "DD-MM-YYYY").valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        item.SERVICE_DOCUMENT_DATE = "/Date(" + milliseconds + "000+0530)/";


                      
                    })

                }

               
                var params = {
                    detailsarray: $scope.dispatchObject
                };


                PRMPOService.saveASNdetails(params)
                         .then(function (response) {
                             if (response.errorMessage == '') {
                                 swal({
                                     title: "Done!",
                                     text: "Saved Successfully.",
                                     type: "success",
                                     showCancelButton: false,
                                     confirmButtonColor: "#DD6B55",
                                     confirmButtonText: "Ok",
                                     closeOnConfirm: true
                                 },
                                     function () {
                                         var url = $state.href("list-pendingPO");
                                         $window.open(url, '_self');
                                         //$scope.getDispatchTrack();
                                     });
                             } else {
                                 $scope.getUserDetails(response.errorMessage);
                                 swal("Error", response.errorMessage, 'error');
                             }
                    });

               
            };

            $scope.getUserDetails = function (message) {
                userService.getProfileDetails({ "userid": $scope.currentUserId, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if (response) {
                            $scope.VENDOR_EMAIL = response.email;
                            $scope.VENDOR_PHONE = response.phoneNum;
                            $scope.VENDOR_NAME = userService.getFirstname() + ' ' + userService.getLastname();
                            $scope.VENDOR_COMP_NAME = userService.getVendorCompanyName();
                            $scope.createTicket(message);
                        }
                    });
            }

            $scope.createTicket = function (message) {
                var tickectParams = {
                    name: $scope.VENDOR_NAME,
                    email: $scope.VENDOR_EMAIL,
                    phone: $scope.VENDOR_PHONE,
                    subject: ' ASN issue of ' + $scope.VENDOR_COMP_NAME + ' for ' + "Neuland Laboratories",
                    status: 2,
                    priority: 3,
                    description: message,
                    email_config_id: 81000024659,
                    group_id: 81000274957,
                    source: 3,
                    type: "PO Issue",
                    responder_id: 81072905341,
                    tags: ['OPS Screen'],
                    "custom_fields": { "cf_client": "Neuland Laboratories Ltd" }
                };


                $http({
                    method: 'POST',
                    url: "https://prm360.freshdesk.com/api/v2/tickets",
                    encodeURI: true,
                    headers: {
                        'Content-Type': 'application/json',
                        "Authorization": "Basic " + window.btoa('oLRuHDsQvII8LrfthZAj' + ":x")
                    },
                    data: tickectParams,
                    dataType: "json"
                }).then(function (response) {
                    if (response.data) {
                    }
                });


            };


            $scope.removeAttach = function (index, item) {
                $scope.dispatchObject[0].attachmentsArray.splice(index, 1);
            }
           
            $scope.cancelPO = function () {
                $state.go("list-pendingPO");
            }
          
        }]);