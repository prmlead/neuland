﻿prmApp
    .controller('paymentDetailsCtrl', function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter, workflowService) {


        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;
        $scope.userID = userService.getUserId();
        $scope.compId = userService.getUserCompanyId();
        $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
        $scope.invoiceNumber = $stateParams.invoiceNumber ? +$stateParams.invoiceNumber : '';




        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
            $scope.GetpaymentDetailsList(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };
        /*PAGINATION CODE*/

        $scope.filters = {
            fromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
            toDate: moment().format('YYYY-MM-DD'),
            type: '',
            searchKeyword: ''
        };


       
        $scope.tableColumns = [];
        $scope.GetpaymentDetailsList = function (recordsFetchFrom, pageSize, searchString) {

            if (_.isEmpty($scope.filters.fromDate)) {
                fromDate = '';
            } else {
                fromDate = $scope.filters.fromDate;
            }

            if (_.isEmpty($scope.filters.toDate)) {
                toDate = '';
            } else {
                toDate = $scope.filters.toDate;
            }

            if (_.isEmpty($scope.filters.type)) {
                type = '';
            } else {
                type = $scope.filters.type;
            }

            var params =
            {
                "compID": $scope.isCustomer ? $scope.compId : 0,
                "userID": $scope.isCustomer ? 0 : $scope.userID,
                "sessionID": userService.getUserToken(),
                "search": searchString ? searchString : "",
                "pageSize": recordsFetchFrom * pageSize,
                "fromDate": fromDate,
                "toDate": toDate,
                "numberOfRecords": pageSize
            };

            PRMPOService.getpaymentDetailsList(params)
                .then(function (response) {
                    if (response) {
                        $scope.rows = [];
                        $scope.tableColumns = [];
                        $scope.totalItems = 0;
                        var arr = JSON.parse(response).Table;
                        if (arr && arr.length > 0) {
                            $scope.totalItems = arr[0].TOTAL_COUNT;
                            arr.forEach(a => delete a.TOTAL_COUNT);
                            $scope.tableColumnsTemp = angular.copy(_.keys(arr[0]));
                            $scope.tableColumnsTemp.forEach(function (item, index) {
                                item = item.replaceAll("_", " ");
                                var foundIndex = _.findIndex($scope.tableColumns, function (val) { return val === item; });
                                if (foundIndex <= -1) {
                                    $scope.tableColumns.push(item);
                                }
                            });
                            $scope.rows = arr;
                            arr.forEach(function (item, index) {

                                var obj = angular.copy(_.values(item));
                                if (obj) {
                                    item.tableValues = [];
                                    obj.forEach(function (value, valueIndex) {
                                        item.tableValues.push(value);
                                    });
                                }
                            });
                        } else {
                            var arr = JSON.parse(response);
                            arr = arr.filter(function (a) { return a !== 'TOTAL_COUNT' });
                            $scope.tableColumnsTemp = arr;
                            $scope.tableColumnsTemp.forEach(function (item, index) {
                                item = item.replaceAll("_", " ");
                                $scope.tableColumns.push(item);
                            });
                        }

                    } else {
                        if ($scope.tableColumnsTemp && $scope.tableColumnsTemp.length > 0) {
                            $scope.tableColumnsTemp.forEach(function (item, index) {
                                item = item.replaceAll("_", " ");
                                var foundIndex = _.findIndex($scope.tableColumns, function (val) { return val === item; });
                                if (foundIndex <= -1) {
                                    $scope.tableColumns.push(item);
                                }
                            });
                        }
                        swal("Status!", "No Records Found..!", "error");
                    }
                });
        };

        $scope.GetpaymentDetailsList(0, 10, $scope.invoiceNumber);

        $scope.filterByDate = function () {
            $scope.rows = [];
            $scope.tableColumns = [];
            $scope.totalItems = 0;
            $scope.GetpaymentDetailsList(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);

        };


        if ($scope.invoiceNumber) {
            $scope.filters.searchKeyword = $scope.invoiceNumber;
        }

    });