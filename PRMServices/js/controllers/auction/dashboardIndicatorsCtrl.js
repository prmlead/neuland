prmApp
    // =========================================================================
    // Auction Tiles
    // =========================================================================
    .controller('dashboardIndicatorsCtrl', ["$timeout", "$state", "$scope", "$log", "growlService", "userService", "auctionsService", "$http", "$rootScope",
        "SignalRFactory", "signalRHubName", "logisticServices", "$filter", "store", "poService", "workflowService",
        function ($timeout, $state, $scope, $log, growlService, userService, auctionsService, $http, $rootScope, $filter, SignalRFactory, signalRHubName, logisticServices, store, poService,
            workflowService) {
            $scope.sessionid = userService.getUserToken();
            $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
            $scope.currentUserId = +userService.getUserId();
            $scope.subUserID = $scope.currentUserId;
            $scope.compId = +userService.getUserCompanyId();
            $scope.dashboard = {
                fromDate: moment().add('days', -30).format('YYYY-MM-DD'),
                toDate: moment().format('YYYY-MM-DD')
            };
            $scope.days = [];
            $scope.requirementDetailsChartShow = false;
            $scope.qcsDetailsChartShow = false;
            $scope.vendorDetailsChartShow = false;
            $scope.vendorTrainedChartShow = false;
            $scope.resolvedDetailsChartShow = false;
            $scope.requirementDetailsChartShowDay = false;

            $scope.reqDetailsData = [];
            $scope.qcsDetailsData = [];
            $scope.vendorDetailsData = [];
            $scope.vendorTrainedData = [];
            $scope.resolvedDetailsData = [];
            $scope.allTicketsData = [];
            $scope.titleText = '';
            $scope.subTitleText = '';
            $scope.yAxisText = '';

            $scope.ticketData = [];
            $scope.categoriesData = [];
            function daysInMonth(month, year) {
                return new Date(year, month, 0).getDate();
            }

            // TICKETS START

            $scope.responderTicketAllData = [];
            $scope.RefreshTime = '';
            $scope.responderTicket = [];
            $scope.responderTicketTemp = [];

            // TICKETS END

            $('.selected-items-box').bind('click', function (e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });

            $scope.subUsers = [];
            $scope.subUserName = '';
            $scope.userDepartmentsAdded = [];
            $scope.userDepartments = [];
            $scope.userDepartments1 = [];

            userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.subUsers = response;

                    if ($scope.subUsers && $scope.subUsers.length > 0) {
                        $scope.subUsers.forEach(function (subUserItem, subUserIndex) {
                            subUserItem.fullName = subUserItem.firstName.concat(" ").concat(subUserItem.lastName);
                        });
                    }

                });
            //userService.getCompanyUsers({ "compId": $scope.compId, "sessionId": userService.getUserToken() })
            //    .then(function (response) {
            //        $scope.subUsers = response;

            //        if ($scope.subUsers && $scope.subUsers.length > 0) {
            //            $scope.subUsers.forEach(function (subUserItem, subUserIndex) {
            //                subUserItem.fullName = subUserItem.firstName.concat(" ").concat(subUserItem.lastName);
            //            });
            //        }
            //    });

            $scope.getCompanyDepartments = function () {
                auctionsService.GetCompanyDepartments(userService.getUserId(), userService.getUserToken())
                    .then(function (response) {
                        $scope.userDepartments = response;
                        $scope.userDepartments.forEach(function (user) {
                            user.isDepartmentUser = false;
                        });
                        $scope.userDepartments1 = $scope.userDepartments;
                    })
            }

            $scope.getCompanyDepartments();

            $scope.autofillUser = function (userName, fieldType) {
                if (userName && userName != '' && userName.length > 0) {
                    $scope.searchingUserUsers = angular.lowercase(userName);
                    $scope.dropDownData = $scope.subUsers.filter(function (user) {
                        return user.fullName.toLowerCase().indexOf($scope.searchingUserUsers) > -1
                    });
                    if (fieldType == 'NAME') {
                        $scope["filterUsers_"] = $scope.dropDownData;
                    }
                   
                } else {
                    $scope.subUserName = '';
                    $scope.userDepartments = [];
                    $scope['filterUsers_'] = null;
                }
                $scope.userDepartmentsAdded = [];
                $scope.subUser = '';
                $scope.getCompanyDepartments();
            };

            $scope.fillTextbox = function (selProd) {
                $scope.subUserName = selProd.fullName;
                $scope.subUserID = selProd.userID;
                $scope['filterUsers_'] = null;
                $scope.userDepartmentsAdded = [];
                $scope.subUser = _.find($scope.subUsers, function (item) { return item.fullName.toLowerCase() == $scope.subUserName.toLowerCase() })
                auctionsService.GetUserDepartments($scope.subUserID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response = response.filter(function (user) {
                                return user.isValid == true;
                            })
                            $scope.userDepartments = response;
                            $scope.userDepartments.forEach(function (user) {
                                user.isDepartmentUser = false;
                            });
                            $scope.userDepartments1 = $scope.userDepartments;
                        }
                    })
            };

            $scope.addUserDepartments = function (user) {
                user.isDepartmentUser = !user.isDepartmentUser;
                if (!$scope.subUser) {
                    if (!user.isDepartmentUser) {
                        $scope.userDepartmentsAdded = $scope.userDepartmentsAdded.filter(function (user1) {
                            return user1.DEPT_ID != user.deptID;
                        })
                    } else {
                        $scope.userDepartmentsAdded.push({
                            DEPT_CODE: user.deptCode,
                            DEPT_ID: user.deptID,
                            isDepartmentUser: user.isDepartmentUser
                        });
                    }
                } else {
                    if (!user.isDepartmentUser) {
                        $scope.userDepartmentsAdded = $scope.userDepartmentsAdded.filter(function (user1) {
                            return user1.DEPT_ID != user.companyDepartments.deptID;
                        })
                    } else {
                        $scope.userDepartmentsAdded.push({
                            DEPT_CODE: user.companyDepartments.deptCode,
                            DEPT_ID: user.companyDepartments.deptID,
                            isDepartmentUser: user.isDepartmentUser
                        });
                    }
                }

            };

            $scope.searchDepartment = function (search) {
                $scope.userDepartments = $scope.userDepartments1;
                if (search && $scope.subUser) {
                    $scope.userDepartments = $scope.userDepartments1.filter(function (user) {
                        return user.companyDepartments.deptCode.toLowerCase().indexOf(search.toLowerCase()) > -1;
                    })
                } else if (search && !$scope.subUser) {
                    $scope.userDepartments = $scope.userDepartments1.filter(function (user) {
                        return user.deptCode.toLowerCase().indexOf(search.toLowerCase()) > -1;
                    })
                }

            }

            $scope.reset = function () {
                $scope.dashboard = {
                    fromDate: moment().add('days', -30).format('YYYY-MM-DD'),
                    toDate: moment().format('YYYY-MM-DD')
                };
                $scope.subUserID = $scope.currentUserId;
                $scope.subUserName = '';
                $scope.subUser = '';
                $scope.userDepartmentsAdded = [];
                //$scope.userDepartments = [];
                $scope.getCompanyDepartments();
                $scope['filterUsers_'] = null;

                $scope.ticketData = [];
                $scope.getChartsData();

            }

           
            $scope.xAxisCategoriesData = function (year, month) {

                var d = new Date(month + '1, 00 00:00:00');
                var monthNumber = d.getMonth();
                var monthInDays = daysInMonth(monthNumber + 1, year)
                if (monthInDays == 30) {
                    $scope.categoriesData = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30']
                } else if (monthInDays == 31) {
                    $scope.categoriesData = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31']
                } else if (monthInDays == 29) {
                    $scope.categoriesData = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29']
                } else {
                    $scope.categoriesData = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28']
                }
            }



            $scope.fillDataValuesByDate = function (arr, fillArr, year, monthYear, month, type) {
                $scope.xAxisCategoriesData(year, month)
                if (type === 'req') {
                    $scope.titleText = 'Daily Requirements(' + monthYear + ')';
                    $scope.subTitleText = 'No Of Requirements Posted';
                    $scope.yAxisText = 'Requirements Number';
                } else if (type === 'qcs') {
                    $scope.titleText = 'Daily Qcs(' + monthYear + ')';
                    $scope.subTitleText = 'No Of Qcs Created';
                    $scope.yAxisText = 'Qcs Number';
                } else if (type === 'vendorDetails') {
                    $scope.titleText = 'Daily Vendors(' + monthYear + ')';
                    $scope.subTitleText = 'No Of Vendors Created';
                    $scope.yAxisText = 'Vendors Created Number';
                } else if (type === 'vendorTrained') {
                    $scope.titleText = 'Vendors(' + monthYear + ')';
                    $scope.subTitleText = 'No Of Vendors Trained';
                    $scope.yAxisText = 'Vendors Trained Number';
                }
                let dayValues = [];
                dayValues = _(arr).groupBy('MONTH_YEAR').value()[monthYear];

                fillArr.series = [];

                fillArr.series[0] = {
                    name: year,
                    data: []
                }

                $scope.categoriesData.forEach(function (day, monthIndex) {

                    var ifExists = _.findIndex(dayValues, function (dayValue) { return moment(dayValue.POSTED_DATES).format('D') === day });
                    if (ifExists >= 0) {
                        fillArr.series[0].data.push(dayValues[ifExists].COUNT_BY_DATES);
                    } else {
                        fillArr.series[0].data.push(0);
                    }
                });
                $scope.requirementDetailsChartShowDay = true;


            }


            $scope.fillDataValuesByDateTickets = function (arr, fillArr, year, monthYear, month, type) {

                $scope.xAxisCategoriesData(year, month)

                $scope.titleText = 'Tickets(' + monthYear + ')';
                $scope.subTitleText = 'No Of Tickets';
                $scope.yAxisText = 'Tickets Number';
                let dayValues = [];
                let resolvedDayValues = [];
                var resolvedArr = arr.filter(function (item) { return item.NAME == 'Closed' });
                var allArr = arr.filter(function (item) { return item.NAME == 'Raised' });

                dayValues = _(allArr).groupBy('MONTH_YEAR').value()[monthYear];
                var dayValuesFinal = [];
                dayValues.forEach(function (item) {
                    var temp = _.find(dayValuesFinal, function (day) { return item.POSTED_DATES === day.POSTED_DATES })
                    if (temp) {
                        temp.COUNT_BY_DATES += item.COUNT_BY_DATES;

                    } else {
                        dayValuesFinal.push({
                            POSTED_DATES: item.POSTED_DATES,
                            COUNT_BY_DATES: item.COUNT_BY_DATES
                        })
                    }
                })

                resolvedDayValues = _(resolvedArr).groupBy('MONTH_YEAR').value()[monthYear];
                var resolvedDayValuesFinal = [];
                resolvedDayValues.forEach(function (item) {
                    var temp = _.find(resolvedDayValuesFinal, function (day) { return item.POSTED_DATES === day.POSTED_DATES })
                    if (temp) {
                        temp.COUNT_BY_DATES += item.COUNT_BY_DATES;

                    } else {
                        resolvedDayValuesFinal.push({
                            POSTED_DATES: item.POSTED_DATES,
                            COUNT_BY_DATES: item.COUNT_BY_DATES
                        })
                    }
                })
                fillArr.series[0] = {
                    name: 'Closed-' + monthYear,
                    data: []
                };
                fillArr.series[1] = {
                    name: 'Raised-' + monthYear,
                    data: []
                };

                $scope.categoriesData.forEach(function (day, monthIndex) {

                    var ifExists = _.findIndex(dayValuesFinal, function (dayValue) { return moment(dayValue.POSTED_DATES, 'DD-MM-YYYY').format('D') === day });
                    if (ifExists >= 0) {
                        fillArr.series[1].data.push(dayValuesFinal[ifExists].COUNT_BY_DATES);
                    } else {
                        fillArr.series[1].data.push(0);
                    }

                    var ifExists1 = _.findIndex(resolvedDayValuesFinal, function (dayValue) { return moment(dayValue.POSTED_DATES, 'DD-MM-YYYY').format('D') === day });
                    if (ifExists1 >= 0) {
                        fillArr.series[0].data.push(resolvedDayValuesFinal[ifExists1].COUNT_BY_DATES);
                    } else {
                        fillArr.series[0].data.push(0);
                    }
                });
                $scope.requirementDetailsChartShowDay = true;


            }

            $scope.chartDetailsByMonth = {

                credits: {
                    enabled: false
                },
                chart: {

                    width: 800,

                    height: 350,
                    events: {
                        load: function () {

                            var chart = this,
                                series = [];
                            $('#upateChartButton').click(function () {
                                series = $scope.chartDetailsByMonth.series
                                chart.update({
                                    series: series,
                                    title: {
                                        text: $scope.titleText
                                    },
                                    subtitle: {
                                        text: $scope.subTitleText
                                    },
                                    yAxis: {

                                        title: {
                                            text: $scope.yAxisText
                                        }

                                    }, xAxis: {
                                        categories: $scope.categoriesData
                                    },

                                }, true, true);
                            });
                        }
                    }
                },
                title: {
                    text: $scope.titleText
                },
                subtitle: {
                    text: $scope.subTitleText
                },
                xAxis: {
                    categories: $scope.categoriesData
                },
                yAxis: {

                    title: {
                        text: $scope.yAxisText
                    }

                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        }

                    }
                },
                series: []
            };
            $scope.requirementDetails = {
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Daily Requirements'
                },
                subtitle: {
                    text: 'No Of Requirements Posted'
                },
                xAxis: {
                    categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                },
                yAxis: {

                    title: {
                        text: 'Requirements Number'
                    }

                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        point: {
                            events: {
                                click: function () {
                                    var monthYear = this.category + this.series.name;
                                    var month = this.category;
                                    $scope.fillDataValuesByDate($scope.reqDetailsData, $scope.chartDetailsByMonth, this.series.name, monthYear, month, 'req');
                                    $('#upateChartButton').click();
                                    $('#chartModal').modal('show');

                                }


                            }
                        }
                    }
                },

                series: []
            };




            $scope.qcsDetails = {
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Daily Qcs'
                },
                subtitle: {
                    text: 'No Of Qcs Created'
                },
                xAxis: {
                    categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                },
                yAxis: {
                    title: {
                        text: 'Qcs Numbers'
                    }
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    },
                    series: {

                        point: {
                            events: {
                                click: function () {
                                    var monthYear = this.category + this.series.name;
                                    var month = this.category;
                                    $scope.fillDataValuesByDate($scope.qcsDetailsData, $scope.chartDetailsByMonth, this.series.name, monthYear, month, 'qcs');
                                    $('#upateChartButton').click();
                                    $('#chartModal').modal('show');

                                }


                            }
                        }
                    }
                },
                series: []
            };

            $scope.vendorDetails = {
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Daily Vendors'
                },
                subtitle: {
                    text: 'No Of Vendors Created'
                },
                xAxis: {
                    categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                },
                yAxis: {
                    title: {
                        text: 'Vendors Created Numbers'
                    }
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }, series: {

                        point: {
                            events: {
                                click: function () {
                                    var monthYear = this.category + this.series.name;
                                    var month = this.category;
                                    $scope.fillDataValuesByDate($scope.vendorDetailsData, $scope.chartDetailsByMonth, this.series.name, monthYear, month, 'vendorDetails');
                                    $('#upateChartButton').click();
                                    $('#chartModal').modal('show');

                                }


                            }
                        }
                    }
                },
                series: []
            };

            $scope.vendorTrained = {
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Vendors'
                },
                subtitle: {
                    text: 'No Of Vendors Trained'
                },
                xAxis: {
                    categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                },
                yAxis: {
                    title: {
                        text: 'Vendors Trained Numbers'
                    }
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }, series: {

                        point: {
                            events: {
                                click: function () {
                                    var monthYear = this.category + this.series.name;
                                    var month = this.category;
                                    $scope.fillDataValuesByDate($scope.vendorTrainedData, $scope.chartDetailsByMonth, this.series.name, monthYear, month, 'vendorTrained');
                                    $('#upateChartButton').click();
                                    $('#chartModal').modal('show');

                                }


                            }
                        }
                    }
                },
                series: []
            };


            $scope.dashboardIndicator = {};

            $scope.getChartsData = function () {

                //var values = $scope.dashboard.monthDate.format("YYYY-MM").split("-");
                $scope.requirementDetailsChartShow = false;
                $scope.qcsDetailsChartShow = false;
                $scope.vendorDetailsChartShow = false;
                $scope.vendorTrainedChartShow = false;
                var deptId = _.map($scope.userDepartmentsAdded, 'DEPT_ID').join(',')
                var params = {
                    userId: $scope.subUserID,
                    compId: $scope.compId,
                    deptId: deptId,
                    sessionId: userService.getUserToken(),
                    //month: +values[1],
                    //year: +values[0]
                    fromDate: $scope.dashboard.fromDate,
                    toDate: $scope.dashboard.toDate
                };
                auctionsService.getDashboardIndicators(params)
                    .then(function (response) {
                        $scope.dashboardIndicator = response;
                        if ($scope.dashboardIndicator) {
                            $scope.dashboardIndicator.TOTAL_SAVINGS = Math.round($scope.precisionRound(parseFloat($scope.dashboardIndicator.TOTAL_SAVINGS), $rootScope.companyRoundingDecimalSetting));
                            $scope.dashboardIndicator.TOTAL_PROFIT = Math.round($scope.precisionRound(parseFloat($scope.dashboardIndicator.TOTAL_PROFIT), $rootScope.companyRoundingDecimalSetting));
                            $scope.dashboardIndicator.TOTAL_LOSS = Math.round($scope.precisionRound(parseFloat($scope.dashboardIndicator.TOTAL_LOSS), $rootScope.companyRoundingDecimalSetting));
                            $scope.dashboardIndicator.QCS_TOTAL_SAVINGS = Math.round($scope.precisionRound(parseFloat($scope.dashboardIndicator.QCS_TOTAL_SAVINGS), $rootScope.companyRoundingDecimalSetting));
                            $scope.dashboardIndicator.TOTAL_PROCUREMENT_VALUE = Math.round($scope.precisionRound(parseFloat($scope.dashboardIndicator.TOTAL_PROCUREMENT_VALUE), $rootScope.companyRoundingDecimalSetting));
                        }
                        if (response && response.CHART_SERIES.length > 0) {
                            $scope.years = [];
                            $scope.years1 = [];
                            $scope.years2 = [];
                            $scope.years3 = [];
                            $scope.requirementDetails.series = [];
                            $scope.qcsDetails.series = [];
                            $scope.vendorDetails.series = [];
                            $scope.vendorTrained.series = [];
                            var reqDetails = response.CHART_SERIES.filter(function (item) { return item.MODULE_NAME === 'RFQ'; });
                            var qcsDetails = response.CHART_SERIES.filter(function (item) { return item.MODULE_NAME === 'QCS'; });
                            var vendorDetails = response.CHART_SERIES.filter(function (item) { return item.MODULE_NAME === 'VENDOR'; });
                            var vendorTrained = response.CHART_SERIES.filter(function (item) { return item.MODULE_NAME === 'VENDOR_TRAINED'; });
                            if (reqDetails && reqDetails.length > 0) {
                                reqDetails.forEach(function (obj, index) {
                                    obj.YEARS = moment(obj.POSTED_DATES).format('YYYY');
                                    obj.MONTHS = moment(obj.POSTED_DATES).format('MMMM');
                                    obj.MONTH_YEAR = obj.MONTHS.concat(obj.YEARS);
                                });
                                $scope.years = returnOnlyYears(reqDetails);
                                if ($scope.years.length > 0) {
                                    $scope.years.forEach(function (item, index) {
                                        var obj =
                                        {
                                            name: item,
                                            data: []
                                        };
                                        $scope.requirementDetails.series.push(obj);
                                        fillDataValues(reqDetails, item, index, $scope.requirementDetails);
                                    });
                                }
                                $scope.requirementDetailsChartShow = true;
                                $scope.reqDetailsData = reqDetails;
                            }

                            if (qcsDetails && qcsDetails.length > 0) {
                                qcsDetails.forEach(function (obj, index) {
                                    obj.YEARS = moment(obj.POSTED_DATES).format('YYYY');
                                    obj.MONTHS = moment(obj.POSTED_DATES).format('MMMM');
                                    obj.MONTH_YEAR = obj.MONTHS.concat(obj.YEARS);
                                });
                                $scope.years1 = returnOnlyYears(qcsDetails);
                                if ($scope.years1.length > 0) {
                                    $scope.years1.forEach(function (item, index) {
                                        var obj =
                                        {
                                            name: item,
                                            data: []
                                        };
                                        $scope.qcsDetails.series.push(obj);
                                        fillDataValues(qcsDetails, item, index, $scope.qcsDetails);
                                    });
                                }
                                $scope.qcsDetailsChartShow = true;
                                $scope.qcsDetailsData = qcsDetails;

                            }

                            if (vendorDetails && vendorDetails.length > 0) {
                                vendorDetails.forEach(function (obj, index) {
                                    obj.YEARS = moment(obj.POSTED_DATES).format('YYYY');
                                    obj.MONTHS = moment(obj.POSTED_DATES).format('MMMM');
                                    obj.MONTH_YEAR = obj.MONTHS.concat(obj.YEARS);
                                });
                                $scope.years2 = returnOnlyYears(vendorDetails);
                                if ($scope.years2.length > 0) {
                                    $scope.years2.forEach(function (item, index) {
                                        var obj =
                                        {
                                            name: item,
                                            data: []
                                        };
                                        $scope.vendorDetails.series.push(obj);
                                        fillDataValues(vendorDetails, item, index, $scope.vendorDetails);
                                    });
                                }
                                $scope.vendorDetailsChartShow = true;
                                $scope.vendorDetailsData = vendorDetails;
                            }

                            if (vendorTrained && vendorTrained.length > 0) {
                                vendorTrained.forEach(function (obj, index) {
                                    obj.YEARS = moment(obj.POSTED_DATES).format('YYYY');
                                    obj.MONTHS = moment(obj.POSTED_DATES).format('MMMM');
                                    obj.MONTH_YEAR = obj.MONTHS.concat(obj.YEARS);
                                });
                                $scope.years3 = returnOnlyYears(vendorTrained);
                                if ($scope.years3.length > 0) {
                                    $scope.years3.forEach(function (item, index) {
                                        var obj =
                                        {
                                            name: item,
                                            data: []
                                        };
                                        $scope.vendorTrained.series.push(obj);
                                        fillDataValues(vendorTrained, item, index, $scope.vendorTrained);
                                    });
                                }
                                $scope.vendorTrainedChartShow = true;
                                $scope.vendorTrainedData = vendorTrained;
                            }
                        }
                    });
            };
            if (!$scope.isVendor) {
                $scope.getChartsData();
            }

            $scope.numDifferentiation = function (value) {
                var val = value;
                //if (val >= 10000000) {
                //    val = (val / 10000000).toFixed(2) + ' Cr';
                //} else if (val >= 100000) {
                //    val = (val / 100000).toFixed(2) + ' Lakh';
                //}
                return val;
            };

            function returnOnlyYears(arr) {
                let yearsArr = [];
                yearsArr = _.uniqBy(arr, 'YEARS');
                yearsArr = _(yearsArr)
                    .map('YEARS')
                    .value();
                return yearsArr;
            }


            function fillDataValues(arr, year, yearIndex, fillArr) {
                let monthValues = [];
                monthValues = _(arr).groupBy('MONTH_YEAR')
                    .map((objs, key) => ({
                        'MONTH_YEAR': key,
                        'COUNT_BY_DATES': _.sumBy(objs, 'COUNT_BY_DATES')
                    })).value();
                fillArr.xAxis.categories.forEach(function (month, monthIndex) {
                    var filterYearArayValues = monthValues.filter(function (item) {
                        return item.MONTH_YEAR.toLowerCase().contains(year);
                    });
                    var ifExists = _.findIndex(filterYearArayValues, function (yearArr) { return yearArr.MONTH_YEAR.toLowerCase() === month.toLowerCase().concat(year); });
                    if (ifExists >= 0) {
                        fillArr.series[yearIndex].data.push(filterYearArayValues[ifExists].COUNT_BY_DATES);
                    } else {
                        fillArr.series[yearIndex].data.push(0);
                    }
                });
            }
            $scope.AllTickets = [];
            $scope.UpdateTickets = [];


            $scope.closedView = function () {
                //var ticketsClosed = [];
                $scope.ClosedArray = [];
                $scope.resolvedArray = [];
                $scope.ticketsClosedArray = [];

                $scope.AllTickets = [];
                $scope.UpdateTickets = [];
                // $scope.ticketData = _.uniqBy($scope.ticketData, 'TICKET_ID');
                var newRefreshTime = moment().format("YYYY-MM-DD HH:mm:ss");

                //$scope.ticketData = $scope.ticketData.filter(function (ticketItem, ticketIndex) {
                //    if (ticketItem && (ticketItem.status != 5 || ticketItem.status != 9)) {
                //        return ticketItem;
                //    }
                //});

                var UnMatchedData = $scope.ticketData.filter(function (obj) {
                    return !$scope.responderTicketAllData.some(function (obj2) {
                        obj2.tickectID = parseInt(obj2.tickectID);
                        return obj.id == obj2.tickectID;
                    });
                });

                var UpdateData = $scope.ticketData.filter(function (obj) {
                    obj.updated_at = new moment(obj.updated_at).format("DD-MM-YYYY HH:mm");
                    obj.created_at = new moment(obj.created_at).format("DD-MM-YYYY HH:mm");
                    return !$scope.responderTicketAllData.some(function (obj2) {
                        return obj.updated_at == obj2.updated_at;
                    });
                });

                var UpdateDataTemp = UpdateData.filter(function (obj) {
                    return !UnMatchedData.some(function (obj2) {
                        return obj.id == obj2.id;
                    });
                })

                UnMatchedData.forEach(function (item) {
                    // $scope.responderTicket.push(item);
                    $scope.ticket = {
                        source: item.source, status: item.status,
                        created_at: item.created_at, updated_at: item.updated_at,
                        type: item.type, to_emails: item.to_emails ? item.to_emails.toString() : "",
                        tags: item.tags ? item.tags.toString() : "", subject: item.subject,
                        sourceName: item.custom_fields.cf_client, tickectID: item.id,
                        comments: "", trainingMode: "",
                        responderName: item.responderName, responder_id: item.responder_id ? item.responder_id : 0,
                        requester_id: item.requester_id, group_id: item.group_id,
                        priority: item.priority,
                        refreshTime: newRefreshTime
                    };
                    $scope.AllTickets.push($scope.ticket);
                });

                UpdateDataTemp.forEach(function (item) {

                    $scope.ticket = {
                        source: item.source, status: item.status,
                        created_at: item.created_at, updated_at: item.updated_at,
                        type: item.type, to_emails: item.to_emails ? item.to_emails.toString() : "",
                        tags: item.tags ? item.tags.toString() : "", subject: item.subject,
                        sourceName: item.custom_fields.cf_client, tickectID: item.id,
                        comments: "", trainingMode: "",
                        responderName: "", responder_id: item.responder_id ? item.responder_id : 0,
                        requester_id: item.requester_id, group_id: item.group_id,
                        priority: item.priority,
                        refreshTime: newRefreshTime
                    };
                    $scope.UpdateTickets.push($scope.ticket);
                });
                var params = {
                    AssignedTickets: $scope.AllTickets,
                    UpdateData: $scope.UpdateTickets
                }


                auctionsService.SaveAssignTickets(params).then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");

                    }
                    else if (response.errorMessage == "") {
                        //  $scope.count.insertCount = response.insertCount;
                        //  $scope.count.updateCount = response.updateCount;
                        growlService.growl("Saved Successfully");
                        //$scope.getTicket(page + 1);
                        $scope.GetAssignetTickets();
                    }

                })


            }

            $scope.resolvedDetailsArray = {

                credits: {
                    enabled: false

                },
                title: {
                    text: 'Tickets'
                },
                subtitle: {
                    text: 'No of Tickets'
                },
                xAxis: {
                    categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                },
                yAxis: {

                    title: {
                        text: 'Tickets Count'
                    }

                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        point: {
                            events: {
                                click: function () {
                                    var monthYear = this.category + this.series.name.split('-')[1];
                                    var month = this.category;
                                    $scope.fillDataValuesByDateTickets($scope.allTicketsArray, $scope.chartDetailsByMonth, this.series.name.split('-')[1], monthYear, month);
                                    $('#upateChartButton').click();
                                    $('#chartModal').modal('show');

                                }


                            }
                        }
                    }
                },

                series: []
            };

            $scope.getTicket = function (page) {
                $scope.ticketData = [];
                //$scope.GetAssignetTickets();
                $scope.getTicketData(page);
            }

            $scope.getTicketData = function (page) {
                $scope.updateTime = moment($scope.dashboard.fromDate).utc().format("YYYY-MM-DDTHH:mm:ss[Z]");
                $scope.resolvedDetailsChartShow = false;
                //$scope.GetAssignetTickets();
                $http({
                    method: 'GET',
                    url: "https://prm360.freshdesk.com/api/v2/tickets?updated_since=" + $scope.updateTime + "&per_page=100&page=" + page,
                    // url: "https://prm360.freshdesk.com/api/v2/tickets?per_page=100&page=" + page,

                    headers: {
                        'Content-Type': 'application/json',
                        "Authorization": "Basic " + window.btoa('oLRuHDsQvII8LrfthZAj' + ":x")
                    },
                    dataType: "json"
                }).then(function (response) {
                    if (response.data && response.data.length > 0) {

                        response.data.forEach(function (item) {
                            if (item.tags.join('').toLowerCase().indexOf($scope.companyName.toLowerCase()) > -1 || (item.custom_fields.cf_client && item.custom_fields.cf_client.toLowerCase().indexOf($scope.companyName.toLowerCase()) > -1)) {
                                $scope.ticketData.push(item);

                            }


                        });

                        if (response.data.length < 100) {
                            //$scope.getChartsData();

                            $scope.closedView();

                            return
                        } else {
                            $scope.getTicketData(page + 1);

                        }

                    }

                });
            };
            userService.getCompanyName().then(function (response) {
                $scope.companyName = response;
                //$scope.getTicketData(1);

            });



            $scope.GetAssignetTickets = function (page) {
                $scope.responderTicketAllData = [];
                $scope.responderTicket = [];
                auctionsService.GetAssignetTickets($scope.sessionid).then(function (response) {

                    if (response.length > 0) {
                        $scope.responderTicketAllData = response;
                        $scope.RefreshTime = moment(response[0].refreshTime).utc().format("YYYY-MM-DDTHH:mm:ss[Z]");
                        $scope.RefreshTimeDisplay = moment(response[0].refreshTime).format("YYYY-MM-DD HH:mm:ss");
                        $scope.responderTicketAllData.forEach(function (item) {
                            item.tickectID = parseInt(item.tickectID)
                            $scope.responderTicket.push(item);

                        })
                        $scope.responderTicket = _.orderBy($scope.responderTicket, ['tickectID'], ['desc']);

                        // $scope.transformData(page);

                        // graph start
                        //if ($scope.dashboard.toDate) {
                        //    var ticketsClosed = [];

                        //    ticketsClosed = $scope.responderTicket.filter(function (item) {
                        //        if (item.created_at) {
                        //            var temp = moment($scope.dashboard.toDate, 'YYYY-MM-DD').add('days', 1).format('YYYY-MM-DD')
                        //            var temp1 = moment($scope.dashboard.fromDate, 'YYYY-MM-DD').add('days', 1).format('YYYY-MM-DD')

                        //            return moment(item.created_at, 'DD-MM-YYYY HH:mm') >= moment(temp1, 'YYYY-MM-DD') && moment(item.created_at, 'DD-MM-YYYY HH:mm') <= moment(temp, 'YYYY-MM-DD')
                        //        }


                        //    });
                        //}

                        var ticketsClosed = [];

                        ticketsClosed = $scope.responderTicket;

                        ticketsClosed.forEach(function (ticket) {
                            if (ticket.status == 2) {
                                ticket.status = 'Open'
                            } else if (ticket.status == 3) {
                                ticket.status = 'Pending'
                            } else if (ticket.status == 4) {
                                ticket.status = 'Resolved'
                            } else if (ticket.status == 5) {
                                ticket.status = 'Closed'
                            } else if (ticket.status == 6) {
                                ticket.status = 'Pending Training Confirmation'
                            } else if (ticket.status == 7) {
                                ticket.status = 'Trainings Scheduled'
                            } else if (ticket.status == 8) {
                                ticket.status = 'Awaiting response'
                            } else if (ticket.status == 9) {
                                ticket.status = 'Suspended Training'
                            } else if (ticket.status == 10) {
                                ticket.status = 'Escalated'
                            }

                            ticket.updated_at = moment(ticket.updated_at, 'DD-MM-YYYY HH:mm').format('DD-MM-YYYY');
                            ticket.created_at = moment(ticket.created_at, 'DD-MM-YYYY HH:mm').format('DD-MM-YYYY');


                            //ticket.updated_at = moment(ticket.updated_at, 'YYYY-MM-DDTHH:mm:ss[Z]').format('YYYY-MM-DD');

                            //var temp1 = _.find($scope.ticketsClosedArray, function (trainer) { return moment(trainer.POSTED_DATES, 'YYYY-MM-DD').format('YYYY-MM-DD') === moment(ticket.updated_at, 'YYYY-MM-DDTHH:mm:ss[Z]').format('YYYY-MM-DD') &&(ticket.status=='Resolved'||ticket.status=='Closed') });

                            //if (temp1) {
                            //    temp1.COUNT_BY_DATES++;

                            //} else {
                            //    $scope.ticketsClosedArray.push({
                            //        POSTED_DATES: ticket.updated_at.split('T')[0],
                            //        COUNT_BY_DATES: 1,
                            //        MODULE_NAME: ticket.status
                            //    });


                            //}

                            //var temp2 = _.find($scope.ticketsClosedArray, function (trainer) { return moment(trainer.POSTED_DATES, 'YYYY-MM-DD').format('YYYY-MM-DD') === moment(ticket.updated_at, 'YYYY-MM-DDTHH:mm:ss[Z]').format('YYYY-MM-DD') });

                            //if (temp2) {
                            //    temp2.COUNT_BY_DATES++;

                            //} else {
                            //    $scope.ticketsClosedArray.push({
                            //        POSTED_DATES: ticket.updated_at.split('T')[0],
                            //        COUNT_BY_DATES: 1,
                            //        MODULE_NAME: ticket.status
                            //    });


                            //}
                        })

                        let tempArray = [];
                        tempArray = _(ticketsClosed).groupBy('status')
                            .map((objs, key) => {
                                return _(objs).groupBy('updated_at').map((objs1, key1) => ({
                                    'MODULE_NAME': key,
                                    'COUNT_BY_DATES': objs1.length,
                                    'POSTED_DATES': key1,
                                    'NAME': 'Closed'
                                })).value();

                            }).value();
                        let tempArray1 = [];
                        tempArray1 = _(ticketsClosed).groupBy('status')
                            .map((objs, key) => {
                                return _(objs).groupBy('created_at').map((objs1, key1) => ({
                                    'MODULE_NAME': key,
                                    'COUNT_BY_DATES': objs1.length,
                                    'POSTED_DATES': key1,
                                    'NAME': 'Raised'
                                })).value();

                            }).value();
                        $scope.ticketsResolvedArray = [].concat.apply([], tempArray);
                        $scope.ticketsArray = [].concat.apply([], tempArray1);
                        $scope.ticketsResolvedArray = $scope.ticketsResolvedArray.filter(function (item) { return item.MODULE_NAME === 'Closed' || item.MODULE_NAME === 'Resolved'});
                        $scope.ticketsArray = $scope.ticketsArray.filter(function (item) { return item.MODULE_NAME !== '' });


                        $scope.years4 = [];
                        $scope.years5 = [];
                        $scope.resolvedDetailsChartShow = false;
                        $scope.resolvedDetailsArray.series = [];

                        $scope.ticketsResolvedArray.forEach(function (obj, index) {
                            obj.YEARS = moment(obj.POSTED_DATES, 'DD-MM-YYYY').format('YYYY');
                            obj.MONTHS = moment(obj.POSTED_DATES, 'DD-MM-YYYY').format('MMMM');
                            obj.MONTH_YEAR = obj.MONTHS.concat(obj.YEARS);
                        });
                        $scope.ticketsArray.forEach(function (obj, index) {
                            obj.YEARS = moment(obj.POSTED_DATES, 'DD-MM-YYYY').format('YYYY');
                            obj.MONTHS = moment(obj.POSTED_DATES, 'DD-MM-YYYY').format('MMMM');
                            obj.MONTH_YEAR = obj.MONTHS.concat(obj.YEARS);
                        });
                        var years = returnOnlyYears($scope.ticketsResolvedArray);
                        years.forEach(function (year) {
                            $scope.years4.push('Closed-' + year);
                        });
                        var years1 = returnOnlyYears($scope.ticketsArray);
                        years1.forEach(function (year) {
                            $scope.years4.push('Raised-' + year);
                        });
                        $scope.allTicketsArray = _.unionBy($scope.ticketsResolvedArray, $scope.ticketsArray);
                        if ($scope.years4.length > 0) {
                            $scope.years4.forEach(function (item, index) {
                                var obj =
                                {
                                    name: item,
                                    data: []
                                };
                                if (item.indexOf('Closed') > -1) {
                                    $scope.resolvedDetailsArray.series.push(obj);
                                    fillDataValues($scope.ticketsResolvedArray, item.split('-')[1], index, $scope.resolvedDetailsArray);
                                } else if (item.indexOf('Raised') > -1) {
                                    $scope.resolvedDetailsArray.series.push(obj);
                                    fillDataValues($scope.ticketsArray, item.split('-')[1], index, $scope.resolvedDetailsArray);
                                }

                            });

                        }
                        $scope.resolvedDetailsChartShow = true;


                        // graph end

                    }
                })
            }
            $scope.GetAssignetTickets();

            $scope.precisionRound = function (number, precision) {
                var factor = Math.pow(10, precision);
                return Math.round(number * factor) / factor;
            };
        }]);