﻿prmApp
    .controller('comparativesCtrl', ["$scope", "$state", "$log", "$stateParams", "userService", "auctionsService", "$window",
        "$timeout", "reportingService", "PRMCustomFieldService",
        function ($scope, $state, $log, $stateParams, userService, auctionsService, $window, $timeout, reportingService, PRMCustomFieldService) {
        $scope.reqId = $stateParams.reqID;
        $scope.isUOMDifferent = false;
        $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
        if (!$scope.isCustomer) {
            $state.go('home');
        };

        $scope.userId = userService.getUserId();
        $scope.sessionid = userService.getUserToken();
        $scope.listRequirementTaxes = [];
        $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
        $scope.uomDetails = [];
            $scope.requirementSettings = [];
            $scope.selectedTemplate = {};
            $scope.prmFieldMappingDetails = {};
        $scope.subIemsColumnsVisibility = {
            hideSPECIFICATION: true,
            hideQUANTITY: true,
            hideTAX: true,
            hidePRICE: true
        };

       

            $scope.handleUOMItems = function (item) {
                if (item) {
                    item.reqVendors.forEach(function (vendor, index) {
                        var uomDetailsObj = {
                            itemId: item.itemID,
                            vendorId: vendor.vendorID,
                            containsUOMItem: false
                        };

                        if (vendor.quotationPrices) {
                            uomDetailsObj.containsUOMItem = item.productQuantityIn.toUpperCase() != vendor.quotationPrices.vendorUnits.toUpperCase();
                        }

                        $scope.uomDetails.push(uomDetailsObj);
                    });
                }
            };

            $scope.containsUOMITems = function (vendorId) {
                var filteredUOMitems = _.filter($scope.uomDetails, function (uomItem) { return uomItem.vendorId == vendorId && uomItem.containsUOMItem == true; });
                if (filteredUOMitems && filteredUOMitems.length > 0) {
                    return true;
                }

                return false;
            };

        $scope.ReqReportForExcel = [];
            $scope.GetReqReportForExcel = function () {
                reportingService.GetReqReportForExcel($scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        $scope.ReqReportForExcel = response;
                        //$scope.ReqReportForExcel.currentDate = new moment($scope.ReqReportForExcel.currentDate).format("DD-MM-YYYY HH:mm");
                        //$scope.ReqReportForExcel.postedOn = new moment($scope.ReqReportForExcel.postedOn).format("DD-MM-YYYY HH:mm");
                        //$scope.ReqReportForExcel.startTime = new moment($scope.ReqReportForExcel.startTime).format("DD-MM-YYYY HH:mm");
                        // // // #INTERNATIONALIZATION-0-2019-02-12-0-2019-02-12
                        $scope.ReqReportForExcel.currentDate = userService.toLocalDate($scope.ReqReportForExcel.currentDate);
                        $scope.ReqReportForExcel.postedOn = userService.toLocalDate($scope.ReqReportForExcel.postedOn);
                        $scope.ReqReportForExcel.startTime = userService.toLocalDate($scope.ReqReportForExcel.startTime);

                        $scope.ReqReportForExcel.reqItems.forEach(function (item, index) {
                            $scope.handleUOMItems(item);
                            if (item.reqVendors && item.reqVendors.length > 0) {
                                item.reqVendors.forEach(function (vendor, index) {
                                    if (vendor.quotationPrices.productQuotationTemplateJson !== '') {
                                        vendor.quotationPrices.productQuotationTemplateArray1 = JSON.parse(vendor.quotationPrices.productQuotationTemplateJson);
                                    }
                                });
                            }
                        });
                    });
            };

            $scope.GetReqReportForExcel();


        $scope.doPrint = false;

            $scope.printReport = function () {
                $scope.doPrint = true;
                $timeout(function () {
                    $window.print();
                    $scope.doPrint = false
                }, 1000);
            };


        $scope.htmlToCanvasSaveLoading = false;

        $scope.htmlToCanvasSave = function (format) {
            $scope.htmlToCanvasSaveLoading = true;
            setTimeout(function () {
                try {
                    var name = "Comparisions-ReqID-" + $scope.reqId + "." + format;
                    var canvas = document.createElement("canvas");
                    if (format == 'pdf') {
                        document.getElementById("widget").style["display"] = "";
                    }
                    else {
                        document.getElementById("widget").style["display"] = "inline-block";
                    }

                    html2canvas($("#widget"), {
                        onrendered: function (canvas) {
                            theCanvas = canvas;

                            const a = document.createElement("a");
                            a.style = "display: none";
                            a.href = canvas.toDataURL();

                            // Add Image to HTML
                            //document.body.appendChild(canvas);

                            /* Save As PDF */
                            if (format == 'pdf') {
                                var imgData = canvas.toDataURL();
                                var pdf = new jsPDF();
                                pdf.addImage(imgData, 'JPEG', 0, 0);
                                pdf.save(name);
                            }
                            else {
                                a.download = name;
                                a.click();
                            }

                            // Clean up 
                            //document.body.removeChild(canvas);
                            document.getElementById("widget").style["display"] = "";
                            $scope.htmlToCanvasSaveLoading = false;
                        }
                    });
                }
                catch (err) {
                    document.getElementById("widget").style["display"] = "";
                    $scope.htmlToCanvasSaveLoading = false;
                }
                finally {

                }

            }, 500);

        };

            $scope.getTotalTax = function (quotation) {
                if (quotation) {
                    if (!quotation.cGst || quotation.cGst == undefined) {
                        quotation.cGst = 0;
                    }

                    if (!quotation.sGst || quotation.sGst == undefined) {
                        quotation.sGst = 0;
                    }

                    if (!quotation.iGst || quotation.iGst == undefined) {
                        quotation.iGst = 0;
                    }

                    return quotation.cGst + quotation.sGst + quotation.iGst;

                } else {
                    return 0;
                }
            };

            $scope.isOnlySpecificationTemplate = function (productQuotationTemplateArray) {
                var isOnlySpecificatoin = true;
                if (productQuotationTemplateArray.length > 0) {
                    var items = _.filter(productQuotationTemplateArray, function (item) { return item.HAS_PRICE; });
                    if (items && items.length > 0) {
                        isOnlySpecificatoin = false;
                    }
                }

                return isOnlySpecificatoin;
            };

            $scope.handleSubItemsVisibility = function (productQuotationTemplateArray) {
                if (productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_SPECIFICATION === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hideSPECIFICATION = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_QUANTITY === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hideQUANTITY = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_TAX === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hideTAX = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_PRICE === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hidePRICE = false;
                    }
                }
            };



            $scope.detectLinks = function urlify(text) {
                var urlRegex = /(https?:\/\/[^\s]+)/g;
                return text.replace(urlRegex, function (url) {
                    return '<a target="_blank" href="' + url + '">' + url + '</a>';
                });
            };


            $scope.getRequirementSettings = function () {
                $scope.requirementSettings = [];
                $scope.selectedTemplate.TEMPLATE_NAME = 'PRM_DEFAULT';
                auctionsService.getRequirementSettings({ "reqid": $scope.reqId, "sessionid": $scope.sessionid })
                    .then(function (response) {
                        $scope.requirementSettings = response;
                        if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                            var template = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'TEMPLATE_ID';
                            });

                            if (template && template.length > 0) {
                                $scope.selectedTemplate.TEMPLATE_ID = template[0].REQ_SETTING_VALUE;
                            }
                        }

                        if ($scope.selectedTemplate.TEMPLATE_ID || $scope.selectedTemplate.TEMPLATE_NAME) {
                            $scope.GetPRMTemplateFields();
                        }
                    });
            };

            $scope.getRequirementSettings();

            $scope.GetPRMTemplateFields = function () {
                $scope.prmFieldMappingDetails = {};
                var params = {
                    "templateid": $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0,
                    "templatename": $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : '',
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });
                });
            };
    }]);