prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('formCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService", "$http", "$window", "domain", "fileReader", "growlService", "$log", "$filter", "ngDialog", "techevalService", "fwdauctionsService", "catalogService",
        function ($state, $stateParams, $scope, auctionsService, userService, $http, $window, domain, fileReader, growlService, $log, $filter, ngDialog, techevalService, fwdauctionsService, catalogService) {

            if ($stateParams.Id) {
                $scope.stateParamsReqID = $stateParams.Id;
                $scope.postRequestLoding = true;
            } else {
                $scope.stateParamsReqID = 0;
                $scope.postRequestLoding = false;
            };
            $scope.itemPreviousPrice = {};
            $scope.itemPreviousPrice.lastPrice = -1;
            $scope.bestPriceEnable = 0;
            $scope.companyItemUnits = [];

            $scope.SelectedVendorsCount = 0;
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
          //  alert("in form catalogue");
            $scope.isTechEval = false;
            $scope.isForwardBidding = false;
            $scope.allCompanyVendors = [];

            var curDate = new Date();
            var today = moment();
            var tomorrow = today.add('days', 1);
            //var dateObj = $('.datetimepicker').datetimepicker({
            //    format: 'DD/MM/YYYY',
            //    useCurrent: false,
            //    minDate: tomorrow,
            //    keepOpen: false
            //});
            $scope.subcategories = [];
            $scope.sub = {
                selectedSubcategories: [],
            }
            $scope.selectedCurrency = {};
            $scope.currencies = [];
            $scope.questionnaireList = [];

            //$scope.postRequestLoding = false;
            $scope.selectedSubcategories = [];



            $scope.selectVendorShow = true;
            $scope.isEdit = false;
            //Input Slider
            this.nouisliderValue = 4;
            this.nouisliderFrom = 25;
            this.nouisliderTo = 80;
            this.nouisliderRed = 35;
            this.nouisliderBlue = 90;
            this.nouisliderCyan = 20;
            this.nouisliderAmber = 60;
            this.nouisliderGreen = 75;

            //Color Picker
            this.color = '#03A9F4';
            this.color2 = '#8BC34A';
            this.color3 = '#F44336';
            this.color4 = '#FFC107';

            $scope.Vendors = [];
            $scope.VendorsTemp = [];
            $scope.categories = [];
            $scope.selectedA = [];
            $scope.selectedB = [];
            $scope.showCategoryDropdown = false;
            $scope.checkVendorPhoneUniqueResult = false;
            $scope.checkVendorEmailUniqueResult = false;
            $scope.checkVendorPanUniqueResult = false;
            $scope.checkVendorTinUniqueResult = false;
            $scope.checkVendorStnUniqueResult = false;
            $scope.showFreeCreditsMsg = false;
            $scope.showNoFreeCreditsMsg = false;
            $scope.formRequest = {
                isTabular: true,
                auctionVendors: [],
                listRequirementItems: [],
                isQuotationPriceLimit: false,
                quotationPriceLimit: 0,
                quotationFreezTime: '',
                deleteQuotations: false,
                expStartTimeName: '',
                isDiscountQuotation: 0,
                isRevUnitDiscountEnable: 0,
                multipleAttachments: [],
                contractStartTime: '',
                contractEndTime: '',
                isContract: false
            };
            $scope.Vendors.city = "";
            $scope.Vendors.quotationUrl = "";
            $scope.vendorsLoaded = false;
            $scope.requirementAttachment = [];


            $scope.sessionid = userService.getUserToken();

            $scope.selectedQuestionnaire == {}

            $scope.formRequest.indentID = 0;

            $scope.cijList = [];

            $scope.indentList = [];

            $scope.compID = userService.getUserCompanyId();
            //console.log('------------------------------------------$scope.compID');
            //console.log($scope.compID);
            //console.log('------------------------------------------$scope.compID');
            //if ($scope.compID == 0) {
            //    location.reload();
            //}

            auctionsService.GetCompanyConfiguration($scope.compID, "ITEM_UNITS", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = unitResponse;
                });

            $scope.getPreviousItemPrice = function (itemDetails, index) {
                $scope.itemPreviousPrice = {};
                $scope.itemPreviousPrice.lastPrice = -1;
                $scope.bestPriceEnable = index;
                $log.info(itemDetails);
                itemDetails.sessionID = userService.getUserToken();
                itemDetails.compID = userService.getUserCompanyId();
                auctionsService.getPreviousItemPrice(itemDetails)
                    .then(function (response) {
                        if (response && response.errorMessage == '') {
                            $scope.itemPreviousPrice.lastPrice = Number(response.initialPrice);
                            // // #INTERNATIONALIZATION-0-2019-02-12
                            $scope.itemPreviousPrice.lastPriceDate = userService.toLocalDate(response.currentTime);
                            $scope.itemPreviousPrice.lastPriceVendor = response.companyName;
                            $log.info($scope.itemPreviousPrice);
                        }
                    });
            };

            $scope.budgetValidate = function () {
                if ($scope.formRequest.budget != "" && (isNaN($scope.formRequest.budget) || $scope.formRequest.budget.indexOf('.') > -1)) {
                    $scope.postRequestLoding = false;
                    swal({
                        title: "Error!",
                        text: "Please enter valid budget, budget should be greater than 1,00,000.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {

                        });

                    $scope.formRequest.budget = "";
                }
            };

            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsAddNewReq.html', width: 1000, height: 500 });
            };

            $scope.changeCategory = function () {

                $scope.selectedSubCategoriesList = [];
                //$scope.formRequest.auctionVendors = [];
                $scope.loadSubCategories();
                $scope.getvendors();
            }

            $scope.getCreditCount = function () {
                userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.userDetails = response;
                        $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                        $scope.selectedCurrency = $scope.selectedCurrency[0];
                        if (response.creditsLeft) {
                            $scope.showFreeCreditsMsg = true;
                        } else {
                            $scope.showNoFreeCreditsMsg = true;
                        }
                    });
            }



            $scope.getvendors = function () {
                $scope.vendorsLoaded = false;
                var category = [];

                category.push($scope.formRequest.category);
                //$scope.formRequest.category = category;
                if ($scope.formRequest.category != undefined) {
                    var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendors',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                // if (allVendors === 1) {
                                // $scope.allCompanyVendors = response.data;
                                // allVendors = 0;
                                // }  
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }

                                $scope.VendorsTemp = $scope.Vendors;
                                $scope.searchVendors('');

                            }
                            //$scope.formRequest.auctionVendors =[];
                        } else {
                        }
                    }, function (result) {
                    });
                }

            };

            $scope.serchVendorString = '';

            $scope.searchGlobalVendors = function (str) {

                if (str == '' || str == null || str == undefined) {
                    $scope.globalVendors = [];

                } else {
                    str = String(str).toUpperCase();
                    $scope.globalVendors = $scope.VendorsTemp1.filter(function (vendor) {
                        return (String(vendor.vendorName).toUpperCase().includes(str) == true
                            || String(vendor.companyName).toUpperCase().includes(str) == true);
                    });
                }
                $scope.totalItems = $scope.globalVendors.length;
            }

            $scope.serchVendorString = '';

            $scope.getvendorswithoutcategories = function () {
                $scope.vendorsLoaded = false;
                //var category = [];

                // category.push($scope.formRequest.category);
                //$scope.formRequest.category = category;
                var params = { 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0, 'sessionID': userService.getUserToken() };
                $http({
                    method: 'POST',
                    url: domain + 'getvendorswithoutcategories',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.Vendors1 = response.data;
                            // if (allVendors === 1) {
                            // $scope.allCompanyVendors = response.data;
                            // allVendors = 0;
                            // }  
                            $scope.vendorsLoaded = true;
                            for (var j in $scope.formRequest.auctionVendors) {
                                for (var i in $scope.Vendors1) {
                                    if ($scope.Vendors1[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                        $scope.Vendors1.splice(i, 1);
                                    }
                                }
                            }

                            $scope.VendorsTemp1 = $scope.Vendors1;
                            $scope.searchVendorsAllCategory('');

                        }
                        //$scope.formRequest.auctionVendors =[];
                    } else {
                    }
                }, function (result) {
                });

            };
            $scope.getvendorswithoutcategories();

            $scope.getReqQuestionnaire = function () {


            }

            $scope.getQuestionnaireList = function () {
                techevalService.getquestionnairelist(0)
                    .then(function (response) {
                        $scope.questionnaireList = $filter('filter')(response, { reqID: 0 });
                        if ($stateParams.Id && $stateParams.Id > 0) {
                            techevalService.getreqquestionnaire($stateParams.Id, 1)
                                .then(function (response) {
                                    $scope.selectedQuestionnaire = response;

                                    if ($scope.selectedQuestionnaire && $scope.selectedQuestionnaire.evalID > 0) {
                                        $scope.isTechEval = true;
                                    }

                                    $scope.questionnaireList.push($scope.selectedQuestionnaire);
                                })
                        }
                    })
            };


            


            // $scope.getQuestionnaireList();

            /*$scope.getvendorsbysubcat = function () {
                $scope.vendorsLoaded = false;
                var category = [];
    
                category.push($scope.sub.selectedSubcategories);
                //$scope.formRequest.category = category;
                if ($scope.formRequest.category != undefined) {
                    var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId() };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendorsbycatnsubcat',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }
                            }
                            //$scope.formRequest.auctionVendors =[];
                        } else {
                        }
                    }, function (result) {
                    });
                }
    
            };*/

            $scope.isRequirementPosted = 0;


            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));
                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    } else {
                    }
                }, function (result) {
                });

            };


            $scope.getCurrencies = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getkeyvaluepairs?parameter=CURRENCY',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.currencies = response.data;
                            $scope.getCreditCount();
                        }
                    } else {
                    }
                }, function (result) {
                });
            };




            $scope.getData = function () {

                // $scope.getCategories();

                if ($stateParams.Id) {
                    var id = $stateParams.Id;
                    $scope.isEdit = true;

                    auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), "userid": userService.getUserId() })
                        .then(function (response) {

                            var category = response.category[0];
                            response.category = category;
                            response.taxes = parseInt(response.taxes);
                            //response.paymentTerms = parseInt(response.paymentTerms);
                            $scope.formRequest = response;

                            $scope.itemSNo = $scope.formRequest.itemSNoCount;

                            $scope.formRequest.checkBoxEmail = true;
                            $scope.formRequest.checkBoxSms = true;
                            $scope.loadSubCategories();

                            $scope.isRequirementPosted = $scope.formRequest.auctionVendors.length;
                            $scope.formRequest.selectedCurrency= response.currency;


                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }
                            $scope.formRequest.attFile = response.attachmentName;
                            if ($scope.formRequest.attFile != '' && $scope.formRequest.attFile != null && $scope.formRequest.attFile != undefined) {


                                var attchArray = $scope.formRequest.attFile.split(',');

                                attchArray.forEach(function (att, index) {

                                    var fileUpload = {
                                        fileStream: [],
                                        fileName: '',
                                        fileID: att
                                    };

                                    $scope.formRequest.multipleAttachments.push(fileUpload);
                                })

                            }


                            $scope.selectedSubcategories = response.subcategories.split(",");
                            for (i = 0; i < $scope.selectedSubcategories.length; i++) {
                                for (j = 0; j < $scope.subcategories.length; j++) {
                                    if ($scope.selectedSubcategories[i] == $scope.subcategories[j].subcategory) {
                                        $scope.subcategories[j].ticked = true;
                                    }
                                }
                            }
                            //$scope.getvendors();
                            $scope.selectSubcat();
                            $scope.formRequest.attFile = response.attachmentName;
                            //$scope.formRequest.deliveryTime = new moment($scope.formRequest.deliveryTime).format("DD-MM-YYYY");
                            // // #INTERNATIONALIZATION-0-2019-02-12
                            $scope.formRequest.quotationFreezTime = userService.toLocalDate($scope.formRequest.quotationFreezTime);
                            //$scope.formRequest.urgency.push(urgency);
                            // // #INTERNATIONALIZATION-0-2019-02-12
                            $scope.formRequest.expStartTime = userService.toLocalDate($scope.formRequest.expStartTime);

                            $scope.SelectedVendors = $scope.formRequest.auctionVendors;

                            $scope.SelectedVendorsCount = $scope.formRequest.auctionVendors.length;




                            $scope.postRequestLoding = false;
                        });
                }

            };

            $scope.showSimilarNegotiationsButton = function (value, searchstring) {
                $scope.showSimilarNegotiations = value;
                if (!value) {
                    $scope.CompanyLeads = {};
                }
                if (value) {
                    if (searchstring.length < 3) {
                        $scope.CompanyLeads = {};
                    }
                    if (searchstring.length > 2) {
                        $scope.searchstring = searchstring;
                        $scope.GetCompanyLeads(searchstring);
                    }
                }
                return $scope.showSimilarNegotiations;
            }

            $scope.showSimilarNegotiations = false;

            $scope.CompanyLeads = {};

            $scope.searchstring = '';

            $scope.GetCompanyLeads = function (searchstring) {
                if (searchstring.length < 3) {
                    $scope.CompanyLeads = {};
                }
                if ($scope.showSimilarNegotiations && searchstring.length > 2) {
                    $scope.searchstring = searchstring;
                    var params = { "userid": userService.getUserId(), "searchstring": $scope.searchstring, "searchtype": 'Title', "sessionid": userService.getUserToken() };
                    auctionsService.GetCompanyLeads(params)
                        .then(function (response) {
                            $scope.CompanyLeads = response;
                            $scope.CompanyLeads.forEach(function (item, index) {
                                // // #INTERNATIONALIZATION-0-2019-02-12
                                item.postedOn = userService.toLocalDate(item.postedOn);
                            })
                        });
                }
            }


            $scope.changeScheduledAuctionsLimit = function () {
                $scope.scheduledLimit = 8;
                $scope.getMiniItems();
            }

            $scope.loadSubCategories = function () {
                $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.formRequest.category });
                /*$scope.subcategories = _.map($scope.subcategories, 'subcategory');*/
            }

            $scope.selectedSubCategoriesList = [];

            $scope.selectSubcat = function (subcat) {

                $scope.selectedSubCategoriesList = [];

                if (!$scope.isEdit) {
                    $scope.formRequest.auctionVendors = [];
                }
                $scope.vendorsLoaded = false;
                var category = [];
                var count = 0;
                var succategory = "";
                $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
                selectedcount = $scope.sub.selectedSubcategories.length;
                if (selectedcount > 0) {
                    succategory = _.map($scope.sub.selectedSubcategories, 'id');
                    category.push(succategory);

                    $scope.selectedSubCategoriesList = succategory;

                    //$scope.formRequest.category = category;
                    if ($scope.formRequest.category != undefined) {
                        var params = { 'Categories': succategory, 'sessionID': userService.getUserToken(), 'count': selectedcount, 'uID': userService.getUserId(), evalID: $scope.selectedQuestionnaire ? $scope.selectedQuestionnaire.evalID : 0 };
                        $http({
                            method: 'POST',
                            url: domain + 'getvendorsbycatnsubcat',
                            encodeURI: true,
                            headers: { 'Content-Type': 'application/json' },
                            data: params
                        }).then(function (response) {
                            if (response && response.data) {
                                if (response.data.length > 0) {
                                    $scope.Vendors = response.data;
                                    $scope.vendorsLoaded = true;
                                    for (var j in $scope.formRequest.auctionVendors) {
                                        for (var i in $scope.Vendors) {
                                            if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                                $scope.Vendors.splice(i, 1);
                                            }
                                        }
                                    }

                                    $scope.VendorsTemp = $scope.Vendors;
                                    $scope.searchVendors('');

                                }
                                //$scope.formRequest.auctionVendors =[];
                            } else {
                            }
                        }, function (result) {
                        });
                    }
                } else {
                    $scope.getvendors();
                }

            }

            $scope.getData();

            $scope.checkVendorUniqueResult = function (idtype, inputvalue) {


                if (idtype == "PHONE") {
                    $scope.checkVendorPhoneUniqueResult = false;
                } else if (idtype == "EMAIL") {
                    $scope.checkVendorEmailUniqueResult = false;
                }
                else if (idtype == "PAN") {
                    $scope.checkVendorPanUniqueResult = false;
                }
                else if (idtype == "TIN") {
                    $scope.checkVendorTinUniqueResult = false;
                }
                else if (idtype == "STN") {
                    $scope.checkVendorStnUniqueResult = false;
                }

                if (inputvalue == "" || inputvalue == undefined) {
                    return false;
                }

                userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                    if (idtype == "PHONE") {
                        $scope.checkVendorPhoneUniqueResult = !response;
                    } else if (idtype == "EMAIL") {
                        $scope.checkVendorEmailUniqueResult = !response;
                    }
                    else if (idtype == "PAN") {
                        $scope.checkVendorPanUniqueResult = !response;
                    }
                    else if (idtype == "TIN") {
                        $scope.checkVendorTinUniqueResult = !response;
                    }
                    else if (idtype == "STN") {
                        $scope.checkVendorStnUniqueResult = !response;
                    }
                });
            };
            $scope.selectForA = function (item) {

                var item1 = item;
                $scope.VendorsTemp.forEach(function (v, vi) {
                    if (v.vendorID == item.vendorID) {
                        item1 = v;
                    }
                })

                var index = $scope.selectedA.indexOf(item1);
                if (index > -1) {
                    $scope.selectedA.splice(index, 1);
                } else {
                    $scope.selectedA.splice($scope.selectedA.length, 0, item1);
                }
                for (i = 0; i < $scope.selectedA.length; i++) {
                    $scope.formRequest.auctionVendors.push($scope.selectedA[i]);
                    $scope.Vendors.splice($scope.Vendors.indexOf($scope.selectedA[i]), 1);
                    $scope.VendorsTemp.splice($scope.VendorsTemp.indexOf($scope.selectedA[i]), 1);
                }
                $scope.reset();
            }


            $scope.selectFormGlobalSearch = function (vendor) {
                var error = false;


                $scope.formRequest.auctionVendors.forEach(function (SV, SVIndex) {
                    if (SV.vendorID == vendor.vendorID) {
                        error = true;
                        growlService.growl("Vendor already selected", "inverse");

                    }
                })

                if (error == true) {
                    return;
                }

                $scope.Vendors.forEach(function (UV, UVIndex) {
                    if (UV.vendorID == vendor.vendorID) {
                        error = true;
                        //growlService.growl("Please select vendor from fetched list by category", "inverse");
                    }
                })



                if (error == false) {
                    vendor.isFromGlobalSearch = true;
                    
                    $scope.formRequest.auctionVendors.push(vendor);
                }
                else {
                    $scope.selectForA(vendor);
                    //$scope.selectedB.splice($scope.formRequest.auctionVendors.indexOf($scope.selectedB[i]), 1);
                    vendor.isChecked = true;
                }
            }

            $scope.selectForB = function (item) {
                if (item && item.isFromGlobalSearch) {

                    $scope.searchGlobalVendors(item.vendorName);

                    $scope.formRequest.auctionVendors.splice($scope.formRequest.auctionVendors.indexOf(item), 1);

                    $scope.globalVendors.forEach(function (vendor, vendorIndex) {
                        if (vendor.vendorID = item.vendorID) {
                            vendor.isChecked = false;
                            vendor.isFromGlobalSearch = false;
                        }
                    })
                }
                else {
                    var index = $scope.selectedB.indexOf(item);
                    if (index > -1) {
                        $scope.selectedB.splice(index, 1);
                    } else {
                        $scope.selectedB.splice($scope.selectedA.length, 0, item);
                    }
                    for (i = 0; i < $scope.selectedB.length; i++) {
                        $scope.Vendors.push($scope.selectedB[i]);
                        $scope.VendorsTemp.push($scope.selectedB[i]);
                        $scope.formRequest.auctionVendors.splice($scope.formRequest.auctionVendors.indexOf($scope.selectedB[i]), 1);
                    }

                    $scope.searchGlobalVendors(item.vendorName);
                    $scope.globalVendors.forEach(function (ve, veIndex) {
                        if (ve.vendorID == item.vendorID) {
                            ve.isChecked = false;
                        }
                    })

                    $scope.reset();
                }
            }

            $scope.AtoB = function () {

            }

            $scope.BtoA = function () {

            }

            $scope.reset = function () {
                $scope.selectedA = [];
                $scope.selectedB = [];
            }

            // $scope.getFile = function () {
            //     $scope.progress = 0;
            //     fileReader.readAsDataUrl($scope.file, $scope)
            //     .then(function(result) {
            //         $scope.formRequest.attachment = result;
            //     });
            // };

            $scope.multipleAttachments = [];

            $scope.getFile = function () {
                $scope.progress = 0;

                //$scope.file = $("#attachement")[0].files[0];
                $scope.multipleAttachments = $("#attachement")[0].files;

                $scope.multipleAttachments = Object.values($scope.multipleAttachments)


                $scope.multipleAttachments.forEach(function (item, index) {

                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };

                            var bytearray = new Uint8Array(result);

                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;

                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }

                            $scope.formRequest.multipleAttachments.push(fileUpload);

                        });

                })

            };


            $scope.newVendor = {};
            $scope.Attaachmentparams = {};
            $scope.deleteAttachment = function (reqid) {
                $scope.Attaachmentparams = {
                    reqID: reqid,
                    userID: userService.getUserId()
                }
                auctionsService.deleteAttachment($scope.Attaachmentparams)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Attachment deleted Successfully", "inverse");
                            $scope.getData();
                        }
                    });
            }

            $scope.newVendor.panno = "";
            $scope.newVendor.vatNum = "";
            $scope.newVendor.serviceTaxNo = "";

            $scope.addVendor = function () {
                $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                $scope.mobileRegx = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
                $scope.panregx = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                //$scope.emailRegx = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
                var addVendorValidationStatus = false;
                $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                if ($scope.newVendor.companyName == "" || $scope.newVendor.companyName === undefined) {
                    $scope.companyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
                    $scope.firstvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
                    $scope.lastvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
                    $scope.contactvalidation = true;
                    addVendorValidationStatus = true;
                }
                //else if ($scope.newVendor.contactNum.length != 10) {
                //    $scope.contactvalidationlength = true;
                //    addVendorValidationStatus = true;
                //}
                else if ($scope.newVendor.contactNum.length < 10 || $scope.newVendor.contactNum.length > 15) {
                    $scope.contactvalidationlength = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
                    $scope.emailvalidation = true;
                    addVendorValidationStatus = true;
                }
                else if (!$scope.emailRegx.test($scope.newVendor.email)) {
                    $scope.emailregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vendorcurrency == "" || $scope.newVendor.vendorcurrency === undefined) {
                    $scope.vendorcurrencyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.panno != "" && $scope.newVendor.panno != undefined && !$scope.panregx.test($scope.newVendor.panno)) {
                    $scope.panregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vatNum != "" && $scope.newVendor.vatNum != undefined && $scope.newVendor.vatNum.length != 11) {
                    $scope.tinvalidation = true;
                    addVendorValidationStatus = true;
                }

                if ($scope.newVendor.serviceTaxNo != "" && $scope.newVendor.serviceTaxNo != undefined && $scope.newVendor.serviceTaxNo.length != 15) {
                    $scope.stnvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.formRequest.category == "" || $scope.formRequest.category === undefined) {
                    $scope.categoryvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.checkVendorEmailUniqueResult || $scope.checkVendorEmailUniqueResult) {
                    addVendorValidationStatus = true;
                }
                if (addVendorValidationStatus) {
                    return false;
                }
                var vendCAtegories = [];
                $scope.newVendor.category = $scope.formRequest.category;
                vendCAtegories.push($scope.newVendor.category);
                var params = {
                    "register": {
                        "firstName": $scope.newVendor.firstName,
                        "lastName": $scope.newVendor.lastName,
                        "email": $scope.newVendor.email,
                        "phoneNum": $scope.newVendor.contactNum,
                        "username": $scope.newVendor.contactNum,
                        "password": $scope.newVendor.contactNum,
                        "companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
                        "isOTPVerified": 0,
                        "category": $scope.newVendor.category,
                        "userType": "VENDOR",
                        "panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                        "stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                        "vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                        "referringUserID": userService.getUserId(),
                        "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                        "errorMessage": "",
                        "sessionID": "",
                        "userID": 0,
                        "department": "",
                        "currency": $scope.newVendor.vendorcurrency.key,
                        "altPhoneNum": $scope.newVendor.altPhoneNum,
                        "altEmail": $scope.newVendor.altEmail,
                        "subcategories": $scope.selectedSubCategoriesList
                    }
                };
                $http({
                    method: 'POST',
                    url: domain + 'register',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if ((response && response.data && response.data.errorMessage == "") || response.data.errorMessage == 'User assigned to Company.') {
                        $scope.formRequest.auctionVendors.push({ vendorName: $scope.newVendor.firstName + " " + $scope.newVendor.lastName, companyName: $scope.newVendor.companyName, vendorID: response.data.objectID });
                        $scope.newVendor = null;
                        $scope.newVendor = {};
                        //$scope.addVendorForm.$setPristine();
                        $scope.addVendorShow = false;
                        $scope.selectVendorShow = true;
                        $scope.newVendor = {};
                        $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                        growlService.growl("Vendor Added Successfully.", 'inverse');
                    } else if (response && response.data && response.data.errorMessage) {
                        growlService.growl(response.data.errorMessage, 'inverse');
                    } else {
                        growlService.growl('Unexpected Error Occurred', 'inverse');
                    }
                });
            }

            //$scope.checkboxModel = {
            //    value1: true
            //};

            $scope.formRequest.checkBoxEmail = true;
            $scope.formRequest.checkBoxSms = true;
            //$scope.postRequestLoding = false;
            $scope.formRequest.urgency = '';
            $scope.formRequest.deliveryLocation = '';
            $scope.formRequest.paymentTerms = '';
            $scope.formRequest.isSubmit = 0;


            $scope.titleValidation = $scope.attachmentNameValidation = false;
            $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
            $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

            $scope.postRequest = function (isSubmit, pageNo, navigateToView, stage) {

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation =
                $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation =
                $scope.questionnaireValidation = $scope.expNegotiationTimeValidation = false;

                $scope.postRequestLoding = true;
                $scope.formRequest.isSubmit = isSubmit;

                if (isSubmit == 1 || pageNo == 1) {
                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if (isSubmit == 1) {
                        if ($scope.formRequest.isTabular) {
                            $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                if (!item.attachmentName || item.attachmentName == '') {
                                    $scope.attachmentNameValidation = true;
                                    return false;
                                }
                            })

                        }
                        if (!$scope.formRequest.isTabular) {
                            if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                                $scope.descriptionValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                        }
                    }
                }

                if (isSubmit == 1) {
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                        $scope.paymentTermsValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                }

                if (isSubmit == 1) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    //var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    //var m = moment(ts);
                    //var quotationFreezTime = new Date(m);
                    //var CurrentDate = moment(new Date());


                    //var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    //var m = moment(ts);
                    //var expStartTime = new Date(m);
                    //var CurrentDate = moment(new Date());

                    //if (quotationFreezTime < CurrentDate) {
                    //    $scope.quotationFreezTimeValidation = true;
                    //    $scope.postRequestLoding = false;
                    //    return false;

                    //}

                    //if (quotationFreezTime >= expStartTime) {
                    //    $scope.expNegotiationTimeValidation = true;
                    //    $scope.postRequestLoding = false;
                    //    return false;

                    //}







                    //var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    //var m = moment(ts);
                    //var quotationFreezTime = new Date(m);

                    //var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    //var m = moment(ts);
                    //var expStartTime = new Date(m);

                    //auctionsService.getdate()
                    //    .then(function (GetDateResponse) {
                    //        var CurrentDate = moment(new Date(parseInt(GetDateResponse.substr(6))));

                    //        if (quotationFreezTime < CurrentDate) {
                    //            $scope.quotationFreezTimeValidation = true;
                    //            $scope.postRequestLoding = false;
                    //            return false;
                    //        }

                    //        if (quotationFreezTime >= expStartTime) {
                    //            $scope.expNegotiationTimeValidation = true;
                    //            $scope.postRequestLoding = false;
                    //            return false;
                    //        }

                    //    })

                   
                }

                if (!$scope.postRequestLoding) {
                    return false;
                }


                if (pageNo != 4) {
                    $scope.textMessage = "Save as Draft.";
                }

                if ($scope.formRequest.checkBoxEmail == true && $scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email and sms invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxEmail == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    $scope.textMessage = "This will send an SMS invite to all the vendors selected above.";
                }
                else if (pageNo == 4) {
                    $scope.textMessage = "This will not send an SMS or EMAIL to the vendors selected above.";
                }
                $scope.formRequest.currency = $scope.selectedCurrency.value;
                $scope.formRequest.timeZoneID = 190;
                $scope.postRequestLoding = false;
                swal({
                    title: "Are you sure?",
                    text: $scope.textMessage,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    $scope.postRequestLoding = true;
                    //var ts = userService.toUTCTicks($scope.formRequest.deliveryTime);
                    //var m = moment(ts);
                    //var deliveryDate = new Date(m);
                    //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    //$scope.formRequest.deliveryTime = "/Date(" + milliseconds + "000+0530)/";

                    // // #INTERNATIONALIZATION-0-2019-02-12
                    var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000+0530)/";



                    // // #INTERNATIONALIZATION-0-2019-02-12
                    var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";


                    //$scope.validatequotationFreezTime = function () {
                    //    if ($scope.formRequest.quotationFreezTime == $scope.formRequest.expStartTime && $scope.formRequest.quotationFreezTime > $scope.formRequest.expStartTime) {

                    //    }
                    //}


                    $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                    $scope.formRequest.customerID = userService.getUserId();
                    $scope.formRequest.customerFirstname = userService.getFirstname();
                    $scope.formRequest.customerLastname = userService.getLastname();
                    $scope.formRequest.isClosed = "NOTSTARTED";
                    $scope.formRequest.endTime = "";
                    $scope.formRequest.sessionID = userService.getUserToken();
                    $scope.formRequest.subcategories = "";
                    $scope.formRequest.budget = 100000;
                    $scope.formRequest.custCompID = userService.getUserCompanyId();
                    $scope.formRequest.customerCompanyName = userService.getUserCompanyId();

                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }
                    var category = [];
                    category.push($scope.formRequest.category);
                    $scope.formRequest.category = category;
                    if (!$scope.formRequest.isForwardBidding) {
                        $scope.formRequest.cloneID = 0;
                        if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                            $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;

                            $scope.formRequest.auctionVendors = _.filter($scope.formRequest.auctionVendors, function (x) { return x.companyName !== 'PRICE_CAP'; });

                            //$scope.formRequest.55

                        }
                        auctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {

                                    $scope.SaveReqDepartments(response.objectID);
                                    //$scope.SaveReqDeptDesig(response.objectID);

                                    //$scope.DeleteRequirementTerms();
                                    //$scope.SaveRequirementTerms(response.objectID);

                                    if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                        techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                            .then(function (response1) {
                                                $scope.selectedQuestionnaire = response1;

                                                if ($scope.isTechEval) {
                                                    $scope.selectedQuestionnaire.reqID = response.objectID;
                                                }
                                                else {
                                                    $scope.selectedQuestionnaire.reqID = 0;
                                                }
                                                $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                                var params = {
                                                    questionnaire: $scope.selectedQuestionnaire
                                                }
                                                techevalService.assignquestionnaire(params)
                                                    .then(function (response) {
                                                    })
                                            })
                                    }
                                    if (stage) {
                                        $state.go('save-requirement', { Id: response.objectID });
                                    }
                                    setTimeout(function () {
                                        swal({
                                            title: "Done!",
                                            text: "Requirement Saved Successfully",
                                            type: "success",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                        function () {
                                            //$scope.postRequestLoding = false;
                                            //$state.go('view-requirement');

                                            if (navigateToView && stage == undefined) {
                                                $state.go('view-requirement', { 'Id': response.objectID });
                                            } else {
                                                if ($scope.stateParamsReqID > 0 && stage == undefined) {
                                                    location.reload();
                                                }
                                                else {
                                                    if (stage == undefined) {
                                                        //$state.go('form.addnewrequirement', { 'Id': response.objectID });
                                                        $state.go('save-requirement', { Id: response.objectID });
                                                    }
                                                }
                                            }
                                        });
                                    }, 2000);
                                    





                                }
                            });
                    } else {
                        fwdauctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {

                                    //$scope.SaveReqDepartments(response.objectID);
                                    $scope.SaveReqDeptDesig(response.objectID);

                                    swal({
                                        title: "Done!",
                                        text: "Requirement Created Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            //$scope.postRequestLoding = false;
                                            //$state.go('view-requirement');
                                            $state.go('fwd-view-req', { 'Id': response.objectID });
                                        });
                                }
                            });
                    }
                });
                //$scope.postRequestLoding = false;
            };


            $scope.ItemFile = '';
            $scope.itemSNo = 1;
            $scope.ItemFileName = '';

            $scope.itemnumber = $scope.formRequest.listRequirementItems.length;

            $scope.requirementItems =
                {
                    productSNo: $scope.itemSNo++,
                    ItemID: 0,
                    productIDorName: '',
                    productNo: '',
                    productDescription: '',
                    productQuantity: 0,
                    productBrand: '',
                    othersBrands: '',
                    isDeleted: 0,
                    itemAttachment: '',
                    attachmentName: '',
                    itemMinReduction: 0
                }

            $scope.formRequest.listRequirementItems.push($scope.requirementItems);
            $scope.AddItem = function () {
                $scope.itemnumber = $scope.formRequest.listRequirementItems.length;
                $scope.requirementItems =
                    {
                        productSNo: $scope.itemSNo++,
                        ItemID: 0,
                        productIDorName: '',
                        productNo: '',
                        productDescription: '',
                        productQuantity: 0,
                        productBrand: '',
                        othersBrands: '',
                        isDeleted: 0,
                        itemAttachment: '',
                        attachmentName: '',
                        itemMinReduction: 0
                    }
                $scope.formRequest.listRequirementItems.push($scope.requirementItems);
            };

            $scope.deleteItem = function (SNo) {
                //$scope.formRequest.listRequirementItems[SNo].isDeleted = 0;
                //$scope.formRequest.listRequirementItems.splice(SNo, 1);
                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 1) {
                    $scope.formRequest.listRequirementItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.productSNo !== SNo; });
                };
            };



            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "itemsAttachment") {
                            if (ext != "xlsx") {
                                swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                                return;
                            }
                            var bytearray = new Uint8Array(result);
                            $scope.formRequest.itemsAttachment = $.makeArray(bytearray);
                            if (!$scope.isEdit) {
                                $scope.formRequest.listRequirementItems = [];
                            }

                            $scope.postRequest(0, 1, false);
                            //$scope.formRequest.itemsAttachmentName = $scope.file.name;
                        }

                        if (id != "itemsAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = _.indexOf($scope.formRequest.listRequirementItems, _.find($scope.formRequest.listRequirementItems, function (o) { return o.productSNo == id; }));
                            var obj = $scope.formRequest.listRequirementItems[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.formRequest.listRequirementItems.splice(index, 1, obj);
                        }
                    });
            }

            $scope.pageNo = 1;

            $scope.nextpage = function (pageNo) {
                $scope.tableValidation = false;
                $scope.tableValidationMsg = '';
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = departmentsValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.remindersTimeIntervalValidation = $scope.questionnaireValidation = false;

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation =
                    $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation =
                $scope.questionnaireValidation = $scope.expNegotiationTimeValidation = false;

                if (pageNo == 1 || pageNo == 4) {

                    if ($scope.reqDepartments == null || $scope.reqDepartments == undefined || $scope.reqDepartments.length == 0) {
                        $scope.GetReqDepartments();
                    }

                    if ($scope.currencies == null || $scope.currencies == undefined || $scope.currencies.length == 0) {
                        $scope.getCurrencies();
                    }

                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    if ($scope.formRequest.isTabular) {

                        $scope.formRequest.listRequirementItems.forEach(function (item, index) {

                            if ($scope.tableValidation) {
                                return false;
                            }

                            var sno = parseInt(index) + 1;

                            if (item.productIDorName == null || item.productIDorName == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product ID for item: ' + sno;
                                return;
                            }
                            //if (item.productNo == null || item.productNo == '') {
                            //    $scope.tableValidation = true;
                            //}
                            //if (item.productDescription == null || item.productDescription == '') {
                            //    $scope.tableValidation = true;
                            //}
                            if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product Quantity for item: ' + sno;
                                return;
                            }
                            if (item.productQuantityIn == null || item.productQuantityIn == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product Units for item: ' + sno;
                                return;
                            }
                            //if (item.productBrand == null || item.productBrand == '') {
                            //    $scope.tableValidation = true;
                            //}
                        });


                        if ($scope.tableValidation) {
                            return false;
                        }

                    }
                    if (!$scope.formRequest.isTabular) {
                        if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                            $scope.descriptionValidation = true;
                            $scope.postRequestLoding = false;
                            return false;
                        }
                    }


                    var phn = '';
                    if ($scope.userObj && $scope.userObj.phoneNum) {
                        phn = $scope.userObj.phoneNum;
                        $scope.formRequest.phoneNum = phn;
                        $scope.formRequest.contactDetails = phn;
                    }
                    else {
                        $scope.formRequest.phoneNum = '';
                        $scope.formRequest.contactDetails = '';
                    }



                }

                if (pageNo == 2 || pageNo == 4) {



                    if ($scope.questionnaireList == null || $scope.questionnaireList == undefined || $scope.questionnaireList.length == 0) {
                        $scope.getQuestionnaireList();
                    }

                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        var d = new Date();
                        d.setHours(18, 00, 00);
                        d = new moment(d).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.quotationFreezTime = d;

                    }


                    if ($scope.formRequest.expStartTime == null || $scope.formRequest.expStartTime == '') {
                        var dt = new Date();
                        var tomorrowNoon = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 1, 12, 0, 0);
                        tomorrowNoon = new moment(tomorrowNoon).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.expStartTime = tomorrowNoon;

                    }

                  //  $scope.$apply(function () {
                    //$scope.selectedCurrency =  $scope.formRequest.selectedCurrency;
                    if ($scope.formRequest.selectedCurrency) {
                        $scope.selectedCurrency = _.filter($scope.currencies, function (item) { return item.value.toUpperCase().indexOf($scope.formRequest.selectedCurrency.toUpperCase()) > -1; });
                        $scope.selectedCurrency = $scope.selectedCurrency[0];
                    }
                   // }); 

                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.formRequest.noOfQuotationReminders = 3;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.formRequest.remindersTimeInterval = 2;
                    }
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                        $scope.paymentTermsValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }


                    if ($scope.reqDepartments.length > 0) {

                        $scope.departmentsValidation = true;

                        $scope.reqDepartments.forEach(function (item, index) {
                            if (item.isValid == true || $scope.noDepartments == true)
                                $scope.departmentsValidation = false;
                        });

                        if ($scope.departmentsValidation == true) {
                            return false;
                        };
                    }

                }

                if (pageNo == 3 || pageNo == 4) {

                    if ($scope.categories == null || $scope.categories == undefined || $scope.categories.length == 0) {
                        $scope.getCategories();
                    }

                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }


                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= 0) && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }


                    //var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    //var m = moment(ts);
                    //var quotationFreezTime = new Date(m);
                    //var CurrentDate = moment(new Date());


                    //var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    //var m = moment(ts);
                    //var expStartTime = new Date(m);
                    //var CurrentDate = moment(new Date());
                   
                    //if (quotationFreezTime < CurrentDate) {
                    //    $scope.quotationFreezTimeValidation = true;
                    //    $scope.postRequestLoding = false;
                    //    return false;

                    //}

                    //if (quotationFreezTime >= expStartTime) {
                    //    $scope.expNegotiationTimeValidation = true;
                    //    $scope.postRequestLoding = false;
                    //    return false;

                    //}

                    var ts = moment($scope.formRequest.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var quotationFreezTime = new Date(m);

                    var ts = moment($scope.formRequest.expStartTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var expStartTime = new Date(m);

                    auctionsService.getdate()
                        .then(function (GetDateResponse) {
                            //var CurrentDate = moment(new Date(parseInt(GetDateResponse.substr(6))));

                            var CurrentDateToLocal = userService.toLocalDate(GetDateResponse);

                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));

                            if (quotationFreezTime < CurrentDate) {
                                $scope.quotationFreezTimeValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                            else
                                if (quotationFreezTime >= expStartTime) {
                                $scope.expNegotiationTimeValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                            $scope.pageNo = $scope.pageNo + 1;

                        })
                }
                if (pageNo == 4) {
                    $scope.formRequest.subcategoriesView = '';
                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategoriesView += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }
                    $scope.pageNo = $scope.pageNo + 1;
                }

                if (pageNo != 3) {
                    $scope.pageNo = $scope.pageNo + 1;
                }
                // return $scope.pageNo;
                // location.reload();
            }

            $scope.previouspage = function () {
                $scope.pageNo = $scope.pageNo - 1;
                // location.reload();
                //return $scope.pageNo;
            }

            $scope.gotopage = function (pageNo) {
                $scope.pageNo = pageNo;


                return $scope.pageNo;
            }


            $scope.reqDepartments = [];
            $scope.userDepartments = [];

            $scope.GetReqDepartments = function () {
                $scope.reqDepartments = [];
                auctionsService.GetReqDepartments(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDepartments = response;

                            $scope.noDepartments = true;

                            $scope.reqDepartments.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });

                        }
                    });
            }

            // $scope.GetReqDepartments();


            $scope.SaveReqDepartments = function (reqID) {

                $scope.reqDepartments.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDepartments,
                    sessionID: userService.getUserToken()
                };
                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                } else {
                    auctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                }


            };

            $scope.noDepartments = false;


            $scope.noDepartmentsFunction = function (value) {

                $scope.departmentsValidation = false;

                if (value == 'NA') {
                    $scope.reqDepartments.forEach(function (item, index) {
                        item.isValid = false;
                    });
                }
                else {
                    $scope.reqDepartments.forEach(function (item, index) {
                        if (item.isValid == true)
                            $scope.noDepartments = false;
                    });
                }
            };










            $scope.reqDeptDesig = [];

            $scope.GetReqDeptDesig = function () {
                $scope.reqDeptDesig = [];
                auctionsService.GetReqDeptDesig(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDeptDesig = response;
                            $scope.noDepartments = true;
                            $scope.reqDeptDesig.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });
                        }
                    });
            };

            // $scope.GetReqDeptDesig();


            $scope.SaveReqDeptDesig = function (reqID) {

                $scope.reqDeptDesig.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDeptDesig,
                    sessionID: userService.getUserToken()
                };

                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDeptDesig(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                } else {
                    auctionsService.SaveReqDeptDesig(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                }


            };

            $scope.AssignVendorToCompany = function (vendorPhone, vendorEmail) {

                if ($scope.newVendor.altPhoneNum == undefined) {
                    $scope.newVendor.altPhoneNum = '';
                }
                if ($scope.newVendor.altEmail == undefined) {
                    $scope.newVendor.altEmail = '';
                }

                var params = {
                    userID: userService.getUserId(),
                    vendorPhone: vendorPhone,
                    vendorEmail: vendorEmail,
                    sessionID: userService.getUserToken(),
                    category: $scope.formRequest.category,
                    subCategory: $scope.selectedSubCategoriesList,
                    altPhoneNum: $scope.newVendor.altPhoneNum,
                    altEmail: $scope.newVendor.altEmail
                };

                auctionsService.AssignVendorToCompany(params)
                    .then(function (response) {

                        $scope.vendor = response;

                        if ($scope.vendor.errorMessage == '' || $scope.vendor.errorMessage == 'VENDOR ADDED SUCCESSFULLY') {
                            $scope.formRequest.auctionVendors.push({ vendorName: $scope.vendor.firstName + ' ' + $scope.vendor.lastName, companyName: $scope.vendor.email, vendorID: $scope.vendor.userID });
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl('Vendor Added Successfully', "success");
                        }
                        else {
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl($scope.vendor.errorMessage, "inverse");
                        }
                    });
            };



            $scope.searchvendorstring = '';

            $scope.searchVendors = function (value) {
                $scope.Vendors = _.filter($scope.VendorsTemp, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // $scope.vendorsLoaded = true;
                // $scope.Vendors = _.filter($scope.allCompanyVendors, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // var temp = $scope.formRequest.auctionVendors;
                // $scope.formRequest.auctionVendors.forEach(function (item, index) {
                // $scope.Vendors = _.filter($scope.Vendors, function (vendor) { return vendor.vendorID !== item.vendorID; });
                // });
            }

            $scope.searchallvendors = '';

            $scope.searchVendorsAllCategory = function (value) {
                $scope.Vendors1 = _.filter($scope.VendorsTemp1, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // $scope.vendorsLoaded = true;
                // $scope.Vendors = _.filter($scope.allCompanyVendors, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // var temp = $scope.formRequest.auctionVendors;
                // $scope.formRequest.auctionVendors.forEach(function (item, index) {
                // $scope.Vendors = _.filter($scope.Vendors, function (vendor) { return vendor.vendorID !== item.vendorID; });
                // });
            }


            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'RequirementDetails',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                alasql('SELECT itemID as [ItemID], productIDorName as [ProductName], productNo as [ProductNumber], productDescription as [Description], productQuantity as [Quantity], productQuantityIn as [Units], productBrand as PreferredBrand, othersBrands as OtherBrands INTO XLSX(?,{headers:true,sheetid: "RequirementDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["RequirementDetails.xlsx", $scope.formRequest.listRequirementItems]);
            }


            $scope.cancelClick = function () {
                $window.history.back();
            }




            $scope.paymentRadio = false;
            $scope.paymentlist = [];

            $scope.addpaymentvalue = function () {
                var listpaymet =
                    {
                        reqTermsID: 0,
                        reqID: $scope.stateParamsReqID,
                        userID: userService.getUserId(),
                        reqTermsDays: 0,
                        reqTermsPercent: 0,
                        reqTermsType: 'PAYMENT',
                        paymentType: '+',
                        isRevised: 0
                    };
                $scope.paymentlist.push(listpaymet);
            };

            $scope.delpaymentvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }

                $scope.paymentlist.splice(val, 1);
            };

            $scope.resetpayment = function () {
                $scope.paymentlist = [];
            };




            $scope.deliveryRadio = false;
            $scope.deliveryList = [];

            $scope.adddeliveryvalue = function () {
                var deliveryObj =
                    {
                        reqTermsID: 0,
                        reqID: $scope.stateParamsReqID,
                        userID: userService.getUserId(),
                        reqTermsDays: 0,
                        reqTermsPercent: 0,
                        reqTermsType: 'DELIVERY',
                        isRevised: 0
                    };
                $scope.deliveryList.push(deliveryObj);
            };

            $scope.deldeliveryvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }
                $scope.deliveryList.splice(val, 1);
            };

            $scope.resetdelivery = function () {
                $scope.deliveryList = [];
            };










            $scope.listRequirementTerms = [];

            $scope.SaveRequirementTerms = function (reqID) {

                $scope.listRequirementTerms = [];

                $scope.deliveryList.forEach(function (item, index) {
                    item.reqID = reqID;
                    $scope.listRequirementTerms.push(item);
                });

                $scope.paymentlist.forEach(function (item, index) {

                    item.reqID = reqID;

                    if (item.paymentType == '-') {
                        item.reqTermsDays = -(item.reqTermsDays);
                    }

                    $scope.listRequirementTerms.push(item);
                });

                var params = {
                    "listRequirementTerms": $scope.listRequirementTerms,
                    sessionID: userService.getUserToken()
                };
                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                } else {
                    auctionsService.SaveRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                }


            };




            $scope.GetRequirementTerms = function () {
                auctionsService.GetRequirementTerms(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.listRequirementTerms = response;

                            $scope.listRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });



                        }
                    });
            }

            // $scope.GetRequirementTerms();




            $scope.listTerms = [];

            $scope.DeleteRequirementTerms = function () {

                var params = {
                    "listTerms": $scope.listTerms,
                    sessionID: userService.getUserToken()
                };

                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                } else {
                    auctionsService.DeleteRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                }
            };




            $scope.paymentTypeFunction = function (type) {
                if (type == 'ADVANCE') {

                }
                if (type == 'POST') {

                }
            }







            $scope.GetCIJList = function () {
                auctionsService.GetCIJList($scope.compID, $scope.sessionid)
                    .then(function (response) {
                        $scope.cijList = response;

                        $scope.cijList.forEach(function (item, index) {
                            var cijObj = $.parseJSON(item.cij);
                            item.cij = cijObj;
                        });


                    })
            }


            // $scope.GetCIJList();



            $scope.GetIndentList = function () {
                auctionsService.GetIndentList(userService.getUserCompanyId(), userService.getUserToken())
                    .then(function (response) {
                        $scope.indentList = response;
                    })
            }

            // $scope.GetIndentList();


            $scope.changeIndent = function (val) {
            };

            $scope.formRequest.category = '';

            $scope.goToStage = function (stage, currentStage) {
                $scope.postRequestLoding = true;
                $scope.tableValidation = $scope.validationFailed = false;
                if (stage == 2 && currentStage == 1) {

                }
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                        $scope.tableValidation = true;
                        $scope.validationFailed = true;
                    }
                });
                if ($scope.tableValidation) {
                    $scope.postRequestLoding = false;
                    return false;
                }

                if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                    $scope.titleValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }

                if (currentStage == 2 && stage > currentStage) {
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                        $scope.paymentTermsValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (currentStage == 3 && stage > currentStage) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }

                    if ($scope.formRequest.noOfQuotationReminders > 0) {
                        $scope.formRequest.noOfQuotationReminders = parseInt($scope.formRequest.noOfQuotationReminders);
                    }
                    if ($scope.formRequest.remindersTimeInterval > 0) {
                        $scope.formRequest.remindersTimeInterval = parseInt($scope.formRequest.remindersTimeInterval);
                    }


                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (!$scope.postRequestLoding) {
                    return false;
                } else {
                    $scope.postRequestLoding = true;
                    //var ts = userService.toUTCTicks($scope.formRequest.deliveryTime);
                    //var m = moment(ts);
                    //var deliveryDate = new Date(m);
                    //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    //$scope.formRequest.deliveryTime = "/Date(" + milliseconds + "000+0530)/";

                    // // #INTERNATIONALIZATION-0-2019-02-12
                    var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000+0530)/";

                    // // #INTERNATIONALIZATION-0-2019-02-12
                    var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";

                    $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                    $scope.formRequest.customerID = userService.getUserId();
                    $scope.formRequest.customerFirstname = userService.getFirstname();
                    $scope.formRequest.customerLastname = userService.getLastname();
                    $scope.formRequest.isClosed = "NOTSTARTED";
                    $scope.formRequest.endTime = "";
                    $scope.formRequest.sessionID = userService.getUserToken();
                    $scope.formRequest.subcategories = "";
                    $scope.formRequest.budget = 100000;
                    $scope.formRequest.custCompID = userService.getUserCompanyId();
                    $scope.formRequest.customerCompanyName = userService.getUserCompanyId();

                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }

                    var category = [];
                    if ($scope.formRequest.category) {
                        category.push($scope.formRequest.category);
                    }
                    $scope.formRequest.category = category;
                    if (!$scope.formRequest.isForwardBidding) {
                        $scope.formRequest.cloneID = 0;
                        if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                            $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                        }
                        auctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {

                                    $scope.SaveReqDepartments(response.objectID);
                                    //$scope.SaveReqDeptDesig(response.objectID);

                                    //$scope.DeleteRequirementTerms();
                                    // $scope.SaveRequirementTerms(response.objectID);

                                    if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                        techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                            .then(function (response1) {
                                                $scope.selectedQuestionnaire = response1;

                                                if ($scope.isTechEval) {
                                                    $scope.selectedQuestionnaire.reqID = response.objectID;
                                                }
                                                else {
                                                    $scope.selectedQuestionnaire.reqID = 0;
                                                }
                                                $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                                var params = {
                                                    questionnaire: $scope.selectedQuestionnaire
                                                }
                                                techevalService.assignquestionnaire(params)
                                                    .then(function (response) {
                                                    })
                                            })
                                    }
                                    if (stage) {
                                        $state.go('save-requirement', { Id: response.objectID });
                                    }
                                }
                            });
                    } else {
                        fwdauctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {

                                    //$scope.SaveReqDepartments(response.objectID);
                                    $scope.SaveReqDeptDesig(response.objectID);

                                    swal({
                                        title: "Done!",
                                        text: "Requirement Created Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            $scope.postRequestLoding = false;
                                            //$state.go('view-requirement');
                                            $state.go('fwd-view-req', { 'Id': response.objectID });
                                        });
                                }
                            });
                    }
                }
                //$scope.postRequest(0, stage - 1, false, stage);                
            }

            $scope.goBack = function (stage, reqID) {
                $state.go('save-requirement', { Id: reqID });
            }

            $scope.removeAttach = function (index) {
                $scope.formRequest.multipleAttachments.splice(index, 1);
            }

            $scope.productsList = [];
            $scope.getProducts = function () {
                catalogService.getProducts($scope.compID)
                    .then(function (response) {
                        $scope.productsListRaw = response;
                        $scope.productsListRaw.forEach(function (item, index) {
                            $scope.productsList.push(item.prodName);
                        });
                    });
            }
            $scope.getProducts();
            // ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];
            $scope.autofillProduct = function (prodName, index) {

                var output = [];
                if (prodName && prodName.trim() != '') {
                    angular.forEach($scope.productsList, function (prod) {
                        if (prod.toLowerCase().indexOf(prodName.trim().toLowerCase()) >= 0) {
                            output.push(prod);
                        }
                    });
                }
                $scope["filterProducts_" + index] = output;
            }
            $scope.fillTextbox = function (selProd, index) {
                $scope.formRequest.listRequirementItems[index].productIDorName = selProd;
                $scope['filterProducts_' + index] = null;
            }

            $scope.userDepartments = [];

            $scope.GetUserDepartments = function () {
                auctionsService.GetUserDepartments(userService.getUserId(), userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.userDepartments = response;
                            $log.info('$scope.userDepartments');
                            $log.info($scope.userDepartments);
                        }
                    })
            };

            $scope.GetUserDepartments();
        

        }]);