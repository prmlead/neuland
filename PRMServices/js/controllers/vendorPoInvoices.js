﻿prmApp
    .controller('vendorPoInvoicesCtrl', function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter) {

        console.log('1234');

        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        $scope.userID = userService.getUserId();
        $scope.compID = userService.getUserCompanyId();
        //$scope.customerCompanyId = userService.getCustomerCompanyId();
        $scope.sessionId = userService.getUserToken();
        $scope.invoiceComments = '';
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;
        $scope.invoiceDetails = [];
        $scope.searchKeyword = '';
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
            $scope.getInvoiceHeaderLevelList(($scope.currentPage - 1), 10, $scope.searchKeyword);
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };


        $scope.setFilters = function () {
            $scope.getInvoiceHeaderLevelList(($scope.currentPage - 1), 10, $scope.searchKeyword);
        }


        //$scope.filters = {
        //    searchKeyword: '',
        //    type: '',
        //    status: '',
        //    fromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
        //    toDate: moment().format('YYYY-MM-DD'),
        //}

        if ($stateParams.invoiceNumber == '') {
            $scope.filters = {
                searchKeyword: '',
                type: '',
                status: '',
                fromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
                toDate: moment().format('YYYY-MM-DD'),
            }
        } else {
            $scope.filters = {
                searchKeyword: $stateParams.invoiceNumber,
                type: '',
                status: '',
                fromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
                toDate: moment().format('YYYY-MM-DD'),
            }
        }

        if ($stateParams.fromDate == '' && $stateParams.toDate == '') {
            $scope.filters = {
                searchKeyword: '',
                type: '',
                status: '',
                fromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
                toDate: moment().format('YYYY-MM-DD'),
            }
        } else {
            $scope.filters = {
                searchKeyword: '',
                type: '',
                status: '',
                fromDate: $stateParams.fromDate,
                toDate: $stateParams.toDate,
            }
        }


        

        //$scope.getInvoiceList = function () {
        //    $scope.params = {
        //        "COMP_ID": !$scope.isVendor ? +$scope.compID : 0,
        //        "U_ID": !$scope.isVendor ? 0 : +$scope.userID,
        //        "fromDate": $scope.filters.fromDate,
        //        "toDate": $scope.filters.toDate,
        //        "PO_NUMBER": '',
        //        "INVOICE_NUMBER": ''
        //    }

        //    PRMPOService.getInvoiceList($scope.params)
        //        .then(function (response) {
        //            $scope.invoiceDetails = response;
        //            $scope.invoiceDetails.forEach(function (item) {
        //                item.INVOICE_DATE = new moment(item.INVOICE_DATE).format("DD-MM-YYYY");
        //            })
        //            $scope.totalItems = $scope.invoiceDetails.length;

        //        });
        //}
        //$scope.getInvoiceList();


        $scope.invStatus =
            [
            { "ID" : "" , "STATUS": "ALL" },
            { "ID": "SUBMITTED" , "STATUS": "SUBMITTED" },
            { "ID" : "REJECTED" , "STATUS": "REJECTED" }
            ];


        $scope.filters.STATUS = $scope.invStatus[0].ID;

        $scope.getInvoiceHeaderLevelList = function (recordsFetchFrom, pageSize,search) {
            

            $scope.params = {
                "COMP_ID": +$scope.compID,
                "U_ID": +$scope.userID,
                "fromDate": $scope.filters.fromDate,
                "toDate": $scope.filters.toDate,
                "search": search,
                "status": $scope.filters.STATUS,
                "PageSize": recordsFetchFrom * pageSize,
                "NumberOfRecords": pageSize
            }


            PRMPOService.getInvoiceList($scope.params)
                .then(function (response) {
                    $scope.invoiceDetails = response;
                    $scope.totalItems = 0;
                    if ($scope.invoiceDetails && $scope.invoiceDetails.length > 0) {
                        $scope.invoiceDetails.forEach(function (item) {
                            item.INVOICE_DATE = new moment(item.INVOICE_DATE).format("DD-MM-YYYY");
                            item.DATE_CREATED = new moment(item.DATE_CREATED).format("DD-MM-YYYY");

                        })
                        $scope.totalItems = $scope.invoiceDetails[0].TOTAL_COUNT;
                    }

                });
        }
        $scope.getInvoiceHeaderLevelList(0,10,$scope.filters.searchKeyword);



        $scope.createInvoice = function (invoice) {
            var url = $state.href("createInvoice", { "poNumber": invoice.PO_NUMBER, "invoiceNumber": invoice.INVOICE_NUMBER, "invoiceID": invoice.INVOICE_ID });
            $window.open(url, '_blank');
        };

        $scope.deleteInvoice = function (invoiceNumber) {
            $scope.invoice = invoiceNumber;
            swal({
                title: "Delete!",
                text: "Are you sure to delete the Invoice?",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            },
                function () {
                    $scope.params = {
                        "invoiceNumber": $scope.invoice,
                        "sessionID": userService.getUserToken()
                    }
                    PRMPOService.DeleteInvoice($scope.params)
                        .then(function (response) {
                            if (response.errorMessage == "") {
                               // growlService.growl("Invoice Deleted Successfully.", "success");
                                location.reload();
                            }
                        });
                });
        }

        $scope.sendForIntegration = function (invoice) {
            $scope.params = {
                "invoiceNumber": invoice.INVOICE_NUMBER,
                "poNumber": invoice.PO_NUMBER,
                "invoiceId": invoice.INVOICE_ID,
                "compid": $scope.compID,
                "user": $scope.userID,
                "sessionid": $scope.sessionId
            };

            PRMPOService.sendForInvoiceIntegration($scope.params)
                .then(function (response) {

                });
        }

        
        $scope.showInvoiceComments = function (invoice) {
            $scope.invoiceComments = invoice.COMMENTS;
        };

        $scope.showRejectionDetails = function (invoice)
        {
            //angular.element('#showRejectionDetails').modal('show');
            $scope.invoiceRejectionDetails = invoice;
        };

        $scope.closePopUp = function () {
            angular.element('#showRejectionDetails').modal('hide');
        };

        $scope.routetoPayment = function (obj) {

            $state.go("paymentDetails", { "invoiceNumber": obj})

        }
    });