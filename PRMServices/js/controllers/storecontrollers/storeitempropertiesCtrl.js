﻿prmApp
    .controller('storeitempropertiesCtrl', ["$scope", "$state", "$stateParams", "$window", "fileReader", "userService", "storeService", "growlService",
        function ($scope, $state, $stateParams, $window, fileReader, userService, storeService, growlService) {
            $scope.compID = $stateParams.compID;
            $scope.itemID = $stateParams.itemID;

            $scope.sessionID = userService.getUserToken();

            $scope.storeItemProperties = [];
            $scope.storeItemProperty = {
                custPropID: 0,
                companyID: $scope.compID,
                custPropCode: '',
                custPropDesc: '',
                custPropType: '',
                custPropDefault: '',
                entityID: $scope.itemID,
                custPropValue: '',
                moduleName: 'STORE',
                sessionID: $scope.sessionID
            };

            storeService.companypropvalues($scope.itemID)
                .then(function (response) {
                    $scope.storeItemProperties = response;
                });

            $scope.showSaveProperty = false;

            $scope.showSavePropertyFunction = function (val) {
                $scope.storeItemProperty = {
                    custPropID: 0,
                    companyID: $scope.compID,
                    custPropCode: '',
                    custPropDesc: '',
                    custPropType: '',
                    custPropDefault: '',
                    entityID: $scope.itemID,
                    custPropValue: '',
                    moduleName: 'STORE',
                    sessionID: $scope.sessionID
                };
                $scope.showSaveProperty = val;
            };

            $scope.savestoreitemprop = function (itemProperty, isEdit) {
                itemProperty.sessionID = $scope.sessionID;
                var params = {
                    "companyProp": itemProperty
                };
                storeService.savecompanyprop(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            // growlService.growl("Store Item Property Saved Successfully.", "success");
                            $scope.storeItemProperty.custPropID = response.objectID;
                            $scope.savestoreitempropvalue(itemProperty);
                        }
                    })
            };

            $scope.savestoreitempropvalue = function (storeItemProperty) {
                storeItemProperty.sessionID = $scope.sessionID;
                storeItemProperty.itemID = $scope.itemID;
                var params = {
                    companyProp: storeItemProperty
                };

                storeService.savecompanypropvalue(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                            storeItemProperty.custPropID = response.objectID;
                        }
                        else {
                            growlService.growl("Store Item Property Value Saved Successfully.", "success");
                            $scope.showSaveProperty = false;

                            var index = _.find($scope.storeItemProperties, function (o) { return o.custPropID == storeItemProperty.custPropID; });
                            if (!index) {
                                $scope.storeItemProperties.push(storeItemProperty);
                            }
                        }
                    })
            };

            $scope.cancelClick = function () {
                $window.history.back();
            };

            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'CompanyPropValues',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                alasql.fn.getstoreid = function (x) {
                    return x.storeID;
                }

                alasql('SELECT custPropID as [CUST_PROP_ID], companyID as [COMP_ID], custPropCode as [CUST_PROP_CODE], custPropDesc as [CUST_PROP_DESC], custPropType as [CUST_PROP_TYPE], custPropDefault as [CUST_PROP_DEFAULT], entityID as [ENTITY_ID], custPropValue as [CUST_PROP_VALUE], moduleName AS CUST_MODULE INTO XLSX(?,{headers:true,sheetid: "CompanyPropValues", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["StoreItemsPropValues.xlsx", $scope.storeItemProperties]);
            };

            $scope.entityObj = {
                entityName: 'CompanyPropValues',
                attachment: [],
                userid: userService.getUserId(),
                sessionID: $scope.sessionID
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (id == "excelItemProperties") {
                            var bytearray = new Uint8Array(result);
                            $scope.entityObj.attachment = $.makeArray(bytearray);
                            $scope.importEntity();
                        }
                    });
            };

            $scope.importEntity = function () {

                var params = {
                    "entity": $scope.entityObj
                };

                storeService.importentity(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Saved Successfully.", "success");
                            location.reload();

                        }
                    })
            };


            $scope.editstoreitemprop = function (storeItemProperty, valu) {
                $scope.storeItemProperty = storeItemProperty;
                $scope.showSaveProperty = true;

                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox

            };


        }]);