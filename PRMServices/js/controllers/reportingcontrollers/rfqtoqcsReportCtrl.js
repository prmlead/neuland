﻿
prmApp
    .controller('rfqtoqcsReportCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService", "PRMPRServices",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, PRMPRServices) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;
            $scope.rfqtoqcsReport = [];
            $scope.rfqtoqcsReportTemp = [];

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 8;
            $scope.revisedUserL1 = [];


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getrfqtoqcsReport(($scope.currentPage - 1), 10);

            };
            $scope.rfqtoqcsExcelReport = [];

            $scope.pageChanged = function () {
            };

            $scope.filtersList = {
                categoryList: [],
                buyerList: [],
                materialWiseList: [],
                vendorWiseList: []
            };

            $scope.filters = {
                category: {},
                buyer: {},
                materialwise: {},
                vendorwise: {},
                reportToDate: '',
                reportFromDate: ''
            };


            $scope.categoryList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];

            $scope.buyerList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            
            $scope.materialWiseList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.vendorWiseList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            //$scope.reportFromDate = '';
            //$scope.reportToDate = '';

            $scope.filters.reportToDate = moment().format('YYYY-MM-DD');
            $scope.filters.reportFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.downloadExcel = false;


            $scope.setFilters = function (currentPage) {
                $scope.downloadExcel = false;
                /*!_.isEmpty($scope.filters.category) ||*/
                if (!_.isEmpty($scope.filters.buyer) || !_.isEmpty($scope.filters.materialwise) ||
                    !_.isEmpty($scope.filters.vendorwise)) {
                    $scope.getrfqtoqcsReport(0, 10);
                }

            };

            $scope.getFilterValues = function () {
                var req_id;
                console.log("in call");
                if (_.isEmpty($scope.rfqtoqcsReport)) {
                    req_id = '';
                } else if ($scope.rfqtoqcsReport && $scope.rfqtoqcsReport.length > 0) {
                    var req_ids = _.uniq(_($scope.rfqtoqcsReport)
                        .filter(item => item.REQ_ID)
                        .map('REQ_ID')
                        .value());
                    req_id = req_ids.join(',');
                }

                var params =
                {
                    "compID": userService.getUserCompanyId(),
                    "reqid": req_id,
                    'fromdate': $scope.filters.reportFromDate,
                    'todate': $scope.filters.reportToDate
                };

                let categoryListTemp = [];
                let buyerListTemp = [];
                let materialWiseListTemp = [];
                let vendorWiseListTemp = [];
                console.log("in call");
                reportingService.getFilterValues(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.TYPE === 'CATEGORY') {
                                        categoryListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'BUYER') {
                                        buyerListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'MATERIAL') {
                                        materialWiseListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'VENDOR') {
                                        vendorWiseListTemp.push({ id: item.ID, name: item.NAME });
                                    }

                                });

                                $scope.filtersList.categoryList = categoryListTemp;
                                $scope.filtersList.buyerList = buyerListTemp;
                                $scope.filtersList.materialWiseList = materialWiseListTemp;
                                $scope.filtersList.vendorWiseList = vendorWiseListTemp;
                            }
                        }
                    });

            };



            $scope.getrfqtoqcsReport = function (recordsFetchFrom, pageSize) {
                var fromDate, toDate,category,buyer,material,vendor;

                if (_.isEmpty($scope.filters.reportFromDate)) {
                    fromDate = '';
                } else {
                    fromDate = $scope.filters.reportFromDate;
                }

                if (_.isEmpty($scope.filters.reportToDate)) {
                    toDate = '';
                } else {
                    toDate = $scope.filters.reportToDate;
                }

                if (_.isEmpty($scope.filters.category)) {
                    category = '';
                } else if ($scope.filters.category && $scope.filters.category.length > 0) {
                    var categories = _($scope.filters.category)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    category = categories.join(',');
                }

                if (_.isEmpty($scope.filters.buyer)) {
                    buyer = '';
                } else if ($scope.filters.buyer && $scope.filters.buyer.length > 0) {
                    var buyers = _($scope.filters.buyer)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    buyer = buyers.join(',');
                }

                if (_.isEmpty($scope.filters.materialwise)) {
                    material = '';
                } else if ($scope.filters.materialwise && $scope.filters.materialwise.length > 0) {
                    var materials = _($scope.filters.materialwise)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    material = materials.join(',');
                }

                if (_.isEmpty($scope.filters.vendorwise)) {
                    vendor = '';
                } else if ($scope.filters.vendorwise && $scope.filters.vendorwise.length > 0) {
                    var vendors = _($scope.filters.vendorwise)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    vendor = vendors.join(',');
                }

                var params = {
                    "userid": userService.getUserId(),
                    "sessionid": userService.getUserToken(),
                    "compID": userService.getUserCompanyId(),
                    "material": material,
                    "companyName": vendor,
                    "buyer": buyer,
                    "fromdate": fromDate,
                    "todate": toDate,
                    "PageSize": recordsFetchFrom * pageSize,
                    "NumberOfRecords": pageSize
                };

                $scope.pageSizeTemp = (params.PageSize + 1);
                //$scope.NumberOfRecords = recordsFetchFrom > 0 ? ((recordsFetchFrom + 1) * pageSize) : $scope.PRStats.totalPRs;
                $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);

                //var reportToDate = "/Date(" + milliseconds + "000+0530)/";

                reportingService.getRfqtoQcsReport(params)
                    .then(function (response) {
                        if ($scope.downloadExcel) {
                            if (response && response.length > 0) {
                                $scope.rfqtoqcsExcelReport = response;
                                $scope.rfqtoqcsExcelReport.forEach(function (item, index) {
                                    item.REQ_POSTED_ON = $scope.GetDateconverted(item.REQ_POSTED_ON);
                                    item.WORKFLOW_FINAL_APPROVER_DATE = $scope.GetDateconverted(item.WORKFLOW_FINAL_APPROVER_DATE);
                                    item.AWARDED_VENDOR = item.AWARDED_VENDOR ? item.AWARDED_VENDOR : '-';
                                    item.AWARDED_VENDOR_NAME = item.AWARDED_VENDOR_NAME ? item.AWARDED_VENDOR_NAME : '-';
                                });
                                $scope.rfqtoqcsDownloadExcel();
                            } else {
                                swal("Error!", "No records.", "error");
                                $scope.downloadExcel = false;
                            }
                        } else {
                            $scope.rfqtoqcsReport = response;
                            $scope.rfqtoqcsReportTemp = $scope.rfqtoqcsReport;

                            $scope.totalItems = $scope.rfqtoqcsReport.length > 0 ? $scope.rfqtoqcsReport[0].TOTAL_COUNT : 0;
                            $scope.rfqtoqcsReport.forEach(function (item, index) {
                                item.REQ_POSTED_ON = $scope.GetDateconverted(item.REQ_POSTED_ON);
                                item.WORKFLOW_FINAL_APPROVER_DATE = $scope.GetDateconverted(item.WORKFLOW_FINAL_APPROVER_DATE);
                                item.AWARDED_VENDOR = item.AWARDED_VENDOR ? item.AWARDED_VENDOR : '-';
                                item.AWARDED_VENDOR_NAME = item.AWARDED_VENDOR_NAME ? item.AWARDED_VENDOR_NAME : '-';
                            });
                        }

                        if ($scope.rfqtoqcsReport.length > 0) {
                            $scope.getFilterValues();
                        }

                    });
            };
            
            $scope.getrfqtoqcsReport(0, 10);

            $scope.GetReport = function () {
                $scope.downloadExcel = true;
                $scope.getrfqtoqcsReport(0, 0);
            };

            $scope.rfqtoqcsDownloadExcel = function () {


                //QCS_AWARDED_VALUE as [QCS value (as per L1)],
                 alasql('SELECT REQ_NUMBER as [RFQ No],QCS_ID as [Qcs Id],POSTED_BY as [Posted By],ITEMS_COUNT as [No. of materials], ' +
                    'TOTAL_PARTICIPATED_VENDORS as [No. of vendors participated],TOTAL_VENDORS as [No. of vendors invited],  ' +
                    'TOTAL_PROFIT_GAIN as [Budget vs Actual (RM category)], AWARDED_VENDOR as [Awarded Vendor], AWARDED_VENDOR_NAME as [Awarded vendor name],TOTAL_PROCUREMENT_VALUE_IN_REQUIRED_CURRENCY as [QCS Awarded value], ' +
                    'REQ_STATUS as [RFQ status],  REQ_POSTED_ON as [RFQ Posted date], ' +
                    'WORKFLOW_FINAL_APPROVER_DATE as [QCS approval date]' +
                    'INTO XLSX(?, { headers: true, sheetid: "RFQ_TO_QCS_REPORT", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["RFQ_TO_QCS_REPORT.xlsx", $scope.rfqtoqcsExcelReport]);


            }
            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };


        }]);