﻿prmApp
.controller('qcsCtrl', ["$scope", "$state", "$log", "$stateParams", "userService", "auctionsService", "$window",
    "$timeout", "reportingService", "growlService", "workflowService",
    function ($scope, $state, $log, $stateParams, userService, auctionsService, $window,
        $timeout, reportingService, growlService, workflowService) {

        $scope.reqId = $stateParams.reqID;
        $scope.qcsID = $stateParams.qcsID;
        $scope.isUOMDifferent = false;
        $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
        if (!$scope.isCustomer) {
            $state.go('home');
        };

        $scope.userId = userService.getUserId();
        $scope.sessionid = userService.getUserToken();

        $scope.editForm = false;

        $scope.listRequirementTaxes = [];
        $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
        $scope.uomDetails = [];

        /*region start WORKFLOW*/
        $scope.workflowList = [];
        $scope.itemWorkflow = [];
        $scope.workflowObj = {};
        $scope.workflowObj.workflowID = 0;
        $scope.currentStep = 0;
        $scope.orderInfo = 0;
        $scope.assignToShow = '';
        $scope.isWorkflowCompleted = false;
        $scope.WorkflowModule = 'QCS';
        $scope.disableWFSelection = false;
            /*region end WORKFLOW*/



        $scope.QCSDetails = {
            QCS_ID: 0,
            REQ_ID: $scope.reqId,
            U_ID: userService.getUserId(),
            QCS_CODE: '',
            PO_CODE: '',
            RECOMMENDATIONS: '',
            UNIT_CODE: ''
        };

        $scope.handleUOMItems = function (item) {
            if (item) {
                item.reqVendors.forEach(function (vendor, index) {
                    var uomDetailsObj = {
                        itemId: item.itemID,
                        vendorId: vendor.vendorID,
                        containsUOMItem: false
                    }

                    if (vendor.quotationPrices) {
                        uomDetailsObj.containsUOMItem = item.productQuantityIn.toUpperCase() != vendor.quotationPrices.vendorUnits.toUpperCase();
                    }

                    $scope.uomDetails.push(uomDetailsObj);
                });
            }
        }

        $scope.containsUOMITems = function (vendorId) {
            var filteredUOMitems = _.filter($scope.uomDetails, function (uomItem) { return uomItem.vendorId == vendorId && uomItem.containsUOMItem == true; });
            if (filteredUOMitems && filteredUOMitems.length > 0) {
                return true;
            }

            return false;
        }

        $scope.ReqReportForExcel = [];
        $scope.GetReqReportForExcel = function () {
            reportingService.GetReqReportForExcel($scope.reqId, userService.getUserToken())
                .then(function (response) {
                    $scope.ReqReportForExcel = response;
                    // // // #INTERNATIONALIZATION-0-2019-02-12-0-2019-02-12
                    $scope.ReqReportForExcel.currentDate = userService.toLocalDate($scope.ReqReportForExcel.currentDate);
                    $scope.ReqReportForExcel.postedOn = userService.toLocalDate($scope.ReqReportForExcel.postedOn);
                    $scope.ReqReportForExcel.startTime = userService.toLocalDate($scope.ReqReportForExcel.startTime);

                    $scope.ReqReportForExcel.reqItems.forEach(function (item, itemIndex) {

                        $scope.handleUOMItems(item)

                        //#region ITEM_LEVEL
                        item.reqVendors.forEach(function (vendor, vendorIndex) {
                            if (vendor.quotationPrices) {
                                if (item.itemID == vendor.quotationPrices.itemID) {
                                vendor.quotationPrices.Gst = vendor.quotationPrices.cGst + vendor.quotationPrices.sGst + vendor.quotationPrices.iGst;

                                }
                                vendor.quotationPrices.revGstAmount = 0;
                                vendor.quotationPrices.revGstAmount = ((vendor.quotationPrices.revUnitPrice / 100) * (vendor.quotationPrices.Gst));
                                vendor.quotationPrices.revItemPriceWithoutTax = 0;
                                vendor.quotationPrices.revItemPriceWithoutTax = ((vendor.quotationPrices.revUnitPrice) * (item.productQuantity));

                                vendor.packingChargesGstAmount = ((vendor.revpackingCharges / 100) * (vendor.packingChargesTaxPercentage));
                                vendor.freightChargesGstAmount = ((vendor.revfreightCharges / 100) * (vendor.freightChargesTaxPercentage));
                                vendor.installationChargesGstAmount = ((vendor.revinstallationCharges / 100) * (vendor.installationChargesTaxPercentage));

                            }
                        })
                        //#endregion ITEM_LEVEL

                        //#region SUB_TOTAL(Overall) and GST_AMOUNT(Item Level)

                        item.reqVendors.forEach(function (vendor, vendorIndex) {

                            //#region SUB_TOTAL_WITHOUT_TAX
                            if (item && item.reqVendors && item.reqVendors[vendorIndex] && item.reqVendors[vendorIndex].quotationPrices && item.reqVendors[vendorIndex].quotationPrices.revitemPrice) {
                                if ($scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorSubTotalWithoutGST > 0) { } else { $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorSubTotalWithoutGST = 0 }
                                $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorSubTotalWithoutGST += item.reqVendors[vendorIndex].quotationPrices.revUnitPrice * item.productQuantity;
                            }
                            //#endregion SUB_TOTAL_WITHOUT_TAX

                            //#region GST_AMOUNT_WITH_QUANTITY
                            if (item && item.reqVendors && item.reqVendors[vendorIndex] && item.reqVendors[vendorIndex].quotationPrices && item.reqVendors[vendorIndex].quotationPrices.revGstAmount) {
                                if ($scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorGstAmountWithQuantity > 0) { } else { $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorGstAmountWithQuantity = 0 }
                                $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorGstAmountWithQuantity += item.reqVendors[vendorIndex].quotationPrices.revGstAmount * item.productQuantity;
                            }
                            //#endregion GST_AMOUNT_WITH_QUANTITY

                            ////#region INITIAL_BASEPRICE
                            //if (item && item.reqVendors && item.reqVendors[vendorIndex] && item.reqVendors[vendorIndex].quotationPrices && item.reqVendors[vendorIndex].quotationPrices.unitPrice) {
                            //    if ($scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].initialVendorBasePrice > 0) { } else { $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].initialVendorBasePrice = 0 }
                            //    $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].initialVendorBasePrice += item.reqVendors[vendorIndex].quotationPrices.unitPrice;
                            //}
                            ////#endregion INITIAL_BASEPRICE

                            ////#region REV_BASEPRICE
                            //if (item && item.reqVendors && item.reqVendors[vendorIndex] && item.reqVendors[vendorIndex].quotationPrices && item.reqVendors[vendorIndex].quotationPrices.revUnitPrice) {
                            //    if ($scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorBasePrice > 0) { } else { $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorBasePrice = 0 }
                            //    $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorBasePrice += item.reqVendors[vendorIndex].quotationPrices.revUnitPrice;
                            //}
                            ////#endregion REV_BASEPRICE
                 
                            ////#region GST_AMOUNT_WITHOUT_QUANTITY
                            //if (item && item.reqVendors && item.reqVendors[vendorIndex] && item.reqVendors[vendorIndex].quotationPrices && item.reqVendors[vendorIndex].quotationPrices.revGstAmount) {
                            //    if ($scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorGstAmountWithoutQuantity > 0) { } else { $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorGstAmountWithoutQuantity = 0 }
                            //    $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorGstAmountWithoutQuantity += item.reqVendors[vendorIndex].quotationPrices.revGstAmount;
                            //}
                            ////#endregion GST_AMOUNT_WITHOUT_QUANTITY

                            ////#region SUB_TOTAL
                            //if (item && item.reqVendors && item.reqVendors[vendorIndex] && item.reqVendors[vendorIndex].quotationPrices && item.reqVendors[vendorIndex].quotationPrices.revitemPrice) {
                            //    if ($scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorSubTotal > 0) { } else { $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorSubTotal = 0 }
                            //    $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorSubTotal += item.reqVendors[vendorIndex].quotationPrices.revitemPrice;
                            //}
                            ////#endregion SUB_TOTAL

                        })

                        //#endregion SUB_TOTAL(Overall) and GST_AMOUNT(Item Level)

                        //#region VENDOR_NOM(SUB_TOTAL+FREIGHT+INSTALLATION+PACKING)
                        $scope.ReqReportForExcel.reqItems[0].reqVendors.forEach(function (vendor, vendorIndex) {
                            vendor.NOM = vendor.revVendorSubTotalWithoutGST +
                                            vendor.revpackingCharges +
                                            vendor.revfreightCharges +
                                            vendor.revinstallationCharges;
                        })
                        //#endregion VENDOR_NOM(SUB_TOTAL+FREIGHT+INSTALLATION+PACKING)

                        //#region VENDOR_GST_AMOUNT
                        $scope.ReqReportForExcel.reqItems[0].reqVendors.forEach(function (vendor, vendorIndex) {
                            vendor.totalTAX = vendor.revVendorGstAmountWithQuantity +
                                            vendor.packingChargesGstAmount +
                                            vendor.freightChargesGstAmount +
                                            vendor.installationChargesGstAmount;
                        })
                        //#endregion VENDOR_GST_AMOUNT

                    });
                });
        }

        $scope.GetReqReportForExcel();
        
        $scope.doPrint = false;

        $scope.printReport = function () {
            $scope.doPrint = true;
            $timeout(function () {
                $window.print();
                $scope.doPrint = false
            }, 1000);
        };

        $scope.calcuateDiscountedPrice = function (vendor, perc) {
            var QP = 0;
            var NP = 0;
            $scope.ReqReportForExcel.reqItems.forEach(function (item, vendorIndex) {
                var vendorPRices = item.reqVendors.filter(function (vendorTemp) {
                    return vendorTemp.vendorID === vendor.vendorID;

                });

                if (vendorPRices && vendorPRices.length > 0) {
                    QP = QP + vendorPRices[0].quotationPrices.unitPrice;
                    NP = NP + vendorPRices[0].quotationPrices.revUnitPrice;
                }
            });

            return ((QP - NP) / QP) * perc;
        };


        $scope.htmlToCanvasSaveLoading = false;

        $scope.htmlToCanvasSave = function (format) {
            $scope.htmlToCanvasSaveLoading = true;
            setTimeout(function () {
                try {
                    var name = "Comparisions-ReqID-" + $scope.reqId + "." + format;
                    var canvas = document.createElement("canvas");
                    if (format == 'pdf') {
                        document.getElementById("widget").style["display"] = "";
                    }
                    else {
                        document.getElementById("widget").style["display"] = "inline-block";
                    }

                    html2canvas($("#widget"), {
                        onrendered: function (canvas) {
                            theCanvas = canvas;

                            const a = document.createElement("a");
                            a.style = "display: none";
                            a.href = canvas.toDataURL();

                            // Add Image to HTML
                            //document.body.appendChild(canvas);

                            /* Save As PDF */
                            if (format == 'pdf') {
                                var imgData = canvas.toDataURL();
                                var pdf = new jsPDF();
                                pdf.addImage(imgData, 'JPEG', 0, 0);
                                pdf.save(name);
                            }
                            else {
                                a.download = name;
                                a.click();
                            }

                            // Clean up 
                            //document.body.removeChild(canvas);
                            document.getElementById("widget").style["display"] = "";
                            $scope.htmlToCanvasSaveLoading = false;
                        }
                    });
                }
                catch (err) {
                    document.getElementById("widget").style["display"] = "";
                    $scope.htmlToCanvasSaveLoading = false;
                }
                finally {

                }
            }, 500);

        };

        $scope.getTotalTax = function (quotation) {
            if (quotation) {
                if (!quotation.cGst || quotation.cGst == undefined) {
                    quotation.cGst = 0;
                }

                if (!quotation.sGst || quotation.sGst == undefined) {
                    quotation.sGst = 0;
                }

                if (!quotation.iGst || quotation.iGst == undefined) {
                    quotation.iGst = 0;
                }

                return quotation.cGst + quotation.sGst + quotation.iGst;

            } else {
                return 0;
            }
        }

        $scope.SaveQCSDetails = function () {

            if ($scope.QCSDetails.QCS_CODE == '' || $scope.QCSDetails.QCS_CODE == null || $scope.QCSDetails.QCS_CODE == undefined) {
                //growlService.growl('Please enter QCS Number.', "inverse");
                growlService.growl('Please enter PROJECT NAME', "inverse");
                return;
            }

            var params = {
                "qcsdetails": $scope.QCSDetails,
                "sessionid": userService.getUserToken()
            };

            params.qcsdetails.WF_ID = $scope.workflowObj.workflowID;

            if (!params.qcsdetails.WF_ID > 0) {
                growlService.growl('Please select Workflow', "inverse");
                return;
            }

            reportingService.SaveQCSDetails(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Details saved successfully.", "success");
                        $scope.goToQCSList($scope.reqId);
                    }
                })
        };

        $scope.checkIsFormDisable = function () {
            $scope.isFormdisabled = true;
            if ($scope.QCSDetails.U_ID == userService.getUserId()) {
                $scope.isFormdisabled = false;
            }
        };


        //if ($stateParams.qcsID > 0) {
        //   // $scope.checkIsFormDisable();
        //    $scope.getItemWorkflow();
        //}
        

        $scope.GetQCSDetails = function () {
            var params = {
                "qcsid": $stateParams.qcsID,
                "sessionid": userService.getUserToken()
            };
            reportingService.GetQCSDetails(params)
                .then(function (response) {
                    $scope.QCSDetails = response;
                    $scope.getItemWorkflow();
                })
        };

        if ($stateParams.qcsID > 0) {
            $scope.GetQCSDetails();
        };

        $scope.goToQCSList = function (reqID) {
            var url = $state.href("list-qcs", { "reqID": $scope.reqId });
            window.open(url, '_self');
        };
        
        /*region start WORKFLOW*/

        $scope.getWorkflows = function () {
            workflowService.getWorkflowList()
                .then(function (response) {
                    $scope.workflowList = [];
                    $scope.workflowListTemp = response;
                    $scope.workflowListTemp.forEach(function (item, index) {
                        if (item.WorkflowModule == $scope.WorkflowModule) {
                            $scope.workflowList.push(item);
                        }
                    });

                    if (userService.getUserObj().isSuperUser) {
                        $scope.workflowList = $scope.workflowList;
                    }
                    else {
                        $scope.workflowList = $scope.workflowList.filter(function (item) {
                            return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                        });
                    }



                });
        };

        $scope.getWorkflows();

        $scope.getItemWorkflow = function () {
            workflowService.getItemWorkflow(0, $stateParams.qcsID, $scope.WorkflowModule)
                .then(function (response) {
                    $scope.itemWorkflow = response;
                    if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                        $scope.currentStep = 0;

                        var count = 0;

                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

                            if (track.status == 'APPROVED' || track.status == 'HOLD') {
                                $scope.isFormdisabled = true;
                            }

                            if (track.status == 'APPROVED') {
                                $scope.isWorkflowCompleted = true;
                                $scope.orderInfo = track.order;
                                $scope.assignToShow = track.status;

                            }
                            else {
                                $scope.isWorkflowCompleted = false;
                            }



                            if (track.status == 'REJECTED' && count == 0) {
                                count = count + 1;
                            }

                            if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                                count = count + 1;
                                $scope.IsUserApproverForStage(track.approverID);
                                $scope.currentAccess = track.order;
                            }

                            if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                                $scope.currentStep = track.order;
                                return false;
                            }
                        });
                    }
                });

        };



        $scope.updateTrack = function (step, status) {
            $scope.disableAssignPR = true;
            $scope.commentsError = '';

            var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
            if (step.order == tempArray.order && status == 'APPROVED') {
                $scope.disableAssignPR = false;
            } else {
                $scope.disableAssignPR = true;
            }

            if ($scope.isReject) {
                $scope.commentsError = 'Please Save Rejected Items/Qty';
                return false;
            }

            if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
                $scope.commentsError = 'Please enter comments';
                return false;
            }

            step.status = status;
            step.sessionID = $scope.sessionID;
            step.modifiedBy = userService.getUserId();

            step.moduleName = $scope.WorkflowModule;

            workflowService.SaveWorkflowTrack(step)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        $scope.getItemWorkflow();
                        //location.reload();
                      //  $state.go('list-pr');
                    }
                })
        };

        $scope.assignWorkflow = function (moduleID) {
            workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                        $scope.isSaveDisable = false;
                    }
                    else {
                      //  $state.go('list-pr');
                    }
                })
        };

        $scope.IsUserApprover = false;

        $scope.functionResponse = false;

        $scope.IsUserApproverForStage = function (approverID) {
            workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                .then(function (response) {
                    $scope.IsUserApprover = response;
                });
        };

        $scope.isApproverDisable = function (index) {

            var disable = true;

            var previousStep = {};

            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                if (index == stepIndex) {
                    if (stepIndex == 0) {
                        if (userService.getSelectedUserDeptID() == step.department.deptID && userService.getSelectedUserDesigID() == step.approver.desigID &&
                            (step.status == 'PENDING' || step.status == 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                    else if (stepIndex > 0) {
                        if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
                            disable = true;
                        }
                        else if (userService.getSelectedUserDeptID() == step.department.deptID && userService.getSelectedUserDesigID() == step.approver.desigID &&
                            (step.status == 'PENDING' || step.status == 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                }
                previousStep = step;
            })

            return disable;
        };

    /*region end WORKFLOW*/




























}]);