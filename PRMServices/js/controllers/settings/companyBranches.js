﻿prmApp
    .controller('companyBranchesCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.companyBranches = [];
            $scope.branch = {
                userID: $scope.userID,
                branchID: 0,
                sessionID: $scope.sessionID
            };
            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/

            $scope.addNewBranchView = false;

            $scope.editBranch = function (companyBranch) {
                $scope.addNewBranchView = true;
                
                $scope.branch.sessionID = $scope.sessionID;
                $scope.branch.userID = $scope.userID;
                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox
            };

            $scope.closeEditBranch = function () {
                $scope.addNewBranchView = false;
                $scope.branch = {
                    userID: $scope.userID,
                    
                    sessionID: $scope.sessionID
                };
            };

        }]);