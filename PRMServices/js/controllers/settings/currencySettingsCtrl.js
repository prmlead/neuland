﻿prmApp
    .controller('currencySettingsCtrl', ["$scope", "userService", "auctionsService", "growlService", "$http", "domain",
        "$location", "$anchorScroll",
        function ($scope, userService, auctionsService, growlService, $http, domain,  $location, $anchorScroll) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            $scope.currencyFactors = [];
            $scope.currencyFactorAudits = [];
            $scope.displayCard = 0;
            $scope.minDateMoment = moment().subtract(0, 'day');
            $scope.currency = {
                userID: $scope.userID,
                sessionID: $scope.sessionID,
                currencyFactorID: 0,
                currencyCode: '',
                compID: $scope.compID,
                currencyFactor: '',
                startDate: '',
                endDate: '',
                fromDate: '',
                toDate: ''
            };

            $scope.currencyCodeDisabled = false;
            $scope.startDateDisabled = false;
            $scope.currencyEndDate = '';

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.totalItemAudits = 0;
            $scope.currentPage = 1;
            $scope.currentPage1 = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.isCurrEdit = {
                isEdit: 0
            }

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };


            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/


            $scope.currencies = [];
            $scope.addNewCurrencyView = false;
            $scope.GetCurrencyFactors = function () {
                auctionsService.GetCurrencyFactors($scope.userID, $scope.sessionID, $scope.compID)
                    .then(function (response) {
                        $scope.getCurrencies();
                        $scope.currencyFactors = [];
                        $scope.currencyFactorAudits = [];
                        response.forEach(function (item) {
                            item.fromDate = userService.toLocalDateOnly(item.startDate);
                            item.toDate = userService.toLocalDateOnly(item.endDate);
                            if (item.type === 'currencyfactor') {
                                $scope.currencyFactors.push(item);
                            } else if (item.type === 'currencyfactor_audit') {
                                $scope.currencyFactorAudits.push(item);
                            }
                        });

                        $scope.currencyFactors1 = $scope.currencyFactors;
                        $scope.totalItems = $scope.currencyFactors.length;
                        $scope.totalItemAudits = $scope.currencyFactorAudits.length;
                    });
            };

            $scope.GetCurrencyFactors();

            $scope.getCurrencyFactorAudit = function (currencyFactor) {
                $scope.currencyFactorAudits = [];
                auctionsService.GetCurrencyFactorAudit(currencyFactor.currencyCode, $scope.sessionID, $scope.compID)
                    .then(function (response) {
                        $scope.currencyFactorAudits = response;                        
                        $scope.currencyFactorAudits.forEach(function (item) {
                            item.fromDate = userService.toLocalDateOnly(item.startDate);
                            item.toDate = userService.toLocalDateOnly(item.endDate);
                            item.dateModified = userService.toLocalDateOnly(item.dateModified);
                            $scope.curr = item.currencyCode;
                        });
                        
                        $scope.totalItemAudits = $scope.currencyFactorAudits.length;

                        scrollToElement('currencyAudit');
                    });
            };
            //$scope.getCurrencyFactorAudit();

            $scope.editCurrencyFactor = function (currencyFactor, isEdit) {
                $scope.isCurrEdit.isEdit = isEdit;
                $scope.addNewCurrencyView = true;
                $scope.currency = currencyFactor;
                $scope.currency.sessionID = $scope.sessionID;
                $scope.currency.userID = $scope.userID;
                if (currencyFactor.currencyFactorID) {
                    $scope.currencyCodeDisabled = true;
                    $scope.startDateDisabled = false;
                } else {
                    $scope.currencyCodeDisabled = false;
                    $scope.startDateDisabled = false;
                }


                $scope.currencies.push({
                    value: currencyFactor.currencyCode
                });
                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox
            };

            $scope.getCurrencies = function () {
                $http({
                    method: 'get',
                    url: domain + 'getkeyvaluepairs?parameter=CURRENCY',
                    encodeuri: true,
                    headers: { 'content-type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.currencies = response.data;
                            $scope.currencies = $scope.currencies.filter(function (currency) {
                                return currency.value !== 'INR';
                            });

                            $scope.currencyFactors.forEach(function (currencyFactor) {
                                $scope.currencies = $scope.currencies.filter(function (currency) {
                                    return currency.value !== currencyFactor.currencyCode;
                                });
                            });
                        }
                    }
                });
            };

            $scope.SaveCurrencyFactor = function () {

                
                var existCurrency = _.filter($scope.currencyFactors, function (c) {
                    return c.currencyCode.toUpperCase() == $scope.currency.currencyCode.toUpperCase();
                   // return (String(c.currencyCode).toUpperCase().includes($scope.currency.currencyCode.toUpperCase()) == true);
                });

                if (existCurrency.length > 0 && $scope.isCurrEdit.isEdit == 0) {
                    growlService.growl("Currency Name already exists.", 'inverse');
                    return;
                }

                if (!$scope.currency.currencyCode) {
                    growlService.growl("Please select Currency Name.", 'inverse');
                    return;
                }

                if (!$scope.currency.currencyFactor) {
                    growlService.growl("Please enter Currency Code.", 'inverse');
                    return;
                }

                if (!$scope.currency.fromDate) {
                    growlService.growl("Please Select Valid Start Date.", 'inverse');
                    return;
                }

                if (!$scope.currency.toDate || moment($scope.currency.toDate, 'DD-MM-YYYY') < moment($scope.currency.fromDate, 'DD-MM-YYYY')) {
                    growlService.growl("Please Select Valid End Date.", 'inverse');
                    return;
                }

                var params = {
                    "currencyFactor": $scope.currency
                };

                var ts = params.currencyFactor.fromDate;
                var m = moment(ts, 'DD-MM-YYYY');
                var startDate = new Date(m);
                var milliseconds = parseInt(startDate.getTime() / 1000.0);
                params.currencyFactor.startDate = "/Date(" + milliseconds + "000+0530)/";


                var ts = params.currencyFactor.toDate;
                var m = moment(ts, 'DD-MM-YYYY');
                var endDate = new Date(m);
                var milliseconds = parseInt(endDate.getTime() / 1000.0);
                params.currencyFactor.endDate = "/Date(" + milliseconds + "000+0530)/";
                console.log(params);

                auctionsService.SaveCurrencyFactor(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("CurrencyFactor Saved Successfully.", "success");
                            //location.reload();
                            $scope.GetCurrencyFactors();
                            $scope.addNewCurrencyView = false;
                            $scope.currency = {
                                userID: $scope.userID,
                                sessionID: $scope.sessionID,
                                currencyFactorID: 0,
                                currencyCode: '',
                                compID: $scope.compID,
                                currencyFactor: '',
                                startDate: '',
                                endDate: '',
                                fromDate: '',
                                toDate: ''

                            };
                        }
                    });


            };

            $scope.closeCurrencyFactor = function () {
                $scope.addNewCurrencyView = false;
                $scope.currency = {
                    userID: $scope.userID,
                    sessionID: $scope.sessionID,
                    currencyFactorID: 0,
                    currencyCode: '',
                    compID: $scope.compID,
                    currencyFactor: '',
                    startDate: '',
                    endDate: '',
                    fromDate: '',
                    toDate: ''
                };
            };

            function scrollToElement(elementId) {
                $location.hash(elementId);
                $anchorScroll();
            }

            $scope.setFilters = function () {

                $scope.filterArray = [];
                $scope.currencyFactors = $scope.currencyFactors1;
                if ($scope.currencyEndDate) {
                    $scope.filterArray = $scope.currencyFactors.filter(function (item) {
                        return moment(item.toDate, 'DD-MM-YYYY') >= moment($scope.currencyEndDate, 'DD-MM-YYYY') &&
                            moment(item.fromDate, 'DD-MM-YYYY') <= moment($scope.currencyEndDate, 'DD-MM-YYYY');
                    });
                    $scope.currencyFactors = $scope.filterArray;
                    $scope.totalItems = $scope.currencyFactors.length;
                }

            };
        }]);

