﻿
prmApp
    .controller('activeUsersCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "$stateParams", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, $stateParams, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {
          
            $scope.userId = userService.getUserId();
            $scope.userObj = {
                //logoURL: '../js/resources/images/chart.png'
            };

            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            this.userdetails = {};
            $scope.sessionId = userService.getUserToken();
            $scope.companyID = userService.getUserCompanyId();

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.totalVendors = 0;
            $scope.totalSubuser = 0;
            $scope.totalInactiveVendors = 0;
            $scope.totalLeads = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 8;
            $scope.itemsPerPage2 = 8;
            $scope.maxSize = 8;

            $scope.activeUsers = [];
            $scope.activeBuyers = [];
            $scope.activeUsersOnline = [];
            $scope.GetUsersLoginStatus = [];

            $scope.reportToDate = moment().format('YYYY-MM-DD');
            $scope.reportFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                }
            };

            $scope.pageChanged = function () {
                //$scope.getAuctions();
            };


            $scope.onlineStatusObj = 'Not logged in';
            $scope.GetOnlineStatus = function (uID) {
                $http({
                    method: 'GET',
                    url: domain + 'getonlinestatus?sessionid=' + userService.getUserToken() + "&userid=" + uID,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.onlineStatusObj = response.data;
                })
            };

            // $scope.GetOnlineStatus();
            $scope.ErrorMsg = "";
            $scope.ErrorMsgBuyer = "";
            $scope.activeUsersTemp = [];
            $scope.GetActiveUsers = function () {
                auctionsService.GetActiveUsers($scope.companyID, $scope.sessionId)
                    .then(function (response) {
                        $scope.activeUsers = response;
                        $scope.activeUsers = _.filter($scope.activeUsers, function (item) {
                            return item.activeTime === "Online";
                        });
                        //$scope.activeUsersTemp = angular.copy($scope.activeUsers);
                        //$scope.activeUsers = [];
                        //$scope.activeUsersTemp.forEach(function (user, userIndex) {
                        //    if (user.activeTime === "Online") {
                        //        $scope.activeUsers.push(user);
                        //    }

                        //});
                        if ($scope.activeUsers.length == 0) {
                            return $scope.ErrorMsg = "No Vendors are in Online.";
                        } else {
                            return $scope.ErrorMsg = "";
                        }
                    });
            };

            $scope.activeBuyersTemp = [];
            $scope.GetActiveBuyers = function () {
                auctionsService.GetActiveBuyers($scope.userId, $scope.sessionId)
                    .then(function (response) {
                        $scope.activeBuyers = response;

                        $scope.activeBuyers = _.filter($scope.activeBuyers, function (item) {
                            return item.activeTime === "Online";
                        });
                        //$scope.activeBuyersTemp = angular.copy($scope.activeBuyers);
                        //$scope.activeBuyers = [];
                        //$scope.activeBuyersTemp.forEach(function (user, userIndex) {
                        //    if (user.activeTime == "Online") {
                        //        $scope.activeBuyers.push(user);
                        //    }

                        //})
                        if ($scope.activeBuyers.length == 0) {
                            return $scope.ErrorMsgBuyer = "No Buyers are in Online.";
                        } else {
                            return $scope.ErrorMsgBuyer = "";
                        }

                      
                    })
            }

            $scope.GetUsersLoginStatus = function () {
               
                auctionsService.GetUsersLoginStatus($scope.userId, $scope.reportFromDate, $scope.reportToDate, $scope.sessionId)
                    .then(function (response) {
                        $scope.GetUsersLoginStatus = response;

                        //$scope.GetUsersLoginStatus.forEach(function (item, itemindex) {

                        //    item.Da

                        //})
                       
                    })
            }
            $scope.GetUsersLoginStatus();

            $scope.getReport = function (name) {
                reportingService.GetUserLoginTemplates(name, $scope.reportFromDate, $scope.reportToDate, $scope.userId, $scope.sessionId);
            }



        }]);