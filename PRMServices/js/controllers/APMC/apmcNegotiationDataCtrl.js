﻿prmApp
    .controller('apmcNegotiationDataCtrl', ["$scope", "$stateParams", "$state", "$window", "$log", "$filter", "$http", "domain", "userService", "growlService", "apmcService", "SignalRFactory", "fileReader",
        function ($scope, $stateParams, $state, $window, $log, $filter, $http, domain, userService, growlService, apmcService, SignalRFactory, fileReader) {
            $scope.companyID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();
            $scope.currentID = userService.getUserId();
            $scope.isCustomer = userService.getUserType() == 'CUSTOMER';
            $scope.isSuperUser = userService.getUserObj().isSuperUser;

            $scope.quantityNegotiationID = 0;

            $scope.showInclusive = false;

            $scope.units = [
                { unitName: 'MT' },
                { unitName: 'BAGS' },
                { unitName: 'QUINTALS' }
            ]


            $scope.apmcnegotiation = {
                apmcNegotiationList: []
            };
            
            $scope.getapmcdashboard = function () {
                apmcService.getapmcnegotiation($stateParams.apmcnegID)
                    .then(function (response) {
                        $scope.apmcnegotiation = response;
                        $scope.quantityNegotiationID = $scope.apmcnegotiation.apmcInput.apmcNegotiationID;
                        $scope.vendorQuantityObj.price = $scope.apmcnegotiation.apmcNegotiationList[0].vendorPriceExclFinal;
                        if ($scope.vendorQuantityList.length == 0) {
                            $scope.vendorQuantityList.push($scope.vendorQuantityObj);
                        }
                        if ($scope.apmcnegotiation.apmcInput == null || $scope.apmcnegotiation.apmcInput.apmcNegotiationID == 0) {
                            $scope.showAllColumns = false;
                            //$scope.apmcnegotiation.apmcNegotiationList = $scope.apmcvendorlist;
                        }
                        else {
                            $scope.showAllColumns = true;
                            $scope.getapmcaudit($scope.apmcnegotiation.apmcInput.apmcNegotiationID, $scope.apmcnegotiation.apmcNegotiationList[0].vendor.userID);
                            if ($scope.apmcnegotiation.apmcNegotiationList[0].isNewNegotiationStarted == 1) {
                                apmcService.getapmcnegotiation($scope.apmcnegotiation.apmcNegotiationList[0].prevNegotiationID)
                                    .then(function (response2) {
                                        $scope.prevNegotiation = response2;
                                        $scope.quantityNegotiationID = $scope.prevNegotiation.apmcInput.apmcNegotiationID;
                                        $scope.vendorQuantityObj.apmcNegotiationID = $scope.quantityNegotiationID;
                                        $scope.vendorQuantityObj.price = $scope.prevNegotiation.apmcNegotiationList[0].vendorPriceExclFinal;
                                        if ($scope.vendorQuantityList.length == 0) {
                                            $scope.vendorQuantityList.push($scope.vendorQuantityObj);
                                        }
                                        $scope.getapmcaudit($scope.prevNegotiation.apmcInput.apmcNegotiationID, $scope.prevNegotiation.apmcNegotiationList[0].vendor.userID);
                                    })
                            }
                        }
                    });
            }

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "itemsAttachment") {
                            var bytearray = new Uint8Array(result);
                            $scope.formRequest.itemsAttachment = $.makeArray(bytearray);
                        }

                        if (id != "itemsAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = _.indexOf($scope.apmcnegotiation.apmcNegotiationList, _.find($scope.apmcnegotiation.apmcNegotiationList, function (o) { return o.apmcNegotiationID == id; }));
                            var obj = $scope.apmcnegotiation.apmcNegotiationList[index];
                            obj.fileStream = arrayByte;
                            obj.fileName = ItemFileName;
                            $scope.apmcnegotiation.apmcNegotiationList.splice(index, 1, obj);
                        }
                    });
            }

            $scope.saveapmcnegotiation = function (apmcPrice) {
                apmcPrice.userID = userService.getUserId();
                var params = {
                    apmcNegotiation: apmcPrice
                }
                apmcService.saveapmcnegotiation(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Saved Successfully.", "success");
                        }
                    })
            }

            $scope.getapmcaudit = function (apmcNegotiationID, vendorID) {
                apmcService.getapmcaudit(apmcNegotiationID, vendorID)
                    .then(function (response) {
                        $scope.apmcaudit = response;
                        $scope.apmcaudit.forEach(function (item, index) {
                            item.created = new moment(item.created).format("DD-MM-YYYY HH:mm:ss");
                        });
                    })
            }

            $scope.getapmcdashboard();

            
            $scope.gotovendorpurchasehistory = function (apmcnegid, vendorID) {
                //$state.go('apmcpurchasehistory', { apmcnegid: apmcnegid, vendorid: vendorID });

                var url = $state.href("apmcpurchasehistory", { apmcnegid: apmcnegid, vendorid: vendorID });
                window.open(url, '_blank');
            }

            $scope.goToAPMCView = function (apmcID) {
                //$state.go("saveapmc", { "apmcID": apmcID });

                var url = $state.href("viewapmc", { "apmcID": apmcID });
                window.open(url, '_blank');
            }
        }]);