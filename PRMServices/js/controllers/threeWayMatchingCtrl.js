﻿prmApp
    .controller('threeWayMatchingCtrl', function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter, workflowService) {

        
        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;
        $scope.userID = userService.getUserId();
        $scope.compId = userService.getUserCompanyId();
        $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;


        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
            $scope.GetthreeWayMatchingList(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };
        /*PAGINATION CODE*/

        $scope.filters = {
            fromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
            toDate: moment().format('YYYY-MM-DD'),
            type: '',
            searchKeyword:''
        };
        $scope.tableColumns = [];
        $scope.GetthreeWayMatchingList = function (recordsFetchFrom, pageSize, searchString) {

            if (_.isEmpty($scope.filters.fromDate)) {
                fromDate = '';
            } else {
                fromDate = $scope.filters.fromDate;
            }

            if (_.isEmpty($scope.filters.toDate)) {
                toDate = '';
            } else {
                toDate = $scope.filters.toDate;
            }

            if (_.isEmpty($scope.filters.type)) {
                type = '';
            } else {
                type = $scope.filters.type;
            }

            var params =
            {
                "companyId": $scope.isCustomer ? $scope.compId : 0,
                "uId": $scope.isCustomer ? 0 : $scope.userID,
                "sessionid": userService.getUserToken(),
                "searchString": searchString ? searchString : "",
                "PageSize": recordsFetchFrom * pageSize,
                "fromDate": fromDate,
                "toDate": toDate,
                "type": type,
                "NumberOfRecords": pageSize
            };

            PRMPOService.GetthreeWayMatchingList(params)
                .then(function (response) {
                    if (response) {
                        $scope.rows = [];
                        $scope.tableColumns = [];
                        $scope.totalItems = 0;
                        var arr = JSON.parse(response).Table;
                        if (arr && arr.length > 0) {
                            $scope.totalItems = arr[0].TOTAL_COUNT;
                            arr.forEach(a => delete a.TOTAL_COUNT);
                            $scope.tableColumnsTemp = angular.copy(_.keys(arr[0]));
                            $scope.tableColumnsTemp.forEach(function (item, index) {
                                item = item.replaceAll("_", " ");
                                var foundIndex = _.findIndex($scope.tableColumns, function (val) { return val === item; });
                                if (foundIndex <= -1) {
                                    $scope.tableColumns.push(item);
                                }
                            });
                            $scope.rows = arr;
                            arr.forEach(function (item, index) {
                               
                                var obj = angular.copy(_.values(item));
                                if (obj) {
                                    item.tableValues = [];
                                    obj.forEach(function (value, valueIndex) {
                                        item.tableValues.push(value);
                                    });
                                }
                            });
                        } else {
                            var arr = JSON.parse(response);
                            arr = arr.filter(function (a) { return a !== 'TOTAL_COUNT' });
                            $scope.tableColumnsTemp = arr;
                            $scope.tableColumnsTemp.forEach(function (item, index) {
                                item = item.replaceAll("_", " ");
                                $scope.tableColumns.push(item);
                            });
                        }

                    } else {
                        if ($scope.tableColumnsTemp && $scope.tableColumnsTemp.length > 0) {
                            $scope.tableColumnsTemp.forEach(function (item, index) {
                                item = item.replaceAll("_", " ");
                                var foundIndex = _.findIndex($scope.tableColumns, function (val) { return val === item; });
                                if (foundIndex <= -1) {
                                    $scope.tableColumns.push(item);
                                }
                            });
                        }
                        swal("Status!", "No Records Found..! Please Upload Some Data.", "error");
                    }
                });
        };

        $scope.GetthreeWayMatchingList(0, 10, '');

        $scope.filterByDate = function () {
            $scope.rows = [];
            $scope.tableColumns = [];
            $scope.totalItems = 0;
            $scope.GetthreeWayMatchingList(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);

        };

    });