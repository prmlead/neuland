﻿


prmApp.factory("realTimePriceService", ['httpServices', 'reatimepriceDomian', function (httpServices, reatimepriceDomian) {

    var factory = {};
    factory.uploadFile = function (data) {
        return httpServices.post(reatimepriceDomian + "realtimeprice/upload_price/", data)
    }

    factory.getPrices = function (data) {
        return httpServices.post(reatimepriceDomian + "realtimeprice/get_prices/", data)
    }

    factory.filterData = function (id) {
        return httpServices.get(reatimepriceDomian + "realtimeprice/get_filter_data/" + id)
    }

    factory.getReports = function (data) {
        return httpServices.post(reatimepriceDomian + "realtimeprice/get_report/", data)
    }


    factory.getSetting = function () {
        return httpServices.get(reatimepriceDomian + "realtimeprice/get_setting/")
    }

    factory.saveSetting = function (data) {
        return httpServices.post(reatimepriceDomian + "realtimeprice/save_setting/",data)
    }

    factory.sendNotification = function () {
        return httpServices.get(reatimepriceDomian + "realtimeprice/send_notification/")
    }

    return factory;

}])

prmApp.controller("RealTimePriceImportController", ['$uibModal', '$scope', 'SettingService', 'store', 'growlService', 'fileReader', 'realTimePriceService', 'AngularUtility', '$stateParams', function ($uibModal, $scope, SettingService, store, growlService, fileReader, realTimePriceService, AngularUtility, $stateParams) {

    var _realCtrl = this;

    _realCtrl.Attachements = [];


    _realCtrl.onFileSelect = function ($files) {

        if (_realCtrl.RealTimePriceType == "")
        {

            growlService.growl("please select type", "inverse");

            return false

        }
        var files = [];
        for (var i in $files) {
            fileReader.readAsDataUrl($files[i], $scope)
          .then(function (result) {
              var bytearray = new Uint8Array(result);
              var fileobj = {};
              fileobj.fileStream = $.makeArray(bytearray);
              fileobj.fileType = $files[i].type;
              fileobj.name = $files[i].name
              files.push(fileobj);

              var data = {
                  TypeId: _realCtrl.RealTimePriceType,
                  File: fileobj
              }
              realTimePriceService.uploadFile(data).then(function (response) {
                  if (response.errorMessage == "") {
                      growlService.growl(response.message, "inverse");
                  } else {
                      growlService.growl(response.errorMessage, "inverse")

                  }
              })
          });
        }
    }




    _realCtrl.sendNotification = function () {
        realTimePriceService.sendNotification().then(function (response) {
            if (response.errorMessage == "") {
                growlService.growl(response.message, "inverse");
            } else {
                growlService.growl(response.errorMessage, "inverse")

            }
        })
    }




}]);


prmApp.controller("RealTimePriceController", ['$uibModal', '$scope', 'SettingService', 'store', 'growlService', 'fileReader', 'realTimePriceService', 'AngularUtility', '$stateParams', function ($uibModal, $scope, SettingService, store, growlService, fileReader, realTimePriceService, AngularUtility, $stateParams) {

    var _realCtrl = this;

    _realCtrl.Attachements = [];

    _realCtrl.ReportFilter = {
        Product: "",
    }



    _realCtrl.onFileSelect = function ($files) {

        var files = [];
        for (var i in $files) {
            fileReader.readAsDataUrl($files[i], $scope)
          .then(function (result) {
              var bytearray = new Uint8Array(result);
              var fileobj = {};
              fileobj.fileStream = $.makeArray(bytearray);
              fileobj.fileType = $files[i].type;
              fileobj.name = $files[i].name
              files.push(fileobj);

              var data = {
                  TypeId: $stateParams.typeId,
                  File: fileobj
              }
              realTimePriceService.uploadFile(data).then(function (response) {
                  _realCtrl.getByPage(_realCtrl.tableState)
              })
          });
        }
    }

    _realCtrl.gridOpitions = {
        itemsByPage: 20,
        pageSizeOptions: [10, 20, 50, 100],
        displayedPages: 7,
        showFilter: false,
        data: [],
        totalItems: 0,
        loading: false,
        noData: true,
        searchTerm: "",
        enableFilter: true,
        columns: [
            {
                name: 'Log Level',
                field: 'LogLevelId',
                type: 'int',
            },
            {
                name: 'Short message',
                field: 'ShortMessage',
                type: 'string',

            },
            {
                name: 'Created On',
                field: 'CreatedOnUtc',
                type: 'date',
            },
            {
                name: 'IP Address',
                field: 'IpAddress',
                display: true,
                template: '<ip-address ng-model="row.LastIpAddress"></ip-address>'
            },
            {
                name: 'CustomerId',
                field: 'CustomerId',
                type: 'ObjectId',
            },

        ],
    }

    _realCtrl.tableState = {};
    _realCtrl.getByPage = function (table) {
        if (table == undefined)
            return false;

        _realCtrl.tableState = table;

        var pager = AngularUtility.preparePager(table, _realCtrl.gridOpitions.columns);

        console.log(pager)
        pager.criteria.filter.push({
            column: "TypeId",
            value: $stateParams.typeId
        })
        realTimePriceService.getPrices(pager).then(function (response) {
            var _jsonResposne = JSON.parse(response)
            if (_jsonResposne.Data != undefined) {
                _realCtrl.gridOpitions.data = _jsonResposne.Data;
                _realCtrl.gridOpitions.totalItems = _jsonResposne.TotalCount;
                table.pagination.totalItemCount = _jsonResposne.TotalCount;
                table.pagination.numberOfPages = _jsonResposne.TotalPages;
                _realCtrl.gridOpitions.loading = false;
            }
            else {
                _realCtrl.gridOpitions.data = [];
            }


        })
    }

    _realCtrl.FilterData = {};
    _realCtrl.getfilterData = function () {
        realTimePriceService.filterData($stateParams.typeId).then(function (response) {
            _realCtrl.FilterData = response;

            _realCtrl.ReportFilter.Product = _realCtrl.FilterData.Products[0];

            _realCtrl.applyReportFilter(_realCtrl.ReportFilter)
        })
    }

    //$scope.$watch("_realCtrl.ReportFilter.Product", function (newValue) {
    //    if (newValue != "" && newValue !== undefined) {
    //        _realCtrl.ReportFilter.Product = newValue
    //        _realCtrl.getReport(_realCtrl.ReportFilter);

    //    }

    //})

    //$scope.$watch("_realCtrl.ReportFilter.Market", function (newValue) {
    //    if (newValue != "" && newValue !== undefined) {
    //        _realCtrl.ReportFilter.Market = newValue
    //        _realCtrl.getReport(_realCtrl.ReportFilter);
    //    }
    //    else {
    //        _realCtrl.ReportFilter.Market = ""
    //    }


    //})

    //$scope.$watch("_realCtrl.ReportFilter.FromDate", function (newValue) {
    //    if (newValue !== undefined) {
    //        _realCtrl.ReportFilter.FromDate = newValue;

    //        if (_realCtrl.ReportFilter.ToDate !== undefined
    //          && _realCtrl.ReportFilter.FromData !== undefined)
    //            _realCtrl.getReport(_realCtrl.ReportFilter);
    //    }


    //})

    //$scope.$watch("_realCtrl.ReportFilter.ToDate", function (newValue) {
    //    if (newValue !== undefined) {
    //        _realCtrl.ReportFilter.ToDate = newValue
    //        if (_realCtrl.ReportFilter.ToDate !== undefined
    //            && _realCtrl.ReportFilter.FromData !== undefined)
    //            _realCtrl.getReport(_realCtrl.ReportFilter);
    //    }


    //});


    _realCtrl.applyReportFilter = function () {

        if (_realCtrl.ReportFilter.ToDate != undefined && _realCtrl.ReportFilter.FromDate == undefined)
        {
            growlService.growl("please provide from date ", "inverse");
            return false;
        }

        if (_realCtrl.ReportFilter.ToDate == undefined && _realCtrl.ReportFilter.FromDate != undefined) {
            growlService.growl("please provide to date ", "inverse");
            return false;

        }

        _realCtrl.getReport(_realCtrl.ReportFilter);
    }

    _realCtrl.clearReportFilter = function () {
        _realCtrl.ReportFilter = {
            Product: _realCtrl.FilterData.Products[0]
        }
        _realCtrl.getReport(_realCtrl.ReportFilter);
    }



    _realCtrl.getReport = function (data) {
        data.TypeId = $stateParams.typeId;
        realTimePriceService.getReports(data).then(function (response) {
            angular.forEach(response, function (item) {
                item[0] = new Date(parseInt(item[0].substr(6))).getTime()
            });
            loadChart(response)
        })

    }
    _realCtrl.sendNotification = function () {
        realTimePriceService.sendNotification().then(function (response) {
            if (response.errorMessage == "") {
                growlService.growl(response.message, "inverse");
                _realSettingCtrl.getfilterData();
            } else {
                growlService.growl(response.errorMessage, "inverse")

            }
        })
    }



    function loadChart(data) {
        Highcharts.chart('reportChart', {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: 'Product price over time'
            },

            xAxis: {
                type: 'datetime',

            },
            yAxis: {
                title: {
                    text: 'Product price'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                          [0, Highcharts.getOptions().colors[0]],
                          [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: 'Price',
                data: data
            }]
        });
    }
}]);


prmApp.controller("RealTimePriceSettingsController", ['$uibModal', '$scope', 'SettingService', 'store', 'growlService', 'fileReader', 'realTimePriceService', 'AngularUtility', function ($uibModal, $scope, SettingService, store, growlService, fileReader, realTimePriceService, AngularUtility) {

    var _realSettingCtrl = this;
    _realSettingCtrl.FilterData = {};
    _realSettingCtrl.gridOpitions = {
        displayedPages: 7,
        showFilter: false,
        data: [],
        totalItems: 0,
        loading: false,
        noData: true,
        searchTerm: "",
        enableFilter: true,

    }
    _realSettingCtrl.RealTimeSettings = {
        AllowedProducts : []
    }
    _realSettingCtrl.getfilterData = function () {
        realTimePriceService.getSetting().then(function (response) {
            _realSettingCtrl.RealTimeSettings = response;

            _realSettingCtrl.gridOpitions.data = _realSettingCtrl.RealTimeSettings.AllowedProducts;

        })
    }

    _realSettingCtrl.saveSetting = function () {
        var model = angular.copy(_realSettingCtrl.RealTimeSettings);
        model.AllowedProducts = model.AllowedProducts.filter(function (item) { return item.Active });
        realTimePriceService.saveSetting(model).then(function (response) {
            if (response.errorMessage == "") {
                growlService.growl(response.message, "inverse");
                _realSettingCtrl.getfilterData();
            } else {
                growlService.growl(response.errorMessage, "inverse")

            }
        })
    }






}]);


