﻿prmApp
    .controller('vendorInvoices', function ($scope, userService) {

        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        $scope.compID = userService.getUserCompanyId();
        $scope.userID = userService.getUserId();

        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;
        $scope.invoiceDetails = [];
        $scope.searchKeyword = '';
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
            $scope.getInvoiceList(($scope.currentPage - 1), 10, $scope.searchKeyword);
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };


        $scope.getInvoiceList = function () {
            $scope.params = {
                "COMP_ID": +$scope.compID,
                "U_ID": +$scope.userID,
                "fromDate": $scope.filters.fromDate,
                "toDate": $scope.filters.toDate,
                "search":""

            }

            PRMPOService.getInvoiceList($scope.params)
                .then(function (response) {
                    $scope.invoiceDetails = response;
                    $scope.invoiceDetails1 = $scope.invoiceDetails;
                    $scope.invoiceDetails.forEach(function (item) {
                        item.INVOICE_DATE = new moment(item.INVOICE_DATE).format("DD-MM-YYYY");
                    })
                    $scope.totalItems = $scope.invoiceDetails.length;

                });
        }
        $scope.getInvoiceList();
        $scope.invoiceDetails1 = [];
        

    });