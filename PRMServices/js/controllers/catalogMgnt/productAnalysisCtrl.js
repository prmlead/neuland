﻿//(function () { 
prmApp.controller('productAnalysisCtrl', ['$scope', '$state', '$window', '$stateParams', '$filter', 'catalogService', 'userService', 'growlService',
    "catalogReportsServices",
    function ($scope, $state, $window, $stateParams, $filter, catalogService, userService, growlService,
        catalogReportsServices) {

        $scope.stateParamsCatalogId = $stateParams.productId;

        $scope.callID = $stateParams.callID;

        $scope.CovertedDate = '';
        $scope.Name = 'No previous bids';

        $scope.show = false;
        $scope.data = [];
        $scope.categories = [];

        var today = moment();
        //$scope.FromDate = today.add('days', -30).format('YYYY-MM-DD');
        //today = moment().format('YYYY-MM-DD');
        //$scope.ToDate = today;

        $scope.currentSessionId = userService.getUserToken();
        $scope.currentUserCompID = userService.getUserCompanyId();
        $scope.currentUserId = +userService.getUserId();

        $scope.dateobj = {
            FromDate: today.add('days', -30).format('YYYY-MM-DD'),
            ToDate: moment().format('YYYY-MM-DD')
        };

        $scope.PROD_NAME = '';
        
        $scope.GetDateconverted = function (dateBefore) {

            var date = dateBefore.split('+')[0].split('(')[1];
            var newDate = new Date(parseInt(parseInt(date)));
            $scope.CovertedDate = newDate.toString().replace('Z', '');
            return $scope.CovertedDate;

        };

        // Pagination For User Products//
        $scope.userProductsPage = 0;
        $scope.userProductsPageSize = 200;
        $scope.userProductsfetchRecordsFrom = $scope.userProductsPage * $scope.userProductsPageSize;
        $scope.userProductstotalCount = 0;
        // Pagination For User Products//
        $scope.productIDorNameDisplay = false;
        $scope.productsList = [];
        $scope.autofillProduct = function (prodName, fieldType) {
            $scope['ItemSelected_'] = false;
            var output = [];
            if (prodName && prodName != '' && prodName.length > 0) {
                $scope.searchingUserProducts = angular.lowercase(prodName);
                $scope.userproductsLoading = true;
                $scope.getUserProducts(0, $scope.searchingUserProducts, fieldType);
            } else {
                $scope.productIDorNameDisplay = false;
                $scope.PROD_NAME = '';
            }
        };

        $scope.getUserProducts = function (IsPaging, searchString, fieldType) {

            catalogService.getUserProducts($scope.userProductsfetchRecordsFrom, $scope.userProductsPageSize, searchString ? searchString : "", false, false, false).then(function (response) {
                $scope.productsList = response;
                $scope.userproductsLoading = false;
                $scope.fillingProduct($scope.productsList, fieldType, searchString);
            });
        };

        $scope.fillingProduct = function (output, fieldType, searchString) {

            output = $scope.productsList.filter(function (product) {

                return product.isCoreProductCategory === 1;
            });


            if (fieldType == 'NAME') {
                $scope["filterProducts_"] = output;

            }

        };

        $scope.prodObj = {
            productIDorName: ''
        };

        $scope.fillTextbox = function (selProd) {
            $scope['ItemSelected_'] = true;
            $scope.PROD_NAME = selProd.prodName.toUpperCase();
            $scope.productIDorNameDisplay = true;
            $stateParams.productId = selProd.prodId;
            $scope.prodObj.productIDorName = $scope.PROD_NAME;
            $scope['filterProducts_'] = null;
        };


        $scope.getAnalysis = function () {

            if ($stateParams.productId > 0) {
                $scope.GetProductAnalysis();
                $scope.GetProductAnalysis2();
                //$scope.GetVendorsAndOrders();
                // $scope.GetMostOrdersAndVendors();
                $scope.GetProductRequirements();
                $scope.GetProductPriceHistory();
                $scope.GetProductAnalysisByProduct()
            }

            $scope.productIDorNameDisplay = false;
        };

        //#region Original
        $scope.ProductVsOrgOccupationChart = function (onlyProductInOrg, allOtherProductsInOrg) {
            Highcharts.chart('ProductVsOrgOccupationChart', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Procurement',
                    colorByPoint: true,
                    data: [{
                        name: 'Product',
                        y: onlyProductInOrg,
                        sliced: true,
                        selected: true
                    }, {
                        name: 'Other Products in Org',
                        y: allOtherProductsInOrg
                    }]
                }]
            });
        };

        $scope.ProductVsCatOccupationChart = function (onlyProductInCat, allOtherProductsInCat) {
            //Highcharts.chart('ProductVsCatOccupationChart', {
            //    chart: {
            //        plotBackgroundColor: null,
            //        plotBorderWidth: null,
            //        plotShadow: false,
            //        type: 'pie'
            //    },
            //    title: {
            //        text: ''
            //    },
            //    tooltip: {
            //        pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
            //    },
            //    plotOptions: {
            //        pie: {
            //            allowPointSelect: true,
            //            cursor: 'pointer',
            //            dataLabels: {
            //                enabled: false,            
            //                },
            //                showInLegend: true
            //            }
            //        }
            //    },
            //    series: [{
            //        name: 'Procurement',
            //        colorByPoint: true,
            //        data: [{
            //            name: 'P',
            //            y: notProductWithCat,
            //            sliced: true,
            //            selected: true
            //        },
            //        {
            //            name: 'O.P',
            //            y: productWithCat
            //        }
            //        ]
            //    }]
            //});

            Highcharts.chart('ProductVsCatOccupationChart', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Procurement',
                    colorByPoint: true,
                    data: [{
                        name: 'Product',
                        y: onlyProductInCat,
                        sliced: true,
                        selected: true
                    }, {
                        name: 'Other Products in Category',
                        y: allOtherProductsInCat
                    }]
                }]
            });
        }

        $scope.GetProductAnalysis = function () {

            var params = {
                "u_id": userService.getUserId(),
                "prod_id": $stateParams.productId,
                "call_code": 1,
                "fromDate": $scope.dateobj.FromDate,
                "toDate": $scope.dateobj.ToDate,
                "callID": $scope.callID,
                "sessionid": userService.getUserToken()
            };

            catalogReportsServices.GetProductAnalysis(params)
                .then(function (response) {
                    $scope.ProductDetails = response;
                    console.log($scope.ProductDetails);

                    let onlyProductInOrg = ($scope.ProductDetails.PROD_OCCUPATION / ($scope.ProductDetails.PROD_OCCUPATION + $scope.ProductDetails.ALL_PROD_OCCUPATION)) * 100;
                    let allOtherProductsInOrg = ($scope.ProductDetails.ALL_PROD_OCCUPATION / ($scope.ProductDetails.PROD_OCCUPATION + $scope.ProductDetails.ALL_PROD_OCCUPATION)) * 100;

                    let onlyProductInCat = ($scope.ProductDetails.PROD_OCCUPATION / ($scope.ProductDetails.PROD_OCCUPATION + $scope.ProductDetails.CAT_PROD_OCCUPATION)) * 100;
                    let allOtherProductsInCat = ($scope.ProductDetails.CAT_PROD_OCCUPATION / ($scope.ProductDetails.PROD_OCCUPATION + $scope.ProductDetails.CAT_PROD_OCCUPATION)) * 100;

                    console.log(onlyProductInCat);
                    console.log(allOtherProductsInCat);

                    $scope.ProductVsOrgOccupationChart(onlyProductInOrg, allOtherProductsInOrg);
                    $scope.ProductVsCatOccupationChart(onlyProductInCat, allOtherProductsInCat);
                });
        };

        $scope.pad = function (d) {
            return (d < 10) ? '0' + d.toString() : d.toString();
        };


        if ($scope.callID == 0) {
            $scope.GetProductAnalysis();
        }

        $scope.GetProductAnalysis2 = function () {

            var params = {
                "u_id": userService.getUserId(),
                "prod_id": $stateParams.productId,
                "call_code": 2,
                "fromDate": $scope.dateobj.FromDate,
                "toDate": $scope.dateobj.ToDate,
                "callID": $scope.callID,
                "sessionid": userService.getUserToken()
            };

            catalogReportsServices.GetProductAnalysis(params)
                .then(function (response) {
                    $scope.ProductDetails2 = response;
                    $scope.ProductDetails2.BEST_PRICE_DATE = new moment($scope.ProductDetails2.BEST_PRICE_DATE).format("DD-MM-YYYY");
                    $scope.ProductDetails2.LAST_PRICE_DATE = new moment($scope.ProductDetails2.LAST_PRICE_DATE).format("DD-MM-YYYY");


                })
        };
        if ($scope.callID == 0) {
            $scope.GetProductAnalysis2();
        }



        $scope.GetVendorsAndOrders = function () {

            var params = {
                "u_id": userService.getUserId(),
                "prod_id": $stateParams.productId,
                "fromDate": $scope.dateobj.FromDate,
                "toDate": $scope.dateobj.ToDate,
                "callID": $scope.callID,
                "sessionid": userService.getUserToken()
            };

            catalogReportsServices.GetVendorsAndOrders(params)
                .then(function (response) {
                    $scope.VendorsAndOrders = response;
                    $scope.VendorsAndOrders.forEach(function (data, dataIndex) {
                        // data.POSTED_DATE = new moment(data.POSTED_DATE).format("DD-MM-YYYY HH:mm");
                        data.POSTED_DATE = userService.toLocalDate(data.POSTED_DATE);
                    })
                })
        };

        // $scope.GetVendorsAndOrders();


        $scope.GetMostOrdersAndVendors = function () {

            var params = {
                "u_id": userService.getUserId(),
                "prod_id": $stateParams.productId,
                "sessionid": userService.getUserToken()
            };

            catalogReportsServices.GetMostOrdersAndVendors(params)
                .then(function (response) {
                    $scope.MostOrdersAndVendors = response;
                    $scope.MostOrdersAndVendors.forEach(function (data, dataIndex) {
                        // data.POSTED_DATE = new moment(data.POSTED_DATE).format("DD-MM-YYYY HH:mm");
                        data.POSTED_DATE = userService.toLocalDate(data.POSTED_DATE);
                    });
                });
        };

        //$scope.GetMostOrdersAndVendors();


        $scope.GetProductRequirements = function () {

            var params = {
                "u_id": userService.getUserId(),
                "prod_id": $stateParams.productId,
                "fromDate": $scope.dateobj.FromDate,
                "toDate": $scope.dateobj.ToDate,
                "callID": $scope.callID,
                "sessionid": userService.getUserToken()
            };

            catalogReportsServices.GetProductRequirements(params)
                .then(function (response) {
                    $scope.ProductRequirements = response;
                    $scope.ProductRequirements.forEach(function (data, dataIndex) {
                        // data.POSTED_DATE = new moment(data.POSTED_DATE).format("DD-MM-YYYY HH:mm");
                        data.POSTED_DATE = userService.toLocalDate(data.POSTED_DATE);
                    })
                })
        };
        if ($scope.callID == 0) {
            $scope.GetProductRequirements();
        }



        $scope.GetProductPriceHistory = function () {

            var params = {
                "u_id": userService.getUserId(),
                "prod_id": $stateParams.productId,
                "fromDate": $scope.dateobj.FromDate,
                "toDate": $scope.dateobj.ToDate,
                "callID": $scope.callID,
                "sessionid": userService.getUserToken()
            };

            catalogReportsServices.GetProductPriceHistory(params)
                .then(function (response) {
                    $scope.ProductPriceHistory = response;
                    $scope.ProductPriceHistory.forEach(function (data, dataIndex) {
                        // data.POSTED_DATE = new moment(data.POSTED_DATE).format("DD-MM-YYYY HH:mm");
                        data.POSTED_DATE = userService.toLocalDate(data.POSTED_DATE);
                    })


                    $scope.renderChart($scope.ProductPriceHistory);
                    //  $scope.renderChart1($scope.data, $scope.name, $scope.categories, $scope.FromDate, $scope.ToDate);
                })
        };
        if ($scope.callID == 0) {
            $scope.GetProductPriceHistory();
        }



        $scope.name = [];
        $scope.renderChart = function (ProductPriceHistory) {
            $scope.data = [];
            $scope.name = [];
            $scope.categories = [];
            $scope.chartOptions = {
                chart: { width: 0, height: 0 }, title: { text: '' }, xAxis: { categories: [], title: { text: '' } }, yAxis: { title: { text: '' } }, series: [{ name: '', data: [] }]
            };
            if (ProductPriceHistory.length > 0) {
                $scope.Name = ProductPriceHistory[0].V_COMPANY_NAME;
                $scope.dateobj.FromDate = ProductPriceHistory[0].POSTED_DATE;

                ProductPriceHistory.forEach(function (item, index) {
                    $scope.data.push(item.TOTAL_ITEM_PRICE);
                    $scope.name.push(item.V_COMPANY_NAME);
                    $scope.categories.push(item.POSTED_DATE.toString() + ' (' + item.V_COMPANY_NAME.toUpperCase() + ')');

                });


                $scope.dateobj.ToDate = ProductPriceHistory[ProductPriceHistory.length - 1].POSTED_DATE;
            }

            $('#chartOptions').highcharts({
                chart: {
                    width: 530,
                    height: 280
                },

                title: {
                    text: 'Product History Graph'
                },
                xAxis: {
                    categories: $scope.categories,
                    title: {
                        text: 'Time ( Start Time: ' + $scope.dateobj.FromDate + ' - End Time: ' + $scope.dateobj.ToDate + ')'
                    }
                },
                yAxis: {
                    title: {
                        text: 'Product Price'
                    }
                },
                series: [{
                    name: 'Prices',
                    data: $scope.data
                }]
            });

            //$scope.chartOptions = {

            //};

            $scope.show = true;
        }

        $scope.renderChart1 = function (data, name, categories, startTime, endTime) {

            Highcharts.chart('chartOptions', {
                chart: {
                    width: 530,
                    height: 280
                },
                title: {
                    text: 'Product History Graph'
                },
                xAxis: {
                    categories: categories,
                    title: {
                        text: 'Time ( Start Time: ' + startTime + ' - End Time: ' + endTime + ')'
                    }
                },
                yAxis: {
                    title: {
                        text: 'Product Price'
                    }
                },
                series: [{
                    name: 'Prices',
                    data: data
                }]
            });
        }

        //#endregion Original

        $scope.goToPPSView = function (reqid, catid, ppsid) {
            var url = $state.href("view-pps", { "reqid": reqid, "catid": catid, "ppsid": ppsid });
            window.open(url, '_blank');
        };

        $scope.goToReq = function (reqid) {
            var url = $state.href("view-requirement", { "Id": reqid });
            window.open(url, '_blank');
        };






        $scope.h = {
            h1: {
                h: "Pending RFQ:",
                t1: "Other than mark as completed  and deleted RFQ's count will be displayed here"
            },
            h2: {
                h: "Pending PO:",
                t1: "If mark as completed is done and if PPS is not created RFQ's count will be displayed here"
            },
            h3: {
                h: "Total PO:",
                t1: "If PPS is created then the count will be displayed here"
            },
            h4: {
                h: "Total Vendors: ",
                t1: "No.of vendors added to that product"
            },
            h5: {
                h: "Number of vendors got orders: ",
                t1: "Total PO"
            },
            h6: {
                h: "Most number of PO to vendors: ",
                t1: "Name — Vendors who received most number of PO count"
            },
            h7: {
                h: "List Of RFQ: ",
                t1: "All requirements posted for this product"
            },
            h8: {
                h: "Price Graph: ",
                t1: "If RFQ's are marked as completed then those vendors price is displayed"
            },
            h9: {
                h: "Best Price: ",
                t1: "Least price for this product"
            },
            h10: {
                h: "Last Purchase Price: ",
                t1: "Latest PO price"
            },
            h11: {
                h: "Product Material Occupying in Total Procurement  : ",
                t1: "1.If  Price type  — Sum of L1 revised item prices (Ignore - Price cap, Initial Not approved vendors, Revised rejected vendors , Other charges ,unconfirmed, not started, started,deleted,regret).",
                t2: "2. If split — Sum of L1 revised item prices with required quantity(In L1 only least item price for which slab item price is least that price is shown) (Ignore - Price cap, Initial Not approved vendors, Revised rejected vendors , Other charges)"
            },
            h12: {
                h: "Category Material Occupying in Total Procurement: ",
                t1: "Category Material Occupying in Total Procurement"
            },
        };
        $scope.productAnalysis = [];
        $scope.vendorShareArray = [];
        $scope.GetProductAnalysisByProduct = function () {

            var params = {
                "u_id": userService.getUserId(),
                "prod_id": $stateParams.productId,
                "fromDate": $scope.dateobj.FromDate,
                "toDate": $scope.dateobj.ToDate,
                "callID": $scope.callID,
                "sessionid": userService.getUserToken()
            };

            catalogReportsServices.GetProductAnalysisByProduct(params)
                .then(function (response) {
                    $scope.productAnalysis = response;

                    if ($scope.productAnalysis && $scope.productAnalysis.vendorShare.length > 0) {
                        $scope.vendorShareArray = $scope.productAnalysis.vendorShare;

                        $scope.vendorSharePie($scope.vendorShareArray);
                    }

                })
        };

        $scope.vendorSharePie = function (vend) {
            $scope.data = [];
            $scope.dataObj = {
                name: '',
                y: 0
            };
            var othersShareObj = {
                name: 'Others',
                y: 0
            }
            var tempShare = 0;
            vend.forEach(function (v) {
                $scope.dataObj = {
                    name: v.VENDOR_NAME,
                    y: v.VENDOR_SHARE_PRICE
                };
                $scope.data.push($scope.dataObj);
                tempShare += v.VENDOR_SHARE_PRICE;
            })
            othersShareObj = {
                name: 'Others',
                y: 100 - tempShare
            }
            $scope.data.push(othersShareObj);
            Highcharts.chart('vendorShare', {
                credits: {
                    enabled: false
                },

                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    width: 550,
                    height: 400
                },
                title: {
                    text: 'Top 3 vendor share'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y:.2f}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        size: 200,
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y:.2f}',
                            style: {
                                color: 'black'
                            }
                        }

                    }
                },
                series: [{
                    name: 'Share',
                    colorByPoint: true,
                    data: $scope.data
                }]
            });
        };


        if ($scope.callID == 0) {
            $scope.GetProductAnalysisByProduct();
        }

        $scope.gotoCompareProducts = function () {
            var url = $state.href("compareProductAlalysis", { "productId": 0, "callID": 1 });
            window.open(url, '_blank');
        };

        function getUserSettings() {
            userService.getUserSettings({ "userid": $scope.currentUserId, "sessionid": $scope.currentSessionId })
                .then(function (response) {
                    let dateFilterValue = 1;
                    if (response && response.data) {
                        let dateFilter = response.data.filter(function (item) {
                            return item.key1 === 'DATE_FILTER';
                        });

                        if (dateFilter && dateFilter.length > 0) {
                            dateFilterValue = +dateFilter[0].value;
                        }
                    }

                    $scope.dateobj.FromDate = moment().subtract(dateFilterValue * 30, "days").format("YYYY-MM-DD");
                });
        }

        getUserSettings();

    }]);
