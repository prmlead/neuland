var gulp = require('gulp'),
del = require('del'),
runSequence = require('run-sequence'),
concat = require('gulp-concat');
minifyCSS = require('gulp-minify-css'),
sass = require('gulp-sass'),
minify = require('gulp-minify');
cachebust = require('gulp-cache-bust');

var filePath = {
    build: {
        dest : 'dist'
    },
    lint: {
        src: ['./app/*.js', './app/**/*.js', './app/**/*.html']
    },
    browserify: {
        src: './app/app.js',
        watch: [
            '!./app/assets/libs/*.js',
            '!./app/assets/libs/**/*.js',
            '!./app/**/*.spec.js',
            '!./app/*.js',
            '!./app/**/*.js',
            '!./app/**/*.json',
            '!./app/**/*.html'
        ]
    },
    moduleCSS: {
        src: [
            '../css/app.min.1.css',
            '../css/app.min.2.css',
            '../css/demo.css',
            '../css/style.css',
            '../css/media.css',
            '../css/new_css.css'
        ]
    }
}
 
 
gulp.task('clean-full', function () {
    del(['./dist/*'])

});

gulp.task('clean-css', function () {
    del(['../css.min/*'])

});

gulp.task('cache-bust', function () {
   gulp.src('./dist/*.js')
    .pipe(cachebust({
        type: 'timestamp'
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('build-css', function () {
    return gulp.src(filePath.moduleCSS.src)
		.pipe(sass())
        .pipe(minifyCSS())
		.pipe(concat('prm360.min.css'))
        .pipe(gulp.dest('../css.min/'));        
});

/*gulp.task('compress-js', ['clean-full'], function() {
  gulp.src(['../js/app.js','../js/config.js', '../js/modulesRouting.js', '../js/templates.js', '../js/custom.js','../js/Controllers.js', '../js/prmModules.js', '../js/prmServices.js'])
    .pipe(minify({
        ext:{
			src:'-debug.js',
            min:'.min.js'
        },
        ignoreFiles: ['.combo.js', '-min.js']
    }))
	.pipe(gulp.dest('./dist'))
});*/

gulp.task('compress-js', ['clean-full'], function() {
  gulp.src(['../dist/*.js'])
    .pipe(minify({
        ext:{
			src:'-debug.js',
            min:'.js'
        },
        ignoreFiles: ['.combo.js', '-min.js']
    }))
	.pipe(gulp.dest('./dist'))
});

gulp.task('concat-js', function() {
  gulp.src(['./dist/*.min.js' ])
    .pipe(concat('prm360.min.js'))
	.pipe(gulp.dest('../js'))
});



gulp.task('prm', function (callback) {
    runSequence(
        ['compress-js', 'build-css'], ['concat-js'],
        callback
     );
});