﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PRMServices.SQLHelper
{
    public static class DatabaseProvider
    {
        public static IDatabaseHelper GetDatabaseProvider()
        {
            string database = ConfigurationManager.AppSettings["DatabaseProvider"].ToString();
            if(database.Equals("MSSQL" , StringComparison.InvariantCultureIgnoreCase))
            {
                return new MSSQLBizClass();
            }
            else
            {
                return new MySQLBizClass();
            }
        }
    }
}