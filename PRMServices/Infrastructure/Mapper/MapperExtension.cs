﻿using PRM.Core.Domain.Configurations;
using PRM.Core.Domain.Configurations.Requirements;
using PRM.Core.Domain.Masters.ContactDetails;
using PRM.Core.Domain.Masters.DeliveryLocations;
using PRM.Core.Domain.Masters.DeliveryTerms;
using PRM.Core.Domain.Masters.GeneralTerms;
using PRM.Core.Domain.RealTimePrices;
using PRM.Core.Domain.Requirments;
using PRMServices.Models.Masters;
using PRMServices.Models.Requirements;
using PRMServices.Models.Settings.Registrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Infrastructure.Mapper
{
    public static class MapperExtension
    {
        /// <summary>
        /// Auto mapper
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static TDestination MapTo<TSource, TDestination>(this TSource source)
        {
            return AutoMapperConfiguration.Mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination MapTo<TSource, TDestination>(this TSource source, TDestination destination)
        {
            return AutoMapperConfiguration.Mapper.Map(source, destination);
        }


        public static RegistrationForm ToEntity(this RegistrationFormModel model)
        {
            return model.MapTo<RegistrationFormModel, RegistrationForm>();
        }

        public static RegistrationForm ToEntity(this RegistrationFormModel model, RegistrationForm entity)
        {
            return model.MapTo(entity);
        }

        public static RealTimePrice ToEntity(this ExcelRealTimePrice model)
        {
            return model.MapTo<ExcelRealTimePrice, RealTimePrice>();
        }

        public static IList<RealTimePrice> ToEntity(this IList<ExcelRealTimePrice> model)
        {
            return model.MapTo<IList<ExcelRealTimePrice>, IList<RealTimePrice>>();
        }


        public static PaymentTerm ToEntity(this PaymentTermModel model)
        {
            return model.MapTo<PaymentTermModel, PaymentTerm>();
        }

        public static PaymentTerm ToEntity(this PaymentTermModel model, PaymentTerm entity)
        {
            return model.MapTo(entity);
        }



        public static GeneralTerm ToEntity(this GeneralTermModel model)
        {
            return model.MapTo<GeneralTermModel, GeneralTerm>();
        }

        public static GeneralTerm ToEntity(this GeneralTermModel model, GeneralTerm entity)
        {
            return model.MapTo(entity);
        }


        public static DeliveryTerm ToEntity(this DeliveryTermModel model)
        {
            return model.MapTo<DeliveryTermModel, DeliveryTerm>();
        }

        public static DeliveryTerm ToEntity(this DeliveryTermModel model, DeliveryTerm entity)
        {
            return model.MapTo(entity);
        }


        public static DeliveryLocation ToEntity(this DeliveryLocationModel model)
        {
            return model.MapTo<DeliveryLocationModel, DeliveryLocation>();
        }

        public static DeliveryLocation ToEntity(this DeliveryLocationModel model, DeliveryLocation entity)
        {
            return model.MapTo(entity);
        }


        public static ContactDetail ToEntity(this ContactDetailModel model)
        {
            return model.MapTo<ContactDetailModel, ContactDetail>();
        }

        public static ContactDetail ToEntity(this ContactDetailModel model, ContactDetail entity)
        {
            return model.MapTo(entity);
        }


        public static RequirementForm ToEntity(this RequirementFormModel model)
        {
            return model.MapTo<RequirementFormModel, RequirementForm>();
        }

        public static RequirementForm ToEntity(this RequirementFormModel model, RequirementForm entity)
        {
            return model.MapTo(entity);
        }


        public static RequirementTemplate ToEntity(this RequirementTemplateModel model)
        {
            return model.MapTo<RequirementTemplateModel, RequirementTemplate>();
        }

        public static RequirementTemplate ToEntity(this RequirementTemplateModel model, RequirementTemplate entity)
        {
            return model.MapTo(entity);
        }

    }
}