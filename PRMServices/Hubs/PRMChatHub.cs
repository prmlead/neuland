﻿using Microsoft.AspNet.SignalR;
using PRMServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PRMServices.SignalR
{
    public class PRMChatHub : Hub
    {
        public Task JoinGroup(string groupName)
        {
            return Groups.Add(Context.ConnectionId, groupName);
        }

        public Task LeaveGroup(string groupName)
        {
            return Groups.Remove(Context.ConnectionId, groupName);
        }

        public void SendMessage(ChatHistory chatHistory)
        {
            //Clients.All.broadcastMessage(/chatHistory., "");
            Clients.Group(Utilities.PRMChatGroupName + chatHistory.ModuleId).chatUpdate(chatHistory);
        }
    }
}