﻿prmApp
    .controller('prCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "storeService", "growlService", "PRMPRServices", "poService", "$rootScope", "catalogService",
        "fileReader",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            storeService, growlService, PRMPRServices, poService, $rootScope, catalogService,
            fileReader) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.prID = $stateParams.Id;

            $scope.prStatus = [
                {
                    display: 'PENDING',
                    value: 'PENDING'
                },
                {
                    display: 'COMPLETED',
                    value: 'COMPLETED'
                },
                {
                    display: 'CANCELLED',
                    value: 'CANCELLED'
                },
                {
                    display: 'REJECTED',
                    value: 'REJECTED'
                }
            ];


            $scope.companyItemUnits = [];


            $scope.companyDepartments = [];

            $scope.PRDetails = {
                isTabular: true,
                PR_ID: 0,
                U_ID: userService.getUserId(),
                COMP_ID: 0,
                PR_NUMBER: '',
                PR_TYPE: 0,
                ASSET_TYPE: '',
                PRIORITY: '',
                PRIORITY_COMMENTS: '',
                DEPARTMENT: 0,
                REQUEST_DATE: '',
                REQUIRED_DATE: '',
                ATTACHMENTS: '',
                TOTAL_BASE_PRICE: 0,
                TOTAL_GST_PRICE: 0,
                TOTAL_PRICE: 0,
                SUB_TOTAL: 0,
                GST_PRICE: 0,
                WF_ID:0,
                STATUS: 'PENDING',
                CLASSIFICATION: '',
                COMPANY: '',
                WORK_ORDER_DURATION: '',
                PURPOSE_IN_BRIEF:'',

                PRItemsList: [],

                PURCHASE:true,
                PRSHOW :true
            };

            $scope.PRItem = {

                ITEM_ID: 0,
                PR_ID: 0,
                ITEM_NAME: '',
                HSN_CODE: '',
                ITEM_CODE: '',
                ITEM_DESCRIPTION: '',
                BRAND: '',
                UNITS: '',
                EXIST_QUANTITY: 0,
                REQUIRED_QUANTITY: 0,
                UNIT_PRICE: 0,
                C_GST_PERCENTAGE: 0,
                S_GST_PERCENTAGE: 0,
                I_GST_PERCENTAGE: 0,
                TOTAL_PRICE: 0,
                COMMENTS: '',
                ATTACHMENTS: '',
                CREATED_BY: 0,
                CREATED_DATE: '',
                MODIFIED_BY: 0,
                MODIFIED_DATE: '',
                CATALOGUE_ID: 0,
                U_ID: $scope.userID,
                sessionID: $scope.sessionID,
                itemAttachment: [],
                attachmentName: '',
                ITEM_NUM:''
            };

            auctionsService.GetCompanyDepartments($scope.userID, $scope.sessionID)
                .then(function (response) {
                    $scope.companyDepartments = response;
                });

            $scope.compID = userService.getUserCompanyId();
            auctionsService.GetCompanyConfiguration($scope.compID, "ITEM_UNITS", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = unitResponse;
                });

            $scope.PRDetails.PRItemsList.push($scope.PRItem);

            $scope.addPrItem = function () {
                var PRItem = {
                    ITEM_ID: 0,
                    PR_ID: 0,
                    ITEM_NAME: '',
                    HSN_CODE: '',
                    ITEM_CODE: '',
                    ITEM_DESCRIPTION: '',
                    BRAND: '',
                    UNITS: '',
                    EXIST_QUANTITY: 0,
                    REQUIRED_QUANTITY: 0,
                    UNIT_PRICE: 0,
                    C_GST_PERCENTAGE: 0,
                    S_GST_PERCENTAGE: 0,
                    I_GST_PERCENTAGE: 0,
                    TOTAL_PRICE: 0,
                    COMMENTS: '',
                    ATTACHMENTS: '',
                    CREATED_BY: 0,
                    CREATED_DATE: '',
                    MODIFIED_BY: 0,
                    MODIFIED_DATE: '',
                    CATALOGUE_ID: 0,
                    U_ID: $scope.userID,
                    sessionID: $scope.sessionID,
                    ITEM_NUM :''
                };

                $scope.PRDetails.PRItemsList.push(PRItem);

            };

            $scope.checkIsFormDisable = function () {
                $scope.isFormdisabled = true;
                if ($scope.PRDetails.CREATED_BY == $scope.userID) {
                    $scope.isFormdisabled = false;
                }
            };
           
            $scope.savePr = function () {
                $scope.PRDetails.U_ID = userService.getUserId();
                var params = {
                    "prdetails": $scope.PRDetails,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.savePrDetails(params)
                    .then(function (response) {

                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Saved Successfully.", "success");
                            $state.go('list-pr');

                        }

                    });
            };


            $scope.getprdetails = function () {
                var params = {
                    "prid": $stateParams.Id,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.getprdetails(params)
                    .then(function (response) {

                        $scope.PRDetails = response;
                        if ($scope.PRDetails.TYPE == "PR") {
                            $scope.PRDetails.PURCHASE = true;
                            $scope.PRDetails.PRSHOW = true;
                        } else {
                            $scope.PRDetails.PURCHASE = false;
                            $scope.PRDetails.PRSHOW = false;
                        }
                        $scope.PRDetails.REQUEST_DATE = userService.toLocalDate($scope.PRDetails.REQUEST_DATE);//new moment($scope.PRDetails.REQUEST_DATE).format("YYYY-MM-DD HH:mm");
                        $scope.PRDetails.REQUIRED_DATE = userService.toLocalDate($scope.PRDetails.REQUIRED_DATE);//new moment($scope.PRDetails.REQUIRED_DATE).format("YYYY-MM-DD HH:mm");
                        if ($stateParams.Id > 0) {
                            $scope.checkIsFormDisable();
                        }
                    });
            };


            if ($stateParams.Id > 0) {
                $scope.getprdetails();
            }

            $scope.deletePrItem = function (index) {
                if ($scope.PRDetails.PRItemsList.length > 1) {
                    $scope.PRDetails.PRItemsList.splice(index, 1);
                    $scope.prUnitPriceCalculation();
                } else {
                    growlService.growl("Can't Delete this Item", "inverse");
                }
            };
            
            $scope.prUnitPriceCalculation = function () {
                $scope.PRDetails.TOTAL_PRICE = 0;
                $scope.PRDetails.TOTAL_BASE_PRICE = 0;
                $scope.PRDetails.TOTAL_GST_PRICE = 0;

                $scope.PRDetails.PRItemsList.forEach(function (item, index) {
                    if (item.UNIT_PRICE == undefined || item.UNIT_PRICE <= 0) {
                        item.UNIT_PRICE = 0;
                    };


                    item.TOTAL_BASE_PRICE = item.UNIT_PRICE * item.REQUIRED_QUANTITY;
                    item.TOTAL_PRICE = item.TOTAL_BASE_PRICE + ((item.TOTAL_BASE_PRICE / 100) * (item.C_GST_PERCENTAGE));

                    $scope.PRDetails.TOTAL_GST_PRICE += (item.TOTAL_BASE_PRICE / 100) * (item.C_GST_PERCENTAGE);
                    $scope.PRDetails.TOTAL_BASE_PRICE += item.TOTAL_BASE_PRICE;
                    $scope.PRDetails.TOTAL_PRICE += item.TOTAL_PRICE;

                });
            };

            $scope.userDepartments = [];
            $scope.userDepartments.push(userService.getSelectedUserDepartmentDesignation());
            if ($scope.PRDetails.PR_ID == 0) {
                $scope.PRDetails.DEPARTMENT = $scope.userDepartments[0].deptID;
                $scope.PRDetails.DEPT_CODE = $scope.userDepartments[0].deptCode;
            }
            $scope.goToPrList = function (id) {
                var url = $state.href("list-pr");
                window.open(url, '_self');
            };

            $scope.deptChanged = function () {
                $scope.userDepartments.filter(function (udd) {
                    if (udd.deptID == $scope.PRDetails.DEPARTMENT) {
                        $scope.PRDetails.DEPT_CODE = udd.deptCode;
                    }
                });
            };



            //#region Catalog
            $scope.productsList = [];
            $scope.getProducts = function () {
                catalogService.getProducts($scope.compID)
                    .then(function (response) {
                        $scope.productsList = response;
                    });
            }
            $scope.getProducts();
            $scope.autofillProduct = function (prodName, index) {
                $scope['ItemSelected_' + index] = false;
                var output = [];
                if (prodName && prodName.trim() != '') {
                    angular.forEach($scope.productsList, function (prod) {
                        if (prod.prodName.toLowerCase().indexOf(prodName.trim().toLowerCase()) >= 0) {
                            output.push(prod);
                        }
                    });
                }
                $scope["filterProducts_" + index] = output;
            }
            $scope.fillTextbox = function (selProd, index) {
                $scope['ItemSelected_' + index] = true;
                $scope.PRDetails.PRItemsList[index].ITEM_NAME = selProd.prodName;
                $scope.PRDetails.PRItemsList[index].ITEM_CODE = selProd.prodCode;
                $scope.PRDetails.PRItemsList[index].ITEM_NUM = selProd.prodNo;
                //ITEM_NUM
                $scope.PRDetails.PRItemsList[index].CATALOGUE_ID = selProd.prodId;
                $scope.PRDetails.PRItemsList[index].ITEM_DESCRIPTION = selProd.prodDesc;
                $scope.PRDetails.PRItemsList[index].UNITS = selProd.prodQty;
                $scope.PRDetails.PRItemsList[index].HSN_CODE = selProd.prodHSNCode;
                $scope['filterProducts_' + index] = null;

                $scope.loadUserDepartments(selProd.prodId, index);

            }

            $scope.onBlurProduct = function (index) {
                if ($scope['ItemSelected_' + index] == false) {
                    $scope.PRDetails.PRItemsList[index].ITEM_NAME = "";
                }
            }
            //#endregion Catalog

            $scope.storeDetailsEdit = [];

            $scope.loadUserDepartments = function (productid, index) {

                var params = {
                    storeid: 0,
                    productid: productid
                }

                var inStock = 0, storeCode = '';
                $scope.inStock = 0;
                $scope.storeName = '';
                storeService.getcompanystoreitems(params.storeid, params.productid)
                    .then(function (response) {
                        $scope.storeDetails = response;
                        $scope.storeDetails.forEach(function (item, index) {
                            $scope.inStock += item.inStock;

                        });

                        //inStock = response[0].inStock;
                        $scope.PRDetails.PRItemsList[index].EXIST_QUANTITY = $scope.inStock;
                    })
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (id != "itemsAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = id;
                            var obj = $scope.PRDetails.PRItemsList[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.PRDetails.PRItemsList.splice(index, 1, obj);
                            console.log(index);
                            console.log($scope.PRDetails.PRItemsList);
                        }
                    });
            };

            $scope.GetCompanyRFQCreators = function () {
                var params = {
                    pr_id: $stateParams.Id//,
                   // dept_id: userService.getSelectedUserDepartmentDesignation().deptID
                }
                PRMPRServices.GetCompanyRFQCreators(params)
                    .then(function (response) {
                        $scope.CompanyRFQCreators = response;

                        $scope.CompanyRFQCreators = $scope.CompanyRFQCreators.filter(function (item, index) {
                            if (item.DEPT_ID == userService.getSelectedUserDepartmentDesignation().deptID) {
                                return item;
                            } else {

                            }
                        })

                        if ($scope.CompanyRFQCreators && $scope.CompanyRFQCreators.length > 0)
                        {
                            $scope.CompanyRFQCreators.forEach(function (item, index) {
                                if (item.IS_ASSIGNED > 0) {
                                    item.IS_ASSIGNED = true;
                                }
                                else {
                                    item.IS_ASSIGNED = false;
                                }
                            })
                        }

                    })
            };

            $scope.GetCompanyRFQCreators();

            //if ($rootScope.isUserEntitled(591168))
            //{
            //    $scope.GetCompanyRFQCreators();
            //}

            
            $scope.SaveCompanyRFQCreators = function () {
                var params = {
                    listPRRFQCreator: $scope.CompanyRFQCreators,
                    sessionid: userService.getUserToken()
                }

                if (params.listPRRFQCreator && params.listPRRFQCreator.length > 0) {
                    params.listPRRFQCreator.forEach(function (item, index) {
                        item.PR_ID = $stateParams.Id;
                        if (item.IS_ASSIGNED) {
                            item.IS_ASSIGNED = 1;
                        }
                        else {
                            item.IS_ASSIGNED = 0;
                        }
                    })
                }

                PRMPRServices.SaveCompanyRFQCreators(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.GetCompanyRFQCreators();
                        } else {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    })
            };



            $scope.display = function (type) {
                if (type == 'PR') {
                    $scope.PRDetails.PRSHOW = true;
                    $scope.PRDetails.PRIORITY = '';
                    $scope.PRDetails.COMPANY = '';
                  //  $scope.PRDetails.REQUEST_DATE = today;
                } else {
                    $scope.PRDetails.PRSHOW = false;
                    $scope.PRDetails.PR_NUMBER = '';
                    $scope.GetSeries('', userService.getSelectedUserDeptID());
                    $scope.PRDetails.PRItemsList = [];
                    //$scope.PRDetails = {
                    $scope.PRDetailsisTabular = true;
                    $scope.PRDetails.PR_ID = 0,
                        $scope.PRDetails.U_ID = userService.getUserId();
                    $scope.PRDetails.COMP_ID = 0;
                    $scope.PRDetails.PR_NUMBER = '';
                    $scope.PRDetails.PR_TYPE = 0;
                    $scope.PRDetails.ASSET_TYPE = '';
                    $scope.PRDetails.PRIORITY = 'Anantapur';
                    $scope.PRDetails.PRIORITY_COMMENTS = '';
                    $scope.PRDetails.DEPARTMENT = userService.getSelectedUserDeptID();
                 //   $scope.PRDetails.REQUEST_DATE = today;
                    $scope.PRDetails.REQUIRED_DATE = '';
                    $scope.PRDetails.ATTACHMENTS = '';
                    $scope.PRDetails.TOTAL_BASE_PRICE = 0;
                    $scope.PRDetails.TOTAL_GST_PRICE = 0;
                    $scope.PRDetails.TOTAL_PRICE = 0;
                    $scope.PRDetails.SUB_TOTAL = 0;
                    $scope.PRDetails.GST_PRICE = 0;
                    $scope.PRDetails.WF_ID = 0;
                    //CREATED_BY: 0,
                    //CREATED_DATE: '',
                    //MODIFIED_BY: 0,
                    //MODIFIED_DATE: '',
                    $scope.PRDetails.CLASSIFICATION = '';
                    $scope.PRDetails.COMPANY = 'Greenko';
                    $scope.PRDetails.WORK_ORDER_DURATION = '';
                    $scope.PRDetails.PURPOSE_IN_BRIEF = '';

                    $scope.PRDetails.PRItemsList = [];

                    //};


                    var PRItem = {

                        ITEM_ID: 0,
                        PR_ID: 0,
                        ITEM_NAME: '',
                        HSN_CODE: '',
                        ITEM_CODE: '',
                        ITEM_DESCRIPTION: '',
                        BRAND: '',
                        UNITS: '',
                        EXIST_QUANTITY: 0,
                        REQUIRED_QUANTITY: 0,
                        UNIT_PRICE: 0,
                        C_GST_PERCENTAGE: 0,
                        S_GST_PERCENTAGE: 0,
                        I_GST_PERCENTAGE: 0,
                        TOTAL_PRICE: 0,
                        COMMENTS: '',
                        ATTACHMENTS: '',
                        CREATED_BY: 0,
                        CREATED_DATE: '',
                        MODIFIED_BY: 0,
                        MODIFIED_DATE: '',
                        CATALOGUE_ID: 0,
                        U_ID: $scope.userID,
                        sessionID: $scope.sessionID,
                        itemAttachment: [],
                        attachmentName: '',
                        ITEM_NUM: ''

                    };
                    $scope.PRDetails.PRItemsList.push(PRItem);
                }
            }

          //  console.log("purchase value is>>>>>>" + $scope.PRDetails.PURCHASE);
        }]);
