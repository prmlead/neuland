﻿prmApp

    .controller('purchaserAnalysisCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService", "PRMAnalysisServices",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService, PRMAnalysisServices) {
            $scope.sessionid = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            $scope.SUB_U_ID = 0;
            $scope.filters = {
                FromDate: '',
                ToDate: ''
            };

            $scope.currentSessionId = userService.getUserToken();
            $scope.currentUserCompID = userService.getUserCompanyId();
            $scope.currentUserId = +userService.getUserId();

            $scope.SUB_U_ID_TEMP = 0;
            $scope.subUsers = [];
            $scope.subUsersTemp = [];
            $scope.PurchaserAnalysis = {
                U_ID: 0,
                USER_SAVINGS: 0,
                TOTAL_MATERIAL: 0,
                TOTAL_RFQ: 0,
                TOTAL_VENDORS: 0,
                DELETED_VENDORS: 0,
                ADDED_VENDORS: 0,
                TOTAL_VOLUME: 0,
                RFQ_TAT: 0,
                QCS_TAT: 0,
                TOTAL_ITEM_SUM: 0
            };
            userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.subUsers = response;

                });


            var today = moment();
            $scope.filters.FromDate = today.add('days', -30).format('YYYY-MM-DD');
            today = moment().format('YYYY-MM-DD');
            $scope.filters.ToDate = today;

            $scope.D = 0;
            $scope.H = 0;
            $scope.M = 0;

            $scope.D1 = 0;
            $scope.H1 = 0;
            $scope.M1 = 0;

            $scope.GetPurchaserAnalysis = function (uid) {
                var params = {
                    userID: uid,
                    fromDate: $scope.filters.FromDate,
                    toDate: $scope.filters.ToDate
                };
                PRMAnalysisServices.GetPurchaserAnalysis(params)
                    .then(function (response) {

                        $scope.PurchaserAnalysis = response;

                        $scope.D = parseInt($scope.PurchaserAnalysis.RFQ_TAT / (24 * 60));
                        $scope.H = parseInt(($scope.PurchaserAnalysis.RFQ_TAT % (24 * 60)) / 60);
                        $scope.M = parseInt(($scope.PurchaserAnalysis.RFQ_TAT % (24 * 60)) % 60);

                        $scope.D1 = parseInt($scope.PurchaserAnalysis.QCS_TAT / (24 * 60));
                        $scope.H1 = parseInt(($scope.PurchaserAnalysis.QCS_TAT % (24 * 60)) / 60);
                        $scope.M1 = parseInt(($scope.PurchaserAnalysis.QCS_TAT % (24 * 60)) % 60);
                    });
            };

            $scope.getPurchaserAnalysis = function () {
                if ($scope.SUB_U_ID == 0) {
                    $scope.SUB_U_ID_TEMP = userService.getUserId();
                } else {
                    $scope.SUB_U_ID_TEMP = $scope.SUB_U_ID;
                }
                $scope.GetPurchaserAnalysis($scope.SUB_U_ID_TEMP);
            };

            function getUserSettings() {
                userService.getUserSettings({ "userid": $scope.currentUserId, "sessionid": $scope.currentSessionId })
                    .then(function (response) {
                        let dateFilterValue = 1;
                        if (response && response.data) {
                            let dateFilter = response.data.filter(function (item) {
                                return item.key1 === 'DATE_FILTER';
                            });

                            if (dateFilter && dateFilter.length > 0) {
                                dateFilterValue = +dateFilter[0].value;
                            }
                        }

                        $scope.filters.FromDate = moment().subtract(+dateFilterValue * 30, "days").format("YYYY-MM-DD");
                        $scope.getPurchaserAnalysis();
                    });
            }

            getUserSettings();
        }
    ]);