﻿prmApp

    .controller('savingsDashBoardCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService) {

            $scope.currentUserCompID = userService.getUserCompanyId();
            $scope.currentUserId = +userService.getUserId();
            $scope.currentSessionId = userService.getUserToken();
            $scope.categoryStatsArray = [];
            $scope.categoryStatsArrayTemp = [];
            $scope.monthlyStatsArray = [];
            $scope.monthlyArrayTemp = [];
            $scope.departmentStatsArray = [];
            $scope.departmentStatsArrayTemp = [];
            $scope.plantStatsArrayTemp = [];
            $scope.plantStatsArray = [];
            $scope.filters = {
                FromDate: '',
                ToDate: ''
            };

            var today = moment();
            $scope.filters.FromDate = today.add('days', -30).format('YYYY-MM-DD');
            today = moment().format('YYYY-MM-DD');
            $scope.filters.ToDate = today;

            $scope.getCompanySavingStats = function () {
                var params = {
                    compid: $scope.currentUserCompID,
                    sessionid: $scope.currentSessionId,
                    fromDate: $scope.filters.FromDate,
                    toDate: $scope.filters.ToDate
                };

                reportingService.GetCompanySavingStats(params)
                    .then(function (response) {
                        if (response) {
                            var categoryStats = response.filter(function (item) {
                                return item.name === 'CATEGORY';
                            });

                            if (categoryStats && categoryStats.length > 0) {
                                $scope.categoryStatsArray = categoryStats[0].arrayPair;
                                $scope.categoryStatsArrayTemp = $scope.categoryStatsArray;
                            }

                            var monthlyStats = response.filter(function (item) {
                                return item.name === 'MONTHLY';
                            });

                            if (monthlyStats && monthlyStats.length > 0) {
                                $scope.monthlyStatsArray = monthlyStats[0].arrayPair;
                                var monthlyStatsArrayTEMP = [];
                                monthlyStats[0].arrayPair.forEach(function (item, itemIndexs) {
                                    monthlyStatsArrayTEMP.push([item.key1, item.decimalVal]);
                                });

                                $scope.monthlyArrayTemp = $scope.monthlyStatsArray;

                                $scope.savingsbymonth(monthlyStatsArrayTEMP);
                            }

                            var departmentStats = response.filter(function (item) {
                                return item.name === 'DEPARTMENT';
                            });

                            if (departmentStats && departmentStats.length > 0) {
                                $scope.departmentStatsArray = departmentStats[0].arrayPair;
                                $scope.departmentStatsArrayTemp = $scope.departmentStatsArray;
                            }

                            var plantStats = response.filter(function (item) {
                                return item.name === 'PLANT_NAME';
                            });

                            if (plantStats && plantStats.length > 0) {
                                $scope.plantStatsArray = plantStats[0].arrayPair;

                                $scope.plantStatsArray.forEach(function (item, idx) {
                                    var str = "";
                                    str = item.key1;
                                    item.key1 = str.replace(/^,|,$/g, '');
                                })

                                $scope.plantStatsArrayTemp = $scope.plantStatsArray;

                            }

                        }
                    });
            };



            $scope.savingsFunnel = function (funnelData) {
                Highcharts.chart('savingsFunnel', {
                    chart: {
                        type: 'funnel'
                    },
                    title: {
                        text: 'Savings By Categry'
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b> ({point.y:,.0f})',
                                softConnector: true
                            },
                            center: ['40%', '50%'],
                            neckWidth: '30%',
                            neckHeight: '25%',
                            width: '80%'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    series: [{
                        name: 'Categories',
                        data: funnelData
                    }],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                plotOptions: {
                                    series: {
                                        dataLabels: {
                                            inside: true
                                        },
                                        center: ['50%', '50%'],
                                        width: '100%'
                                    }
                                }
                            }
                        }]
                    }
                });
            };

            //$scope.savingsFunnel();

            $scope.savingsPie = function (departmentStatsArray) {
                // Make monochrome colors
                var pieColors = (function () {
                    var colors = [],
                        base = Highcharts.getOptions().colors[0],
                        i;

                    for (i = 0; i < 10; i += 1) {
                        // Start out with a darkened base color (negative brighten), and end
                        // up with a much brighter color
                        colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
                    }
                    return colors;
                }());

                // Build the chart
                Highcharts.chart('savingsPie', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Savings by Department'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            colors: pieColors,
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                                distance: -50,
                                filter: {
                                    property: 'percentage',
                                    operator: '>',
                                    value: 4
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Share',
                        data: departmentStatsArray
                    }]
                });
            };



            $scope.targetBar = function () {
                Highcharts.chart('targetBar', {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Savings Vs Org Target bar chart'
                    },
                    xAxis: {
                        categories: ['Savings', 'Target']
                    },
                    yAxis: {
                        min: 0,
                        max: 5000,
                        title: {
                            text: 'Gap to Target 162,616(4%)$'
                        }

                    },
                    legend: {
                        reversed: true
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: [{
                        showInLegend: false,
                        name: '',
                        data: [4000, 3500]
                    }]
                });
            }
            //$scope.targetBar();

            $scope.savingsbymonth = function (monthlyStatsArray) {
                Highcharts.chart('savingsByMonth', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Savings By Month'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {

                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Savings: <b>{point.y:.1f} millions</b>'
                    },
                    series: [{
                        // name: '',
                        data: monthlyStatsArray,
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });
            };

            $scope.filterCategories = function (search) {

                search = search.toLowerCase();

                if (search) {
                    $scope.categoryStatsArray = $scope.categoryStatsArrayTemp.filter(function (item) {
                        return item.key1.toLowerCase().indexOf(search) >= 0;
                    });
                } else {
                    $scope.categoryStatsArray = $scope.categoryStatsArrayTemp;
                }
            };

            $scope.filterPlant = function (search) {

                search = search.toLowerCase();

                if (search) {
                    $scope.plantStatsArray = $scope.plantStatsArrayTemp.filter(function (item) {
                        return item.key1.toLowerCase().indexOf(search) >= 0;
                    });
                } else {
                    $scope.plantStatsArray = $scope.plantStatsArrayTemp;
                }
            };

            $scope.filterDeapartment = function (search) {

                search = search.toLowerCase();

                if (search) {
                    $scope.departmentStatsArray = $scope.departmentStatsArrayTemp.filter(function (item) {
                        return item.key1.toLowerCase().indexOf(search) >= 0;
                    });
                } else {
                    $scope.departmentStatsArray = $scope.departmentStatsArrayTemp;
                }
            };

            $scope.filterMonth = function (search) {

                search = search.toLowerCase();

                if (search) {
                    $scope.monthlyStatsArray = $scope.monthlyArrayTemp.filter(function (item) {
                        return (item.key1.toLowerCase().indexOf(search) >= 0 || item.year.toLowerCase().indexOf(search) >= 0);
                    });
                } else {
                    $scope.monthlyStatsArray = $scope.monthlyArrayTemp;
                }
            };

            function getUserSettings() {
                userService.getUserSettings({ "userid": $scope.currentUserId, "sessionid": $scope.currentSessionId })
                    .then(function (response) {
                        let dateFilterValue = 1;
                        if (response && response.data) {
                            let dateFilter = response.data.filter(function (item) {
                                return item.key1 === 'DATE_FILTER';
                            });

                            if (dateFilter && dateFilter.length > 0) {
                                dateFilterValue = +dateFilter[0].value;
                            }
                        }

                        $scope.filters.FromDate = moment().subtract(+dateFilterValue * 30, "days").format("YYYY-MM-DD");
                        $scope.getCompanySavingStats();
                    });
            }

            getUserSettings();

        }
    ]);