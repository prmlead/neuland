﻿prmApp

    .controller('openPOReportCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$log", "$stateParams", "$timeout", "auctionsService",
        "userService", "SignalRFactory", "fileReader", "growlService", "PRMAnalysisServices","$uibModal",
        function ($scope, $http, $state, domain, $filter, $log, $stateParams, $timeout, auctionsService,
            userService, SignalRFactory, fileReader, growlService, PRMAnalysisServices, $uibModal) {

            $scope.sessionId = userService.getUserToken();
            $scope.companyId = userService.getUserCompanyId();
            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 5;
            $scope.maxSize = 5; //Number of pager buttons to show

            /*PAGINATION CODE*/

            $scope.openPOArr = [];
            $scope.openPOArrInitial = [];
            $scope.openPOArrExport = [];
            $scope.eportOpenPOData = false;
            $scope.openPOPivot = [];
            $scope.openPOPivotData = [];
            $scope.UIFilters = {
                filterModelPurchaseUser: 'ALL',
                filterModelCategory: 'ALL',
                yearmonth: ''
            };           

            $scope.getOpenPOPivot = function () {
                PRMAnalysisServices.GetOpenPOStatsbyMonth({ 'compid': $scope.companyId, 'sessionid': $scope.sessionId})
                    .then(function (response) {
                        $scope.openPOPivot = response;
                        if ($scope.openPOPivot && $scope.openPOPivot.length > 0) {
                            $scope.openPOPivot.forEach(function (obj, key) {
                                var temp = {
                                    "name": obj.key1,
                                    "y": obj.value1
                                };

                                $scope.openPOPivotData.push(temp);
                            });

                            loadHighCharts();
                        }
                    });
            };

            $scope.filterGraph = function () {
                $scope.getOpenPOPivot();
            };

            $scope.filterGraph();

            $scope.getOPenPOData = function (yearmonth, page, pagesize) {
                PRMAnalysisServices.GetOpenPOData({ 'compid': $scope.companyId, 'yearmonth': yearmonth, 'page': page, 'pagesize': pagesize,  'sessionid': $scope.sessionId })
                    .then(function (response) {
                        if (response && response.length > 0) {
                            if ($scope.eportOpenPOData) {
                                $scope.openPOArrExport = response;
                                $scope.openPOArrExport.forEach(function (obj, key) {
                                    obj.PO_RELEASE_DATE1 = userService.toLocalDate(obj.PO_RELEASE_DATE);
                                    obj.ACK_DATE1 = '-';
                                    if (obj.ACK_DATE) {
                                        obj.ACK_DATE1 = userService.toLocalDate(obj.ACK_DATE);
                                    }
                                });
                                $scope.exportData();
                            } else {
                                $scope.openPOArrInitial = [];
                                $scope.openPOArrInitial = response;
                                $scope.openPOArrInitial.forEach(function (obj, key) {
                                    obj.PO_RELEASE_DATE1 = userService.toLocalDate(obj.PO_RELEASE_DATE);
                                    obj.ACK_DATE1 = '-';
                                    if (obj.ACK_DATE) {
                                        obj.ACK_DATE1 = userService.toLocalDate(obj.ACK_DATE);
                                    }
                                });
                            }
                        }
                    });
            };

            $scope.filterTableData = function (yearmonth, page, pagesize) {
                $scope.getOPenPOData($scope.UIFilters.yearmonth, page, pagesize);
            };

            $scope.filterTableData('', 0, $scope.itemsPerPage);


            $scope.pageChanged = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.filterTableData($scope.UIFilters.yearmonth, +($scope.currentPage - 1) * $scope.itemsPerPage, $scope.itemsPerPage);
            };

            function loadHighCharts() {
                Highcharts.chart('container', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Open PO'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        title: {
                            text: 'Total No.of PO\'s'
                        }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                            },
                            point: {
                                events: {
                                    click: function () {
                                        $scope.UIFilters.yearmonth = this.name;
                                        $scope.filterTableData(this.name, 0, $scope.itemsPerPage);
                                    }
                                }
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
                    },

                    "series": [
                        {
                            "name": "PO COUNT",
                            "colorByPoint": true,
                            "data": $scope.openPOPivotData
                        }
                    ],
                    "drilldown": {

                    }
                });
            }

            $scope.exportItemsToExcel = function () {
                $scope.eportOpenPOData = true;
                $scope.filterTableData('', 0, 0);                
            };

            $scope.exportData = function () {
                $scope.eportOpenPOData = false;
                var mystyle = {
                    sheetid: 'OpenPO',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                alasql.fn.decimalRound = function (x) { return Math.round(x); };
                alasql('SELECT PO_NUMBER as [PO Number], PO_LINE_ITEM as [PO Line Item], PO_RELEASE_DATE1 as [PO Release Date], ' +
                    ' PO_CREATOR as [PO Creator], PO_MATERIAL_DESC as [Material Desc.], ' +
                    ' VENDOR_CODE as [Vendor Code], VENDOR_COMPANY as [Vendor Company], decimalRound(ORDER_QTY) as [Order Qty], ' +
                    ' UOM as [UOM], NET_PRICE as [Net Price], TOTAL_VALUE as [Total Value], decimalRound(PENDING_QTY) as [Pending Qty], PO_STATUS as [PO Status],' +
                    ' ACK_DATE1 as [Ack. Date], ACK_QTY as [Ack. Qty.] INTO XLSX(?, { ' +
                    ' headers: true, sheetid: "PDetails", ' +
                    ' style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ? ', ["OpenPO.xlsx", $scope.openPOArrExport]);
            };

        }]);