﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class PurchaserAnalysis : Entity
    {
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("USER_SAVINGS")] public double USER_SAVINGS { get; set; }
        [DataMember] [DataNames("TOTAL_VOLUME")] public double TOTAL_VOLUME { get; set; }
        [DataMember] [DataNames("TOTAL_MATERIAL")] public int TOTAL_MATERIAL { get; set; }
        [DataMember] [DataNames("TOTAL_RFQ")] public int TOTAL_RFQ { get; set; }
        [DataMember] [DataNames("TOTAL_VENDORS")] public int TOTAL_VENDORS { get; set; }
        [DataMember] [DataNames("DELETED_VENDORS")] public int DELETED_VENDORS { get; set; }
        [DataMember] [DataNames("ADDED_VENDORS")] public int ADDED_VENDORS { get; set; }
        [DataMember] [DataNames("RFQ_TAT")] public double RFQ_TAT { get; set; }
        [DataMember] [DataNames("QCS_TAT")] public double QCS_TAT { get; set; }
        [DataMember] [DataNames("TOTAL_ITEM_SUM")] public double TOTAL_ITEM_SUM { get; set; }
    }
}