﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class ChatHistory : Entity
    {
        [DataMember] [DataNames("ChatId")] public int ChatId { get; set; }
        [DataMember] [DataNames("Message")] public string Message { get; set; }
        [DataMember] [DataNames("FromId")] public int FromId { get; set; }
        [DataMember] [DataNames("ToId")] public int ToId { get; set; }
        [DataMember] [DataNames("FromName")] public string FromName { get; set; }
        [DataMember] [DataNames("ToName")] public string ToName { get; set; }
        [DataMember] [DataNames("ModuleId")] public int ModuleId { get; set; }
        [DataMember] [DataNames("Module")] public string Module { get; set; }
        [DataMember] [DataNames("DateCreated")] public DateTime DateCreated { get; set; }
        [DataMember] [DataNames("DateModified")] public DateTime DateModified { get; set; }
        [DataMember] [DataNames("CreatedBy")] public int CreatedBy { get; set; }
        [DataMember] [DataNames("ModifiedBy")] public int ModifiedBy { get; set; }
        [DataMember] [DataNames("IsCustomer")] public bool IsCustomer { get; set; }
    }
}