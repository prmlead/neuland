﻿using System;
using System.Collections.Generic;
using System.Data;
using PRMServices.Models;
using PRMServices.SQLHelper;

namespace PRMServices.Common
{
    public static class LogisticsUtility
    {
        public static LogisticVendorDetails GetVendorsObject(DataRow row)
        {
            LogisticVendorDetails vendor = new LogisticVendorDetails();
            vendor.PO = new RequirementPO();
            vendor.ListReqItems = new List<LogisticRequirementItems>();

            vendor.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
            vendor.VendorName = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
            vendor.InitialPrice = row["VEND_INIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_INIT_PRICE"]) : 0;
            vendor.RunningPrice = row["VEND_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_RUN_PRICE"]) : 0;
            vendor.TotalInitialPrice = row["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_TOTAL_PRICE"]) : 0;
            vendor.TotalRunningPrice = row["VEND_TOTAL_PRICE_RUNNING"] != DBNull.Value ? Convert.ToDouble(row["VEND_TOTAL_PRICE_RUNNING"]) : 0;
            vendor.City = row["UAD_CITY"] != DBNull.Value ? Convert.ToString(row["UAD_CITY"]) : string.Empty;
            vendor.Taxes = row["TAXES"] != DBNull.Value ? Convert.ToDouble(row["TAXES"]) : 0;
            vendor.Rating = row["RATING"] != DBNull.Value ? Convert.ToDouble(row["RATING"]) : 0;
            vendor.Taxes = row["VEND_TAXES"] != DBNull.Value ? Convert.ToInt32(row["VEND_TAXES"]) : 0;
            vendor.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
            vendor.InitialPriceWithOutTaxFreight = row["VEND_INIT_PRICE_WITHOUT_TAX"] != DBNull.Value ? Convert.ToDouble(row["VEND_INIT_PRICE_WITHOUT_TAX"]) : 0;
            vendor.VendorFreight = row["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["VEND_FREIGHT"]) : 0;
            vendor.Warranty = row["WARRANTY"] != DBNull.Value ? Convert.ToString(row["WARRANTY"]) : string.Empty;
            vendor.Payment = row["PAYMENT"] != DBNull.Value ? Convert.ToString(row["PAYMENT"]) : string.Empty;
            vendor.Duration = row["DURATION"] != DBNull.Value ? Convert.ToString(row["DURATION"]) : string.Empty;
            vendor.Validity = row["VALIDITY"] != DBNull.Value ? Convert.ToString(row["VALIDITY"]) : string.Empty;
            vendor.OtherProperties = row["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row["OTHER_PROPERTIES"]) : string.Empty;
            vendor.IsQuotationRejected = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_QUOTATION_REJECTED"]) : 0;
            vendor.QuotationRejectedComment = row["QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row["QUOTATION_REJECTED_REASON"]) : string.Empty;
            vendor.PO = new RequirementPO();
            vendor.PO.POLink = row["PO_ATT_ID"] != DBNull.Value ? Convert.ToString(row["PO_ATT_ID"]) : "0";
            string fileName1 = row["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row["QUOTATION_URL"]) : string.Empty;
            string URL1 = fileName1 != string.Empty ? fileName1 : string.Empty;
            vendor.QuotationUrl = URL1;
            string fileName2 = row["REV_QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row["REV_QUOTATION_URL"]) : string.Empty;
            string URL2 = fileName2 != string.Empty ? fileName2 : string.Empty;
            vendor.RevQuotationUrl = URL2;
            vendor.RevPrice = row["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_PRICE"]) : 0;
            vendor.RevVendorFreight = row["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_FREIGHT"]) : 0;
            vendor.RevVendorTotalPrice = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
            vendor.IsRevQuotationRejected = row["IS_REV_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_REV_QUOTATION_REJECTED"]) : 0;
            vendor.RevQuotationRejectedComment = row["REV_QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row["REV_QUOTATION_REJECTED_REASON"]) : string.Empty;
            vendor.LastActiveTime = row["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row["LAST_ACTIVE_TIME"]) : string.Empty;
            vendor.Discount = row["DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row["DISCOUNT"]) : 0;
            vendor.ReductionPercentage = row["REDUCTION_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["REDUCTION_PERCENTAGE"]) : 0;
            vendor.Vendor = new User();
            vendor.Vendor.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
            vendor.Vendor.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
            vendor.PO.MaterialDispachmentLink = row["MAT_DIS_LINK"] != DBNull.Value ? Convert.ToInt32(row["MAT_DIS_LINK"]) : 0;
            vendor.PO.MaterialReceivedLink = row["MR_LINK"] != DBNull.Value ? Convert.ToInt32(row["MR_LINK"]) : 0;
            vendor.LandingPrice = row["LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row["LANDING_PRICE"]) : 0;
            vendor.RevLandingPrice = row["REV_LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row["REV_LANDING_PRICE"]) : 0;
            vendor.TechEvalScore = row["TECH_EVAL_SCORE"] != DBNull.Value ? Convert.ToDecimal(row["TECH_EVAL_SCORE"]) : 0;
            vendor.SumOfMargin = row["PARAM_SUM_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row["PARAM_SUM_MARGIN"]) : 0;
            vendor.SumOfInitialMargin = row["PARAM_SUM_INITIAL_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row["PARAM_SUM_INITIAL_MARGIN"]) : 0;
            vendor.LandingPrice = Math.Round(vendor.LandingPrice, 2);
            vendor.RevLandingPrice = Math.Round(vendor.RevLandingPrice, 2);
            vendor.ReductionPercentage = Math.Round(vendor.ReductionPercentage, 3);
            vendor.IsAcceptedTC = row["IS_ACCEPTED_TC"] != DBNull.Value ? Convert.ToInt32(row["IS_ACCEPTED_TC"]) : 0;
            vendor.IsSent = row["IS_SENT"] != DBNull.Value ? Convert.ToInt32(row["IS_SENT"]) : 0;

            return vendor;
        }

        public static LogisticRequirementItems GetReqItemsObject(DataRow row)
        {
            LogisticRequirementItems RequirementItems = new LogisticRequirementItems();

            RequirementItems.ItemID = row["ITEM_ID"] != DBNull.Value? Convert.ToInt32(row["ITEM_ID"]) : 0;
            RequirementItems.RequirementID = row["REQ_ID"] != DBNull.Value? Convert.ToInt32(row["REQ_ID"]) : 0;
            RequirementItems.ProductIDorName = row["PROD_ID"] != DBNull.Value? Convert.ToString(row["PROD_ID"]) : string.Empty;
            RequirementItems.ProductNo = row["PROD_NO"] != DBNull.Value? Convert.ToString(row["PROD_NO"]) : string.Empty;
            RequirementItems.ProductDescription = row["DESCRIPTION"] != DBNull.Value? Convert.ToString(row["DESCRIPTION"]) : string.Empty;
            RequirementItems.ProductQuantity = row["QUANTITY"] != DBNull.Value? Convert.ToDouble(row["QUANTITY"]) : 0;
            RequirementItems.ProductBrand = row["BRAND"] != DBNull.Value? Convert.ToString(row["BRAND"]) : string.Empty;
            RequirementItems.OthersBrands = row["OTHER_BRAND"] != DBNull.Value? Convert.ToString(row["OTHER_BRAND"]) : string.Empty;
            RequirementItems.ProductImageID = row["IMAGE_ID"] != DBNull.Value? Convert.ToInt32(row["IMAGE_ID"]) : 0;
            RequirementItems.IsDeleted = row["IS_DELETED"] != DBNull.Value? Convert.ToInt32(row["IS_DELETED"]) : 0;
            RequirementItems.ProductQuantityIn = row["QUANTITY_IN"] != DBNull.Value? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
            RequirementItems.SelectedVendorID = row["SELECTED_VENDOR_ID"] != DBNull.Value? Convert.ToInt32(row["SELECTED_VENDOR_ID"]) : 0;
            RequirementItems.TypeOfPacking = row["TYPE_OF_PACKING"] != DBNull.Value? Convert.ToString(row["TYPE_OF_PACKING"]) : string.Empty;
            RequirementItems.ModeOfShipment = row["MODE_OF_SHIPMENT"] != DBNull.Value? Convert.ToString(row["MODE_OF_SHIPMENT"]) : string.Empty;
            RequirementItems.NatureOfGoods = row["NATURE_OF_GOODS"] != DBNull.Value? Convert.ToString(row["NATURE_OF_GOODS"]) : string.Empty;
            RequirementItems.StorageCondition = row["STORAGE_CONDITION"] != DBNull.Value? Convert.ToString(row["STORAGE_CONDITION"]) : string.Empty;
            RequirementItems.PortOfLanding = row["PORT_OF_LANDING"] != DBNull.Value? Convert.ToString(row["PORT_OF_LANDING"]) : string.Empty;
            RequirementItems.FinalDestination = row["FINAL_DESTINATION"] != DBNull.Value? Convert.ToString(row["FINAL_DESTINATION"]) : string.Empty;
            RequirementItems.DeliveryLocation = row["DELIVERY_LOCATION"] != DBNull.Value? Convert.ToString(row["DELIVERY_LOCATION"]) : string.Empty;
            RequirementItems.PreferredAirlines = row["PREFERRED_AIRLINES"] != DBNull.Value? Convert.ToString(row["PREFERRED_AIRLINES"]) : string.Empty;
            RequirementItems.ProductIDorNameCustomer = row["PROD_ID"] != DBNull.Value? Convert.ToString(row["PROD_ID"]) : string.Empty;
            RequirementItems.ProductNoCustomer = row["PROD_NO"] != DBNull.Value? Convert.ToString(row["PROD_NO"]) : string.Empty;
            RequirementItems.ProductDescriptionCustomer = row["DESCRIPTION"] != DBNull.Value? Convert.ToString(row["DESCRIPTION"]) : string.Empty;
            RequirementItems.ProductBrandCustomer = row["BRAND"] != DBNull.Value? Convert.ToString(row["BRAND"]) : string.Empty;
            RequirementItems.Ispalletize = row["IS_PALLETIZE"] != DBNull.Value? Convert.ToInt32(row["IS_PALLETIZE"]) : 0;
            RequirementItems.NetWeight = row["NET_WEIGHT"] != DBNull.Value? Convert.ToDouble(row["NET_WEIGHT"]) : 0;
            RequirementItems.PalletizeQty = row["PALLETIZE_QTY"] != DBNull.Value? Convert.ToDouble(row["PALLETIZE_QTY"]) : 0;
            RequirementItems.PalletizeLength = row["PALLETIZE_LENGTH"] != DBNull.Value? Convert.ToDouble(row["PALLETIZE_LENGTH"]) : 0;
            RequirementItems.PalletizeBreadth = row["PALLETIZE_BREADTH"] != DBNull.Value? Convert.ToDouble(row["PALLETIZE_BREADTH"]) : 0;
            RequirementItems.PalletizeHeight = row["PALLETIZE_HEIGHT"] != DBNull.Value? Convert.ToDouble(row["PALLETIZE_HEIGHT"]) : 0;
            RequirementItems.CATALOGUE_ID = row["CATALOGUE_ID"] != DBNull.Value ? Convert.ToDouble(row["CATALOGUE_ID"]) : 0;



            RequirementItems.IsEnabled = true;

            return RequirementItems;
        }

        public static LogisticQuotationItems GetQuotationItemsObject(DataRow row)
        {
            LogisticQuotationItems QI = new LogisticQuotationItems();

            QI.QuotationItemID = row["QUOT_ID"] != DBNull.Value ? Convert.ToInt32(row["QUOT_ID"]) : 0;
            QI.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
            QI.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
            QI.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
            QI.Airline = row["AIRLINE"] != DBNull.Value ? Convert.ToString(row["AIRLINE"]) : string.Empty;




            QI.unitTariff = row["UNIT_TARIFF"] != DBNull.Value ? Convert.ToDouble(row["UNIT_TARIFF"]) : 0;
            QI.UnitPrice = row["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["UNIT_PRICE"]) : 0;
            QI.RevUnitPrice = row["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE"]) : 0;
            QI.ItemPrice = row["PRICE"] != DBNull.Value ? Convert.ToDouble(row["PRICE"]) : 0;
            QI.RevItemPrice = row["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE"]) : 0;
            QI.CGst = row["C_GST"] != DBNull.Value ? Convert.ToDouble(row["C_GST"]) : 0;
            QI.SGst = row["S_GST"] != DBNull.Value ? Convert.ToDouble(row["S_GST"]) : 0;
            QI.IGst = row["I_GST"] != DBNull.Value ? Convert.ToDouble(row["I_GST"]) : 0;
            QI.XRay = row["X_RAY"] != DBNull.Value ? Convert.ToDouble(row["X_RAY"]) : 0;
            QI.Fsc = row["FSC"] != DBNull.Value ? Convert.ToDouble(row["FSC"]) : 0;
            QI.Ssc = row["SSC"] != DBNull.Value ? Convert.ToDouble(row["SSC"]) : 0;
            QI.Misc = row["MISC"] != DBNull.Value ? Convert.ToDouble(row["MISC"]) : 0;
            QI.HazCharges = row["HAZ_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["HAZ_CHARGES"]) : 0;
            QI.customClearance = row["CUSTOM_CLEARANCE"] != DBNull.Value ? Convert.ToDouble(row["CUSTOM_CLEARANCE"]) : 0;
            QI.RevCustomClearance = row["REV_CUSTOM_CLEARANCE"] != DBNull.Value ? Convert.ToDouble(row["REV_CUSTOM_CLEARANCE"]) : 0;
            QI.CgFees = row["CG_FEES"] != DBNull.Value ? Convert.ToDouble(row["CG_FEES"]) : 0;
            QI.Adc = row["ADC"] != DBNull.Value ? Convert.ToDouble(row["ADC"]) : 0;
            QI.AwbFees = row["AWB_FEES"] != DBNull.Value ? Convert.ToDouble(row["AWB_FEES"]) : 0;
            QI.EdiFees = row["EDI_FEES"] != DBNull.Value ? Convert.ToDouble(row["EDI_FEES"]) : 0;
            QI.ServiceCharges = row["SERVICE_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["SERVICE_CHARGES"]) : 0;
            QI.RevServiceCharges = row["REV_SERVICE_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_SERVICE_CHARGES"]) : 0;
            QI.OtherCharges = row["OTHER_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["OTHER_CHARGES"]) : 0;
            QI.RevOtherCharges = row["REV_OTHER_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_OTHER_CHARGES"]) : 0;
            QI.TerminalHandling = row["TERMINAL_HANDLING"] != DBNull.Value ? Convert.ToDouble(row["TERMINAL_HANDLING"]) : 0;
            QI.Ams = row["AMS"] != DBNull.Value ? Convert.ToDouble(row["AMS"]) : 0;
            QI.ForwordDgFees = row["FORWORD_DG_FEE"] != DBNull.Value ? Convert.ToDouble(row["FORWORD_DG_FEE"]) : 0;





            QI.unitTariffType = row["UNIT_TARIFF_TYPE"] != DBNull.Value ? Convert.ToInt32(row["UNIT_TARIFF_TYPE"]) : 0;
            QI.UnitPriceType = row["UNIT_PRICE_TYPE"] != DBNull.Value ? Convert.ToInt32(row["UNIT_PRICE_TYPE"]) : 0;
            QI.RevUnitPriceType = row["REV_UNIT_PRICE_TYPE"] != DBNull.Value ? Convert.ToInt32(row["REV_UNIT_PRICE_TYPE"]) : 0;
            QI.ItemPriceType = row["PRICE_TYPE"] != DBNull.Value ? Convert.ToInt32(row["PRICE_TYPE"]) : 0;
            QI.RevItemPriceType = row["REVICED_PRICE_TYPE"] != DBNull.Value ? Convert.ToInt32(row["REVICED_PRICE_TYPE"]) : 0;
            QI.CGstType = row["C_GST_TYPE"] != DBNull.Value ? Convert.ToInt32(row["C_GST_TYPE"]) : 0;
            QI.SGstType = row["S_GST_TYPE"] != DBNull.Value ? Convert.ToInt32(row["S_GST_TYPE"]) : 0;
            QI.IGstType = row["I_GST_TYPE"] != DBNull.Value ? Convert.ToInt32(row["I_GST_TYPE"]) : 0;
            QI.XRayType = row["X_RAY_TYPE"] != DBNull.Value ? Convert.ToInt32(row["X_RAY_TYPE"]) : 0;
            QI.FscType = row["FSC_TYPE"] != DBNull.Value ? Convert.ToInt32(row["FSC_TYPE"]) : 0;
            QI.SscType = row["SSC_TYPE"] != DBNull.Value ? Convert.ToInt32(row["SSC_TYPE"]) : 0;
            QI.MiscType = row["MISC_TYPE"] != DBNull.Value ? Convert.ToInt32(row["MISC_TYPE"]) : 0;
            QI.HazChargesType = row["HAZ_CHARGES_TYPE"] != DBNull.Value ? Convert.ToInt32(row["HAZ_CHARGES_TYPE"]) : 0;
            QI.customClearanceType = row["CUSTOM_CLEARANCE_TYPE"] != DBNull.Value ? Convert.ToInt32(row["CUSTOM_CLEARANCE_TYPE"]) : 0;
            QI.RevCustomClearanceType = row["REV_CUSTOM_CLEARANCE_TYPE"] != DBNull.Value ? Convert.ToInt32(row["REV_CUSTOM_CLEARANCE_TYPE"]) : 0;
            QI.CgFeesType = row["CG_FEES_TYPE"] != DBNull.Value ? Convert.ToInt32(row["CG_FEES_TYPE"]) : 0;
            QI.AdcType = row["ADC_TYPE"] != DBNull.Value ? Convert.ToInt32(row["ADC_TYPE"]) : 0;
            QI.AwbFeesType = row["AWB_FEES_TYPE"] != DBNull.Value ? Convert.ToInt32(row["AWB_FEES_TYPE"]) : 0;
            QI.EdiFeesType = row["EDI_FEES_TYPE"] != DBNull.Value ? Convert.ToInt32(row["EDI_FEES_TYPE"]) : 0;
            QI.ServiceChargesType = row["SERVICE_CHARGES_TYPE"] != DBNull.Value ? Convert.ToInt32(row["SERVICE_CHARGES_TYPE"]) : 0;
            QI.RevServiceChargesType = row["REV_SERVICE_CHARGES_TYPE"] != DBNull.Value ? Convert.ToInt32(row["REV_SERVICE_CHARGES_TYPE"]) : 0;
            QI.OtherChargesType = row["OTHER_CHARGES_TYPE"] != DBNull.Value ? Convert.ToInt32(row["OTHER_CHARGES_TYPE"]) : 0;
            QI.RevOtherChargesType = row["REV_OTHER_CHARGES_TYPE"] != DBNull.Value ? Convert.ToInt32(row["REV_OTHER_CHARGES_TYPE"]) : 0;
            QI.TerminalHandlingType = row["TERMINAL_HANDLING_TYPE"] != DBNull.Value ? Convert.ToInt32(row["TERMINAL_HANDLING_TYPE"]) : 0;
            QI.AmsType = row["AMS_TYPE"] != DBNull.Value ? Convert.ToInt32(row["AMS_TYPE"]) : 0;
            QI.ForwordDgFeesType = row["FORWORD_DG_FEE_TYPE"] != DBNull.Value ? Convert.ToInt32(row["FORWORD_DG_FEE_TYPE"]) : 0;



            QI.Routing = row["ROUTING"] != DBNull.Value ? Convert.ToString(row["ROUTING"]) : string.Empty;
            QI.Transit = row["TRANSIT"] != DBNull.Value ? Convert.ToString(row["TRANSIT"]) : string.Empty;
            QI.IsSelected = row["IS_SELECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_SELECTED"]) : 0;
            QI.IsRevised = row["IS_REVISED"] != DBNull.Value ? Convert.ToInt32(row["IS_REVISED"]) : 0;
            QI.ProductImageID = row["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row["IMAGE_ID"]) : 0;
            //QI.CreatedDate = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MinValue;
            //QI.ModifiedDate = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]) : DateTime.MinValue;
            //QI.CreatedBy = row["CREATED_BY"] != DBNull.Value ? Convert.ToInt32(row["CREATED_BY"]) : 0;
            //QI.ModifiedBy = row["MODIFIED_BY"] != DBNull.Value ? Convert.ToInt32(row["MODIFIED_BY"]) : 0;
            QI.IsDeleted = row["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row["IS_DELETED"]) : 0;

            QI.IsEnabled = true;

            return QI;
        }

        public static DataSet SubmitQuotationObj(LogisticQuotationItems QI)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_QUOT_ID", QI.QuotationItemID);
            sd.Add("P_ITEM_ID", QI.ItemID);
            sd.Add("P_REQ_ID", QI.RequirementID);
            sd.Add("P_U_ID", QI.VendorID);
            sd.Add("P_AIRLINE", QI.Airline);
            sd.Add("P_UNIT_TARIFF", QI.unitTariff);
            sd.Add("P_UNIT_PRICE", QI.UnitPrice);
            sd.Add("P_REV_UNIT_PRICE", QI.RevUnitPrice);
            sd.Add("P_PRICE", QI.ItemPrice);
            sd.Add("P_REVICED_PRICE", QI.RevItemPrice);
            sd.Add("P_C_GST", QI.CGst);
            sd.Add("P_S_GST", QI.SGst);
            sd.Add("P_I_GST", QI.IGst);
            sd.Add("P_X_RAY", QI.XRay);
            sd.Add("P_FSC", QI.Fsc);
            sd.Add("P_SSC", QI.Ssc);
            sd.Add("P_MISC", QI.Misc);
            sd.Add("P_HAZ_CHARGES", QI.HazCharges);
            sd.Add("P_CUSTOM_CLEARANCE", QI.customClearance);
            sd.Add("P_REV_CUSTOM_CLEARANCE", QI.RevCustomClearance);
            sd.Add("P_CG_FEES", QI.CgFees);
            sd.Add("P_ADC", QI.Adc);
            sd.Add("P_AWB_FEES", QI.AwbFees);
            sd.Add("P_EDI_FEES", QI.EdiFees);
            sd.Add("P_SERVICE_CHARGES", QI.ServiceCharges);
            sd.Add("P_REV_SERVICE_CHARGES", QI.RevServiceCharges);
            sd.Add("P_OTHER_CHARGES", QI.OtherCharges);
            sd.Add("P_REV_OTHER_CHARGES", QI.RevOtherCharges);
            sd.Add("P_TERMINAL_HANDLING", QI.TerminalHandling);
            sd.Add("P_AMS", QI.Ams);
            sd.Add("P_FORWORD_DG_FEE", QI.ForwordDgFees);
            sd.Add("P_UNIT_TARIFF_TYPE", QI.unitTariffType);
            sd.Add("P_UNIT_PRICE_TYPE", QI.UnitPriceType);
            sd.Add("P_REV_UNIT_PRICE_TYPE", QI.RevUnitPriceType);
            sd.Add("P_PRICE_TYPE", QI.ItemPriceType);
            sd.Add("P_REVICED_PRICE_TYPE", QI.RevItemPriceType);
            sd.Add("P_C_GST_TYPE", QI.CGstType);
            sd.Add("P_S_GST_TYPE", QI.SGstType);
            sd.Add("P_I_GST_TYPE", QI.IGstType);
            sd.Add("P_X_RAY_TYPE", QI.XRayType);
            sd.Add("P_FSC_TYPE", QI.FscType);
            sd.Add("P_SSC_TYPE", QI.SscType);
            sd.Add("P_MISC_TYPE", QI.MiscType);
            sd.Add("P_HAZ_CHARGES_TYPE", QI.HazChargesType);
            sd.Add("P_CUSTOM_CLEARANCE_TYPE", QI.customClearanceType);
            sd.Add("P_REV_CUSTOM_CLEARANCE_TYPE", QI.RevCustomClearanceType);
            sd.Add("P_CG_FEES_TYPE", QI.CgFeesType);
            sd.Add("P_ADC_TYPE", QI.AdcType);
            sd.Add("P_AWB_FEES_TYPE", QI.AwbFeesType);
            sd.Add("P_EDI_FEES_TYPE", QI.EdiFeesType);
            sd.Add("P_SERVICE_CHARGES_TYPE", QI.ServiceChargesType);
            sd.Add("P_REV_SERVICE_CHARGES_TYPE", QI.RevServiceChargesType);
            sd.Add("P_OTHER_CHARGES_TYPE", QI.OtherChargesType);
            sd.Add("P_REV_OTHER_CHARGES_TYPE", QI.RevOtherChargesType);
            sd.Add("P_TERMINAL_HANDLING_TYPE", QI.TerminalHandlingType);
            sd.Add("P_AMS_TYPE", QI.AmsType);
            sd.Add("P_FORWORD_DG_FEE_TYPE", QI.ForwordDgFeesType);
            sd.Add("P_ROUTING", QI.Routing);
            sd.Add("P_TRANSIT", QI.Transit);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("lgstc_SubmitQuotation", sd);

            return ds;
        }
    }
}