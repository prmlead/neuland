﻿using System.Collections.Generic;
using System.Data;
using PRMServices.Models;
using PRMServices.SQLHelper;

namespace PRMServices.Common
{
    public class APMCUtility
    {
        public static DataSet SaveAPMCObject(APMC apmc)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_APMC_ID", apmc.APMCID);
            sd.Add("P_COMP_ID", apmc.Company.CompanyID);
            sd.Add("P_APMC_NAME", apmc.APMCName);
            sd.Add("P_APMC_DESC", apmc.APMCDescription);
            sd.Add("P_IS_VALID", apmc.IsValid);
            sd.Add("P_CATEGORY", apmc.Category);
            sd.Add("P_U_ID", apmc.Customer.UserID);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("cp_SaveAPMC", sd);
            return ds;
        }

        public static DataSet SaveAPMCSettings(APMCSettings aPMCSettings)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_APMC_SET_ID", aPMCSettings.APMCSettingID);
            sd.Add("P_APMC_ID", aPMCSettings.APMCID);
            sd.Add("P_COMP_ID", aPMCSettings.CompanyID);
            sd.Add("P_RELATED_CESS", aPMCSettings.RelatedCess);
            sd.Add("P_HAMALI", aPMCSettings.Hamali);
            sd.Add("P_FREIGHT", aPMCSettings.Freight);
            sd.Add("P_SUTLI", aPMCSettings.Sutli);
            sd.Add("P_SC", aPMCSettings.SC);
            sd.Add("P_BROKERAGE", aPMCSettings.Brokerage);
            sd.Add("P_ADAT", aPMCSettings.Adat);
            sd.Add("P_PACKING", aPMCSettings.Packing);
            sd.Add("P_U_ID", aPMCSettings.Customer.UserID);

            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("cp_SaveAPMCSettings", sd);

            return ds;
        }

        public static DataSet SaveAPMCNegotiation(APMCNegotiation apmcNegotiation, int apmcNegotiationID, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            if (apmcNegotiation.NewPrice > 0)
            {
                apmcNegotiation.CustPriceInclFinal = apmcNegotiation.VendorPriceExclFinal = apmcNegotiation.NewPrice;
            }

            sd.Add("P_APMC_NEG_ID", apmcNegotiation.APMCNegotiationID);
            sd.Add("P_APMC_ID", apmcNegotiation.APMCID);
            sd.Add("P_VENDOR_ID", apmcNegotiation.Vendor.UserID);

            sd.Add("P_CUST_PRICE_INCL", apmcNegotiation.CustPriceInclFinal);

            sd.Add("P_VEND_PRICE_EXCL", apmcNegotiation.VendorPriceExclFinal);

            sd.Add("P_IS_FROZEN", apmcNegotiation.IsFrozen);
            sd.Add("P_VEND_QUANTITY", apmcNegotiation.Quantity);
            sd.Add("P_COMP_ID", apmcNegotiation.VendorCompany.CompanyID);
            sd.Add("P_QUANTITY_UPDATED", apmcNegotiation.QuantityUpdated);
            sd.Add("P_U_ID", apmcNegotiation.UserID);
            sd.Add("P_ATTACHMENT", apmcNegotiation.AttachmentID);

            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("cp_SaveAPMCNegotiation", sd);
            return ds;
        }

        internal static DataSet SaveAPMCVendor(APMCVendor apmcVendor)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_APMC_ID", apmcVendor.APMCID);
            sd.Add("P_VENDOR_ID", apmcVendor.VendorID);
            sd.Add("P_APMC_V_ID", apmcVendor.APMCVendorID);
            sd.Add("P_U_ID", apmcVendor.UserID);
            sd.Add("P_COMP_ID", apmcVendor.CompanyID);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("cp_SaveAPMCVendor", sd);
            return ds;
        }

        internal static DataSet SaveAPMCInput(APMCInput apmcInput)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_APMC_ID", apmcInput.APMCID);
            sd.Add("P_EXP_PRICE_INCL", apmcInput.InputPrice);
            sd.Add("P_U_ID", apmcInput.UserID);
            sd.Add("P_COMP_ID", apmcInput.Company.CompanyID);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("cp_SaveAPMCInput", sd);
            return ds;
        }

        internal static DataSet SaveAPMCVendorQuantity(APMCVendorQuantity vendorQ)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_APMC_NEG_ID", vendorQ.APMCNegotiationID);
            sd.Add("P_APMC_VQ_ID", vendorQ.APMCVendorQuantityID);
            sd.Add("P_U_ID", vendorQ.UserID);
            sd.Add("P_VENDOR_ID", vendorQ.UserID);
            sd.Add("P_QUANTITY", vendorQ.Quantity);
            sd.Add("P_PRICE", vendorQ.Price);
            sd.Add("P_UNITS", vendorQ.Units);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("cp_SaveAPMCVendorQuantity", sd);
            return ds;
        }
    }
}