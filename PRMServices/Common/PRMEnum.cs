﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Reflection;

namespace PRMServices.Common
{
    public enum PRMStatus
    {
        [Description("UNCONFIRMED")]
        UNCONFIRMED,
        [Description("NOTSTARTED")]
        NOTSTARTED,
        [Description("STARTED")]
        STARTED,
        [Description("Negotiation Ended")]
        NegotiationEnded,
        [Description("Vendor Selected")]
        VendorSelected,
        [Description("PO Processing")]
        POProcessing,
        [Description("PO Generated")]
        POGenerated,
        [Description("PO Sent")]
        POSent,
        [Description("PO Accepted")]
        POAccepted,
        [Description("MATERIAL DISPATCHED")]
        MaterialDispatched,
        [Description("MATERIAL RECEIVED")]
        MaterialReceived,
        [Description("PAYMENT PROCESSING")]
        PaymentPROCESSING,
        [Description("PAYMENT RECEIVED")]
        PaymentReceived,
        [Description("DELETED")]
        DELETED
    }

    public enum PRMStatusCombos
    {
        [Description("UNCONFIRMED")]
        UNCONFIRMED,
        [Description("NOTSTARTED")]
        NOTSTARTED,
        [Description("STARTED")]
        STARTED,
        [Description("Negotiation Ended")]
        NegotiationEnded,
        [Description("Vendor Selected")]
        VendorSelected,
        [Description("PO Processing")]
        POProcessing,
        [Description("PO Generated")]
        POGenerated,
        [Description("PO Sent")]
        POSent,
        [Description("PO Accepted")]
        POAccepted,
        [Description("MATERIAL DISPATCHED")]
        MaterialDispatched,
        [Description("MATERIAL RECEIVED")]
        MaterialReceived,
        [Description("PAYMENT PROCESSING")]
        PaymentPROCESSING,
        [Description("PAYMENT RECEIVED")]
        PaymentReceived,
        [Description("DELETED")]
        DELETED,
        [Description("'UNCONFIRMED','NOTSTARTED'")]
        BEFORESTARTING
    }

    public enum PRMRoles
    {
        [Description("CUSTOMER")]
        CUSTOMER,
        [Description("VENDOR")]
        VENDOR
    }

    public enum PRMSchedule
    {
        [Description("CURRENT")]
        CURRENT
    }
}