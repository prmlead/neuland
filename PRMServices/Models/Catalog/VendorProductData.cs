﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRM.Core.Models;
using PRM.Core.Common;

namespace PRMServices.Models.Catalog
{
    [DataContract]
    public class VendorProductData : EntityExt
    {
        #region 

        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("PR_ID")] public int PR_ID { get; set; }
        [DataMember] [DataNames("PR_CODE")] public string PR_CODE { get; set; }
        [DataMember] [DataNames("CATALOGUE_ITEM_ID")] public int CATALOGUE_ITEM_ID { get; set; }
        [DataMember] [DataNames("PROD_ID")] public string PROD_ID { get; set; }
        [DataMember] [DataNames("REV_UNIT_PRICE")] public string REV_UNIT_PRICE { get; set; }

        [DataMember] [DataNames("LAST_MODIFIED_DATE")] public DateTime LAST_MODIFIED_DATE { get; set; }
        [DataMember] [DataNames("VENDOR_COMPANY")] public string VENDOR_COMPANY { get; set; }
        [DataMember] [DataNames("VENDOR_NAME")] public string VENDOR_NAME { get; set; }
        [DataMember] [DataNames("UNIT_FREIGHT")] public string UNIT_FREIGHT { get; set; }
        [DataMember] [DataNames("REV_UNIT_FREIGHT")] public string REV_UNIT_FREIGHT { get; set; }
        [DataMember] [DataNames("PPS_FIELDS_PO_JSON")] public string PPS_FIELDS_PO_JSON { get; set; }
        [DataMember] [DataNames("UNIT_PRICE")] public string UNIT_PRICE { get; set; }
        [DataMember] [DataNames("REQ_POSTED_ON")] public DateTime? REQ_POSTED_ON { get; set; }
        [DataMember] [DataNames("QUANTITY")] public decimal QUANTITY { get; set; }
        [DataMember] [DataNames("FOR_TO_PAY")] public int FOR_TO_PAY { get; set; }
        [DataMember] [DataNames("IS_SPLIT_ENABLED")] public int IS_SPLIT_ENABLED { get; set; }
        [DataMember] [DataNames("VEND_PROD_DETAILS")] public List<VendorProductDetails> VEND_PROD_DETAILS { get; set; }

       

        #endregion

        #region

        [DataMember] [DataNames("PPS_ID")] public int PPS_ID { get; set; }
        [DataMember] [DataNames("CATALOGUE_ID")] public int CATALOGUE_ID { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DTAE_MODIFIED")] public DateTime? DTAE_MODIFIED { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
        [DataMember] [DataNames("REQ_TITLE")] public string REQ_TITLE { get; set; }
        [DataMember] [DataNames("ITEM_NAME")] public string ITEM_NAME { get; set; }
        [DataMember] [DataNames("PO_SELECTED_VENDOR_ID")] public int PO_SELECTED_VENDOR_ID { get; set; }
        [DataMember] [DataNames("PO_SELECTED_ITEM_ID")] public int PO_SELECTED_ITEM_ID { get; set; }
        [DataMember] [DataNames("PPS_STATUS")] public string PPS_STATUS { get; set; }
        [DataMember] [DataNames("CLOSED")] public string CLOSED { get; set; }

       

        #endregion
    }
}