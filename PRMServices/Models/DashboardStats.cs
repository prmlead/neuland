﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class DashboardStats : Entity
    {
        [DataMember(Name = "totalAuctions")]
        public int TotalAuctions { get; set; }

        [DataMember(Name = "totalAuctionsPending")]
        public int TotalAuctionsPending { get; set; }

        [DataMember(Name = "totalAuctionsCompleted")]
        public int TotalAuctionsCompleted { get; set; }

        [DataMember(Name = "totalTurnaroundTime")]
        public double TotalTurnaroundTime { get; set; }

        [DataMember(Name="totalTurnover")]
        public double TotalTurnover { get; set; }

        [DataMember(Name = "totalSaved")]
        public double TotalSaved { get; set; }

        [DataMember(Name = "totalOrders")]
        public int TotalOrders { get; set; }

        [DataMember(Name = "totalPos")]
        public int TotalPos { get; set; }

        [DataMember(Name = "requirementsList")]
        public List<Requirement> RequirementsList { get; set; }

        [DataMember(Name = "stats")]
        public List<CArrayKeyValue> CArrayKeyValue { get; set; }

        [DataMember(Name = "asnList")]
        public List<DispatchTrack> ASNList { get; set; }

        [DataMember(Name = "noOfIndentsReceived")]
        public int NoOfIndentsReceived { get; set; }

        [DataMember(Name = "noOfPendingRFQ")]
        public int NoOfPendingRFQ { get; set; }

        [DataMember(Name = "noOfClosedRFQ")]
        public int NoOfClosedRFQ { get; set; }


        [DataMember(Name = "TOTAL_RFQS")]
        public int TOTAL_RFQS { get; set; }

        [DataMember(Name = "TOTAL_OPEN_RFQS")]
        public int TOTAL_OPEN_RFQS { get; set; }

        [DataMember(Name = "TOTAL_RFPS")]
        public int TOTAL_RFPS { get; set; }

        [DataMember(Name = "TOTAL_OPEN_RFPS")]
        public int TOTAL_OPEN_RFPS { get; set; }
        [DataMember(Name = "TOTAL_POS")]
        public int TOTAL_POS { get; set; }
        [DataMember(Name = "TOTAL_PENDING_POS")]
        public int TOTAL_PENDING_POS { get; set; }

        [DataMember(Name = "TOTAL_CONTRACTS")]
        public int TOTAL_CONTRACTS { get; set; }
        [DataMember(Name = "TOTAL_PENDING_CONTRACTS")]
        public int TOTAL_PENDING_CONTRACTS { get; set; }
        [DataMember(Name = "TOTAL_GRNS")]
        public int TOTAL_GRNS { get; set; }
        [DataMember(Name = "TOTAL_OPEN_GRNS")]
        public int TOTAL_OPEN_GRNS { get; set; }
        [DataMember(Name = "TOTAL_CLOSED_GRNS")]
        public int TOTAL_CLOSED_GRNS { get; set; }

        [DataMember(Name = "TOTAL_INVOICES")]
        public int TOTAL_INVOICES { get; set; }

    }
}