﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class Questionnaire : ResponseAudit
    {
        [DataMember(Name = "evalID")]
        public int EvalID { get; set; }

        [DataMember(Name="reqID")]
        public int ReqID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID{ get; set; }

        string title = string.Empty;
        [DataMember(Name = "title")]
        public string Title
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                {
                    return title;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    title = value;
                }
            }
        }

        string description = string.Empty;
        [DataMember(Name = "description")]
        public string Description
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                {
                    return description;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    description = value;
                }
            }
        }

        [DataMember(Name = "startDate")]
        public DateTime? StartDate { get; set; }

        [DataMember(Name = "endDate")]
        public DateTime? EndDate { get; set; }

        [DataMember(Name = "questions")]
        public Question[] Questions { get; set; }

        [DataMember(Name = "vendors")]
        public List<VendorDetails> Vendors { get; set; }

        [DataMember(Name = "category")]
        public string Category { get; set; }

        [DataMember(Name = "totalMarks")]
        public double TotalMarks { get; set; }

        [DataMember(Name = "isApproved")]
        public int IsApproved { get; set; }

        [DataMember(Name = "isSentToVendor")]
        public int IsSentToVendor { get; set; }

        [DataMember(Name = "qualifyingMarks")]
        public double QualifyingMarks { get; set; }
    }
}