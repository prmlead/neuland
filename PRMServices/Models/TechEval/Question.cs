﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class Question : ResponseAudit
    {
        [DataMember(Name = "evalID")]
        public int EvalID { get; set; }

        [DataMember(Name = "questionID")]
        public int QuestionID { get; set; }

        [DataMember(Name = "attachmentID")]
        public int AttachmentID { get; set; }

        string type = string.Empty;
        [DataMember(Name = "type")]
        public string Type
        {
            get
            {
                if (!string.IsNullOrEmpty(type))
                {
                    return type;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    type = value;
                }
            }
        }

        string text = string.Empty;
        [DataMember(Name = "text")]
        public string Text
        {
            get
            {
                if (!string.IsNullOrEmpty(text))
                {
                    return text;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    text = value;
                }
            }
        }

        string options = string.Empty;
        [DataMember(Name = "options")]
        public string Options
        {
            get
            {
                if (!string.IsNullOrEmpty(options))
                {
                    return options;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    options = value;
                }
            }
        }

        [DataMember(Name = "marks")]
        public decimal Marks { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "attachment")]
        public byte[] Attachment { get; set; }

        [DataMember(Name = "attachmentName")]
        public string AttachmentName { get; set; }
        
    }
}