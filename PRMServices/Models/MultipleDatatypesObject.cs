﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class MultipleDatatypesObject : Entity
    {
        [DataMember(Name= "INT_1")]
        public int INT_1 { get; set; }

        [DataMember(Name = "INT_2")]
        public int INT_2 { get; set; }

        [DataMember(Name = "INT_3")]
        public int INT_3 { get; set; }

    }
}