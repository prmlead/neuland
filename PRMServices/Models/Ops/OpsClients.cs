﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models


//ClientID, CL_CompName, CL_Domain, DateCreated, DateModified, CreatedBy, ModifiedBy, IsValid, ClientType, ClientStatus, ClientDB
{
    [DataContract]
    public class OpsClients : Entity 
    {
      

        [DataMember(Name = "clientID")]
        public int ClientID { get; set; }

        [DataMember(Name = "cl_compID")]
        public int CL_CompID { get; set; }

        [DataMember(Name = "cl_compName")]
        public string CL_CompName { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "cl_domain")]
        public string CL_Domain { get; set; }

        [DataMember(Name = "dateCreated")]
        public DateTime DateCreated { get
            {
                return DateTime.Now;
            }
            set { }
        }

        [DataMember(Name = "dateModified")]
        public DateTime DateModified
        {
            get
            {
                return DateTime.Now;
            }
            set { }
        }

        [DataMember(Name = "createdBy")]
        public int CreatedBy { get; set; }

        [DataMember(Name = "modifiedBy")]
        public int ModifiedBy { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "clientType")]
        public string ClientType { get; set; }

        [DataMember(Name = "clientStatus")]
        public string ClientStatus { get; set; }

        [DataMember(Name = "clientDB")]
        public string ClientDB { get; set; }


        [DataMember(Name = "reqInfo")]
        public OpsClientReqInfo ReqInfo { get; set; }

        [DataMember(Name = "vendorInfo")]
        public OpsClientVendorInfo VendorInfo{ get; set; }

        [DataMember(Name = "islogisticEnabled")]
        public int IslogisticEnabled { get; set; }

        [DataMember(Name = "isFarwordEnabled")]
        public int IsFarwordEnabled { get; set; }

        [DataMember(Name = "isPREnabled")]
        public int IsPREnabled { get; set; }

        [DataMember(Name = "clientSource")]
        public string ClientSource { get; set; }

        [DataMember(Name = "clientURL")]
        public string ClientURL { get; set; }

        [DataMember(Name = "clientURLPRM")]
        public string ClientURLPRM { get; set; }
    }
}