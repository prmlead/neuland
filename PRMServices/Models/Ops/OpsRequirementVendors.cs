﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models

{
    [DataContract]
    public class OpsRequirementVendors : Entity 
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

       

        string title = string.Empty;
        [DataMember(Name = "REQ_TITLE")]
        public string REQ_TITLE
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                { return title; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { title = value; }
            }
        }

        [DataMember(Name = "reqUserID")]
        public int ReqUserID { get; set; }

        [DataMember(Name = "req_POSTED_ON")]
        public DateTime? REQ_POSTED_ON { get; set; }
        //{
        //    get
        //    { return DateTime.Now; }
        //    set { }
        //}

        [DataMember(Name = "is_QUOTATION_REJECTED")]
        public string IS_QUOTATION_REJECTED { get; set; }

        [DataMember(Name = "quotationUrl")]
        public string QUOTATION_URL { get; set; }

        //[DataMember(Name = "loginDate")]
        //public DateTime? LoginDate { get; set; }

        [DataMember(Name = "u_ID")]
        public int U_ID { get; set; }

        [DataMember(Name = "company_Name")]
        public string COMPANY_NAME { get; set; }

        
        [DataMember(Name = "vendorName")]
        public string VendorName { get; set; }

        [DataMember(Name = "u_PHONE")]
        public string U_PHONE { get; set; }

        [DataMember(Name = "u_EMAIL")]
        public string U_EMAIL { get; set; }

        [DataMember(Name = "u_ALTPHONE")]
        public string U_ALTPHONE { get; set; }

        [DataMember(Name = "quotation_FREEZ_TIME")]
        public DateTime? QUOTATION_FREEZ_TIME { get; set; }

        [DataMember(Name = "exp_START_TIME")]
        public DateTime? EXP_START_TIME { get; set; }

        //[DataMember(Name = "quotation_FREEZ_TIME")]
        //public DateTime QUOTATION_FREEZ_TIME
        //{
        //    get
        //    { return DateTime.Now; }
        //    set { }
        //}

        //[DataMember(Name = "exp_START_TIME")]
        //public DateTime EXP_START_TIME
        //{
        //    get
        //    { return DateTime.Now; }
        //    set { }
        //}

        [DataMember(Name = "start_TIME")]
        public DateTime? START_TIME { get; set; }
        //{
        //    get
        //    { return DateTime.Now; }
        //    set { }
        //}

        //[DataMember(Name = "negotiationendTime")]
        //public string NegotiationendTime { get; set; }

        //[DataMember(Name = "recuirementValue")]
        //public Double RecuirementValue { get; set; }

        //[DataMember(Name = "savings")]
        //public Double Savings { get; set; }

        //[DataMember(Name = "savingsBeforeNeg")]
        //public Double SavingsBeforeNeg { get; set; }

        [DataMember(Name = "lastActiveTime")]
        public string LastActiveTime { get; set; }

        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "vendorPhone")]
        public string VendorPhone { get; set; }

        [DataMember(Name = "vendorAtlMail")]
        public string VendorAtlMail { get; set; }

        [DataMember(Name = "opsVendorDetails")]
        public List<OpsVendorDetails> OpsVendorDetails { get; set; }

        [DataMember(Name = "slotDetails")]
        public List<SlotDetails> slotDetails { get; set; }

        [DataMember(Name = "isSlotCreated")]
        public int IsSlotCreated { get; set; }

        [DataMember(Name = "isOtpVerified")]
        public int IsOtpVerified { get; set; }

        [DataMember(Name = "isEmailVerified")]
        public int IsEmailVerified { get; set; }

        [DataMember(Name = "smsOtp")]
        public int SmsOtp { get; set; }

        [DataMember(Name = "emailOtp")]
        public int EmailOtp { get; set; }

        [DataMember(Name = "vendorLoginID")]
        public string VendorLoginID { get; set; }

        [DataMember(Name = "vendorLoginPass")]
        public string VendorLoginPass { get; set; }

        [DataMember(Name = "bidding_type")]
        public string BIDDING_TYPE { get; set; }

        [DataMember(Name = "closed")]
        public string CLOSED { get; set; }

        [DataMember(Name = "TOTAL_QUOTATIONS")]
        public int TOTAL_QUOTATIONS { get; set; }

        [DataMember(Name = "tranierName")]
        public string TranierName { get; set; }
    }
}