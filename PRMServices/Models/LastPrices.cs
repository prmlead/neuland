﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class LastPrices : Entity
    {
        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "revPrice")]
        public double RevPrice { get; set; }

        string companyName = string.Empty;
        [DataMember(Name = "companyName")]
        public string CompanyName
        {
            get
            {
                return this.companyName;
            }
            set
            {
                this.companyName = value;
            }
        }

        [DataMember(Name = "airline")]
        public string Airline { get; set; }

        string productIDorName = string.Empty;
        [DataMember(Name = "productIDorName")]
        public string ProductIDorName
        {
            get
            {
                if (!string.IsNullOrEmpty(productIDorName))
                { return productIDorName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productIDorName = value; }
            }
        }

        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

        [DataMember(Name = "unitPrice")]
        public double UnitPrice { get; set; }

        [DataMember(Name = "quantity")]
        public double Quantity { get; set; }
    }
}