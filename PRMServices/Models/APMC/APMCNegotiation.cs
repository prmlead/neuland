﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class APMCNegotiation
    {
        [DataMember(Name = "apmcPriceID")]
        public int APMCPriceID { get; set; }

        [DataMember(Name = "apmcID")]
        public int APMCID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "apmcNegotiationID")]
        public int APMCNegotiationID { get; set; }

        [DataMember(Name = "apmcName")]
        public string APMCName { get; set; }

        [DataMember(Name = "vendor")]
        public User Vendor { get; set; }

        [DataMember(Name = "vendorCompany")]
        public Company VendorCompany { get; set; }

        [DataMember(Name = "custPriceInclInit")]
        public decimal CustPriceInclInit { get; set; }

        [DataMember(Name = "custPriceExclInit")]
        public decimal CustPriceExclInit { get; set; }

        [DataMember(Name = "custPriceInclFinal")]
        public decimal CustPriceInclFinal { get; set; }

        [DataMember(Name = "custPriceExclFinal")]
        public decimal CustPriceExclFinal { get; set; }

        [DataMember(Name = "vendorPriceInclInit")]
        public decimal VendorPriceInclInit { get; set; }

        [DataMember(Name = "vendorPriceExclInit")]
        public decimal VendorPriceExclInit { get; set; }

        [DataMember(Name = "vendorPriceInclFinal")]
        public decimal VendorPriceInclFinal { get; set; }

        [DataMember(Name = "vendorPriceExclFinal")]
        public decimal VendorPriceExclFinal { get; set; }

        [DataMember(Name = "differenceIncl")]
        public decimal DifferenceIncl { get; set; }

        [DataMember(Name = "differenceExcl")]
        public decimal DifferenceExcl { get; set; }

        [DataMember(Name = "newPrice")]
        public decimal NewPrice { get; set; }

        [DataMember(Name = "quantity")]
        public decimal Quantity { get; set; }

        [DataMember(Name = "isFrozen")]
        public int IsFrozen { get; set; }

        [DataMember(Name = "quantityUpdated")]
        public int QuantityUpdated { get; set; }

        [DataMember(Name = "modifiedBy")]
        public int ModifiedBy { get; set; }

        [DataMember(Name = "savings")]
        public decimal Savings { get; set; }

        [DataMember(Name = "isNewNegotiationStarted")]
        public int IsNewNegotiationStarted { get; set; }

        [DataMember(Name = "prevNegotiationID")]
        public int PrevNegotiationID { get; set; }

        private DateTime createdDate = DateTime.Now;
        [DataMember(Name = "createdDate")]
        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        [DataMember(Name = "fileStream")]
        public byte[] FileStream { get; set; }

        [DataMember(Name = "fileName")]
        public string FileName { get; set; }

        [DataMember(Name = "attachmentID")]
        public int AttachmentID { get; set; }
    }
}