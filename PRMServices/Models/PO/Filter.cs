﻿
using System;
using System.Runtime.Serialization;
using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class Filter : ResponseAudit
    {

        [DataMember(Name = "ID")]
        public int ID { get; set; }

        string type = string.Empty;
        [DataMember(Name = "type")]
        public string TYPE
        {
            get
            {
                if (!string.IsNullOrEmpty(type))
                { return type; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { type = value; }
            }
        }

        string name = string.Empty;
        [DataMember(Name = "name")]
        public string NAME
        {
            get
            {
                if (!string.IsNullOrEmpty(name))
                { return name; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { name = value; }
            }
        }

    }
}