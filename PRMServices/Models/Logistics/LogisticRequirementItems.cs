﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class LogisticRequirementItems : Entity
    {
        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "listQuotationItems")]
        public List<LogisticQuotationItems> ListQuotationItems { get; set; }

        string productIDorName = string.Empty;
        [DataMember(Name = "productIDorName")]
        public string ProductIDorName
        {
            get
            {
                if (!string.IsNullOrEmpty(productIDorName))
                { return productIDorName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productIDorName = value; }
            }
        }

        string productIDorNameCustomer = string.Empty;
        [DataMember(Name = "productIDorNameCustomer")]
        public string ProductIDorNameCustomer
        {
            get
            {
                if (!string.IsNullOrEmpty(productIDorNameCustomer))
                { return productIDorNameCustomer; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productIDorNameCustomer = value; }
            }
        }

        string productNo = string.Empty;
        [DataMember(Name = "productNo")]
        public string ProductNo
        {
            get
            {
                if (!string.IsNullOrEmpty(productNo))
                { return productNo; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productNo = value; }
            }
        }

        string productNoCustomer = string.Empty;
        [DataMember(Name = "productNoCustomer")]
        public string ProductNoCustomer
        {
            get
            {
                if (!string.IsNullOrEmpty(productNoCustomer))
                { return productNoCustomer; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productNoCustomer = value; }
            }
        }

        string productDescription = string.Empty;
        [DataMember(Name = "productDescription")]
        public string ProductDescription
        {
            get
            {
                if (!string.IsNullOrEmpty(productDescription))
                { return productDescription; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productDescription = value; }
            }
        }

        string productDescriptionCustomer = string.Empty;
        [DataMember(Name = "productDescriptionCustomer")]
        public string ProductDescriptionCustomer
        {
            get
            {
                if (!string.IsNullOrEmpty(productDescriptionCustomer))
                { return productDescriptionCustomer; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productDescriptionCustomer = value; }
            }
        }

        string productBrand = string.Empty;
        [DataMember(Name = "productBrand")]
        public string ProductBrand
        {
            get
            {
                if (!string.IsNullOrEmpty(productBrand))
                { return productBrand; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productBrand = value; }
            }
        }

        string productBrandCustomer = string.Empty;
        [DataMember(Name = "productBrandCustomer")]
        public string ProductBrandCustomer
        {
            get
            {
                if (!string.IsNullOrEmpty(productBrandCustomer))
                { return productBrandCustomer; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productBrandCustomer = value; }
            }
        }

        string othersBrands = string.Empty;
        [DataMember(Name = "othersBrands")]
        public string OthersBrands
        {
            get
            {
                if (!string.IsNullOrEmpty(othersBrands))
                { return othersBrands; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { othersBrands = value; }
            }
        }

        [DataMember(Name = "isDeleted")]
        public int IsDeleted { get; set; }


        [DataMember(Name = "productQuantity")]
        public double ProductQuantity { get; set; }

        string productQuantityIn = string.Empty;
        [DataMember(Name = "productQuantityIn")]
        public string ProductQuantityIn
        {
            get
            {
                if (!string.IsNullOrEmpty(productQuantityIn))
                { return productQuantityIn; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productQuantityIn = value; }
            }
        }

        [DataMember(Name = "productImageID")]
        public int ProductImageID { get; set; }

        [DataMember(Name = "createdDate")]
        public DateTime? CreatedDate { get; set; }

        string attachmentName = string.Empty;
        [DataMember(Name = "attachmentName")]
        public string AttachmentName
        {
            get
            {
                if (!string.IsNullOrEmpty(attachmentName))
                { return attachmentName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { attachmentName = value; }
            }
        }

        string attachmentBase64 = string.Empty;
        [DataMember(Name = "attachmentBase64")]
        public string AttachmentBase64
        {
            get
            {
                if (!string.IsNullOrEmpty(attachmentBase64))
                { return attachmentBase64; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { attachmentBase64 = value; }
            }
        }

        [DataMember(Name = "itemAttachment")]
        public byte[] ItemAttachment { get; set; }

        [DataMember(Name = "quotation")]
        public byte[] Quotation { get; set; }

        [DataMember(Name = "fileType")]
        public string FileType { get; set; }

        [DataMember(Name = "itemPrice")]
        public double ItemPrice { get; set; }

        [DataMember(Name = "tax")]
        public double Tax { get; set; }

        [DataMember(Name = "productSNo")]
        public int ProductSNo { get; set; }

        [DataMember(Name = "selectedVendorID")]
        public int SelectedVendorID { get; set; }

        [DataMember(Name = "revisedItemPrice")]
        public double RevisedItemPrice { get; set; }

        [DataMember(Name = "revisedInitialprice")]
        public double RevisedInitialprice { get; set; }

        [DataMember(Name = "revisedFreightcharges")]
        public double RevisedFreightcharges { get; set; }

        [DataMember(Name = "revisedVendorBidPrice")]
        public double RevisedVendorBidPrice { get; set; }

        [DataMember(Name = "isRevised")]
        public int IsRevised { get; set; }

        [DataMember(Name = "revitemPrice")]
        public double RevItemPrice { get; set; }

        [DataMember(Name = "unitPrice")]
        public double UnitPrice { get; set; }

        [DataMember(Name = "revUnitPrice")]
        public double RevUnitPrice { get; set; }

        [DataMember(Name = "cGst")]
        public double CGst { get; set; }

        [DataMember(Name = "sGst")]
        public double SGst { get; set; }

        [DataMember(Name = "iGst")]
        public double IGst { get; set; }

        [DataMember(Name = "unitMRP")]
        public double UnitMRP { get; set; }

        [DataMember(Name = "unitDiscount")]
        public double UnitDiscount { get; set; }

        [DataMember(Name = "revUnitDiscount")]
        public double RevUnitDiscount { get; set; }

        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "costPrice")]
        public double CostPrice { get; set; }

        [DataMember(Name = "netPrice")]
        public double NetPrice { get; set; }

        [DataMember(Name = "marginAmount")]
        public double MarginAmount { get; set; }


        string vendorRemarks = string.Empty;
        [DataMember(Name = "vendorRemarks")]
        public string VendorRemarks
        {
            get
            {
                if (!string.IsNullOrEmpty(vendorRemarks))
                { return vendorRemarks; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { vendorRemarks = value; }
            }
        }




        //   TYPE_OF_PACKING, MODE_OF_SHIPMENT, NATURE_OF_GOODS, 
        //    STORAGE_CONDITION, PORT_OF_LANDING, FINAL_DESTINATION, DELIVERY_LOCATION, PREFERRED_AIRLINES

        string typeOfPacking = string.Empty;
        [DataMember(Name = "typeOfPacking")]
        public string TypeOfPacking
        {
            get
            {
                if (!string.IsNullOrEmpty(typeOfPacking))
                { return typeOfPacking; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { typeOfPacking = value; }
            }
        }

        string modeOfShipment = string.Empty;
        [DataMember(Name = "modeOfShipment")]
        public string ModeOfShipment
        {
            get
            {
                if (!string.IsNullOrEmpty(modeOfShipment))
                { return modeOfShipment; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { modeOfShipment = value; }
            }
        }

        string natureOfGoods = string.Empty;
        [DataMember(Name = "natureOfGoods")]
        public string NatureOfGoods
        {
            get
            {
                if (!string.IsNullOrEmpty(natureOfGoods))
                { return natureOfGoods; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { natureOfGoods = value; }
            }
        }

        string storageCondition = string.Empty;
        [DataMember(Name = "storageCondition")]
        public string StorageCondition
        {
            get
            {
                if (!string.IsNullOrEmpty(storageCondition))
                { return storageCondition; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { storageCondition = value; }
            }
        }

        string portOfLanding = string.Empty;
        [DataMember(Name = "portOfLanding")]
        public string PortOfLanding
        {
            get
            {
                if (!string.IsNullOrEmpty(portOfLanding))
                { return portOfLanding; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { portOfLanding = value; }
            }
        }

        string finalDestination = string.Empty;
        [DataMember(Name = "finalDestination")]
        public string FinalDestination
        {
            get
            {
                if (!string.IsNullOrEmpty(finalDestination))
                { return finalDestination; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { finalDestination = value; }
            }
        }

        string deliveryLocation = string.Empty;
        [DataMember(Name = "deliveryLocation")]
        public string DeliveryLocation
        {
            get
            {
                if (!string.IsNullOrEmpty(deliveryLocation))
                { return deliveryLocation; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { deliveryLocation = value; }
            }
        }

        string preferredAirlines = string.Empty;
        [DataMember(Name = "preferredAirlines")]
        public string PreferredAirlines
        {
            get
            {
                if (!string.IsNullOrEmpty(preferredAirlines))
                { return preferredAirlines; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { preferredAirlines = value; }
            }
        }

        //   TYPE_OF_PACKING, MODE_OF_SHIPMENT, NATURE_OF_GOODS, 
        //    STORAGE_CONDITION, PORT_OF_LANDING, FINAL_DESTINATION, DELIVERY_LOCATION, PREFERRED_AIRLINES

        [DataMember(Name = "isEnabled")]
        public Boolean IsEnabled { get; set; }

        [DataMember(Name = "netWeight")]
        public double NetWeight { get; set; }

        [DataMember(Name = "ispalletize")]
        public int Ispalletize { get; set; }

        [DataMember(Name = "palletizeQty")]
        public double PalletizeQty { get; set; }

        [DataMember(Name = "palletizeLength")]
        public double PalletizeLength { get; set; }

        [DataMember(Name = "palletizeBreadth")]
        public double PalletizeBreadth { get; set; }

        [DataMember(Name = "palletizeHeight")]
        public double PalletizeHeight { get; set; }

        string productId = string.Empty;
        [DataMember(Name = "productId")]
        public string ProductId
        {
            get
            {
                if (!string.IsNullOrEmpty(productId))
                { return productId; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productId = value; }
            }
        }

        [DataMember(Name = "CATALOGUE_ID")]
        public double CATALOGUE_ID { get; set; }
        


    }
}