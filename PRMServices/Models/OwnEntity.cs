﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class OwnEntity : Entity
    {
        [DataMember(Name = "purchaser")]
        public string purchaser { get; set; }

        [DataMember(Name = "email")]
        public string email { get; set; }
    }
}