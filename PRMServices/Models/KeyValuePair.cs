﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class KeyValuePair : Entity
    {
        [DataMember(Name = "key")]
        public int Key { get; set; }

        [DataMember(Name = "key1")]
        public string Key1 { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "year")]
        public string Year { get; set; }

        [DataMember(Name = "value1")]
        public int Value1 { get; set; }

        [DataMember(Name = "decimalVal")]
        public decimal DecimalVal { get; set; }

        [DataMember(Name = "decimalVal1")]
        public decimal DecimalVal1 { get; set; }

        [DataMember(Name = "arrayVal")]
        public object[] ArrayVal { get; set; }

        [DataMember(Name = "keyDateTime")]
        public DateTime? KeyDateTime { get; set; }
    }

    [DataContract]
    public class CArrayKeyValue
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "arrayPair")]
        public List<KeyValuePair> ArrayPair { get; set; }
    }
}