﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]  
    public class ReductionSetting : Entity
    {
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("REDUCTION_SETTING")] public string REDUCTION_SETTING { get; set; }
        [DataMember] [DataNames("REDUCTION_SETTING_VALUE")] public string REDUCTION_SETTING_VALUE { get; set; }
        [DataMember] [DataNames("USER")] public int USER { get; set; }
    }
}