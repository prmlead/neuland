﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class ReqMaterialDetails : Entity
    {
        [DataMember(Name = "requirementID")]
        public int ReqID { get; set; }

        [DataMember(Name = "customerID")]
        public int CustomerID { get; set; }

        [DataMember(Name = "dispatchedFrom")]
        public string DisPatchedFrom { get; set; }

        [DataMember(Name = "dispatchedMode")]
        public string DispatchedMode { get; set; }

        [DataMember(Name = "dispatchedThrough")]
        public string DispatchedThrough { get; set; }

        [DataMember(Name = "expectedDelivery")]
        public DateTime ExpectedDelivery { get; set; }

        [DataMember(Name = "comments")]
        public string Comments { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "invoiceNumber")]
        public string invoiceNumber { get; set; }

        [DataMember(Name = "dCNumber")]
        public string DCNumber { get; set; }

        [DataMember(Name = "trackingNumber")]
        public string TrackingNumber { get; set; }

        [DataMember(Name = "attachment")]
        public byte[] Attachment { get; set; }

        [DataMember(Name = "attachmentName")]
        public string AttachmentName { get; set; }
        
    }
}