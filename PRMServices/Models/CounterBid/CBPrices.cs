﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class CBPrices : Entity
    {
        [DataMember] [DataNames("CB_AH_ID")] public int CB_AH_ID { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("V_ID")] public int V_ID { get; set; }

        [DataMember] [DataNames("REV_PF_CHARGES")] public decimal REV_PF_CHARGES { get; set; }
        [DataMember] [DataNames("REV_PF_TOTAL_CHARGES")] public decimal REV_PF_TOTAL_CHARGES { get; set; }
        [DataMember] [DataNames("REV_INST_CHARGES")] public decimal REV_INST_CHARGES { get; set; }
        [DataMember] [DataNames("REV_INST_TOTAL_CHARGES")] public decimal REV_INST_TOTAL_CHARGES { get; set; }
        [DataMember] [DataNames("REV_TOTAL_PRICE_WITHOUTTAX")] public decimal REV_TOTAL_PRICE_WITHOUTTAX { get; set; }
        [DataMember] [DataNames("REV_TOTAL_PRICE")] public decimal REV_TOTAL_PRICE { get; set; }

        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }

        [DataMember] [DataNames("CBItemPricesList")] public List<CBItemPrices> CBItemPricesList { get; set; }

        [DataMember] [DataNames("BID_COMMENTS")] public string BID_COMMENTS { get; set; }

        [DataMember] [DataNames("U_FNAME")] public string U_FNAME { get; set; }
        [DataMember] [DataNames("U_LNAME")] public string U_LNAME { get; set; }
        [DataMember] [DataNames("COMPANY_NAME")] public string COMPANY_NAME { get; set; }


    }
}