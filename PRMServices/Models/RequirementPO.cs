﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace PRMServices.Models
{
    [DataContract]
    public class RequirementPO : Entity
    {
        [DataMember(Name = "POID")]
        public string POID { get; set; }

        [DataMember(Name = "requirementID")]
        public int requirementID { get; set; }

        [DataMember(Name = "customerID")]
        public int customerID { get; set; }

        [DataMember(Name="selectedVendor")]
        public VendorDetails SelectedVendor { get; set; }

        [DataMember(Name = "deliveryAddress")]
        public string DeliveryAddress { get; set; }

        [DataMember(Name = "expectedDelivery")]
        public DateTime? ExpectedDelivery { get; set; }

        [DataMember(Name = "billingAddress")]
        public string BillingAddress { get; set; }

        [DataMember(Name = "paymentScheduleDate")]
        public string PaymentScheduleDate { get; set; }

        [DataMember(Name = "paymentMode")]
        public string PaymentMode { get; set; }

        [DataMember(Name = "comments")]
        public string Comments { get; set; }


        [DataMember(Name="poFile")]
        public FileUpload POFile { get; set; }

        [DataMember(Name = "expPaymentDate")]
        public DateTime? ExpPaymentDate { get; set; }

        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "poLink")]
        public string POLink { get; set; }

        [DataMember(Name = "materialDispachmentLink")]
        public int MaterialDispachmentLink { get; set; }

        [DataMember(Name = "materialReceivedLink")]
        public int MaterialReceivedLink { get; set; }
    }
}