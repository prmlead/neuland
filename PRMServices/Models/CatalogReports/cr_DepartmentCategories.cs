﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class cr_DepartmentCategories : Entity
    {

        [DataMember] [DataNames("DEPT_ID")] public int DEPT_ID { get; set; }
        [DataMember] [DataNames("CAT_ID")] public int CAT_ID { get; set; }
        [DataMember] [DataNames("PARENT_CAT_ID")] public int PARENT_CAT_ID { get; set; }
        [DataMember] [DataNames("CAT_CODE")] public string CAT_CODE { get; set; }
        [DataMember] [DataNames("CAT_NAME")] public string CAT_NAME { get; set; }
        [DataMember] [DataNames("CAT_DESC")] public string CAT_DESC { get; set; }

    }
}