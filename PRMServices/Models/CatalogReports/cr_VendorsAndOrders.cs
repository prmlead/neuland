﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class cr_VendorsAndOrders : Entity
    {

        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("PPS_ID")] public int PPS_ID { get; set; }
        [DataMember] [DataNames("CATALOGUE_ITEM_ID")] public int CATALOGUE_ITEM_ID { get; set; }
        [DataMember] [DataNames("V_COMPANY_NAME")] public string V_COMPANY_NAME { get; set; }
        [DataMember] [DataNames("POSTED_DATE")] public DateTime? POSTED_DATE { get; set; }
        [DataMember] [DataNames("TOTAL_POS")] public int TOTAL_POS { get; set; }
        [DataMember] [DataNames("TOTAL_ITEM_PRICE")] public decimal TOTAL_ITEM_PRICE { get; set; }
        [DataMember] [DataNames("REQ_TITLE")] public string REQ_TITLE { get; set; }
        [DataMember] [DataNames("PLANT_NAME")] public string PLANT_NAME { get; set; }
        [DataMember] [DataNames("PLANT_ID")] public int PLANT_ID { get; set; }

    }
}