﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ImportEntity : Entity
    {
        [DataMember(Name = "entityName")]
        public string EntityName
        {
            get;
            set;
        }

        [DataMember(Name = "userid")]
        public int UserID
        {
            get;
            set;
        }

        [DataMember(Name = "companyId")]
        public int CompanyId
        {
            get;
            set;
        }

        [DataMember(Name = "attachment")]
        public byte[] Attachment
        {
            get;
            set;
        }
    }
}