﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class StoreRequestDetails : ResponseAudit
    {
        [DataMember(Name = "requestDetailsID")]
        public int RequestDetailsID { get; set; }

        [DataMember(Name = "storeRequestInfo")]
        public StoreRequest StoreRequestInfo { get; set; }

        [DataMember(Name = "storeItemIfno")]
        public StoreItem StoreItemIfno { get; set; }

        [DataMember(Name = "quantity")]
        public int Quantity { get; set; }

        [DataMember(Name = "comments")]
        public string Comments { get; set; }

        [DataMember(Name = "itemRequestStatus")]
        public string ItemRequestStatus { get; set; }

        [DataMember(Name = "approvalQuantity")]
        public int ApprovalQuantity { get; set; }

        [DataMember(Name = "rejectedQuantity")]
        public int RejectedQuantity { get; set; }

        [DataMember(Name = "itemPrice")]
        public decimal ItemPrice { get; set; }
    }
}