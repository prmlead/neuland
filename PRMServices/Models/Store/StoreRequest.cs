﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class StoreRequest : ResponseAudit
    {
        [DataMember(Name = "requestID")]
        public int RequestID { get; set; }

        [DataMember(Name = "storeDetails")]
        public Store StoreDetails { get; set; }

        [DataMember(Name = "companyID")]
        public int CompanyID { get; set; }

        [DataMember(Name = "isBuffer")]
        public int IsBuffer { get; set; }

        [DataMember(Name = "requestType")]
        public string RequestType { get; set; }

        [DataMember(Name = "requestComments")]
        public string RequestComments { get; set; }

        [DataMember(Name = "requestStatus")]
        public string RequestStatus { get; set; }

        [DataMember(Name = "requester")]
        public int Requester { get; set; }

        [DataMember(Name = "requestDept")]
        public string RequestDept { get; set; }

        [DataMember(Name = "approver")]
        public int Approver { get; set; }

        [DataMember(Name = "requestDate")]
        public DateTime? RequestDate { get; set; }

        [DataMember(Name = "totalAmount")]
        public decimal TotalAmount { get; set; }
    }
}