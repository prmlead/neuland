﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRM.Core.Common;

namespace PRMServices.Models.Vendor
{
    [DataContract]
    public class Ratings
    {
        [DataMember]
        [DataNames("R_ID")]
        public string R_ID { get; set; }

        [DataMember]
        [DataNames("REQ_ID")]
        public string REQ_ID { get; set; }

        [DataMember]
        [DataNames("REQ_TITLE")]
        public string REQ_TITLE { get; set; }

        [DataMember]
        [DataNames("REVIEWEE_ID")]
        public int REVIEWEE_ID { get; set; }

        [DataMember]
        [DataNames("REVIEWER_ID")]
        public int REVIEWER_ID { get; set; }


        [DataMember]
        [DataNames("REVIEWEE_DISP_NAME")]
        public string REVIEWEE_DISP_NAME { get; set; }

        [DataMember]
        [DataNames("REVIEWER_DISP_NAME")]
        public string REVIEWER_DISP_NAME { get; set; }

        [DataMember]
        [DataNames("DELIERY_RATING")]
        public decimal DELIERY_RATING { get; set; }

        [DataMember]
        [DataNames("QUALITY_RATING")]
        public decimal QUALITY_RATING { get; set; }

        [DataMember]
        [DataNames("EMERGENCY_RATING")]
        public decimal EMERGENCY_RATING { get; set; }

        [DataMember]
        [DataNames("SERVICE_RATING")]
        public decimal SERVICE_RATING { get; set; }

        [DataMember]
        [DataNames("RESPONSE_RATING")]
        public decimal RESPONSE_RATING { get; set; }

        [DataMember]
        [DataNames("COMMENTS")]
        public string COMMENTS { get; set; }

        [DataMember]
        [DataNames("DATE_CREATED")]
        public DateTime DATE_CREATED { get; set; }

        [DataMember]
        [DataNames("DATE_MODIFIED")]
        public DateTime DATE_MODIFIED { get; set; }
    }
}