﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class ValueAdditoinToThePatient : Entity
    {
        [DataMember(Name = "patientOutcome")]
        public int PatientOutcome { get; set; }

        [DataMember(Name = "patiencySafety")]
        public int PatiencySafety { get; set; }

        [DataMember(Name = "operationalEfficiency")]
        public int OperationalEfficiency { get; set; }

    }
}