﻿prmApp.constant('PRMPOServiceDomain', 'svc/PRMPOService.svc/REST/');
prmApp.service('PRMGRNService', ["PRMPOServiceDomain", "SAPIntegrationServicesDomain", "userService", "httpServices",
    function (PRMPOServiceDomain, SAPIntegrationServicesDomain, userService, httpServices) {

        var PRMGRNService = this;

        PRMGRNService.getGRNDetailsList = function (params) {

            let url = PRMPOServiceDomain + 'getgrndetailslist?compid=' + params.compid + '&uid=' + params.uid + '&search=' + params.search + '&supplier=' + params.supplier + '&status=' + params.status
                + '&fromdate=' + params.fromdate + '&todate=' + params.todate
                + '&page=' + params.page
                + '&pagesize=' + params.pagesize + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMGRNService.getPOScheduleFilterValues = function (params) {
            let url = PRMPOServiceDomain + 'getposchedulefilters?compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        return PRMGRNService;

    }]);