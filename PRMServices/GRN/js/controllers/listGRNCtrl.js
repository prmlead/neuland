﻿prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('listGRNCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMGRNService", "poService",
        "PRMCustomFieldService", "fileReader",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMGRNService, poService, PRMCustomFieldService, fileReader) {

            $scope.isGRN = $stateParams.isGRN;
            $scope.grnNo = $stateParams.grnNo;
            console.log($scope.isGRN);
            
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            //$scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();

            $scope.grnStats = {
                totalgrns: 0
            };
            $scope.selectedGRNItems = [];

            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.isVendor = userService.getUserType() === "VENDOR" ? true : false;

            $scope.filtersList = {
                itemList: [],
                supplierNameList: []
            };
            if ($stateParams.fromDate == '' && $stateParams.toDate == '') {
                $scope.filters = {
                    itemName: {},
                    supplier: {},
                    grnFromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
                    grnToDate: moment().format('YYYY-MM-DD'),
                    status: 'All'
                };
            } else{
                $scope.filters = {
                    itemName: {},
                    supplier: {},
                    grnFromDate: $stateParams.fromDate ,
                    grnToDate: $stateParams.toDate,
                    status: 'All'
                };
            }


            //$scope.filters = {
            //    itemName: {},
            //    supplier: {},
            //    grnFromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
            //    grnToDate: moment().format('YYYY-MM-DD'),
            //    status:'All'
            //};

            if (+$stateParams.openGRN == 0 && +$stateParams.closedGRN == 0) {
                $scope.filters.status = 'All';
            } else if (+$stateParams.openGRN == 1) {
                $scope.filters.status = 'Open';
            } else if (+$stateParams.closedGRN == 1) {
                $scope.filters.status = 'Closed';
            }

            
            //$scope.filters.grnToDate = moment().format('YYYY-MM-DD');
            //$scope.filters.grnFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getGrnList(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.totalItems2 = 0;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize2 = 5;

            $scope.setPage2 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged2 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            /*PAGINATION CODE*/


            $scope.grnList = [];
            $scope.filteredGrnsList = [];
            $scope.grnItems = [];

            $('.selected-items-box').bind('click', function (e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });
            var isFilter = false;
            $scope.setFilters = function (currentPage) {
                $scope.grnStats.totalgrns = 0;


                $scope.filteredGrnsList = $scope.grnList;
                $scope.totalItems = $scope.filteredGrnsList.length;


                if ($scope.filters.searchKeyword || !_.isEmpty($scope.filters.itemName) || !_.isEmpty($scope.filters.supplier) || !_.isEmpty($scope.filters.status)) {
                    $scope.getGrnList(0, 10, $scope.filters.searchKeyword);
                } else {

                    if ($scope.initialgrnPageArray && $scope.initialgrnPageArray.length > 0) {
                        $scope.grnList = $scope.initialgrnPageArray;
                        if ($scope.grnList && $scope.grnList.length > 0) {

                            $scope.totalItems = $scope.grnList[0].TOTAL_COUNT;
                            $scope.grnStats.totalgrns = $scope.totalItems;

                            $scope.filteredGrnsList = $scope.grnList;
                        }

                    }
                }



            };

            $scope.filterByDate = function () {
                $scope.grnStats.totalgrns = 0;


                $scope.filteredGrnsList = $scope.grnList;
                $scope.totalItems = $scope.filteredGrnsList.length;
                $scope.getGrnList(0, 10, $scope.filters.searchKeyword);

            };

            $scope.totalCount = 0;
            $scope.searchString = '';
            $scope.initialgrnPageArray = [];

            $scope.getGrnList = function (recordsFetchFrom, pageSize, searchString) {

                var itemName, supplier, grnFromDate, grnToDate;


                if (_.isEmpty($scope.filters.grnFromDate)) {
                    grnFromDate = '';
                } else {
                    grnFromDate = $scope.filters.grnFromDate;
                }

                if (_.isEmpty($scope.filters.grnToDate)) {
                    grnToDate = '';
                } else {
                    grnToDate = $scope.filters.grnToDate;
                }

                if (_.isEmpty($scope.filters.status)) {
                    status = '';
                } else {
                    status= $scope.filters.status;
                }
                //if (_.isEmpty($scope.filters.itemName)) {
                //    itemName = '';
                //} else if ($scope.filters.itemName && $scope.filters.itemName.length > 0) {
                //    var itemNames = _($scope.filters.itemName)
                //        .filter(item => item.name)
                //        .map('name')
                //        .value();
                //    itemName = itemNames.join(',');
                //}


                if (_.isEmpty($scope.filters.supplier)) {
                    supplier = '';
                } else if ($scope.filters.supplier && $scope.filters.supplier.length > 0) {
                    var suppliers = _($scope.filters.supplier)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    supplier = suppliers.join(',');
                }


                var params = {
                    "compid": $scope.isCustomer ? $scope.compId : 0,
                    "uid": $scope.isCustomer ? 0 : $scope.userID,
                    "search": searchString ? searchString : "",
                    //"item": itemName,
                    "supplier": supplier,
                    "status": status,
                    "fromdate": $scope.isGRN ? '1970-01-01' : grnFromDate,
                    "todate": $scope.isGRN ? '2100-01-01' : grnToDate,
                    "page": recordsFetchFrom * pageSize,
                    "pagesize": pageSize,
                    "sessionid": userService.getUserToken()

                };

                $scope.pageSizeTemp = (params.page + 1);
                $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);

                PRMGRNService.getGRNDetailsList(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.IS_DISABLED = false;
                                item.ITEM_RECEIVED_DATE = item.ITEM_RECEIVED_DATE ? moment(item.ITEM_RECEIVED_DATE).format("DD-MM-YYYY") : '-';
                                item.GRN_DATE = item.GRN_DATE ? moment(item.GRN_DATE).format("DD-MM-YYYY") : '-';
                                var availableQty = (item.RECEIVED_QUANTITY - item.INVOICE_COMPLETED_QTY);
                                item.IS_DISABLED = availableQty <= 0 ? true : false;
                                item.INVOICE_AVAILABLE_QTY = availableQty;
                            });
                        }
                        $scope.grnList = [];
                        $scope.filteredGrnsList = [];
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                $scope.grnList.push(item);
                                if ($scope.initialgrnPageArray.length <= 9) { // Push Initial 10 Records When Page is Loaded because needed in SetFilters function it's getting called every time (need to modify directive code)  #Crap Code need to remove(should think of another solution)

                                    //var ifExists1 = _.findIndex($scope.initialgrnPageArray, function (po) { return po.PO_NUMBER === item.PO_NUMBER });
                                    //if (ifExists1 <= -1) {
                                    //    $scope.initialgrnPageArray.push(item);
                                    //}

                                    //$scope.initialgrnPageArray.push(item);
                                }
                            });

                        }

                        if ($scope.grnList && $scope.grnList.length > 0) {
                            $scope.totalItems = $scope.grnList[0].TOTAL_COUNT;
                            $scope.grnStats.totalgrns = $scope.totalItems;

                            $scope.filteredGrnsList = $scope.grnList;
                            $scope.makeCheckedGRN();
                        }


                    });
            };


            if ($scope.isGRN) {
                $scope.getGrnList(0, 10, $scope.grnNo);
            } else {
                $scope.getGrnList(0, 10, $scope.searchString);
            }

            $scope.filterValues = [];

            $scope.getFilterValues = function () {
                var params =
                {
                    "compid": $scope.isCustomer ? $scope.compId : 0
                };

                let itemListTemp = [];
                let supplierNameListTemp = [];


                PRMGRNService.getPOScheduleFilterValues(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.name === 'ITEM') {
                                        item.arrayPair.forEach(function (item) {
                                            itemListTemp.push({ id: item.key, name: item.value });
                                        });
                                        departmentListTemp.push({ id: item.arrayPair.key, name: item.arrayPair.value });
                                    } else if (item.name === 'VENDORS') {
                                        item.arrayPair.forEach(function (item) {
                                            supplierNameListTemp.push({ id: item.key, name: item.value });
                                        });
                                    }

                                });


                                $scope.filtersList.itemList = itemListTemp;
                                $scope.filtersList.supplierNameList = supplierNameListTemp;

                            }
                        }
                    });

            };
            $scope.getFilterValues();

            $scope.grnDet = {
                grnLevel: true
            };

            $scope.createInvoice = function (grnDet) {
                $scope.filteredGrnsList.forEach(function (grnObj, prIndex) {
                    if (grnDet.GRN_NUMBER === grnObj.GRN_NUMBER) {
                        if (grnDet.selectGRNForInvoice)  /*&& +grnDet.REQ_ID <= 0*/ {
                            grnObj.isCheckedGRNItem = true;
                            grnObj.selectGRNForInvoice = true;
                            grnObj.isSelected = true;
                            grnObj.GRN_QTY = (grnObj.RECEIVED_QUANTITY - grnObj.REJECTED_QUANTITY);
                            $scope.selectedGRNItems.push(grnObj);
                        } else {
                            grnObj.isCheckedGRNItem = false;
                            grnObj.selectGRNForInvoice = false;
                            grnObj.isSelected = false;
                            let index = _.findIndex($scope.selectedGRNItems, function (grn) { return !grn.isSelected; });
                            if (index >= 0) {
                                $scope.selectedGRNItems.splice(index, 1);
                            }
                        }
                    }
                });
            };




            $scope.showInvoiceButton = function () {
                let isvisible = false;
                $scope.selectedGRNNumbers = '';
                if ($scope.selectedGRNItems && $scope.selectedGRNItems.length > 0) {
                    let validSelectedGRNs = _.filter($scope.selectedGRNItems, function (grnObj) {
                        return grnObj.isSelected;
                    });

                    if (validSelectedGRNs && validSelectedGRNs.length > 0) {
                        var grnNumbers = _(validSelectedGRNs)
                            .map('GRN_NUMBER')
                            .value();
                        grnNumbers = _.uniqBy(grnNumbers);
                        $scope.selectedGRNNumbers = grnNumbers.join(',');
                        isvisible = true;
                    }
                }

                return isvisible;
            };



            $scope.makeCheckedGRN = function () {
                if ($scope.selectedGRNItems && $scope.selectedGRNItems.length > 0) {
                    $scope.selectedGRNItems.forEach(function (item) {
                        $scope.filteredGrnsList.forEach(function (grnObj) {
                            if (grnObj.GRN_NUMBER == item.GRN_NUMBER) {
                                grnObj.isCheckedGRNItem = true;
                                grnObj.selectGRNForInvoice = true;
                            }
                        });
                    })
                }
            };



            $scope.routeToInvoice = function () {
                $scope.selectedGRNItems = _.filter($scope.selectedGRNItems, function (grnItemObj) { return grnItemObj.isSelected; });
                $state.go('createInvoice', { "invoiceID": 0, 'detailsObj': $scope.selectedGRNItems });
            }

            $scope.goToInvoicePage = function (invoice) {
                $state.go('vendorPoInvoices', { 'invoiceNumber':invoice })

            };

        }]);