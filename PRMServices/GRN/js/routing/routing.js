﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('list-ViewGRN', {
                    url: '/list-ViewGRN/:grnNo',
                    templateUrl: 'GRN/views/list-GRN.html',
                    params: {
                        detailsObj: null,
                        isGRN: true
                    }
                })
                .state('list-GRN', {
                    url: '/list-GRN',
                    templateUrl: 'GRN/views/list-GRN.html',
                    params: {
                        detailsObj: null,
                        openGRN: 0,
                        closedGRN: 0,
                        fromDate: '',
                        toDate:''
                    }
                })


        }]);