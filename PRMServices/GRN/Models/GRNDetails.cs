﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class GRNDetails : ResponseAudit
    {
        [DataMember] [DataNames("GRN_NUMBER")] public string GRN_NUMBER { get; set; }
        [DataMember] [DataNames("GRN_LINE_ITEM")] public string GRN_LINE_ITEM { get; set; }
        [DataMember] [DataNames("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
        [DataMember] [DataNames("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
        [DataMember] [DataNames("PO_QUANTITY")] public decimal PO_QUANTITY { get; set; }
        [DataMember] [DataNames("CGST")] public decimal CGST { get; set; }
        [DataMember] [DataNames("SGST")] public decimal SGST { get; set; }
        [DataMember] [DataNames("IGST")] public decimal IGST { get; set; }
        [DataMember] [DataNames("PO_ITEM_UNIT_RATE")] public decimal PO_ITEM_UNIT_RATE { get; set; }
        [DataMember] [DataNames("ITEM_RECEIVED_DATE")] public DateTime? ITEM_RECEIVED_DATE { get; set; }
        [DataMember] [DataNames("GRN_DATE")] public DateTime? GRN_DATE { get; set; }
        [DataMember] [DataNames("RECEIVED_QUANTITY")] public decimal RECEIVED_QUANTITY { get; set; }
        [DataMember] [DataNames("REJECTED_QUANTITY")] public decimal REJECTED_QUANTITY { get; set; }
        [DataMember] [DataNames("SUPPLIER_NAME")] public string SUPPLIER_NAME { get; set; }
        [DataMember] [DataNames("SUPPLIER_CODE")] public string SUPPLIER_CODE { get; set; }
        [DataMember] [DataNames("UOM")] public string UOM { get; set; }
        [DataMember] [DataNames("TOTAL_COUNT")] public int TOTAL_COUNT { get; set; }
        [DataMember] [DataNames("GRN_VAUE")] public decimal GRN_VAUE { get; set; }
        [DataMember] [DataNames("PO_MATERIAL_DESC")] public string PO_MATERIAL_DESC { get; set; }
        [DataMember] [DataNames("CategoryCode")] public string CategoryCode { get; set; }        
        [DataMember] [DataNames("ProductCode")] public string ProductCode { get; set; }
        [DataMember] [DataNames("VENDOR_COUNTRY")] public string VENDOR_COUNTRY { get; set; }
        [DataMember] [DataNames("VENDOR_COUNTRY_CODE")] public string VENDOR_COUNTRY_CODE { get; set; }
        [DataMember] [DataNames("PAYMENT_TERM_CODE")] public string PAYMENT_TERM_CODE { get; set; }
        [DataMember] [DataNames("PAYMENT_TERM_DESC")] public string PAYMENT_TERM_DESC { get; set; }
        [DataMember] [DataNames("INVOICE_COMPLETED_QTY")] public decimal INVOICE_COMPLETED_QTY { get; set; }
        [DataMember] [DataNames("PLANT")] public string PLANT { get; set; }
        [DataMember] [DataNames("HEADER_TEXT")] public string HEADER_TEXT { get; set; }
        [DataMember] [DataNames("DELIVERY_NOTE")] public string DELIVERY_NOTE { get; set; }
        [DataMember] [DataNames("INVOICE_NUMBER")] public string INVOICE_NUMBER { get; set; }
        [DataMember] [DataNames("PER_UNIT_COST")] public decimal PER_UNIT_COST { get; set; }


    }
}