﻿prmApp
    .controller('customFieldListCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMCustomFieldService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMCustomFieldService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.sessionID = userService.getUserToken();

        /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/


            $scope.customFieldList = [];
            $scope.filteredcustomFieldList = [];

            $scope.getCustomFieldList = function () {
                var params = {
                    "compid": userService.getUserCompanyId(),
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.getCustomFieldList(params)
                    .then(function (response) {
                        $scope.customFieldList = response;
                        $scope.filteredcustomFieldList = $scope.customFieldList;

                        /*PAGINATION CODE START*/
                        $scope.totalItems = $scope.customFieldList.length;
                        /*PAGINATION CODE END*/
                        document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                        document.documentElement.scrollTop = 0; // For IE and Firefox
                    });

            };

            $scope.getCustomFieldList();
            
            $scope.goToCustomFieldSave = function (field) {
                var url = $state.href("savefield", { "Id": field.CUST_FIELD_ID });
                window.open(url, '_self');
            };

        }]);