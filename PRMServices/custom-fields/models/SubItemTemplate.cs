﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRM.Core.Common;
using PRM.Core.Models;

namespace PRMServices.models
{
    [DataContract]
    public class SubItemTemplate : Entity
    {
        [DataMember] [DataNames("SUB_ITEM_ID")] public int SUB_ITEM_ID { get; set; }
        [DataMember] [DataNames("TEMPLATE_ID")] public int TEMPLATE_ID { get; set; }
        [DataMember] [DataNames("NAME")] public string NAME { get; set; }
        [DataMember] [DataNames("DESCRIPTION")] public string DESCRIPTION { get; set; }
        [DataMember] [DataNames("HAS_SPECIFICATION")] public int HAS_SPECIFICATION { get; set; }
        [DataMember] [DataNames("HAS_PRICE")] public int HAS_PRICE { get; set; }
        [DataMember] [DataNames("HAS_QUANTITY")] public int HAS_QUANTITY { get; set; }
        [DataMember] [DataNames("HAS_TAX")] public int HAS_TAX { get; set; }
        [DataMember] [DataNames("IS_VALID")] public int IS_VALID { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("UNIT_PRICE")] public decimal UNIT_PRICE { get; set; }
        [DataMember] [DataNames("REV_UNIT_PRICE")] public decimal REV_UNIT_PRICE { get; set; }
        [DataMember] [DataNames("CONSUMPTION")] public decimal CONSUMPTION { get; set; }
        [DataMember] [DataNames("UOM")] public string UOM { get; set; }
        [DataMember] [DataNames("REV_CONSUMPTION")] public decimal REV_CONSUMPTION { get; set; }
        [DataMember] [DataNames("BULK_PRICE")] public decimal BULK_PRICE { get; set; }
        [DataMember] [DataNames("REV_BULK_PRICE")] public decimal REV_BULK_PRICE { get; set; }
        [DataMember] [DataNames("IS_CALCULATED")] public int IS_CALCULATED { get; set; }
    }
}