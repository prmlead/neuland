﻿ALTER TABLE cm_productquotationtemplate ADD HAS_SPECIFICATION int DEFAULT 0;
ALTER TABLE cm_productquotationtemplate ADD HAS_PRICE int DEFAULT 0;
ALTER TABLE cm_productquotationtemplate ADD HAS_QUANTITY int DEFAULT 0;
ALTER TABLE cm_productquotationtemplate ADD HAS_TAX int DEFAULT 0;

SELECT * FROM PRMCustomFields;
SELECT * FROM PRMCustomFieldValues;

cf_SaveCustomField
cf_GetCustomFields
cf_SaveCustomFieldValue
cf_GetModuleFields
