prmApp.constant('PRMJSONRequirementServiceDomain', 'json-requirement/svc/PRMJSONRequirementService.svc/REST/');

prmApp.service('PRMJSONRequirementService', ["PRMJSONRequirementServiceDomain", "userService", "httpServices",
    function (PRMJSONRequirementServiceDomain, userService, httpServices) {
        var PRMJSONRequirementService = this;

        PRMJSONRequirementService.GetJsonRequirementStructure = function (params) {
            let url = PRMJSONRequirementServiceDomain + 'getjsonrequirementstructure?sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMJSONRequirementService.SavePriceCap = function (params) {
            let url = PRMJSONRequirementServiceDomain + 'savepricecap';
            return httpServices.post(url, params);
        };
            
        return PRMJSONRequirementService;

}]);