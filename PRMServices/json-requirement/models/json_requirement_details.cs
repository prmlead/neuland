﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class json_requirement_details : json_entity
    {

        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("IS_CONTRACT_ENABLED")] public int IS_CONTRACT_ENABLED { get; set; }
        [DataMember] [DataNames("NUMBER_OF_REMINDERS")] public int NUMBER_OF_REMINDERS { get; set; }
        [DataMember] [DataNames("REMINDER_TIME_INTERVEL")] public int REMINDER_TIME_INTERVEL { get; set; }
        [DataMember] [DataNames("IS_QUOTATION_PRICE_LIMIT_ENABLED")] public int IS_QUOTATION_PRICE_LIMIT_ENABLED { get; set; }
        [DataMember] [DataNames("REDUCTION_TYPE")] public int REDUCTION_TYPE { get; set; }
        [DataMember] [DataNames("IS_SMS_ENABLED")] public int IS_SMS_ENABLED { get; set; }        
        [DataMember] [DataNames("IS_EMAIL_ENABLED")] public int IS_EMAIL_ENABLED { get; set; }
        [DataMember] [DataNames("TITLE")] public int TITLE { get; set; }
        [DataMember] [DataNames("COMMENTS")] public int COMMENTS { get; set; }
        [DataMember] [DataNames("DELIVERY_TERMS")] public int DELIVERY_TERMS { get; set; }
        [DataMember] [DataNames("DELIVERY_LOCATION")] public int DELIVERY_LOCATION { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public int PAYMENT_TERMS { get; set; }
        [DataMember] [DataNames("CONTACT_DETAILS")] public int CONTACT_DETAILS { get; set; }
        [DataMember] [DataNames("GENERAL_TERMS_CONDITIONS")] public int GENERAL_TERMS_CONDITIONS { get; set; }
        [DataMember] [DataNames("CONTRACT_START_TIME")] public int CONTRACT_START_TIME { get; set; }
        [DataMember] [DataNames("CONTRACT_END_TIME")] public int CONTRACT_END_TIME { get; set; }
        [DataMember] [DataNames("URGENCY")] public int URGENCY { get; set; }
        [DataMember] [DataNames("CURRENCY")] public int CURRENCY { get; set; }
        [DataMember] [DataNames("QUOTATION_FREEZE_TIME")] public int QUOTATION_FREEZE_TIME { get; set; }
        [DataMember] [DataNames("EXPECTED_NEGOTIATION_START_TIME")] public int EXPECTED_NEGOTIATION_START_TIME { get; set; }


        [DataMember] [DataNames("REQ_JSON")] public string REQ_JSON { get; set; }

        [DataMember] [DataNames("list_json_requirement_vendors")] public List<json_requirement_vendors> list_json_requirement_vendors { get; set; }
        [DataMember] [DataNames("list_json_requirement_items")] public List<json_requirement_items> list_json_requirement_items { get; set; }


    }
}