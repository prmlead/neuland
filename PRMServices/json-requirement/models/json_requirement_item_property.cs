﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class json_requirement_item_property
    {

        [DataMember] [DataNames("PROPERTY_ID")] public int PROPERTY_ID { get; set; }
        [DataMember] [DataNames("PROPERTY_NAME")] public int PROPERTY_NAME { get; set; }
        [DataMember] [DataNames("UNIT_PRICE")] public int UNIT_PRICE { get; set; }
        [DataMember] [DataNames("QUANTITY")] public int QUANTITY { get; set; }
        [DataMember] [DataNames("PROPERTY_PRICE")] public int PROPERTY_PRICE { get; set; }


    }
}