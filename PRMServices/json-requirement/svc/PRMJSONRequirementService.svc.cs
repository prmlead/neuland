﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Models;
using PRMServices.SQLHelper;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMJSONRequirementService : IPRMJSONRequirementService
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        public PRMServices prm = new PRMServices();

        #region Services
        
        public json_requirement_details GetJsonRequirementStructure(string sessionid)
        {
            json_requirement_details new_json_requirement_details = new json_requirement_details();
            json_requirement_vendors new_json_requirement_vendors = new json_requirement_vendors();
            json_requirement_items new_json_requirement_items = new json_requirement_items();
            json_requirement_item_property new_json_requirement_item_property = new json_requirement_item_property();
            try
            {
                Utilities.ValidateSession(sessionid);
                new_json_requirement_details.list_json_requirement_vendors = new List<json_requirement_vendors>();
                new_json_requirement_details.list_json_requirement_items = new List<json_requirement_items>();
                new_json_requirement_details.list_json_requirement_vendors.Add(new_json_requirement_vendors);
                new_json_requirement_items.list_json_requirement_item_property = new List<json_requirement_item_property>();
                new_json_requirement_items.list_json_requirement_item_property.Add(new_json_requirement_item_property);
                new_json_requirement_details.list_json_requirement_items.Add(new_json_requirement_items);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
                new_json_requirement_details.ERROR_MESSAGE = "Error in retrieving Data";
            }

            return new_json_requirement_details;
        }       

        #endregion Services

    }

}