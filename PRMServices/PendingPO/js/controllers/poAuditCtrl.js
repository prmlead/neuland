
prmApp.controller('poAuditCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "$filter", "fileReader", "$rootScope","PRMPOService",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, $filter, fileReader, $rootScope, PRMPOService) {
        $scope.USER_ID = userService.getUserId();
        $scope.COMP_ID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.PO_NUMBER = $stateParams.PO_NUMBER;

        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
            $scope.getPoAudit(($scope.currentPage - 1), 10,'');
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };

        $scope.tableColumns = [];
        $scope.tableValues = [];


        $scope.getPoAudit = function (recordsFetchFrom, pageSize, searchString) {
            $scope.params =
            {
                "COMP_ID": $scope.COMP_ID,
                "PO_NUMBER": $scope.PO_NUMBER,
                "sessionid": userService.getUserToken(),
                "searchString": searchString ? searchString : "",
                "PageSize": recordsFetchFrom * pageSize,
                "NumberOfRecords": pageSize
            };

            PRMPOService.GetPoAudit($scope.params)
                .then(function (response) {
                    if (response) {

                        var arr = JSON.parse(response).Table;
                        if (arr && arr.length > 0)
                        {
                            arr.forEach(a => delete a.TOTAL_COUNT);
                            $scope.tableColumnsTemp = angular.copy(_.keys(arr[0]));
                            $scope.tableColumnsTemp.forEach(function (item, index) {
                                item = item.replaceAll("_", " ");
                                $scope.tableColumns.push(item);
                            });
                            $scope.rows = arr;
                            arr.forEach(function (item, index) {
                                var obj = angular.copy(_.values(item));
                                if (obj) {
                                    item.tableValues = [];
                                    obj.forEach(function (value, valueIndex) {
                                        item.tableValues.push(value);
                                    });
                                }
                            });

                            $scope.totalItems = $scope.rows[0].TOTAL_COUNT;
                        }
                    }
                });
        }
        $scope.getPoAudit(0,10,'');

    }]);