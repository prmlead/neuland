﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('list-pendingPO', {
                    url: '/list-pendingPO',
                    templateUrl: 'PendingPO/views/list-pendingPO.html',
                    params: {
                        excludeContracts: true,
                        onlyContracts: false,
                        detailsObj: null
                    }
                })
                .state('poAudit', {
                    url: '/poAudit/:PO_NUMBER',
                    templateUrl: 'PendingPO/views/poAudit.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('list-pendingContracts', {
                    url: '/list-pendingContracts',
                    templateUrl: 'PendingPO/views/list-pendingPO.html',
                    params: {
                        excludeContracts: false,
                        onlyContracts: true,
                        detailsObj: null
                    }
                })
                .state('list-pendingPOOverall', {
                    url: '/list-pendingPOOverall/:poID',
                    templateUrl: 'PendingPO/views/list-pendingPOOverall.html',
                    params: {
                        detailsObj: null
                    }
                })

                .state('uploadvendorinvoice', {
                    url: '/uploadvendorinvoice/:ID',
                    templateUrl: 'PendingPO/views/uploadvendorinvoice.html'
                });

        }]);