﻿prmApp.constant('PRMPOServiceDomain', 'svc/PRMPOService.svc/REST/');
prmApp.service('PRMPOService', ["PRMPOServiceDomain", "SAPIntegrationServicesDomain", "userService", "httpServices", "$window",
    function (PRMPOServiceDomain, SAPIntegrationServicesDomain, userService, httpServices, $window) {

        var PRMPOService = this;

        PRMPOService.getPOScheduleList = function (params) {

            params.onlycontracts = params.onlycontracts ? params.onlycontracts : 0;
            params.excludecontracts = params.excludecontracts ? params.excludecontracts : 0;

            let api = 'getposchedulelist';

            if (params.isExport) {
                api = 'getposchedulelistexport';
            }

            let url = PRMPOServiceDomain + api+ '?compid=' + params.compid + '&uid=' + params.uid + '&search=' + params.search + '&categoryid=' + params.categoryid
                + '&productid=' + params.productid + '&supplier=' + params.supplier + '&postatus=' + params.postatus
                + '&deliverystatus=' + params.deliverystatus
                + '&plant=' + params.plant
                + '&fromdate=' + params.fromdate + '&todate=' + params.todate
                + '&onlycontracts=' + params.onlycontracts + '&excludecontracts=' + params.excludecontracts + '&ackStatus=' + params.ackStatus + '&buyer=' + params.buyer + '&purchaseGroup=' + params.purchaseGroup
                + '&page=' + params.page
                + '&pagesize=' + params.pagesize + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOService.getPOScheduleItems = function (params) {
            let url = PRMPOServiceDomain + 'getposcheduleitems?ponumber=' + params.ponumber + '&moredetails=' + params.moredetails + '&forasn=' + params.forasn + '&sessionid=' + userService.getUserToken();

            return httpServices.get(url);
        };

        PRMPOService.getPOScheduleFilterValues = function (params) {
            let url = PRMPOServiceDomain + 'getposchedulefilters?compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOService.editPOInvoice = function (params) {
            let url = PRMPOServiceDomain + 'editPOInvoice';
            return httpServices.post(url, params);
        };

        PRMPOService.GetPoAudit = function (params) {
            let url = PRMPOServiceDomain + 'GetPoAudit?COMP_ID=' + params.COMP_ID + '&PO_NUMBER=' + params.PO_NUMBER 
                + '&sessionid=' + params.sessionid + '&searchString=' + params.searchString + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords;
            return httpServices.get(url);
        };


        PRMPOService.getPOInvoiceDetails = function (params) {
            if (!params.vendorID) {
                params.vendorID = 0;
            }
            if (!params.invoiceID) {
                params.invoiceID = 0;
            }
            let url = PRMPOServiceDomain + 'getpoinvoicedetails?ponumber=' + params.ponumber + '&sessionid=' + userService.getUserToken() + '&vendorID=' + params.vendorID + '&invoiceID=' + params.invoiceID;
            return httpServices.get(url);
        };

        PRMPOService.getASNDetails = function (params) {
            let url = PRMPOServiceDomain + 'getasndetails?compid=' + params.compid + '&asnid=' + params.asnid + '&ponumber=' + params.ponumber + '&grncode=' + params.grncode + '&asncode=' + params.asncode + '&sessionid=' + userService.getUserToken() + '&vendorid=' + params.vendorid;
            return httpServices.get(url);
        };

        PRMPOService.getASNDetailsList = function (params) {
            let url = PRMPOServiceDomain + 'getasndetailslist?ponumber=' + params.ponumber + '&sessionid=' + userService.getUserToken() + '&vendorid=' + params.vendorid;
            return httpServices.get(url);
        };

        PRMPOService.saveASNdetails = function (params) {
            let url = PRMPOServiceDomain + 'saveasndetails';
            return httpServices.post(url, params);
        };

        PRMPOService.savePOInvoice = function (params) {
            let url = PRMPOServiceDomain + 'savepoinvoice';
            return httpServices.post(url, params);
        };

        PRMPOService.getInvoiceList = function (params) {
            let url = PRMPOServiceDomain + 'getinvoicelist?compid=' + params.COMP_ID + '&userid=' + params.U_ID + '&sessionid=' + userService.getUserToken() + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&search=' + params.search + '&status=' + params.status + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords;
            return httpServices.get(url);
        };

        PRMPOService.savePOInvoiceForm = function (params) {
            let url = PRMPOServiceDomain + 'savepoinvoiceform';
            return httpServices.post(url, params);
        };

        PRMPOService.GetthreeWayMatchingList = function (params) {
            let url = PRMPOServiceDomain + 'GetthreeWayMatchingList?companyId=' + params.companyId + '&uId=' + params.uId
                + '&sessionid=' + params.sessionid + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&type=' + params.type + '&searchString=' + params.searchString + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords ;
            return httpServices.get(url);
        };

        PRMPOService.getpaymentDetailsList = function (params) {
            let url = PRMPOServiceDomain + 'getPaymentDetails?userID=' + params.userID + '&compID=' + params.compID + '&search=' + params.search + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&sessionID=' + params.sessionID + '&pageSize=' + params.pageSize + '&numberOfRecords=' + params.numberOfRecords;
            return httpServices.get(url);
        };


        PRMPOService.savepoattachments = function (params) {
            let url = PRMPOServiceDomain + 'savepoattachments';
            return httpServices.post(url, params);
        };


        PRMPOService.poApproval = function (params) {
            let url = PRMPOServiceDomain + 'poapproval';
            return httpServices.post(url, params);
        };

        PRMPOService.sendForInvoiceIntegration = function (params) {
            let url = PRMPOServiceDomain + 'sendForInvoiceIntegration';
            return httpServices.post(url, params);
        };

        PRMPOService.deletePOInvoice = function (params) {
            if (!params.invoiceId) {
                params.invoiceId = 0;
            }
            if (!params.wfId) {
                params.wfId = 0;
            }
            let url = PRMPOServiceDomain + 'deletepoinvoice?ponumber=' + params.ponumber + '&invoicenumber=' + params.invoicenumber + '&invoiceId=' + params.invoiceId + '&wfId=' + params.wfId + '&sessionid=' + params.sessionid;
            return httpServices.get(url, params);
        };

        PRMPOService.GeneratePOPDF = function (params) {
            let url = PRMPOServiceDomain + 'generatepopdf?ponumber=' + params + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, 'application/octet-stream');
                    var linkElement = document.createElement('a');
                    try {
                        var url = $window.URL.createObjectURL(response);
                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", params + ".pdf");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }

        return PRMPOService;

    }]);