angular = require('angular');

angular.module('profileModule')
.controller('profileMyrequirementCtrl', ["$scope", "loginService", "profileService", function ($scope, loginService, profileService) {
 
	$scope.myAuctions = [];
	$scope.myAuctionsLoaded = false;
	$scope.totalItems = 0;
	$scope.totalLeads = 0;
	$scope.currentPage = 1;
	$scope.itemsPerPage = 5;
	$scope.maxSize = 5;
	$scope.myAuctionsMessage = '';

	$scope.getMyRequirement = function(){
	profileService.getmyAuctions({ "userid": loginService.getUserId(), "sessionid": loginService.getUserToken() })
			.then(function (response) {
				$scope.myAuctions = response;
				if ($scope.myAuctions.length > 0) {							
					$scope.myAuctionsLoaded = true;
					$scope.totalItems = $scope.myAuctions.length;
				} else {
					$scope.myAuctionsLoaded = false;
					$scope.totalItems = 0;
					$scope.myAuctionsMessage = "There are no auctions running right now for you.";
				}
			});
	};
	$scope.getMyRequirement();


	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
	};

	$scope.pageChanged = function () {
	   
	};
	}]);