angular = require('angular');

angular.module('loginModule')
    .controller('loginCtrl', ["$scope", "$state", "loginService", function LoginCtrl($scope, $state, loginService) {
        'use strict';
        //Status
        $scope.user = {};
        $scope.isMobile = (typeof window.orientation !== 'undefined' ? true : false);

        $scope.loginSubmit = function () {
            loginService.login($scope.user).then(function (error) {
                $scope.loggedIn = loginService.isLoggedIn();
                if (error.errorMessage != "") {
                    $scope.showMessage = true;
                    $scope.msg = loginService.getErrMsg(error);
                } 
                // else if (error.userInfo.credentialsVerified == 0 || error.userInfo.isOTPVerified == 0) {
                //     $state.go('profile-home');
                // } 
                else {
                    $state.go('profile-home')
                }
            });
        };

        $scope.closeMsg = function () {
            $scope.showMessage = false;
        }
    }]);
