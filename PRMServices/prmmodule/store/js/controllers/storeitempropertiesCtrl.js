﻿angular = require('angular');

angular.module('storeModule')
    .controller('storeitempropertiesCtrl', ['$scope', '$state', '$stateParams', '$window', 'fileReader', 'loginService', 'storeService', 'growlService', function ($scope, $state, $stateParams, $window, fileReader, loginService, storeService, growlService) {
        $scope.compID = $stateParams.compID;
        $scope.itemID = $stateParams.itemID;

        $scope.sessionID = loginService.getUserToken();

        $scope.storeItemProperties = [];
        $scope.storeItemProperty = {
            custPropID: 0,
            companyID: $scope.compID,
            custPropCode: '',
            custPropDesc: '',
            custPropType: '',
            custPropDefault: '',
            itemID: $scope.itemID,
            custPropValue: '',
            sessionID: $scope.sessionID
        };
       
        storeService.storesitempropvalues($scope.itemID)
            .then(function (response) {
                $scope.storeItemProperties = response;                  
            });




        $scope.showSaveProperty = false;

        $scope.showSavePropertyFunction = function (val) {
            $scope.storeItemProperty = {
                custPropID: 0,
                companyID: $scope.compID,
                custPropCode: '',
                custPropDesc: '',
                custPropType: '',
                custPropDefault: '',
                itemID: $scope.itemID,
                custPropValue: '',
                sessionID: $scope.sessionID
            };
            $scope.showSaveProperty = val;
        };

        $scope.savestoreitemprop = function (itemProperty, isEdit) {
            itemProperty.sessionID = $scope.sessionID;
            var params = {
                "storeItemProp": itemProperty
            };
            storeService.savestoreitemprop(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       // growlService.growl("Store Item Property Saved Successfully.", "success");
                       $scope.storeItemProperty.custPropID = response.objectID;
                       $scope.savestoreitempropvalue(itemProperty);
                   }
               })
        };

        $scope.savestoreitempropvalue = function (storeItemProperty) {
            storeItemProperty.sessionID = $scope.sessionID;
            storeItemProperty.itemID = $scope.itemID;
            var params = {
                storeItemProp: storeItemProperty
            };

            storeService.savestoreitempropvalue(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                       storeItemProperty.custPropID = response.objectID;
                   }
                   else {
                       growlService.growl("Store Item Property Value Saved Successfully.", "success");
                       $scope.showSaveProperty = false;

                       var index = _.find($scope.storeItemProperties, function (o) { return o.custPropID == storeItemProperty.custPropID; });                       
                       if (!index) {
                           $scope.storeItemProperties.push(storeItemProperty);
                       }
                   }
               })
        };
           
        $scope.cancelClick = function () {
            $window.history.back();
        };

        $scope.exportItemsToExcel = function () {
            var mystyle = {
                sheetid: 'StoreItemsPropValues',
                headers: true,
                column: {
                    style: 'font-size:15px;background:#233646;color:#FFF;'
                }
            };

            alasql.fn.getstoreid = function (x) {
                return x.storeID;
            }

            alasql('SELECT custPropID as [CUST_PROP_ID], companyID as [COMP_ID], custPropCode as [CUST_PROP_CODE], custPropDesc as [CUST_PROP_DESC], custPropType as [CUST_PROP_TYPE], custPropDefault as [CUST_PROP_DEFAULT], itemID as [ITEM_ID], custPropValue as [CUST_PROP_VALUE] INTO XLSX(?,{headers:true,sheetid: "StoreItemsPropValues", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["StoreItemsPropValues.xlsx", $scope.storeItemProperties]);
        };

        $scope.entityObj = {
            entityName: 'StoreItemsPropValues',
            attachment: [],
            userid: loginService.getUserId(),
            sessionID: $scope.sessionID
        };

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {

                    if (id == "excelItemProperties") {
                        var bytearray = new Uint8Array(result);
                        $scope.entityObj.attachment = $.makeArray(bytearray);
                        $scope.importEntity();
                    }
                });
        };

        $scope.importEntity = function () {

            var params = {
                "entity": $scope.entityObj
            };

            storeService.importentity(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Saved Successfully.", "success");
                       location.reload();

                   }
               })
        };

    }]);