﻿angular = require('angular');

angular.module('storeModule')
    .controller('storeitemsCtrl', ['$scope', '$state', '$stateParams', 'loginService', 'storeService', 'fileReader', 'growlService', function ($scope, $state, $stateParams, loginService, storeService, fileReader, growlService) {
        $scope.storeID = $stateParams.storeID;
        $scope.itemID = $stateParams.itemID;

        $scope.sessionID = loginService.getUserToken();

        $scope.companyStoreItems = [];
        $scope.companyStoreItemsTemp = [];

        $scope.searchKey = '';

        $scope.storeObj = {
            storeID: $scope.storeID,
            companyID: $scope.companyID,
            //isMainBranch: 0,
            //mainStoreID: 0,
            //storeCode: '',
            //storeDescription: '',
            //storeInCharge: '',
            //storeDetails: '',
            //isValid: 0,
            //storeAddress: '',
            //storeBranches: 0,
            sessionID: $scope.sessionID
        };

        $scope.storeItemsObj = {
            //itemID: $scope.itemID,
            storeDetails: $scope.storeObj,
            //itemCode: '',
            //itemType: '',
            //itemDescription: '',
            //totalStock: 0,
            //inStock: 0,
            //itemPrice: 0,
            //isValid: 0,
            //doAlert: 0,
            //threshold: 0,
            //iGST: 0,
            //cGST: 0,
            //sGST: 0            
        };

        $scope.entityObj = {
            entityName: 'StoreItems',
            attachment: [],
            userid: loginService.getUserId(),
            sessionID: $scope.sessionID
        };

        $scope.getCompanyStoreItems = function () {
            storeService.getcompanystoreitems($scope.storeID, $scope.itemID)
               .then(function (response) {
                   $scope.companyStoreItems = response;
                   $scope.companyStoreItemsTemp = _.filter($scope.companyStoreItems, function (item) {
                       return item.itemCode.indexOf($scope.searchKey) > -1;
                   });

               })
        }

        $scope.getCompanyStoreItems();

        $scope.search = function ()
        {
            $scope.companyStoreItemsTemp = _.filter($scope.companyStoreItems, function (item) {
                return item.itemCode.indexOf($scope.searchKey) > -1;
            });
        }

        $scope.exportItemsToExcel = function () {
            var mystyle = {
                sheetid: 'StoreItems',
                headers: true,
                column: {
                    style: 'font-size:15px;background:#233646;color:#FFF;'
                }
            };

            alasql.fn.getstoreid = function (x) {
                return x.storeID;
            }

            alasql('SELECT itemID as [ITEM_ID], getstoreid(storeDetails) as [STORE_ID], itemCode as [ITEM_CODE], itemType as [ITEM_TYPE], itemDescription as [ITEM_DESC], totalStock as [TOTAL_STOCK], inStock as [IN_STOCK], itemPrice as [ITEM_PRICE], isValid as [IS_VALID], doAlert as [DO_ALERT], threshold as [THRESHOLD], iGST as [IGST], cGST as [CGST], sGST as [SGST], itemUnits as [ITEM_UNITS] INTO XLSX(?,{headers:true,sheetid: "StoreItems", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["StoreItems.xlsx", $scope.companyStoreItems]);
        };

        $scope.downloadTemplate = function () {
            var mystyle = {
                sheetid: 'StoreItems',
                headers: true,
                column: {
                    style: 'font-size:15px;background:#233646;color:#FFF;'
                }
            };

            alasql.fn.getstoreid = function () {                                
                return $scope.storeID;
            }

            alasql('SELECT itemID as [ITEM_ID], getstoreid() as [STORE_ID], itemCode as [ITEM_CODE], itemType as [ITEM_TYPE], itemDescription as [ITEM_DESC], totalStock as [TOTAL_STOCK], inStock as [IN_STOCK], itemPrice as [ITEM_PRICE], isValid as [IS_VALID], doAlert as [DO_ALERT], threshold as [THRESHOLD], iGST as [IGST], cGST as [CGST], sGST as [SGST], itemUnits as [ITEM_UNITS] INTO XLSX(?,{headers:true,sheetid: "StoreItems", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["StoreItems.xlsx", $scope.storeItemsObj]);
        }





        $scope.importEntity = function () {

            var params = {
                "entity": $scope.entityObj
            };

            storeService.importentity(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Items Saved Successfully.", "success");
                       location.reload();

                   }
               })
        }

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {

                    if (id == "excelquotation") {
                        var bytearray = new Uint8Array(result);
                        $scope.entityObj.attachment = $.makeArray(bytearray);
                        $scope.importEntity();
                    }                    
                });
        }



        $scope.companyStoreItem = {};

        $scope.getCompanyStoreItem = function (itemID) {
            $scope.companyStoreItem = _.filter($scope.companyStoreItems, function (x) { return x.itemID == itemID; });
            $scope.singleItemDetails = $scope.companyStoreItem[0];
            return $scope.singleItemDetails;
        }
        

        $scope.goToItemEdit = function (storeitem) {
            $state.go("addnewstoreitem", { "storeID": storeitem.storeDetails.storeID, "itemID": storeitem.itemID, "itemObj": storeitem });
        }


        $scope.deletestoreitem = function (storeItemId) {

            var params = {
                storeItemId: storeItemId,
                sessionID: $scope.sessionID
            };

            storeService.deletestoreitem(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Item Deleted Successfully.", "success");
                       location.reload();
                   }
               })
        }


    }]); 

