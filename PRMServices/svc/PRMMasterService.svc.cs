﻿using Newtonsoft.Json;
using PRM.Core;
using PRM.Core.Domain.Acl;
using PRM.Core.Domain.Masters.ContactDetails;
using PRM.Core.Domain.Masters.DeliveryLocations;
using PRM.Core.Domain.Masters.DeliveryTerms;
using PRM.Core.Domain.Masters.GeneralTerms;
using PRM.Core.Models;
using PRM.Core.Pagers;
using PRM.Services.Acl;
using PRM.Services.Masters.ContactDetails;
using PRM.Services.Masters.DeliveryLocations;
using PRM.Services.Masters.DeliveryTerms;
using PRM.Services.Masters.GeneralTerms;
using PRM.Services.Masters.PaymentTerms;
using PRMServices.Infrastructure.Mapper;
using PRMServices.Models.Common;
using PRMServices.Models.Masters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PRMMasterService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PRMMasterService.svc or PRMMasterService.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMMasterService : IPRMMasterService
    {
        public PRMMasterService(
            IContactDetailService contactDetailService,
        IWorkContext workContext,
        IDeliveryLocationService deliveryLocationService,
        IDeliveryTermService deliverytermService,
        IGeneralTermService generalTermService,
        IPaymentTermService paymentTermService,
        IAclMappingService aclMappingService

            )
        {

            _contactDetailService = contactDetailService;
            _workContext = workContext;
            _deliveryLocationService = deliveryLocationService;
            _generalTermService = generalTermService;
            _paymentTermService = paymentTermService;
            _deliveryTermService = deliverytermService;
            _aclMappingService = aclMappingService;
        }

        private readonly IContactDetailService _contactDetailService;
        private readonly IWorkContext _workContext;
        private readonly IDeliveryLocationService _deliveryLocationService;
        private readonly IDeliveryTermService _deliveryTermService;
        private readonly IGeneralTermService _generalTermService;
        private readonly IPaymentTermService _paymentTermService;
        private readonly IAclMappingService _aclMappingService;


        #region Contact details

        public Response InsertContactDetail(ContactDetailModel model)
        {
            var entity = model.ToEntity();

            entity.Company_Id = _workContext.CurrentCompany;

            _contactDetailService.InsertContactDetail(entity);


            return new Response() { Message = "Contact detail created" };
        }

        public Response UpdateContactDetail(ContactDetailModel model)
        {

            var entity = _contactDetailService.GetContactDetailById(model.Id);

             entity = model.ToEntity(entity);

            _contactDetailService.UpdateContactDetail(entity);
            return new Response() { Message = "Contact detail updated" };
        }

        public dynamic GetByPageContactDetail(Pager pager)
        {

            var lst =   _contactDetailService.GetContactDetailList(pager,_workContext.CurrentCompany);
            return JsonConvert.SerializeObject(new PagedResponse() { Data = lst, HasNextPage = lst.HasNextPage, HasPreviousPage = lst.HasPreviousPage, PageIndex = lst.PageIndex, PageSize = lst.PageSize, TotalPages = lst.TotalPages, TotalCount = lst.TotalCount });
        }


        public ContactDetail GetByIdContactDetail(string id)
        {
            return _contactDetailService.GetContactDetailById(Convert.ToInt32(id));
        }


        public Response DeleteContactDetail(int id)
        {
            var entity = _contactDetailService.GetContactDetailById(Convert.ToInt32(id));
            entity.Deleted = true;
            _contactDetailService.UpdateContactDetail(entity);

            return new Response() { Message = "Contact detail deleted" };

        }


        #endregion


        #region Delivery location

        public Response InsertDeliveryLocation(DeliveryLocationModel model)
        {
            var entity = model.ToEntity();
            entity.Company_Id = _workContext.CurrentCompany;

            _deliveryLocationService.InsertDeliveryLocation(entity);
            return new Response() { Message = "Delivery location created" };
        }




        public Response UpdateDeliveryLocation(DeliveryLocationModel model)
        {

            var entity = _deliveryLocationService.GetDeliveryLocationById(model.Id);

            entity = model.ToEntity(entity);

            _deliveryLocationService.UpdateDeliveryLocation(entity);
            return new Response() { Message = "Delivery location updated" };
        }





        public dynamic GetByPageDeliveryLocation(Pager pager)
        {

            var lst = _deliveryLocationService.GetDeliveryLocationList(pager,_workContext.CurrentCompany);
            return JsonConvert.SerializeObject(new PagedResponse() { Data = lst, HasNextPage = lst.HasNextPage, HasPreviousPage = lst.HasPreviousPage, PageIndex = lst.PageIndex, PageSize = lst.PageSize, TotalPages = lst.TotalPages, TotalCount = lst.TotalCount });
        }


        public DeliveryLocation GetByIdDeliveryLocation(string id)
        {
            return _deliveryLocationService.GetDeliveryLocationById(Convert.ToInt32(id));
        }


        public Response DeleteDeliveryLocation(int id)
        {
            var entity = _deliveryLocationService.GetDeliveryLocationById(id);
            entity.Deleted = true;
            _deliveryLocationService.UpdateDeliveryLocation(entity);

            return new Response() { Message = "Delivery location deleted" };

        }


        #endregion


        #region Delivery terms

        public Response InsertDeliveryTerm(DeliveryTermModel model)
        {
            var entity = model.ToEntity();
            entity.Company_Id = _workContext.CurrentCompany;

            _deliveryTermService.InsertDeliveryTerm(entity);
            return new Response() { Message = "Delivery terms created" };
        }




        public Response UpdateDeliveryTerm(DeliveryTermModel model)
        {

            var entity = _deliveryTermService.GetDeliveryTermById(model.Id);

            entity = model.ToEntity(entity);

            _deliveryTermService.UpdateDeliveryTerm(entity);
            return new Response() { Message = "Delivery terms updated" };
        }





        public dynamic GetByPageDeliveryTerm(Pager pager)
        {

            var lst = _deliveryTermService.GetDeliveryTermList(pager,_workContext.CurrentCompany);
            return JsonConvert.SerializeObject(new PagedResponse() { Data = lst, HasNextPage = lst.HasNextPage, HasPreviousPage = lst.HasPreviousPage, PageIndex = lst.PageIndex, PageSize = lst.PageSize, TotalPages = lst.TotalPages, TotalCount = lst.TotalCount });
        }


        public DeliveryTerm GetByIdDeliveryTerm(string id)
        {
            return _deliveryTermService.GetDeliveryTermById(Convert.ToInt32(id));
        }


        public Response DeleteDeliveryTerm(int id)
        {
            var entity = _deliveryTermService.GetDeliveryTermById(id);
            entity.Deleted = true;
            _deliveryTermService.UpdateDeliveryTerm(entity);

            return new Response() { Message = "Delivery term deleted" };

        }


        #endregion

        #region Payment terms

        public Response InsertPaymentTerm(PaymentTermModel model)
        {
            var entity = model.ToEntity();
            entity.Company_Id = _workContext.CurrentCompany;

            _paymentTermService.InsertPaymentTerm(entity);
            return new Response() { Message = "Payment term created" };
        }




        public Response UpdatePaymentTerm(PaymentTermModel model)
        {

            var entity = _paymentTermService.GetPaymentTermById(model.Id);

            entity = model.ToEntity(entity);

            _paymentTermService.UpdatePaymentTerm(entity);
            return new Response() { Message = "Payment term updated" };
        }





        public dynamic GetByPagePaymentTerm(Pager pager)
        {

            var lst = _paymentTermService.GetPaymentTermList(pager,_workContext.CurrentCompany);
            return JsonConvert.SerializeObject(new PagedResponse() { Data = lst, HasNextPage = lst.HasNextPage, HasPreviousPage = lst.HasPreviousPage, PageIndex = lst.PageIndex, PageSize = lst.PageSize, TotalPages = lst.TotalPages, TotalCount = lst.TotalCount });
        }


        public PaymentTerm GetByIdPaymentTerm(string id)
        {
            return _paymentTermService.GetPaymentTermById(Convert.ToInt32(id));
        }


        public Response DeletePaymentTerm(int id)
        {
            var entity = _paymentTermService.GetPaymentTermById(id);
            entity.Deleted = true;
            _paymentTermService.UpdatePaymentTerm(entity);

            return new Response() { Message = "Payment term deleted" };

        }


        #endregion


        #region General terms

        public Response InsertGeneralTerm(GeneralTermModel model)
        {
            var entity = model.ToEntity();
            entity.Company_Id = _workContext.CurrentCompany;

            _generalTermService.InsertGeneralTerm(entity);
            return new Response() { Message = "General term created" };
        }

        public Response UpdateGeneralTerm(GeneralTermModel model)
        {

            var entity = _generalTermService.GetGeneralTermById(model.Id);

            entity = model.ToEntity(entity);

            _generalTermService.UpdateGeneralTerm(entity);
            return new Response() { Message = "General term updated" };
        }

        public dynamic GetByPageGeneralTerm(Pager pager)
        {

            var lst = _generalTermService.GetGeneralTermList(pager,_workContext.CurrentCompany);
            return JsonConvert.SerializeObject(new PagedResponse() { Data = lst, HasNextPage = lst.HasNextPage, HasPreviousPage = lst.HasPreviousPage, PageIndex = lst.PageIndex, PageSize = lst.PageSize, TotalPages = lst.TotalPages, TotalCount = lst.TotalCount });
        }

        public GeneralTerm GetByIdGeneralTerm(string id)
        {
            return _generalTermService.GetGeneralTermById(Convert.ToInt32(id));
        }

        public Response DeleteGeneralTerm(int id)
        {
            var entity = _generalTermService.GetGeneralTermById(id);
            entity.Deleted = true;
            _generalTermService.UpdateGeneralTerm(entity);

            return new Response() { Message = "General term deleted" };

        }

        public List<ContactDetail> GetContactDetail(ContactDetailGetModel model)
        {


            var list = _contactDetailService.GetContactDetailList(new { Deleted = false, Active = true, Company_Id = _workContext.CurrentCompany }).ToList();

            var listModel = new List<ContactDetail>();
            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "ContactDetail", Company_Id = _workContext.CurrentCompany });


            foreach (var item in list)
            {

                var falg = true ;
                if (item.LimitedToDepartment && !mapper.Any(e => e.Type == (int)AclMappingType.Department && e.EntityId == item.Id && model.Departments.Contains(e.Value)))
                {
                    falg = false;
                }

                if (item.LimitedToUser && !mapper.Any(e => e.Type == (int)AclMappingType.User && e.EntityId == item.Id && e.Value == _workContext.CurrentUser))
                {
                    falg = false;
                }
                if(falg)
                {
                    listModel.Add(item);
                }

            }

            return listModel;
        }

        public List<DeliveryLocation> GetDeliveryLocation(DeliveryLocationGetModel model)
        {
            var list = _deliveryLocationService.GetDeliveryLocationList(new { Deleted = false, Active = true, Company_Id = _workContext.CurrentCompany }).ToList();

            var listModel = new List<DeliveryLocation>();
            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "DeliveryLocation", Company_Id = _workContext.CurrentCompany });


            foreach (var item in list)
            {

                var falg = true;
                if (item.LimitedToDepartment && !mapper.Any(e => e.Type == (int)AclMappingType.Department && e.EntityId == item.Id && model.Departments.Contains(e.Value)))
                {
                    falg = false;
                }

                if (item.LimitedToUser && !mapper.Any(e => e.Type == (int)AclMappingType.User && e.EntityId == item.Id && e.Value == _workContext.CurrentUser))
                {
                    falg = false;
                }
                if (falg)
                {
                    listModel.Add(item);
                }

            }

            return listModel;
        }

        public List<DeliveryTerm> GetDeliveryTerms(DeliveryTermGetModel model)
        {
            var list = _deliveryTermService.GetDeliveryTermList(new { Deleted = false, Active = true, Company_Id = _workContext.CurrentCompany }).ToList();

            var listModel = new List<DeliveryTerm>();
            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "DeliveryTerm", Company_Id = _workContext.CurrentCompany });


            foreach (var item in list)
            {

                var falg = true;
                if (item.LimitedToDepartment && !mapper.Any(e => e.Type == (int)AclMappingType.Department && e.EntityId == item.Id && model.Departments.Contains(e.Value)))
                {
                    falg = false;
                }

                if (item.LimitedToUser && !mapper.Any(e => e.Type == (int)AclMappingType.User && e.EntityId == item.Id && e.Value == _workContext.CurrentUser))
                {
                    falg = false;
                }
                if (falg)
                {
                    listModel.Add(item);
                }

            }

            return listModel;
        }

        public List<PaymentTerm> GetPaymnetTerms(PaymentTermGetModel model)
        {
          var  list = _paymentTermService.GetPaymentTermList(new { Deleted = false, Active = true, Company_Id = _workContext.CurrentCompany }).ToList();


            var listModel = new List<PaymentTerm>();
            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "PaymentTerm", Company_Id = _workContext.CurrentCompany });


            foreach (var item in list)
            {

                var falg = true;
                if (item.LimitedToDepartment && !mapper.Any(e => e.Type == (int)AclMappingType.Department && e.EntityId == item.Id && model.Departments.Contains(e.Value)))
                {
                    falg = false;
                }

                if (item.LimitedToUser && !mapper.Any(e => e.Type == (int)AclMappingType.User && e.EntityId == item.Id && e.Value == _workContext.CurrentUser))
                {
                    falg = false;
                }
                if (falg)
                {
                    listModel.Add(item);
                }

            }

            return listModel;
        }

        public List<GeneralTerm> GetGeneralTerms(GeneralTermGetModel model)
        {
            var list = _generalTermService.GetGeneralTermList(new { Deleted = false, Active = true, Company_Id = _workContext.CurrentCompany }).ToList();

            var listModel = new List<GeneralTerm>();
            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "GeneralTerm", Company_Id = _workContext.CurrentCompany });


            foreach (var item in list)
            {

                var falg = true;
                if (item.LimitedToDepartment && !mapper.Any(e => e.Type == (int)AclMappingType.Department && e.EntityId == item.Id && model.Departments.Contains(e.Value)))
                {
                    falg = false;
                }

                if (item.LimitedToUser && !mapper.Any(e => e.Type == (int)AclMappingType.User && e.EntityId == item.Id && e.Value == _workContext.CurrentUser))
                {
                    falg = false;
                }
                if (falg)
                {
                    listModel.Add(item);
                }

            }

            return listModel;
        }


        #endregion

    }
}
