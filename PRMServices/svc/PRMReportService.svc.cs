﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Common;
using PRMServices.Models;
using OfficeOpenXml;
using OfficeOpenXml.DataValidation;
using System.IO;
using System.Linq;
using System.Drawing;
using OfficeOpenXml.Style;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;
using CATALOG = PRMServices.Models.Catalog;
using PRMServices.Models.Catalog;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMReportService : IPRMReportService
    {
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        private PRMServices prmservices = new PRMServices();
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly int decimal_round = Convert.ToInt32(ConfigurationManager.AppSettings["ROUNDING_DECIMALS"]);

        #region Get

        public LiveBidding[] GetLiveBiddingReport(int reqID, int count, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            if (count <= 0)
            {
                count = 10;
            }

            List<LiveBidding> details = new List<LiveBidding>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_COUNT", count);
                DataSet ds = sqlHelper.SelectList("rp_GetLiveBiddingReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        double factor = 1;
                        string vendorCurrency = row["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? row["SELECTED_VENDOR_CURRENCY"].ToString() : string.Empty;
                        string requirementCurrency = row["REQ_CURRENCY"] != DBNull.Value ? row["REQ_CURRENCY"].ToString() : string.Empty;
                        LiveBidding detail = ReportUtility.GetLiveBiddingReportObject(row, count, factor);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                LiveBidding error = new LiveBidding();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public LiveBidding[] GetLiveBiddingReport2(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<LiveBidding> details = new List<LiveBidding>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                ReportsRequirement reportsrequirement = new ReportsRequirement();
                reportsrequirement = GetReqDetails(reqID, sessionID);
                TimeSpan duration = reportsrequirement.EndTime.Subtract(reportsrequirement.StartTime);
                int interval = 0;
                for (int a = 10; a < duration.Minutes; a = a + 10)
                {
                    interval = duration.Minutes - 10;
                }

                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetLiveBiddingReport2", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LiveBidding detail = ReportUtility.GetLiveBiddingReportObject2(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                LiveBidding error = new LiveBidding();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public ItemWiseReport[] GetItemWiseReport(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ItemWiseReport> details = new List<ItemWiseReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetItemWiseReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    var factor = 1;// Utilities.GetRequirementCurrencyFactor(reqID);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ItemWiseReport detail = ReportUtility.GetItemWiseReportObject(row, factor);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                ItemWiseReport error = new ItemWiseReport();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public DeliveryTimeLine[] GetDeliveryTimeLineReport(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<DeliveryTimeLine> details = new List<DeliveryTimeLine>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetDeliveryTimeLineReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    var factor = 1;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DeliveryTimeLine detail = ReportUtility.GetIDeliveryTimelineObject(row, Convert.ToDecimal(factor));
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                DeliveryTimeLine error = new DeliveryTimeLine();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public DeliveryTimeLine[] GetPaymentTermsReport(int reqID, string sessionID)
        {
            List<DeliveryTimeLine> details = new List<DeliveryTimeLine>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetPaymentTermsReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    var factor = 1;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DeliveryTimeLine detail = ReportUtility.GetIDeliveryTimelineObject(row, Convert.ToDecimal(factor));
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                DeliveryTimeLine error = new DeliveryTimeLine();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public ReportsRequirement GetReqDetails(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            ReportsRequirement reportsrequirement = new ReportsRequirement();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetReqDetails", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    reportsrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    reportsrequirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    reportsrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    reportsrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    reportsrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    reportsrequirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    reportsrequirement.NegotiationDuration = row["NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["NEGOTIATION_DURATION"]) : string.Empty;
                    reportsrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    reportsrequirement.Savings = reportsrequirement.Savings;
                    reportsrequirement.IsTabular = row["REQ_IS_TABULAR"] != DBNull.Value ? (Convert.ToInt32(row["REQ_IS_TABULAR"]) == 1 ? true : false) : false;
                    reportsrequirement.IsUnitPriceBidding = row["IS_UNIT_PRICE_BIDDING"] != DBNull.Value ? Convert.ToInt32(row["IS_UNIT_PRICE_BIDDING"]) : 0;
                    reportsrequirement.NoOfVendorsInvited = row["NO_OF_VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_INVITED"]) : 0;
                    reportsrequirement.NoOfVendorsParticipated = row["NO_OF_VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_PARTICIPATED"]) : 0;
                }
            }
            catch (Exception ex)
            {

            }

            return reportsrequirement;
        }

        public string GetTemplates(string template, int compID, int userID, int reqID, string fromDate, string toDate, int templateid, string sessionID)
        {
            PRMServices service = new PRMServices();
            int maxRows = 500;
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region addvendors
            if (template.ToLower().Contains("addvendors"))
            {
                List<CategoryObj> categories = service.GetCategories(userID);
                var uniqueCategories = categories.Select(o => o.Category).Distinct().Where(o => o.Trim() != string.Empty).ToList();
                List<string> currencies = new List<string>(new string[] { "INR", "USD", "JPY", "GBP", "CAD", "AUD", "HKD", "EUR", "CNY" });
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("AddVendor");
                wsSheet1.Cells["A1"].Value = "FirstName";
                wsSheet1.Cells["B1"].Value = "LasName";
                wsSheet1.Cells["C1"].Value = "Email";
                wsSheet1.Cells["D1"].Value = "PhoneNumber";
                wsSheet1.Cells["E1"].Value = "CompanyName";
                wsSheet1.Cells["F1"].Value = "Category";
                wsSheet1.Cells["G1"].Value = "Currency";
                wsSheet1.Cells["H1"].Value = "KnownSince";
                var validation = wsSheet1.DataValidations.AddListValidation("G2:G100000");
                validation.ShowErrorMessage = true;
                validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                validation.ErrorTitle = "Invalid Currency";
                validation.Error = "Invalid currency selected";
                foreach (var currency in currencies)
                {
                    validation.Formula.Values.Add(currency);
                }

                var catValidation = wsSheet1.DataValidations.AddListValidation("F2:F100000");
                catValidation.ShowErrorMessage = true;
                catValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                catValidation.ErrorTitle = "Invalid Category";
                catValidation.Error = "Invalid category entered";
                int count = 1;
                foreach (var cat in uniqueCategories)
                {
                    wsSheet1.Cells["AA" + count.ToString()].Value = cat;
                    count++;
                }

                catValidation.Formula.ExcelFormula = "AA1:AA" + (count - 1).ToString();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region vendorquotation
            if (template.ToUpper().Contains("MARGIN_QUOTATION"))
            {
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("MARGIN_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";

                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {

                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    Requirement req = service.GetRequirementData(entry.Value.RequirementID, entry.Key, sessionID);

                    string vendorRemarks = "";

                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["L" + index].Value = item.ItemID;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["M" + index].Value = 0;
                        wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["P" + index].Value = vendorRemarks;
                        wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                        wsSheet1.Cells["R" + index].Value = req.Title;
                        wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString();
                        wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                        var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                        wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, decimal_round);
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region revvendorquotation
            if (template.ToUpper().Contains("MARGIN_REV_QUOTATION"))
            {
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("MARGIN_REV_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";

                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    Requirement req = service.GetRequirementData(entry.Value.RequirementID, entry.Key, sessionID);
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["L" + index].Value = item.ItemID;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["M" + index].Value = 0;
                        wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["P" + index].Value = vendorRemarks;
                        wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                        wsSheet1.Cells["R" + index].Value = req.Title;
                        wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString(); ;
                        wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                        var costPrice = 100 * item.UnitMRP / (100 + gst) * (1 + (item.UnitDiscount / 100));
                        wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, decimal_round);
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region unitprice
            #region vendorquotation
            if (template.ToUpper().Contains("UNIT_PRICE_QUOTATION"))
            {
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_PRICE_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "PRICE";
                wsSheet1.Cells["D1"].Value = "GST";
                wsSheet1.Cells["E1"].Value = "FREIGHT";
                wsSheet1.Cells["F1"].Value = "TOTAL_PRICE";
                wsSheet1.Cells["G1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["H1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["I1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["J1"].Value = "PRODUCT_NO";
                wsSheet1.Cells["K1"].Value = "BRAND";
                wsSheet1.Cells["L1"].Value = "QUANTITY";
                wsSheet1.Cells["M1"].Value = "UNITS";
                wsSheet1.Cells["N1"].Value = "DESCRIPTION";
                wsSheet1.Cells["O1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["F2:F6000"].Formula = "IF(A2 > 0, L2*C2*(1+(D2/100)), \"\")";
                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["C" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["D" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["E" + index].Value = 0;//entry.Value.AuctionVendors[0].RevVendorFreight;
                        //wsSheet1.Cells["F" + index].Value = item.RevItemPrice;
                        wsSheet1.Cells["G" + index].Value = 0;
                        wsSheet1.Cells["H" + index].Value = item.ItemID;
                        wsSheet1.Cells["I" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["J" + index].Value = item.ProductNo;
                        wsSheet1.Cells["K" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["L" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["M" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["N" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["O" + index].Value = item.RequirementID;
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion  
            if (template.ToUpper().Contains("UNIT_QUOTATION"))
            {
                List<models.PRMTemplateFields> temlateFields = null;
                PRMCustomFieldService fieldService = new PRMCustomFieldService();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                var reqSettings = service.GetRequirementSettings(reqID, sessionID);
                if (reqSettings != null && reqSettings.Any(s => s.REQ_SETTING == "TEMPLATE_ID"))
                {
                    temlateFields = fieldService.GetTemplateFieldsTemp(Convert.ToInt32(reqSettings.First(s => s.REQ_SETTING == "TEMPLATE_ID").REQ_SETTING_VALUE), string.Empty);
                }
                
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_QUOTATION_" + reqID);
                wsSheet1.Cells["A1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["B1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["C1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["D1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_CODE")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_CODE").FIELD_LABEL : "ProductCode";
                wsSheet1.Cells["E1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NAME")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NAME").FIELD_LABEL : "ProductName";
                wsSheet1.Cells["F1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NUMBER")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NUMBER").FIELD_LABEL : "ProductNumber";
                wsSheet1.Cells["G1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL").FIELD_LABEL : "Make";
                wsSheet1.Cells["H1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS").FIELD_LABEL : "Description"; ;
                wsSheet1.Cells["I1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_QUANTITY")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_QUANTITY").FIELD_LABEL : "QTY";
                wsSheet1.Cells["J1"].Value = "UNIT_PRICE";
               
                if (requirement.IsDiscountQuotation == 1)
                {
                    wsSheet1.Cells["K1"].Value = "UNIT_DISCOUNT";
                    wsSheet1.Cells["L1"].Value = "TAX";
                    wsSheet1.Cells["M1"].Value = "TOTAL_PRICE";
                    wsSheet1.Cells["N1"].Value = "UOM";
                    wsSheet1.Cells["O1"].Value = "Regret";
                    wsSheet1.Cells["P1"].Value = "Regret Comments";
                }
                else
                {
                    wsSheet1.Cells["K1"].Value = "TAX";
                    wsSheet1.Cells["L1"].Value = "TOTAL_PRICE";
                    wsSheet1.Cells["M1"].Value = "UOM";
                    wsSheet1.Cells["N1"].Value = "Regret";
                    wsSheet1.Cells["O1"].Value = "Regret Comments";
                }
              
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }
                
                var listReqItemsTemp = vendorsDictionary[userID].ListRequirementItems;
                if (listReqItemsTemp != null && listReqItemsTemp.Count > 0) {
                    maxRows = 0;
                    listReqItemsTemp = listReqItemsTemp.Where(item => item.IsCoreProductCategory > 0).ToList();
                    maxRows = listReqItemsTemp.Count + 1;
                    foreach (RequirementItems reqItem in listReqItemsTemp) {
                        List<CATALOG.ProductQuotationTemplate> jsonToList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(reqItem.ProductQuotationTemplateJson);
                        if (jsonToList != null && jsonToList.Count > 0) {
                            maxRows += jsonToList.Where(subItems => !subItems.NAME.Equals("Total", StringComparison.InvariantCultureIgnoreCase)).Count();
                        }
                    }
                }

                if (requirement.IsDiscountQuotation == 1)
                {
                    wsSheet1.Cells[$"M2:M{maxRows}"].Formula = "IF(J2 > 0, (J2-((J2*K2)/100))*(1+((L2)/100))*I2, \"0\")";
                }
                else
                {
                    wsSheet1.Cells[$"L2:L{maxRows}"].Formula = "IF(J2 > 0, J2*I2*(1+((K2)/100)), \"0\")";
                }
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1?"A1:P1": "A1:O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:P1" : "A1:O1"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#006c4d"));//GREEN
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:P1" : "A1:O1"].Style.Font.Color.SetColor(Color.White);
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:P1" : "A1:O1"].Style.Font.Bold = true;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:P1" : "A1:O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:P1" : "A1:O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Row(1).Height = 60;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:P" : "A:O"].AutoFitColumns();
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:P" : "A:O"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:P" : "A:O"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:P" : "A:O"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:P" : "A:O"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[$"J2:J{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells[$"J2:J{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
               
                if (requirement.IsDiscountQuotation == 1)
                {
                    wsSheet1.Cells[$"L2:L{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"L2:L{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
                    wsSheet1.Cells[$"K2:K{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"K2:K{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
                }
                else
                {
                    wsSheet1.Cells[$"K2:K{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"K2:K{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
                }
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                
                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    int parentRowNumber = 2;
                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        if (item.IsCoreProductCategory <= 0)
                        {
                            continue;
                        }

                        List<CATALOG.ProductQuotationTemplate> jsonToList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(item.ProductQuotationTemplateJson);
                        wsSheet1.Cells["A" + index].Value = item.RequirementID;
                        wsSheet1.Cells["B" + index].Value = 0;
                        wsSheet1.Cells["C" + index].Value = item.ItemID;
                        wsSheet1.Cells["D" + index].Value = item.ProductCode;
                        wsSheet1.Cells["E" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["F" + index].Value = item.ProductNo;
                        wsSheet1.Cells["G" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["H" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["I" + index].Value = item.ProductQuantity;
                        if (requirement.IsDiscountQuotation == 1)
                        {
                            wsSheet1.Cells["J" + index].Value = item.UnitMRP;
                            wsSheet1.Cells["K" + index].Value = item.RevUnitDiscount;
                            wsSheet1.Cells["L" + index].Value = item.IGst > 0 ? item.IGst : (item.CGst + item.SGst);
                            wsSheet1.Cells["N" + index].Value = item.ProductQuantityIn;
                            wsSheet1.Cells["O" + index].Value = item.IsRegret;
                            wsSheet1.Cells["P" + index].Value = item.RegretComments;
                        }
                        else
                        {
                            wsSheet1.Cells["J" + index].Value = item.RevUnitPrice;
                            wsSheet1.Cells["K" + index].Value = item.IGst > 0 ? item.IGst : (item.CGst + item.SGst);
                            wsSheet1.Cells["M" + index].Value = item.ProductQuantityIn;
                            wsSheet1.Cells["N" + index].Value = item.IsRegret;
                            wsSheet1.Cells["O" + index].Value = item.RegretComments;
                        }
                      
                        if (requirement.IsDiscountQuotation == 1)
                        {
                            wsSheet1.Cells["K" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsSheet1.Cells["K" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                            
                            var unitDiscountValidation = wsSheet1.DataValidations.AddDecimalValidation("K" + index);
                            unitDiscountValidation.ShowErrorMessage = true;
                            unitDiscountValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                            unitDiscountValidation.ErrorTitle = "Invalid Unit Discount";
                            unitDiscountValidation.Error = "Enter Value greater than zero.";
                            unitDiscountValidation.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                            unitDiscountValidation.Formula.Value = 0;
                        }
                        var sgstDiscValidation = wsSheet1.DataValidations.AddDecimalValidation(requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index);
                        sgstDiscValidation.ShowErrorMessage = true;
                        sgstDiscValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        sgstDiscValidation.ErrorTitle = "Invalid Tax";
                        sgstDiscValidation.Error = "Invalid Tax value should 0 and 99.";
                        sgstDiscValidation.Operator = ExcelDataValidationOperator.between;
                        sgstDiscValidation.Formula.Value = 0;
                        sgstDiscValidation.Formula2.Value = 99;
                        var unitPriceValidation = wsSheet1.DataValidations.AddDecimalValidation("J" + index);
                        unitPriceValidation.ShowErrorMessage = true;
                        unitPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        unitPriceValidation.ErrorTitle = "Invalid Unit Price";
                        unitPriceValidation.Error = "Enter Value greater than zero.";
                        unitPriceValidation.Operator = ExcelDataValidationOperator.greaterThan;
                        unitPriceValidation.Formula.Value = 0;

                        var qtyValidation = wsSheet1.DataValidations.AddDecimalValidation("I" + index);
                        qtyValidation.ShowErrorMessage = true;
                        qtyValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        qtyValidation.ErrorTitle = "Invalid Quantity";
                        qtyValidation.Error = "You cannot modify Quantity";
                        qtyValidation.Operator = ExcelDataValidationOperator.equal;
                        qtyValidation.Formula.Value = 0;


                        index++;
                        if (jsonToList != null && jsonToList.Count > 0)
                        {
                            if (jsonToList.Any(j => j.HAS_PRICE > 0))
                            {
                                //var unitPriceValidation = wsSheet1.DataValidations.AddDecimalValidation("H" + parentRowNumber);
                                //unitPriceValidation.ShowErrorMessage = true;
                                //unitPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                //unitPriceValidation.ErrorTitle = "Invalid Unit Price";
                                //unitPriceValidation.Error = "Enter Value greater than zero.";
                                //unitPriceValidation.Operator = ExcelDataValidationOperator.equal;
                                //unitPriceValidation.Formula.Value = 0;
                            }

                            foreach (var subItem in jsonToList)
                            {                                

                                if (!subItem.NAME.Equals("Total", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? $"A{parentRowNumber}:O{parentRowNumber}": $"A{parentRowNumber}:N{parentRowNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? $"A{parentRowNumber}:O{parentRowNumber}" : $"A{parentRowNumber}:N{parentRowNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#f2f2f2"));//Editable
                                    wsSheet1.Cells["C" + index].Value = $"{item.ItemID}-{subItem.T_ID}";
                                    wsSheet1.Cells["E" + index].Value = subItem.NAME;
                                    wsSheet1.Cells["H" + index].Value = subItem.SPECIFICATION;
                                    wsSheet1.Cells["I" + index].Value = subItem.HAS_QUANTITY > 0 ? subItem.CONSUMPTION : 1;
                                   
                                    if (subItem.HAS_SPECIFICATION > 0)
                                    {
                                        wsSheet1.Cells["H" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["H" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                    }
                                    
                                    if (subItem.HAS_QUANTITY <= 0 ||  true)
                                    {
                                        wsSheet1.Cells["I" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["I" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));//NON-Editable
                                        var subItemValicationRule = wsSheet1.DataValidations.AddDecimalValidation("I" + index);
                                        subItemValicationRule.ShowErrorMessage = true;
                                        subItemValicationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        subItemValicationRule.ErrorTitle = "Invalid Quantity for this Item";
                                        subItemValicationRule.Error = "You cannot modify Qantity.";
                                        subItemValicationRule.Operator = ExcelDataValidationOperator.equal;
                                        subItemValicationRule.Formula.Value = Convert.ToDouble(subItem.HAS_QUANTITY > 0 ? subItem.CONSUMPTION : 1);

                                        //wsSheet1.Cells["G" + index].Style.Locked = true;
                                    }
                                    //else
                                    //{
                                    //    wsSheet1.Cells["G" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    //    wsSheet1.Cells["G" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                    //}

                                    if (subItem.HAS_PRICE > 0)
                                    {
                                        wsSheet1.Cells["J" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["J" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                        var unitPriceValidation1 = wsSheet1.DataValidations.AddDecimalValidation("J" + index);
                                        unitPriceValidation1.ShowErrorMessage = true;
                                        unitPriceValidation1.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        unitPriceValidation1.ErrorTitle = "Invalid Unit Price";
                                        unitPriceValidation1.Error = "Enter Value greater than zero.";
                                        unitPriceValidation1.Operator = ExcelDataValidationOperator.greaterThan;
                                        unitPriceValidation1.Formula.Value = 0;
                                    }
                                    else
                                    {
                                        
                                        wsSheet1.Cells["J" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["J" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));//NON-Editable
                                        var subItemValicationRule = wsSheet1.DataValidations.AddDecimalValidation("J" + index);
                                        subItemValicationRule.ShowErrorMessage = true;
                                        subItemValicationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        subItemValicationRule.ErrorTitle = "Invalid Price for this Item";
                                        subItemValicationRule.Error = "You cannot modify Price.";
                                        subItemValicationRule.Operator = ExcelDataValidationOperator.equal;
                                        subItemValicationRule.Formula.Value =0;

                                        wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "M" + index : "L" + index].Value = 0;
                                    }

                                    if (subItem.HAS_TAX > 0)
                                    {
                                        wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable

                                        var sgstValidation = wsSheet1.DataValidations.AddDecimalValidation(requirement.IsDiscountQuotation == 1 ? $"L{index}" : $"K{index}");
                                        sgstValidation.ShowErrorMessage = true;
                                        sgstValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        sgstValidation.ErrorTitle = "Invalid Tax";
                                        sgstValidation.Error = "Invalid Tax value should 0 and 99.";
                                        sgstValidation.Operator = ExcelDataValidationOperator.between;
                                        sgstValidation.Formula.Value = 0;
                                        sgstValidation.Formula2.Value = 99;
                                    }
                                    else
                                    {
                                       
                                        wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));//NON-Editable
                                        //wsSheet1.DataValidations.Remove(wsSheet1.DataValidations["I" + index]);
                                        var subItemValicationRule = wsSheet1.DataValidations.AddDecimalValidation(requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index);
                                        subItemValicationRule.ShowErrorMessage = true;
                                        subItemValicationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        subItemValicationRule.ErrorTitle = "Invalid Tax for this Item";
                                        subItemValicationRule.Error = "You cannot modify Tax.";
                                        subItemValicationRule.Operator = ExcelDataValidationOperator.equal;
                                        subItemValicationRule.Formula.Value = 0;
                                    }

                                    wsSheet1.Cells["J" + index].Value = subItem.BULK_PRICE;
                                    wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index].Value = subItem.TAX;
                                   
                                    if (subItem.HAS_PRICE > 0)
                                    {

                                        if (requirement.IsDiscountQuotation == 1)
                                        {
                                            wsSheet1.Cells["M" + parentRowNumber].Style.Locked = true;
                                            wsSheet1.Cells["M" + parentRowNumber].Formula = $"SUM(M{parentRowNumber + 1}:M{index})*I{parentRowNumber}";

                                            wsSheet1.Cells["J" + parentRowNumber].Style.Locked = true;
                                            wsSheet1.Cells["J" + parentRowNumber].Formula = $"SUM(M{parentRowNumber + 1}:M{index})";
                                            //"IF(H2 > 0, H2*G2*(1+((I2)/100)), \"0\")";
                                            //wsSheet1.Cells["M" + index].Formula = $"IF(J{index} > 0, J{index}*I{index}*(1+((L{index})/100)), \"0\")";
                                            wsSheet1.Cells["M" + index].Formula = $"IF(J{index} > 0, (J{index}-((J{index}*K{index})/100))*(1+((L{index})/100))*I{index}, \"0\")";
                                        }
                                        else
                                        {
                                            wsSheet1.Cells["L" + parentRowNumber].Style.Locked = true;
                                            wsSheet1.Cells["L" + parentRowNumber].Formula = $"SUM(L{parentRowNumber + 1}:L{index})*I{parentRowNumber}";

                                            wsSheet1.Cells["J" + parentRowNumber].Style.Locked = true;
                                            wsSheet1.Cells["J" + parentRowNumber].Formula = $"SUM(L{parentRowNumber + 1}:L{index})";
                                            //"IF(H2 > 0, H2*G2*(1+((I2)/100)), \"0\")";
                                            wsSheet1.Cells["L" + index].Formula = $"IF(J{index} > 0, J{index}*I{index}*(1+((K{index})/100)), \"0\")";
                                        }
                                       
                                    }

                                    if (requirement.IsDiscountQuotation == 1)
                                    {
                                        wsSheet1.Cells["M" + index].Formula = $"IF(J{index} > 0, (J{index}-((J{index}*K{index})/100))*(1+((L{index})/100))*I{index}, \"0\")";

                                        wsSheet1.Cells["K" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["K" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));
                                    }

                                    index++;
                                }
                            }

                            if (!jsonToList.Any(j => j.HAS_PRICE > 0))
                            {
                                wsSheet1.Cells["J" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                wsSheet1.Cells["J" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable

                                if (requirement.IsDiscountQuotation == 1)
                                {

                                    wsSheet1.Cells["K" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["K" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
                                    wsSheet1.Cells["L" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["L" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable   
                                }
                                else
                                {
                                    wsSheet1.Cells["K" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["K" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable   
                                }



                            }

                           

                        }

                        parentRowNumber = index;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            if (template.ToUpper().Contains("UNIT_BIDDING"))
            {
                List<models.PRMTemplateFields> temlateFields = null;
                PRMCustomFieldService fieldService = new PRMCustomFieldService();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                var reqSettings = service.GetRequirementSettings(reqID, sessionID);
                if (reqSettings != null && reqSettings.Any(s => s.REQ_SETTING == "TEMPLATE_ID"))
                {
                    temlateFields = fieldService.GetTemplateFieldsTemp(Convert.ToInt32(reqSettings.First(s => s.REQ_SETTING == "TEMPLATE_ID").REQ_SETTING_VALUE), string.Empty);
                }

                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_BIDDING_" + reqID);
                wsSheet1.Cells["A1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["B1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["C1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["D1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_CODE")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_CODE").FIELD_LABEL : "ProductCode";
                wsSheet1.Cells["E1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NAME")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NAME").FIELD_LABEL : "ProductName";
                wsSheet1.Cells["F1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NUMBER")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NUMBER").FIELD_LABEL : "ProductNumber";
                wsSheet1.Cells["G1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL").FIELD_LABEL : "Make";
                wsSheet1.Cells["H1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS").FIELD_LABEL : "Description";
                wsSheet1.Cells["I1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_QUANTITY")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_QUANTITY").FIELD_LABEL : "QTY";
                wsSheet1.Cells["J1"].Value = "QUOTATION_UNIT_PRICE";
                if (requirement.IsDiscountQuotation == 1)
                {
                    wsSheet1.Cells["K1"].Value = "UNIT_DISCOUNT";
                    wsSheet1.Cells["L1"].Value = "TAX";
                    wsSheet1.Cells["M1"].Value = "REV.UNIT_DISCOUNT";
                    wsSheet1.Cells["N1"].Value = "REV_ITEM_PRICE";
                }
                else
                {
                    wsSheet1.Cells["K1"].Value = "TAX";
                    wsSheet1.Cells["L1"].Value = "REV.UNIT_PRICE";
                    wsSheet1.Cells["M1"].Value = "REV_ITEM_PRICE";
                }

                
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                var listReqItemsTemp = vendorsDictionary[userID].ListRequirementItems;
                if (listReqItemsTemp != null && listReqItemsTemp.Count > 0)
                {
                    maxRows = 0;
                    listReqItemsTemp = listReqItemsTemp.Where(item => item.IsCoreProductCategory > 0).ToList();
                    maxRows = listReqItemsTemp.Count + 1;
                    foreach (RequirementItems reqItem in listReqItemsTemp)
                    {
                        List<CATALOG.ProductQuotationTemplate> jsonToList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(reqItem.ProductQuotationTemplateJson);
                        if (jsonToList != null && jsonToList.Count > 0)
                        {
                            maxRows += jsonToList.Where(subItems => !subItems.NAME.Equals("Total", StringComparison.InvariantCultureIgnoreCase)).Count();
                        }
                    }
                }


                if (requirement.IsDiscountQuotation == 1)
                {
                    wsSheet1.Cells[$"N2:N{maxRows}"].Formula = "IF(I2 > 0, I2 *(J2-((J2*M2)/100))*(1+((L2)/100)), \"0\")";
                }
                else
                {
                    wsSheet1.Cells[$"M2:M{maxRows}"].Formula = "IF(L2 > 0, L2*I2*(1+((K2)/100)), \"0\")";
                    wsSheet1.Cells[$"L2:L{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"L2:L{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
                }


                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:N1" : "A1:M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:N1" : "A1:M1"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#006c4d"));//GREEN
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:N1" : "A1:M1"].Style.Font.Color.SetColor(Color.White);
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:N1" : "A1:M1"].Style.Font.Bold = true;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:N1" : "A1:M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:N1" : "A1:M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Row(1).Height = 60;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:N" : "A:M"].AutoFitColumns();
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:N" : "A:M"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:N" : "A:M"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:N" : "A:M"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:N" : "A:M"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
               
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    int parentRowNumber = 2;
                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        if (item.IsItemQuotationRejected == 1 || item.IsRegret || item.IsCoreProductCategory <= 0)
                        {
                            continue;
                        }

                        List<CATALOG.ProductQuotationTemplate> jsonToList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(item.ProductQuotationTemplateJson);

                        var tax = item.IGst > 0 ? item.IGst : (item.CGst + item.SGst);
                        wsSheet1.Cells["A" + index].Value = reqID;
                        wsSheet1.Cells["B" + index].Value = 0;
                        wsSheet1.Cells["C" + index].Value = item.ItemID;
                        wsSheet1.Cells["D" + index].Value = item.ProductCode;
                        wsSheet1.Cells["E" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["F" + index].Value = item.ProductNo;
                        wsSheet1.Cells["G" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["H" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["I" + index].Value = item.ProductQuantity;
                        if (requirement.IsDiscountQuotation == 1)
                        {
                            wsSheet1.Cells["J" + index].Value = item.UnitMRP;
                            wsSheet1.Cells["K" + index].Value = item.UnitDiscount;
                            wsSheet1.Cells["L" + index].Value = tax;
                            wsSheet1.Cells["M" + index].Value = item.RevUnitDiscount;
                        }
                        else
                        {
                            wsSheet1.Cells["J" + index].Value = item.UnitPrice;
                            wsSheet1.Cells["K" + index].Value = tax;
                            wsSheet1.Cells["L" + index].Value = item.RevUnitPrice;
                        }

                        
                        var quantityDiscValidationRule = wsSheet1.DataValidations.AddDecimalValidation("I" + index);
                        quantityDiscValidationRule.ShowErrorMessage = true;
                        quantityDiscValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        quantityDiscValidationRule.ErrorTitle = "Invalid Qunatity for this Item";
                        quantityDiscValidationRule.Error = "You cannot modify Qunatity.";
                        quantityDiscValidationRule.Operator = ExcelDataValidationOperator.equal;
                        quantityDiscValidationRule.Formula.Value = 0;
                        var unitPriceDiscValidationRule = wsSheet1.DataValidations.AddDecimalValidation("J" + index);
                        unitPriceDiscValidationRule.ShowErrorMessage = true;
                        unitPriceDiscValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        unitPriceDiscValidationRule.ErrorTitle = "Invalid Price for this Item";
                        unitPriceDiscValidationRule.Error = "You cannot modify Price.";
                        unitPriceDiscValidationRule.Operator = ExcelDataValidationOperator.equal;
                        unitPriceDiscValidationRule.Formula.Value = 0;
                        if (requirement.IsDiscountQuotation == 1)
                        {
                           
                            wsSheet1.Cells["M" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsSheet1.Cells["M" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                           
                            wsSheet1.Cells["N" + index].Formula = $"IF(I{index} > 0, I{index} *(J{index}-((J{index}*M{index})/100))*(1+((L{index})/100)), \"0\")";

                            
                            var unitdiscountValidationRule = wsSheet1.DataValidations.AddDecimalValidation("K" + index);
                            unitdiscountValidationRule.ShowErrorMessage = true;
                            unitdiscountValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                            unitdiscountValidationRule.ErrorTitle = "Invalid Discount for this Item";
                            unitdiscountValidationRule.Error = "You cannot modify Discount.";
                            unitdiscountValidationRule.Operator = ExcelDataValidationOperator.equal;
                            unitdiscountValidationRule.Formula.Value = 0;
                            var sgstDiscValidationRule = wsSheet1.DataValidations.AddDecimalValidation("L" + index);
                            sgstDiscValidationRule.ShowErrorMessage = true;
                            sgstDiscValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                            sgstDiscValidationRule.ErrorTitle = "Invalid Tax";
                            sgstDiscValidationRule.Error = "You cannot modify Tax";
                            sgstDiscValidationRule.Operator = ExcelDataValidationOperator.equal;
                            sgstDiscValidationRule.Formula.Value = 0;
                            sgstDiscValidationRule.Formula2.Value = 99;
                            var revunitDiscountValidation = wsSheet1.DataValidations.AddDecimalValidation("M" + index);
                            revunitDiscountValidation.ShowErrorMessage = true;
                            revunitDiscountValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                            revunitDiscountValidation.ErrorTitle = "Invalid Rev. Unit Discount";
                            revunitDiscountValidation.Error = "Enter Value greater than " + item.UnitDiscount.ToString();
                            revunitDiscountValidation.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                            revunitDiscountValidation.Formula.Value = Convert.ToDouble(item.UnitDiscount);
                        }
                        else
                        {
                            wsSheet1.Cells["L" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsSheet1.Cells["L" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                            var sgstDiscValidationRule = wsSheet1.DataValidations.AddDecimalValidation("K" + index);
                            sgstDiscValidationRule.ShowErrorMessage = true;
                            sgstDiscValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                            sgstDiscValidationRule.ErrorTitle = "Invalid Tax";
                            sgstDiscValidationRule.Error = "You cannot modify Tax";
                            sgstDiscValidationRule.Operator = ExcelDataValidationOperator.equal;
                            sgstDiscValidationRule.Formula.Value = 0;
                            sgstDiscValidationRule.Formula2.Value = 99;
                            var revunitPriceValidationRule = wsSheet1.DataValidations.AddDecimalValidation("L" + index);
                            revunitPriceValidationRule.ShowErrorMessage = true;
                            revunitPriceValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                            revunitPriceValidationRule.ErrorTitle = "Invalid Rev. Unit Price";
                            revunitPriceValidationRule.Error = "Enter Value Between 0 and" + item.UnitPrice.ToString();
                            revunitPriceValidationRule.Operator = ExcelDataValidationOperator.between;
                            revunitPriceValidationRule.Formula.Value = 0;
                            revunitPriceValidationRule.Formula2.Value = Convert.ToDouble(item.UnitPrice);
                        }
                        index++;

                        if (jsonToList != null && jsonToList.Count > 0)
                        {
                            if (jsonToList.Any(j => j.HAS_PRICE > 0))
                            {
                                //var unitPriceValidation = wsSheet1.DataValidations.AddDecimalValidation("H" + parentRowNumber);
                                //unitPriceValidation.ShowErrorMessage = true;
                                //unitPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                //unitPriceValidation.ErrorTitle = "Invalid Unit Price";
                                //unitPriceValidation.Error = "Enter Value greater than zero.";
                                //unitPriceValidation.Operator = ExcelDataValidationOperator.equal;
                                //unitPriceValidation.Formula.Value = 0;
                            }

                            foreach (var subItem in jsonToList)
                            {

                                if (!subItem.NAME.Equals("Total", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? $"A{parentRowNumber}:N{parentRowNumber}":$"A{parentRowNumber}:M{parentRowNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? $"A{parentRowNumber}:N{parentRowNumber}":$"A{parentRowNumber}:M{parentRowNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#f2f2f2"));//Editable
                                    wsSheet1.Cells["C" + index].Value = $"{item.ItemID}-{subItem.T_ID}";
                                    wsSheet1.Cells["E" + index].Value = subItem.NAME;
                                    wsSheet1.Cells["H" + index].Value = subItem.SPECIFICATION;
                                    wsSheet1.Cells["I" + index].Value = subItem.HAS_QUANTITY > 0 ? subItem.CONSUMPTION : 1;

                                    wsSheet1.Cells["I" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["I" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));//NON-Editable
                                    var subItemValicationRule = wsSheet1.DataValidations.AddDecimalValidation("I" + index);
                                    subItemValicationRule.ShowErrorMessage = true;
                                    subItemValicationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                    subItemValicationRule.ErrorTitle = "Invalid Quantity for this Item";
                                    subItemValicationRule.Error = "You cannot modify Qantity.";
                                    subItemValicationRule.Operator = ExcelDataValidationOperator.equal;
                                    subItemValicationRule.Formula.Value = subItem.HAS_QUANTITY > 0 ? Convert.ToDouble(subItem.CONSUMPTION) : 1;

                                    wsSheet1.Cells["J" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["J" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));
                                    var unitPriceValidation = wsSheet1.DataValidations.AddDecimalValidation("J" + index);
                                    unitPriceValidation.ShowErrorMessage = true;
                                    unitPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                    unitPriceValidation.ErrorTitle = "Invalid Unit Price";
                                    unitPriceValidation.Error = "Enter Value greater than zero.";
                                    unitPriceValidation.Operator = ExcelDataValidationOperator.equal;
                                    unitPriceValidation.Formula.Value = Convert.ToDouble(subItem.BULK_PRICE);

                                   

                                    if (requirement.IsDiscountQuotation == 1)
                                    {
                                        wsSheet1.Cells["L" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["L" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));
                                        var subItemTaxValicationRule = wsSheet1.DataValidations.AddDecimalValidation("L" + index);
                                        subItemTaxValicationRule.ShowErrorMessage = true;
                                        subItemTaxValicationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        subItemTaxValicationRule.ErrorTitle = "Invalid Tax for this Item";
                                        subItemTaxValicationRule.Error = "You cannot modify Tax.";
                                        subItemTaxValicationRule.Operator = ExcelDataValidationOperator.equal;
                                        subItemTaxValicationRule.Formula.Value = Convert.ToDouble(subItem.TAX);

                                        wsSheet1.Cells["N" + index].Value = subItem.REV_UNIT_PRICE;
                                        wsSheet1.Cells["J" + index].Value = subItem.BULK_PRICE;
                                        wsSheet1.Cells["M" + index].Value = subItem.REV_BULK_PRICE;
                                        wsSheet1.Cells["L" + index].Value = subItem.TAX;

                                        wsSheet1.Cells["N" + parentRowNumber].Formula = $"IF(I{parentRowNumber} > 0, I{parentRowNumber} *(J{parentRowNumber}-((J{parentRowNumber}*M{parentRowNumber})/100))*(1+((L{parentRowNumber})/100)), \"0\")";
                                        wsSheet1.Cells["M" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["M" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
                                    }
                                    else
                                    {
                                        wsSheet1.Cells["K" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["K" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));

                                        var subItemTaxValicationRule = wsSheet1.DataValidations.AddDecimalValidation("K" + index);
                                        subItemTaxValicationRule.ShowErrorMessage = true;
                                        subItemTaxValicationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        subItemTaxValicationRule.ErrorTitle = "Invalid Tax for this Item";
                                        subItemTaxValicationRule.Error = "You cannot modify Tax.";
                                        subItemTaxValicationRule.Operator = ExcelDataValidationOperator.equal;
                                        subItemTaxValicationRule.Formula.Value = Convert.ToDouble(subItem.TAX);

                                        wsSheet1.Cells["M" + index].Value = subItem.REV_UNIT_PRICE;
                                        wsSheet1.Cells["J" + index].Value = subItem.BULK_PRICE;
                                        wsSheet1.Cells["L" + index].Value = subItem.REV_BULK_PRICE;
                                        wsSheet1.Cells["K" + index].Value = subItem.TAX;
                                        if (subItem.HAS_PRICE > 0 && subItem.REV_BULK_PRICE > 0)
                                        {
                                            wsSheet1.Cells["L" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            wsSheet1.Cells["L" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                            var revunitPriceValidation = wsSheet1.DataValidations.AddDecimalValidation("L" + index);
                                            revunitPriceValidation.ShowErrorMessage = true;
                                            revunitPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                            revunitPriceValidation.ErrorTitle = "Invalid Rev. Unit Price";
                                            revunitPriceValidation.Error = "Enter Value less than " + subItem.REV_BULK_PRICE.ToString();
                                            revunitPriceValidation.Operator = ExcelDataValidationOperator.lessThan;
                                            revunitPriceValidation.Formula.Value = Convert.ToDouble(subItem.REV_BULK_PRICE);
                                        }
                                        else
                                        {
                                            wsSheet1.Cells["L" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            wsSheet1.Cells["L" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));//NON-Editable
                                            var revUnitPriceValidationRule = wsSheet1.DataValidations.AddDecimalValidation("L" + index);
                                            revUnitPriceValidationRule.ShowErrorMessage = true;
                                            revUnitPriceValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                            revUnitPriceValidationRule.ErrorTitle = "Invalid Price for this Item";
                                            revUnitPriceValidationRule.Error = "You cannot modify Price.";
                                            revUnitPriceValidationRule.Operator = ExcelDataValidationOperator.equal;
                                            revUnitPriceValidationRule.Formula.Value = 0;
                                        }

                                        if (subItem.HAS_PRICE > 0)
                                        {
                                            wsSheet1.Cells["M" + parentRowNumber].Style.Locked = true;
                                            wsSheet1.Cells["M" + parentRowNumber].Formula = $"SUM(M{parentRowNumber + 1}:M{index})*I{parentRowNumber}";
                                            wsSheet1.Cells["M" + index].Formula = $"IF(L{index} > 0, L{index}*I{index}*(1+((K{index})/100)), \"0\")";

                                            wsSheet1.Cells["L" + parentRowNumber].Style.Locked = true;
                                            wsSheet1.Cells["L" + parentRowNumber].Formula = $"SUM(M{parentRowNumber + 1}:M{index})";
                                        }
                                        else
                                        {
                                            wsSheet1.Cells["M" + parentRowNumber].Formula = $"IF(L{parentRowNumber} > 0, L{parentRowNumber}*I{parentRowNumber}*(1+((K{parentRowNumber})/100)), \"0\")";
                                        }
                                    }

                                   

                                    index++;
                                }

                                
                            }

                          

                            if (!jsonToList.Any(j => j.HAS_PRICE > 0))
                            {
                                //wsSheet1.Cells["G" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //wsSheet1.Cells["G" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                if (requirement.IsDiscountQuotation == 1)
                                {
                                    wsSheet1.Cells["L" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["L" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));//Non-Editable
                                }
                                else
                                {
                                    wsSheet1.Cells["L" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["L" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                    
                                  
                                }

                                

                            }
                        }

                        parentRowNumber = index;
                    }
                }
                if (requirement.IsDiscountQuotation == 1) {
                    wsSheet1.Column(7).Hidden = true;
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #region 
            if (template.ToUpper().Contains("UNIT_PRICE_BIDDING_DATA"))
            {
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                requirement.AuctionVendors = requirement.AuctionVendors.Where(v => v.CompanyName != "PRICE_CAP" && v.IsQuotationRejected != 1).ToList();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_PRICE_BIDDING_DATA");

                wsSheet1.Cells["A1"].Value = "VENDOR NAME";
                wsSheet1.Cells["B1"].Value = "Vendor Rank";
                wsSheet1.Cells["C1"].Value = "Product Name";
                wsSheet1.Cells["D1"].Value = "UNIT";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "Quoted Unit Price";
                wsSheet1.Cells["G1"].Value = "Quoted GST";
                wsSheet1.Cells["H1"].Value = "Final Bid Price";
                wsSheet1.Cells["I1"].Value = "Total Quoted Price(inc GST)";
                wsSheet1.Cells["J1"].Value = "Total Bid Price(inc GST)";
                wsSheet1.Cells["K1"].Value = "Savings %";
                wsSheet1.Cells["L1"].Value = "Saving Value";
                wsSheet1.Cells["M1"].Value = "Vendor wise total saving %";
                wsSheet1.Cells["N1"].Value = "Vendor wise total saving Value";

                var calcColumns = wsSheet1.Cells["A1:N1"];
                wsSheet1.Cells.AutoFitColumns();
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumns.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                //calcColumns.Style.Border.Bottom.Color.SetColor(Color.Red);
                calcColumns.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                calcColumns.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                calcColumnsFont.Bold = true;
                //calcColumnsFont.Size = 16;
                calcColumnsFont.Color.SetColor(Color.White);

                //calcColumnsFont.Italic = true;


                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                foreach (var entry in requirement.AuctionVendors)
                {
                    entry.savingsPercentage = 0;
                    entry.savings = 0;

                    foreach (var item in entry.ListRequirementItems)
                    {
                        //K
                        item.savingsPercentage = Math.Round(
                            (((item.ItemPrice - item.RevItemPrice)
                            / (item.ItemPrice)) * 100),
                            decimal_round);
                        //L
                        item.savings = Math.Round((item.ItemPrice - item.RevItemPrice), decimal_round);

                    }

                    //M
                    entry.savingsPercentage = Math.Round(
                        (((entry.TotalInitialPrice - entry.RevVendorTotalPrice)
                        / (entry.TotalInitialPrice)) * 100),
                        2);

                    //N
                    entry.savings = Math.Round((entry.TotalInitialPrice - entry.RevVendorTotalPrice), decimal_round);

                }




                int index = 2;
                foreach (var entry in requirement.AuctionVendors)
                {
                    var from = index;
                    var to = index;

                    foreach (var item in entry.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;

                        wsSheet1.Cells["A" + index].Value = entry.CompanyName;
                        if (from == to)
                        {
                            wsSheet1.Cells["B" + index].Value = entry.Rank;
                        }
                        wsSheet1.Cells["C" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["D" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["F" + index].Value = Math.Round(item.UnitPrice * Convert.ToDouble(entry.VendorCurrencyFactor), decimal_round);
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["H" + index].Value = Math.Round(item.RevUnitPrice * Convert.ToDouble(entry.VendorCurrencyFactor), decimal_round);
                        wsSheet1.Cells["I" + index].Value = Math.Round(item.ItemPrice * Convert.ToDouble(entry.VendorCurrencyFactor), decimal_round);
                        wsSheet1.Cells["J" + index].Value = Math.Round(item.RevItemPrice * Convert.ToDouble(entry.VendorCurrencyFactor), decimal_round);
                        wsSheet1.Cells["K" + index].Value = item.savingsPercentage;
                        wsSheet1.Cells["L" + index].Value = Math.Round(item.savings * Convert.ToDouble(entry.VendorCurrencyFactor), decimal_round);
                        wsSheet1.Cells["M" + index].Value = entry.savingsPercentage;
                        wsSheet1.Cells["N" + index].Value = Math.Round(entry.savings * Convert.ToDouble(entry.VendorCurrencyFactor), decimal_round);
                        wsSheet1.Cells.AutoFitColumns();

                        index++;
                        to = index;
                    }
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Merge = true;
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Style.Font.Size = 16;

                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Merge = true;
                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Style.Font.Size = 16;

                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Merge = true;
                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Style.Font.Size = 16;


                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion
            #endregion

            #region bulk margin quotations
            if (template.ToUpper().Contains("BULK_MARGIN_VEND_QUOTATION"))
            {
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("BULK_MARGIN_VEND_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";
                wsSheet1.Cells["U1"].Value = "QUANTITY_ORIG";
                wsSheet1.Cells["V1"].Value = "QUOTATION_STATUS";
                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;
                wsSheet1.Column(21).Hidden = true;
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                int index = 2;
                List<Requirement> requirementList = service.GetVendorRequirements(userID, 14, sessionID);
                foreach (Requirement req in requirementList)
                {
                    if (req.AuctionVendors.Count == 0)
                    {
                        continue;
                    }
                    if (req.PostedOn >= DateTime.UtcNow.AddMonths(-1) && req.IsDiscountQuotation == 2 && (req.StartTime == null || req.StartTime > DateTime.UtcNow))
                    {
                        if (req.RequirementID > 0 && req.PostedOn >= DateTime.UtcNow.AddMonths(-1))
                        {
                            foreach (var item in req.ListRequirementItems)
                            {
                                var gst = item.CGst + item.SGst + item.IGst;
                                wsSheet1.Cells["A" + index].Value = userID;
                                wsSheet1.Cells["B" + index].Value = req.AuctionVendors[0].CompanyName;
                                wsSheet1.Cells["L" + index].Value = item.ItemID;
                                wsSheet1.Cells["C" + index].Value = item.ProductNo;
                                wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                                wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                                wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                                wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                                wsSheet1.Cells["M" + index].Value = 0;
                                wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                                wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                                wsSheet1.Cells["P" + index].Value = req.AuctionVendors[0].OtherProperties;
                                wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                                wsSheet1.Cells["R" + index].Value = req.Title;
                                wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString(); ;
                                wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                                wsSheet1.Cells["U" + index].Value = item.ProductQuantity;
                                wsSheet1.Cells["V" + index].Value = req.AuctionVendors[0].IsQuotationRejected == -1 ? "Pending/Not Uploaded" : (req.AuctionVendors[0].IsQuotationRejected == 0 ? "Approved" : "Rejected");
                                var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                                wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, decimal_round);
                                index++;
                            }
                        }
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region bulk margin bidding
            if (template.ToUpper().Contains("BULK_MARGIN_VEND_BIDDING"))
            {
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("BULK_MARGIN_VEND_BIDDING");
                wsSheet1.Cells["A1"].Value = "REQ_ID";
                wsSheet1.Cells["B1"].Value = "REQ_TITLE";
                wsSheet1.Cells["C1"].Value = "POSTED_DATE";
                wsSheet1.Cells["D1"].Value = "NEGOTIATION_DATE";
                wsSheet1.Cells["E1"].Value = "VENDOR_ID";
                wsSheet1.Cells["F1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["G1"].Value = "HSN_CODE";
                wsSheet1.Cells["H1"].Value = "MANUFACTURER_NAME";
                wsSheet1.Cells["I1"].Value = "BRAND";
                wsSheet1.Cells["J1"].Value = "QUANTITY";
                wsSheet1.Cells["K1"].Value = "COST_PRICE";
                wsSheet1.Cells["L1"].Value = "REV_COST_PRICE";
                wsSheet1.Cells["M1"].Value = "DIFFERENCE";
                wsSheet1.Cells["N1"].Value = "GST";
                wsSheet1.Cells["O1"].Value = "MRP";
                wsSheet1.Cells["P1"].Value = "REV_NET_PRICE";
                wsSheet1.Cells["Q1"].Value = "REV_MARGIN_AMOUNT";
                wsSheet1.Cells["R1"].Value = "REV_MARGIN_PERC";
                wsSheet1.Cells["S1"].Value = "ITEM_ID";
                wsSheet1.Cells["T1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["U1"].Value = "TOTAL_INIT_PRICE";
                wsSheet1.Cells["V1"].Value = "REV_TOTAL_PRICE";
                wsSheet1.Cells["W1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["X1"].Value = "UNITS";
                wsSheet1.Cells["Y1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Z1"].Value = "RANK";
                wsSheet1.Cells["AA1"].Value = "REV_COST_PRICE_ORIG";

                #region excelStyling                

                wsSheet1.Cells["R2:R6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Cells["A:AA"].AutoFitColumns();
                wsSheet1.Column(10).Hidden = true;
                wsSheet1.Column(11).Hidden = true;
                wsSheet1.Column(14).Hidden = true;
                wsSheet1.Column(15).Hidden = true;
                wsSheet1.Column(21).Hidden = true;
                wsSheet1.Column(27).Hidden = true;
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#DAF7A6");
                wsSheet1.Cells["P:P"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["P:P"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["Q:Q"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["Q:Q"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["R:R"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["R:R"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["L:L"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["L:L"].Style.Fill.BackgroundColor.SetColor(colFromHex1);
                var calcColumns = wsSheet1.Cells["P:R"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["M2:M6000"].Formula = "IF(E2 > 0, ABS(K2 - L2), \"\")";
                wsSheet1.Cells["P2:P6000"].Formula = "IF(E2 > 0, L2*(1+(N2/100)), \"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                List<Requirement> requirementList = service.GetVendorRequirements(userID, 1, sessionID);
                int index = 2;
                foreach (Requirement req in requirementList)
                {
                    if (req.AuctionVendors.Count > 0 && (req.AuctionVendors[0].IsQuotationRejected == 1 || req.AuctionVendors[0].IsRevQuotationRejected == 1))
                    {
                        continue;
                    }
                    if (req.PostedOn >= DateTime.UtcNow.AddMonths(-1) && req.IsDiscountQuotation == 2 && (req.StartTime < DateTime.UtcNow && req.EndTime > DateTime.UtcNow))
                    {
                        if (req.AuctionVendors == null)
                        {
                            continue;
                        }
                        foreach (var item in req.ListRequirementItems)
                        {
                            var gst = item.CGst + item.SGst + item.IGst;
                            var revCostPrice = (item.UnitMRP * 100 * 100) / ((item.RevUnitDiscount * 100) + 10000 + (item.RevUnitDiscount * gst) + (gst * 100));
                            wsSheet1.Cells["A" + index].Value = req.RequirementID; // entry.Key;
                            wsSheet1.Cells["B" + index].Value = req.Title;  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["C" + index].Value = req.PostedOn.ToString();  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["D" + index].Value = req.StartTime.ToString();  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["E" + index].Value = userID;  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["F" + index].Value = req.AuctionVendors[0].CompanyName;  // entry.Value.AuctionVendors[0].CompanyName;

                            wsSheet1.Cells["G" + index].Value = item.ProductNo;
                            wsSheet1.Cells["H" + index].Value = item.OthersBrands;
                            wsSheet1.Cells["I" + index].Value = item.ProductBrand;
                            wsSheet1.Cells["J" + index].Value = item.ProductQuantity;
                            wsSheet1.Cells["L" + index].Value = Math.Round(revCostPrice, decimal_round);

                            wsSheet1.Cells["N" + index].Value = item.CGst + item.SGst + item.IGst;
                            wsSheet1.Cells["O" + index].Value = item.UnitMRP;
                            wsSheet1.Cells["S" + index].Value = item.ItemID;
                            wsSheet1.Cells["T" + index].Value = 0;
                            wsSheet1.Cells["U" + index].Value = req.AuctionVendors[0].TotalInitialPrice;
                            wsSheet1.Cells["V" + index].Value = req.AuctionVendors[0].RevVendorTotalPrice;
                            wsSheet1.Cells["W" + index].Value = item.ProductIDorName;
                            wsSheet1.Cells["X" + index].Value = item.ProductQuantityIn;
                            wsSheet1.Cells["Y" + index].Value = req.AuctionVendors[0].OtherProperties;
                            wsSheet1.Cells["Z" + index].Value = req.AuctionVendors[0].Rank > 0 ? req.AuctionVendors[0].Rank.ToString() : "NA";
                            wsSheet1.Cells["AA" + index].Value = Math.Round(revCostPrice, decimal_round);

                            if (req.IsRevUnitDiscountEnable == 0)
                            {
                                wsSheet1.Cells["Q" + index].Value = 0;
                                wsSheet1.Cells["R" + index].Value = 0;
                            }
                            if (req.IsRevUnitDiscountEnable == 1)
                            {
                                wsSheet1.Cells["Q" + index].Formula = "IF(E" + index + " > 0, ABS(O" + index + " - P" + index + "), \"\")";
                                wsSheet1.Cells["R" + index].Formula = "IFERROR(Q" + index + "/P" + index + "*100,\"\")";
                            }

                            var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                            wsSheet1.Cells["K" + index].Value = Math.Round(costPrice, decimal_round);
                            index++;
                        }
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region REQUIREMENT_SAVE
            if (template.ToUpper().Contains("REQUIREMENT_SAVE"))
            {
                List<models.PRMTemplateFields> temlateFields = null;
                PRMCustomFieldService fieldService = new PRMCustomFieldService();
                Requirement requirement = new Requirement();
                if (reqID > 0)
                {
                    requirement = service.GetRequirementData(reqID, userID, sessionID);
                }
                
                if (templateid > 0)
                {
                    temlateFields = fieldService.GetTemplateFieldsTemp(templateid, string.Empty);
                }

                PRMCatalogService catalogService = new PRMCatalogService();
                var companyUnits = service.GetCompanyConfiguration(compID, "ITEM_UNITS", sessionID);
                List<Product> products = catalogService.GetProducts(compID, sessionID);
                products = products.Where(p => (p.IsValid > 0 && !string.IsNullOrEmpty(p.ProductCode))).ToList();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("REQUIREMENT_SAVE");
                ExcelWorksheet wsSheet2 = ExcelPkg.Workbook.Worksheets.Add("REQUIREMENT_SAVE_CONFIG");
                wsSheet1.Cells["A1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_CODE")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_CODE").FIELD_LABEL : "ProductCode";
                wsSheet1.Cells["B1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NAME")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NAME").FIELD_LABEL : "ProductName";
                wsSheet1.Cells["C1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NUMBER")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NUMBER").FIELD_LABEL : "ProductNumber";
                wsSheet1.Cells["D1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_QUANTITY")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_QUANTITY").FIELD_LABEL : "Quantity";
                wsSheet1.Cells["E1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_UNITS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_UNITS").FIELD_LABEL : "Units";
                wsSheet1.Cells["F1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL").FIELD_LABEL : "Make";
                wsSheet1.Cells["G1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_DELIVERY_DETAILS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_DELIVERY_DETAILS").FIELD_LABEL : "DeliveryDetails";
                wsSheet1.Cells["H1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS").FIELD_LABEL : "Description";
                wsSheet1.Cells["A:H"].AutoFitColumns();
                var validation = wsSheet1.DataValidations.AddListValidation($"E2:E{maxRows}");
                validation.ShowErrorMessage = true;
                validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                validation.ErrorTitle = "Invalid Units";
                validation.Error = "Invalid Units Selected";

                var quantityPriceValidation = wsSheet1.DataValidations.AddDecimalValidation($"D2:D{maxRows}");
                quantityPriceValidation.ShowErrorMessage = true;
                quantityPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                quantityPriceValidation.ErrorTitle = "Invalid Quantity";
                quantityPriceValidation.Error = "Enter Value greater than zero.";
                quantityPriceValidation.Operator = ExcelDataValidationOperator.greaterThan;
                quantityPriceValidation.Formula.Value = 0;
                wsSheet1.Cells[$"A2:A{maxRows}"].Style.Numberformat.Format = "@";

                int count = 1;
                foreach (var unit in companyUnits)
                {
                    wsSheet2.Cells["A" + count.ToString()].Value = unit.ConfigValue;
                    count++;
                }

                validation.Formula.ExcelFormula = "REQUIREMENT_SAVE_CONFIG!$A1:$A" + (count - 1).ToString();

                //for (int i = 2; i <= maxRows; i++)
                //{
                //    wsSheet1.Cells["B" + (i)].Formula = $"IFNA(INDIRECT(\"REQUIREMENT_SAVE_CONFIG!C\" & MATCH(A{i}, REQUIREMENT_SAVE_CONFIG!$B:$B,0 )),\"\")";
                //    wsSheet1.Cells["C" + (i)].Formula = $"IFNA(INDIRECT(\"REQUIREMENT_SAVE_CONFIG!D\" & MATCH(A{i}, REQUIREMENT_SAVE_CONFIG!$B:$B,0 )),\"\")";
                //}


                if (requirement != null && requirement.ListRequirementItems != null && requirement.ListRequirementItems.Count > 0)
                {
                    int index = 2;
                    foreach (var item in requirement.ListRequirementItems)
                    {
                        if (item.IsCoreProductCategory <= 0)
                        {
                            continue;
                        }

                        wsSheet1.Cells["A" + index].Value = item.ProductCode;
                        wsSheet1.Cells["B" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["F" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["G" + index].Value = item.ProductDeliveryDetails;
                        wsSheet1.Cells["H" + index].Value = item.ProductDescriptionCustomer;
                        index++;
                    }
                }


                //var productValidation = wsSheet1.DataValidations.AddListValidation($"A2:A{maxRows}");
                //productValidation.ShowErrorMessage = true;
                //productValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                //productValidation.ErrorTitle = "Invalid Product";
                //productValidation.Error = "Invalid Product entered";



                count = 1;
                foreach (var product in products)
                {
                    wsSheet2.Cells["B" + count.ToString()].Value = product.ProductCode;
                    wsSheet2.Cells["C" + count.ToString()].Value = product.ProductName;
                    wsSheet2.Cells["D" + count.ToString()].Value = product.ProductNo;
                    count++;
                }

               // productValidation.Formula.ExcelFormula = "=REQUIREMENT_SAVE_CONFIG!$B1:$B" + (count - 1).ToString();


                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                wsSheet2.Protection.IsProtected = false;
                wsSheet2.Protection.AllowSelectLockedCells = false;
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region CEILING_PRICE
            if (template.ToUpper().Contains("CEILING_DETAILS"))
            {
                List<models.PRMTemplateFields> temlateFields = null;
                PRMCustomFieldService fieldService = new PRMCustomFieldService();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                var reqSettings = service.GetRequirementSettings(reqID, sessionID);
                if (reqSettings != null && reqSettings.Any(s => s.REQ_SETTING == "TEMPLATE_ID"))
                {
                    temlateFields = fieldService.GetTemplateFieldsTemp(Convert.ToInt32(reqSettings.First(s => s.REQ_SETTING == "TEMPLATE_ID").REQ_SETTING_VALUE), string.Empty);
                }

                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("CEILING_DETAILS");
                wsSheet1.Cells["A1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["B1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_CODE")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_CODE").FIELD_LABEL : "ProductCode";
                wsSheet1.Cells["C1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NAME")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NAME").FIELD_LABEL : "ProductName";
                wsSheet1.Cells["D1"].Value = "UNIT_PRICE";
                wsSheet1.Cells["E1"].Value = "TAX";
                wsSheet1.Cells["F1"].Value = "CEILING_PRICE";
                wsSheet1.Cells["G1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["H1"].Value = "USER_ID";
                wsSheet1.Cells["I1"].Value = "COMPANY_NAME";
                wsSheet1.Cells["J1"].Value = "USER_TYPE";

                maxRows = requirement.ListRequirementItems.Count + 1;
                wsSheet1.Cells["A1:J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["A1:J1"].Style.Font.Color.SetColor(Color.White);
                wsSheet1.Cells["A1:J1"].Style.Font.Bold = true;
                wsSheet1.Cells["A1:J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A1:J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Row(1).Height = 60;
                wsSheet1.Cells["A:J"].AutoFitColumns();
                wsSheet1.Cells["A:J"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells["A:J"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells["A:J"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells["A:J"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                var requirementItems = requirement.ListRequirementItems;
                string companyName = requirement.CustomerCompanyName;
                string userType = "CUSTOMER";
                if (requirement.AuctionVendors != null && requirement.AuctionVendors.Count > 0 && requirement.AuctionVendors.Any(v => v.VendorID == userID))
                {
                    requirementItems = requirement.AuctionVendors.First(v => v.VendorID == userID).ListRequirementItems;
                    companyName = requirement.AuctionVendors.First(v => v.VendorID == userID).CompanyName;
                    userType = "VENDOR";
                }

                int index = 2;
                foreach (var item in requirement.ListRequirementItems)
                {
                    if (item.IsCoreProductCategory <= 0)
                    {
                        continue;
                    }

                    wsSheet1.Cells["A" + index].Value = item.ItemID;
                    wsSheet1.Cells["B" + index].Value = item.ProductCode;
                    wsSheet1.Cells["C" + index].Value = item.ProductIDorName;
                    wsSheet1.Cells["D" + index].Value = item.UnitPrice;
                    wsSheet1.Cells["E" + index].Value = item.IGst > 0 ? item.IGst : (item.CGst + item.SGst);
                    wsSheet1.Cells["F" + index].Value = item.CeilingPrice;
                    wsSheet1.Cells["G" + index].Value = item.RequirementID;
                    wsSheet1.Cells["H" + index].Value = userID;
                    wsSheet1.Cells["I" + index].Value = companyName;
                    wsSheet1.Cells["J" + index].Value = userType;
                    index++;
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion
            return Convert.ToBase64String(ms.ToArray());
        }

        public ExcelRequirement GetReqReportForExcel(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            ExcelRequirement excelrequirement = new ExcelRequirement();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetReqReportForExcel", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[3].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[3].Rows[0];
                    excelrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    excelrequirement.RequirementNumber = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;
                    excelrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    excelrequirement.RequirementCurrency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                    excelrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.UtcNow;
                    excelrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.UtcNow;
                    excelrequirement.CurrentDate = prmservices.GetDate();
                    excelrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    excelrequirement.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    excelrequirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_ENABLED"]) : 0;
                    excelrequirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_COMPLETED"]) : 0;
                    excelrequirement.PostedByFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    excelrequirement.PostedByLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    excelrequirement.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    excelrequirement.IsTechScoreReq = row["TECH_SCORE_REQ"] != DBNull.Value ? Convert.ToInt32(row["TECH_SCORE_REQ"]) : 0;
                    excelrequirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[4].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[4].Rows[0];
                    excelrequirement.PreSavings = row["PRE_SAVING"] != DBNull.Value ? Convert.ToDouble(row["PRE_SAVING"]) : 0;
                    excelrequirement.PostSavings = row["REV_SAVING"] != DBNull.Value ? Convert.ToDouble(row["REV_SAVING"]) : 0;
                }

                List<ExcelVendorDetails> ListRV = new List<ExcelVendorDetails>();
                int rank = 1;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ExcelVendorDetails RV = new ExcelVendorDetails();

                        #region VENDOR DETAILS
                        RV.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        RV.TechnicalScore = row["TECHNICAL_SCORE"] != DBNull.Value ? Convert.ToDecimal(row["TECHNICAL_SCORE"]) : 0;
                        RV.VendorName = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
                        RV.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;

                        RV.VendorFreight = row["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["VEND_FREIGHT"]) : 0;
                        RV.RevVendorFreight = row["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_FREIGHT"]) : 0;
                        RV.TotalInitialPrice = row["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_TOTAL_PRICE"]) : 0;
                        RV.RevVendorTotalPrice = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
                        RV.Warranty = row["WARRANTY"] != DBNull.Value ? Convert.ToString(row["WARRANTY"]) : string.Empty;
                        RV.Validity = row["VALIDITY"] != DBNull.Value ? Convert.ToString(row["VALIDITY"]) : string.Empty;
                        RV.OtherProperties = row["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row["OTHER_PROPERTIES"]) : string.Empty;
                        RV.Duration = row["DURATION"] != DBNull.Value ? Convert.ToString(row["DURATION"]) : string.Empty;
                        RV.Payment = row["PAYMENT"] != DBNull.Value ? Convert.ToString(row["PAYMENT"]) : string.Empty;
                        RV.GSTNumber = row["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row["GST_NUMBER"]) : string.Empty;
                        RV.SelectedVendorCurrency = row["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row["SELECTED_VENDOR_CURRENCY"]) : string.Empty;


                        RV.FREIGHT_CHARGES = row["FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES"]) : 0; //*
                        RV.FREIGHT_CHARGES_TAX_PERCENTAGE = row["FREIGHT_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.FREIGHT_CHARGES_WITH_TAX = row["FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_WITH_TAX"]) : 0;
                        RV.REV_FREIGHT_CHARGES = row["REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES"]) : 0;
                        RV.REV_FREIGHT_CHARGES_WITH_TAX = row["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0; //*
                        RV.RevVendorFreightCB = row["REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_CB"]) : 0;

                        RV.InstallationCharges = row["INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES"]) : 0; //*
                        RV.InstallationChargesTaxPercentage = row["INSTALLATION_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.InstallationChargesWithTax = row["INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                        RV.RevinstallationCharges = row["REV_INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES"]) : 0; //*
                        RV.RevinstallationChargesWithTax = row["REV_INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                        RV.RevinstallationChargesCB = row["REV_INSTALLATION_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_CB"]) : 0;
                        RV.RevinstallationChargesWithTaxCB = RV.RevinstallationChargesCB + ((RV.RevinstallationChargesCB / 100) * (RV.InstallationChargesTaxPercentage));

                        RV.PackingCharges = row["PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES"]) : 0; //*
                        RV.PackingChargesTaxPercentage = row["PACKING_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.PackingChargesWithTax = row["PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_WITH_TAX"]) : 0;
                        RV.RevpackingCharges = row["REV_PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES"]) : 0; //*
                        RV.RevpackingChargesWithTax = row["REV_PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_WITH_TAX"]) : 0;
                        RV.RevpackingChargesCB = row["REV_PACKING_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_CB"]) : 0;
                        RV.RevpackingChargesWithTaxCB = RV.RevpackingChargesCB + ((RV.RevpackingChargesCB / 100) * (RV.PackingChargesTaxPercentage));

                        RV.RevVendorTotalPriceCB = row["REV_VEND_TOTAL_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE_CB"]) : 0;

                        RV.INCO_TERMS = row["INCO_TERMS"] != DBNull.Value ? Convert.ToString(row["INCO_TERMS"]) : string.Empty;
                        RV.PayLoadFactor = 0;
                        RV.RevPayLoadFactor = 0;
                        RV.RevChargeAny = 0;
                        RV.IsQuotationRejected = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_QUOTATION_REJECTED"]) : 0;
                        RV.IsRevQuotationRejected = row["IS_REV_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_REV_QUOTATION_REJECTED"]) : 0;
                        RV.IS_QUOT_LATE = row["IS_QUOT_LATE"] != DBNull.Value ? Convert.ToInt32(row["IS_QUOT_LATE"]) : 0;
                        RV.VendorCurrencyFactor = row["CURRENCY_FACTOR"] != DBNull.Value ? Convert.ToDecimal(row["CURRENCY_FACTOR"]) : 1;
                        RV.Vendor = new User();
                        RV.Vendor.VendorCode = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                        #endregion VENDOR DETAILS

                        ListRV.Add(RV);
                    }

                    foreach (ExcelVendorDetails vendor in ListRV.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.TotalInitialPrice).ToList())
                    {
                        vendor.Rank = ListRV.IndexOf(vendor) + 1;
                    }
                }

                List<ExcelRequirementItems> ListRITemp = new List<ExcelRequirementItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        ExcelRequirementItems RI = new ExcelRequirementItems();
                        #region ITEM DETAILS
                        RI.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                        RI.ProductIDorName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        RI.ProductNo = row["PROD_NO"] != DBNull.Value ? Convert.ToString(row["PROD_NO"]) : string.Empty;
                        RI.HsnCode = row["HSN_CODE"] != DBNull.Value ? Convert.ToString(row["HSN_CODE"]) : string.Empty;
                        RI.ProductDescription = row["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row["DESCRIPTION"]) : string.Empty;
                        RI.ProductCode = row["PROD_CODE"] != DBNull.Value ? Convert.ToString(row["PROD_CODE"]) : string.Empty;
                        RI.ProductQuantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
                        RI.ProductQuantityIn = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
                        RI.ProductBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                        RI.OthersBrands = row["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row["OTHER_BRAND"]) : string.Empty;
                        RI.ItemLastPrice = row["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row["LAST_ITEM_PRICE"]) : 0;
                        RI.I_LLP_DETAILS = row["I_LLP_DETAILS"] != DBNull.Value ? Convert.ToString(row["I_LLP_DETAILS"]) : string.Empty;
                        RI.ITEM_L1_PRICE = row["ITEM_L1_PRICE"] != DBNull.Value ? Convert.ToDouble(row["ITEM_L1_PRICE"]) : 0;
                        RI.ITEM_L1_COMPANY_NAME = row["ITEM_L1_COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["ITEM_L1_COMPANY_NAME"]) : string.Empty;
                        RI.ITEM_L2_PRICE = row["ITEM_L2_PRICE"] != DBNull.Value ? Convert.ToDouble(row["ITEM_L2_PRICE"]) : 0;
                        RI.ITEM_L2_COMPANY_NAME = row["ITEM_L2_COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["ITEM_L2_COMPANY_NAME"]) : string.Empty;
                        RI.ProductQuotationTemplateJson = row["PRODUCT_QUOTATION_JSON"] != DBNull.Value ? Convert.ToString(row["PRODUCT_QUOTATION_JSON"]) : string.Empty;
                        RI.ITEM_PR_NUMBER = row["ITEM_PR_NUMBER"] != DBNull.Value ? Convert.ToString(row["ITEM_PR_NUMBER"]) : string.Empty;
                        RI.PR_WBS_CODE = row["PR_WBS_CODE"] != DBNull.Value ? Convert.ToString(row["PR_WBS_CODE"]) : string.Empty;
                        RI.IsCore = row["IS_CORE"] != DBNull.Value ? Convert.ToInt32(row["IS_CORE"]) : 0;
                        RI.QtyDistributed = 0;
                        RI.CatalogueItemID = row["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["CATALOGUE_ITEM_ID"]) : 0;
                        if (!string.IsNullOrEmpty(RI.ProductQuotationTemplateJson))
                        {
                            RI.ProductQuotationTemplateArray = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(RI.ProductQuotationTemplateJson);
                        }

                        RI.ReqVendors = ListRV.Select(item => (ExcelVendorDetails)item.Clone()).ToList().ToArray();
                        #endregion ITEM DETAILS

                        ListRITemp.Add(RI);
                    }
                }

                List<ExcelQuotationPrices> ListQ = new List<ExcelQuotationPrices>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        ExcelQuotationPrices Q = new ExcelQuotationPrices();

                        #region QUOTATION DETAILS
                        Q.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        Q.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                        Q.UnitPrice = row["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["UNIT_PRICE"]) : 0;
                        Q.RevUnitPrice = row["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE"]) : 0;
                        Q.CGst = row["C_GST"] != DBNull.Value ? Convert.ToDouble(row["C_GST"]) : 0;
                        Q.SGst = row["S_GST"] != DBNull.Value ? Convert.ToDouble(row["S_GST"]) : 0;
                        Q.IGst = row["I_GST"] != DBNull.Value ? Convert.ToDouble(row["I_GST"]) : 0;
                        Q.ItemPrice = row["PRICE"] != DBNull.Value ? Convert.ToDouble(row["PRICE"]) : 0;
                        Q.RevItemPrice = row["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE"]) : 0;
                        Q.IsRegret = row["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row["IS_REGRET"]) > 0 ? true : false) : false;
                        Q.RegretComments = row["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REGRET_COMMENTS"]) : string.Empty;
                        Q.ItemLevelInitialComments = row["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
                        Q.ItemLevelRevComments = row["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
                        Q.VendorUnits = row["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row["VENDOR_UNITS"]) : string.Empty;
                        Q.VendorBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                        Q.ItemFreightCharges = row["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_CHARGES"]) : 0;
                        Q.ItemRevFreightCharges = row["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_REV_FREIGHT_CHARGES"]) : 0;
                        Q.ItemFreightTAX = row["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_TAX"]) : 0;
                        Q.RevItemPriceCB = row["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE_CB"]) : 0;
                        Q.RevUnitPriceCB = row["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE_CB"]) : 0;
                        Q.UnitDiscount = row["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row["UNIT_DISCOUNT"]) : 0;
                        Q.RevUnitDiscount = row["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_DISCOUNT"]) : 0;
                        Q.ProductQuotationTemplateJson = row["ProductQuotationTemplateJson"] != DBNull.Value ? Convert.ToString(row["ProductQuotationTemplateJson"]) : string.Empty;
                        if (!string.IsNullOrEmpty(Q.ProductQuotationTemplateJson))
                        {
                            Q.ProductQuotationTemplateArray = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(Q.ProductQuotationTemplateJson);
                        }


                        // ITEM LEVEL RANKING //
                        int is_quotation_rejected = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_QUOTATION_REJECTED"]) : 0;
                        Q.ItemRank = 0;
                        if (is_quotation_rejected <= 0 && Q.RevUnitPrice > 0)
                        {
                            Q.ItemRank = 1;// row2["ITEM_RANK"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_RANK"]) : 0;
                        }
                        // ITEM LEVEL RANKING //

                        #endregion QUOTATION DETAILS

                        ListQ.Add(Q);

                    }
                }

                // ITEM LEVEL RANKING //
                ListQ = ListQ.OrderBy(q => q.ItemID).ThenByDescending(q => q.ItemRank).ThenBy(q => q.RevUnitPrice).ToList();
                int rank1 = 1;
                int prevItemId = 0;
                double prevRevUnitPrice = 0;
                foreach (var temp in ListQ)
                {
                    if (temp.ItemRank > 0)
                    {
                        if (prevItemId == temp.ItemID && temp.RevUnitPrice > 0 && prevRevUnitPrice == temp.RevUnitPrice)
                        {
                            temp.ItemRank = rank1;
                        }

                        if (prevItemId == temp.ItemID && temp.RevUnitPrice > 0 && temp.RevUnitPrice > prevRevUnitPrice)
                        {
                            temp.ItemRank = rank1 + 1;
                            rank1 = rank1 + 1;
                        }

                        if (prevItemId != temp.ItemID)
                        {
                            temp.ItemRank = 1;
                            rank1 = 1;
                        }
                    }

                    prevItemId = temp.ItemID;
                    prevRevUnitPrice = temp.RevUnitPrice;
                }
                // ITEM LEVEL RANKING //

                for (int i = 0; i < ListRITemp.Count; i++)
                {
                    for (int v = 0; v < ListRITemp[i].ReqVendors.Length; v++)
                    {
                        ListRITemp[i].ReqVendors[v].QuotationPrices = new ExcelQuotationPrices();
                        try
                        {
                            ListRITemp[i].ReqVendors[v].QuotationPrices = ListQ.Where(quotation => quotation.VendorID == ListRITemp[i].ReqVendors[v].VendorID && quotation.ItemID == ListRITemp[i].ItemID).FirstOrDefault();
                        }
                        catch
                        {
                            ListRITemp[i].ReqVendors[v].QuotationPrices = new ExcelQuotationPrices();
                        }
                    }
                }

                excelrequirement.ReqItems = ListRITemp.ToArray();

                foreach (ExcelRequirementItems item in excelrequirement.ReqItems)
                {
                    if (item.ReqVendors != null)
                    {
                        List<ExcelVendorDetails> vendors = new List<ExcelVendorDetails>();
                        vendors = item.ReqVendors.Where(v => v.RevVendorTotalPrice > 0).OrderBy(v => v.RevVendorTotalPrice).ToList();
                        item.ReqVendors = vendors.ToArray();
                    }
                }

                //CurrencyConversion
                double factor = 1;
                excelrequirement.Savings = Utilities.RoundValue(excelrequirement.Savings / factor);
                excelrequirement.PreSavings = Utilities.RoundValue(excelrequirement.PreSavings / factor);
                excelrequirement.PostSavings = Utilities.RoundValue(excelrequirement.PostSavings / factor);

                if (excelrequirement.ReqVendors != null && excelrequirement.ReqVendors.Length > 0)
                {
                    foreach (var vendor in excelrequirement.ReqVendors)
                    {
                        vendor.InitialPrice = Utilities.RoundValue(vendor.InitialPrice / factor);
                        vendor.RunningPrice = Utilities.RoundValue(vendor.RunningPrice / factor);
                        vendor.TotalInitialPrice = Utilities.RoundValue(vendor.TotalInitialPrice / factor);
                        vendor.TotalRunningPrice = Utilities.RoundValue(vendor.TotalRunningPrice / factor);
                        vendor.InitialPriceWithOutTaxFreight = Utilities.RoundValue(vendor.InitialPriceWithOutTaxFreight / factor);
                        vendor.VendorFreight = Utilities.RoundValue(vendor.VendorFreight / factor);
                        vendor.RevPrice = Utilities.RoundValue(vendor.RevPrice / factor);
                        vendor.RevVendorTotalPrice = Utilities.RoundValue(vendor.RevVendorTotalPrice / factor);
                        vendor.Discount = Utilities.RoundValue(vendor.Discount / factor);
                        vendor.marginRankDiscount = Utilities.RoundValue(vendor.marginRankDiscount / factor);
                        vendor.marginRankRevDiscount = Utilities.RoundValue(vendor.marginRankRevDiscount / factor);

                        vendor.RevVendorFreightCB = Utilities.RoundValue(vendor.RevVendorFreightCB / factor);
                        vendor.VendorFreightCB = Utilities.RoundValue(vendor.VendorFreightCB / factor);
                        vendor.RevinstallationChargesCB = Utilities.RoundValue(vendor.RevinstallationChargesCB / factor);
                        vendor.RevinstallationChargesWithTaxCB = Utilities.RoundValue(vendor.RevinstallationChargesWithTaxCB / factor);
                        vendor.RevpackingChargesCB = Utilities.RoundValue(vendor.RevpackingChargesCB / factor);
                        vendor.RevpackingChargesWithTaxCB = Utilities.RoundValue(vendor.RevpackingChargesWithTaxCB / factor);
                        vendor.RevVendorTotalPriceCB = Utilities.RoundValue(vendor.RevVendorTotalPriceCB / factor);
                    }
                }

                if (excelrequirement.ReqItems != null && excelrequirement.ReqItems.Length > 0)
                {
                    foreach (var item in excelrequirement.ReqItems)
                    {
                        item.ItemPrice = Utilities.RoundValue(item.ItemPrice / factor);
                        item.RevisedItemPrice = Utilities.RoundValue(item.RevisedItemPrice / factor);
                        item.RevisedInitialprice = Utilities.RoundValue(item.RevisedInitialprice / factor);
                        item.RevisedVendorBidPrice = Utilities.RoundValue(item.RevisedVendorBidPrice / factor);
                        item.RevItemPrice = Utilities.RoundValue(item.RevItemPrice / factor);
                        item.UnitPrice = Utilities.RoundValue(item.UnitPrice / factor);
                        item.RevUnitPrice = Utilities.RoundValue(item.RevUnitPrice / factor);
                        item.UnitMRP = Utilities.RoundValue(item.UnitMRP / factor);
                        item.UnitDiscount = Utilities.RoundValue(item.UnitDiscount / factor);
                        item.RevUnitDiscount = Utilities.RoundValue(item.RevUnitDiscount / factor);
                        item.CostPrice = Utilities.RoundValue(item.CostPrice / factor);

                        item.CostPrice = Utilities.RoundValue(item.CostPrice / factor);
                        item.NetPrice = Utilities.RoundValue(item.NetPrice / factor);
                        item.MarginAmount = Utilities.RoundValue(item.MarginAmount / factor);

                        item.ItemLastPrice = Utilities.RoundValue(item.ItemLastPrice / factor);
                        item.ITEM_L1_PRICE = Utilities.RoundValue(item.ITEM_L1_PRICE / factor);
                        item.ITEM_L2_PRICE = Utilities.RoundValue(item.ITEM_L2_PRICE / factor);

                        foreach (var vendor in item.ReqVendors)
                        {
                            vendor.InitialPrice = Utilities.RoundValue(vendor.InitialPrice / factor);
                            vendor.RunningPrice = Utilities.RoundValue(vendor.RunningPrice / factor);
                            vendor.TotalInitialPrice = Utilities.RoundValue(vendor.TotalInitialPrice / factor);
                            vendor.TotalRunningPrice = Utilities.RoundValue(vendor.TotalRunningPrice / factor);
                            vendor.InitialPriceWithOutTaxFreight = Utilities.RoundValue(vendor.InitialPriceWithOutTaxFreight / factor);
                            vendor.VendorFreight = Utilities.RoundValue(vendor.VendorFreight / factor);
                            vendor.RevPrice = Utilities.RoundValue(vendor.RevPrice / factor);
                            vendor.RevVendorTotalPrice = Utilities.RoundValue(vendor.RevVendorTotalPrice / factor);
                            vendor.Discount = Utilities.RoundValue(vendor.Discount / factor);
                            vendor.marginRankDiscount = Utilities.RoundValue(vendor.marginRankDiscount / factor);
                            vendor.marginRankRevDiscount = Utilities.RoundValue(vendor.marginRankRevDiscount / factor);
                            vendor.QuotationPrices.ItemPrice = Utilities.RoundValue(vendor.QuotationPrices.ItemPrice / factor);
                            vendor.QuotationPrices.RevisedItemPrice = Utilities.RoundValue(vendor.QuotationPrices.RevisedItemPrice / factor);
                            vendor.QuotationPrices.RevisedInitialprice = Utilities.RoundValue(vendor.QuotationPrices.RevisedInitialprice / factor);
                            vendor.QuotationPrices.RevisedVendorBidPrice = Utilities.RoundValue(vendor.QuotationPrices.RevisedVendorBidPrice / factor);
                            vendor.QuotationPrices.RevItemPrice = Utilities.RoundValue(vendor.QuotationPrices.RevItemPrice / factor);
                            vendor.QuotationPrices.UnitPrice = Utilities.RoundValue(vendor.QuotationPrices.UnitPrice / factor);
                            vendor.QuotationPrices.RevUnitPrice = Utilities.RoundValue(vendor.QuotationPrices.RevUnitPrice / factor);
                            vendor.QuotationPrices.UnitMRP = Utilities.RoundValue(vendor.QuotationPrices.UnitMRP / factor);
                            vendor.QuotationPrices.UnitDiscount = Utilities.RoundValue(vendor.QuotationPrices.UnitDiscount / factor);
                            vendor.QuotationPrices.RevUnitDiscount = Utilities.RoundValue(vendor.QuotationPrices.RevUnitDiscount / factor);
                            vendor.QuotationPrices.CostPrice = Utilities.RoundValue(vendor.QuotationPrices.CostPrice / factor);
                            vendor.QuotationPrices.NetPrice = Utilities.RoundValue(vendor.QuotationPrices.NetPrice / factor);
                            vendor.QuotationPrices.MarginAmount = Utilities.RoundValue(vendor.QuotationPrices.MarginAmount / factor);
                            vendor.QuotationPrices.CostPrice = Utilities.RoundValue(vendor.QuotationPrices.CostPrice / factor);

                            vendor.RevVendorFreightCB = Utilities.RoundValue(vendor.RevVendorFreightCB / factor);
                            vendor.VendorFreightCB = Utilities.RoundValue(vendor.VendorFreightCB / factor);
                            vendor.RevinstallationChargesCB = Utilities.RoundValue(vendor.RevinstallationChargesCB / factor);
                            vendor.RevinstallationChargesWithTaxCB = Utilities.RoundValue(vendor.RevinstallationChargesWithTaxCB / factor);
                            vendor.RevpackingChargesCB = Utilities.RoundValue(vendor.RevpackingChargesCB / factor);
                            vendor.RevpackingChargesWithTaxCB = Utilities.RoundValue(vendor.RevpackingChargesWithTaxCB / factor);
                            vendor.RevVendorTotalPriceCB = Utilities.RoundValue(vendor.RevVendorTotalPriceCB / factor);
                        }
                    }
                }
                //^CurrencyConversion

            }
            catch (Exception ex)
            {
                excelrequirement.ErrorMessage = ex.Message;
            }

            return excelrequirement;
        }

        //public MACRequirement GetReqItemWiseVendors(int reqID, string sessionID)
        //{
        //    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //    MACRequirement excelrequirement = new MACRequirement();
        //    try
        //    {
        //        int isValidSession = Utilities.ValidateSession(sessionID, null);
        //        sd.Add("P_REQ_ID", reqID);
        //        DataSet ds = sqlHelper.SelectList("rp_GetReqReportForExcel", sd);
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[3].Rows.Count > 0)
        //        {
        //            DataRow row = ds.Tables[3].Rows[0];
        //            excelrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
        //            excelrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
        //            excelrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.UtcNow;
        //            excelrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.UtcNow;
        //            excelrequirement.CurrentDate = prmservices.GetDate();
        //            excelrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
        //            excelrequirement.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
        //            excelrequirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_ENABLED"]) : 0;
        //            excelrequirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_COMPLETED"]) : 0;
        //            excelrequirement.PostedByFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
        //            excelrequirement.PostedByLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
        //        }

        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[4].Rows.Count > 0)
        //        {
        //            DataRow row = ds.Tables[4].Rows[0];
        //            excelrequirement.PreSavings = row["PRE_SAVING"] != DBNull.Value ? Convert.ToDouble(row["PRE_SAVING"]) : 0;
        //            excelrequirement.PostSavings = row["REV_SAVING"] != DBNull.Value ? Convert.ToDouble(row["REV_SAVING"]) : 0;
        //        }

        //        List<MACVendorDetails> ListRV = new List<MACVendorDetails>();
        //        int rank = 1;
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //        {
        //            foreach (DataRow row in ds.Tables[0].Rows)
        //            {
        //                MACVendorDetails RV = new MACVendorDetails();

        //                #region VENDOR DETAILS
        //                RV.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
        //                RV.VendorName = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
        //                RV.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
        //                RV.VendorFreight = row["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["VEND_FREIGHT"]) : 0;
        //                RV.RevVendorFreight = row["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_FREIGHT"]) : 0;
        //                RV.TotalInitialPrice = row["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_TOTAL_PRICE"]) : 0;
        //                RV.RevVendorTotalPrice = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
        //                RV.Warranty = row["WARRANTY"] != DBNull.Value ? Convert.ToString(row["WARRANTY"]) : string.Empty;
        //                RV.Validity = row["VALIDITY"] != DBNull.Value ? Convert.ToString(row["VALIDITY"]) : string.Empty;
        //                RV.OtherProperties = row["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row["OTHER_PROPERTIES"]) : string.Empty;
        //                RV.Duration = row["DURATION"] != DBNull.Value ? Convert.ToString(row["DURATION"]) : string.Empty;
        //                RV.Payment = row["PAYMENT"] != DBNull.Value ? Convert.ToString(row["PAYMENT"]) : string.Empty;
        //                RV.GSTNumber = row["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row["GST_NUMBER"]) : string.Empty;
        //                RV.SelectedVendorCurrency = row["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row["SELECTED_VENDOR_CURRENCY"]) : string.Empty;

        //                RV.PackingChargesTaxPercentage = row["PACKING_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_TAX_PERCENTAGE"]) : 0;
        //                RV.PackingChargesWithTax = row["PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevpackingChargesWithTax = row["REV_PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevpackingChargesCB = row["REV_PACKING_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_CB"]) : 0;
        //                RV.RevpackingChargesWithTaxCB = RV.RevpackingChargesCB + ((RV.RevpackingChargesCB / 100) * (RV.PackingChargesTaxPercentage));

        //                RV.InstallationChargesTaxPercentage = row["INSTALLATION_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_TAX_PERCENTAGE"]) : 0;
        //                RV.InstallationChargesWithTax = row["INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevinstallationChargesWithTax = row["REV_INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevinstallationChargesCB = row["REV_INSTALLATION_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_CB"]) : 0;
        //                RV.RevinstallationChargesWithTaxCB = RV.RevinstallationChargesCB + ((RV.RevinstallationChargesCB / 100) * (RV.InstallationChargesTaxPercentage));

        //                RV.FREIGHT_CHARGES_TAX_PERCENTAGE = row["FREIGHT_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_TAX_PERCENTAGE"]) : 0;
        //                RV.FREIGHT_CHARGES = row["FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES"]) : 0;
        //                RV.FREIGHT_CHARGES_WITH_TAX = row["FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_WITH_TAX"]) : 0;
        //                RV.REV_FREIGHT_CHARGES = row["REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES"]) : 0;
        //                RV.REV_FREIGHT_CHARGES_WITH_TAX = row["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0;
        //                RV.REV_FREIGHT_CHARGES_WITH_TAX = row["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevVendorFreightCB = row["REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_CB"]) : 0;

        //                RV.RevVendorTotalPriceCB = row["REV_VEND_TOTAL_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE_CB"]) : 0;

        //                #endregion VENDOR DETAILS

        //                ListRV.Add(RV);
        //            }

        //            foreach (MACVendorDetails vendor in ListRV.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.TotalInitialPrice).ToList())
        //            {
        //                vendor.Rank = ListRV.IndexOf(vendor) + 1;
        //            }
        //        }

        //        List<MACRequirementItems> ListRITemp = new List<MACRequirementItems>();
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
        //        {
        //            foreach (DataRow row in ds.Tables[1].Rows)
        //            {
        //                MACRequirementItems RI = new MACRequirementItems();
        //                #region ITEM DETAILS
        //                RI.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
        //                RI.ProductIDorName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
        //                RI.ProductNo = row["PROD_NO"] != DBNull.Value ? Convert.ToString(row["PROD_NO"]) : string.Empty;
        //                RI.ProductDescription = row["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row["DESCRIPTION"]) : string.Empty;
        //                RI.ProductQuantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
        //                RI.ProductQuantityIn = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
        //                RI.ProductBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
        //                RI.OthersBrands = row["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row["OTHER_BRAND"]) : string.Empty;
        //                RI.ItemLastPrice = row["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row["LAST_ITEM_PRICE"]) : 0;

        //                RI.ReqVendors = ListRV.Select(item => (MACVendorDetails)item.Clone()).ToList().ToArray();
        //                #endregion ITEM DETAILS

        //                ListRITemp.Add(RI);
        //            }
        //        }

        //        List<MACQuotationPrices> ListQ = new List<MACQuotationPrices>();
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
        //        {
        //            foreach (DataRow row in ds.Tables[2].Rows)
        //            {
        //                MACQuotationPrices Q = new MACQuotationPrices();

        //                #region QUOTATION DETAILS
        //                Q.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
        //                Q.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
        //                Q.UnitPrice = row["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["UNIT_PRICE"]) : 0;
        //                Q.RevUnitPrice = row["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE"]) : 0;
        //                Q.CGst = row["C_GST"] != DBNull.Value ? Convert.ToDouble(row["C_GST"]) : 0;
        //                Q.SGst = row["S_GST"] != DBNull.Value ? Convert.ToDouble(row["S_GST"]) : 0;
        //                Q.IGst = row["I_GST"] != DBNull.Value ? Convert.ToDouble(row["I_GST"]) : 0;
        //                Q.ItemPrice = row["PRICE"] != DBNull.Value ? Convert.ToDouble(row["PRICE"]) : 0;
        //                Q.RevItemPrice = row["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE"]) : 0;
        //                Q.IsRegret = row["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row["IS_REGRET"]) > 0 ? true : false) : false;
        //                Q.RegretComments = row["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REGRET_COMMENTS"]) : string.Empty;
        //                Q.ItemLevelInitialComments = row["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
        //                Q.ItemLevelRevComments = row["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
        //                Q.VendorUnits = row["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row["VENDOR_UNITS"]) : string.Empty;
        //                Q.VendorBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
        //                Q.ItemFreightCharges = row["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_CHARGES"]) : 0;
        //                Q.ItemRevFreightCharges = row["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_REV_FREIGHT_CHARGES"]) : 0;
        //                Q.ItemFreightTAX = row["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_TAX"]) : 0;
        //                Q.RevItemPriceCB = row["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE_CB"]) : 0;
        //                Q.RevUnitPriceCB = row["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE_CB"]) : 0;
        //                #endregion QUOTATION DETAILS

        //                ListQ.Add(Q);

        //            }
        //        }

        //        for (int i = 0; i < ListRITemp.Count; i++)
        //        {
        //            for (int v = 0; v < ListRITemp[i].ReqVendors.Length; v++)
        //            {
        //                ListRITemp[i].ReqVendors[v].QuotationPrices = new MACQuotationPrices();
        //                try
        //                {
        //                    ListRITemp[i].ReqVendors[v].QuotationPrices = ListQ.Where(quotation => quotation.VendorID == ListRITemp[i].ReqVendors[v].VendorID && quotation.ItemID == ListRITemp[i].ItemID).FirstOrDefault();
        //                }
        //                catch
        //                {
        //                    ListRITemp[i].ReqVendors[v].QuotationPrices = new MACQuotationPrices();
        //                }
        //            }
        //        }

        //        excelrequirement.ReqItems = ListRITemp.ToArray();
        //        excelrequirement.ReqVendors = ListRV.ToArray();


        //        //foreach (ExcelRequirementItems item in excelrequirement.ReqItems) {
        //        //    if (item.ReqVendors != null)
        //        //    {
        //        //        List<ExcelVendorDetails> vendors = new List<ExcelVendorDetails>();
        //        //        vendors = item.ReqVendors.OrderBy(v => v.RevVendorTotalPrice).ToList();
        //        //        item.ReqVendors = vendors.ToArray();
        //        //    }
        //        //}

        //    }
        //    catch (Exception ex)
        //    {
        //        excelrequirement.ErrorMessage = ex.Message;
        //    }

        //    return excelrequirement;
        //}

        public List<ConsolidatedReport> GetConsolidatedReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ConsolidatedReport> consolidatedReports = new List<ConsolidatedReport>();
            List<RequirementUsersByRank> revisedReport = new List<RequirementUsersByRank>();
            List<RequirementUsersByRank> initialReport = new List<RequirementUsersByRank>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.UtcNow.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.UtcNow.ToString();
                }

                //DateTime fromDate = Convert.ToDateTime(from);
                //DateTime toDate = Convert.ToDateTime(to);

                //sd.Add("P_FROM_DATE", Convert.ToDateTime(from).ToUniversalTime());
                //sd.Add("P_TO_DATE", Convert.ToDateTime(to).ToUniversalTime());

                sd.Add("P_FROM_DATE", from);
                sd.Add("P_TO_DATE", to);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetConsolidatedReports", sd);
                
                #region Revised Details (Table 1)

                //List<RevisedDetails> revisedDetails = new List<RevisedDetails>();

                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[1].Rows)
                    {
                        RequirementUsersByRank revised = new RequirementUsersByRank();
                        revised.UID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                        revised.RequirementID = row1["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row1["REQ_ID"]) : 0;
                        revised.RevBasePrice = row1["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_BASE_PRICE"]) : 0;
                        revised.InitBasePrice = row1["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["INIT_BASE_PRICE"]) : 0;
                        revised.RevVendTotalPrice = row1["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE"]) : 0;
                        revised.Rank = row1["RN"] != DBNull.Value ? Convert.ToInt32(row1["RN"]) : 0;
                        //revised.Quantity = row1["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row1["QUANTITY"]) : 0;
                        revised.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;
                        revised.SelectedVendorCurrency = row1["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row1["SELECTED_VENDOR_CURRENCY"]) : string.Empty;

                        revisedReport.Add(revised);
                    }
                }

                #endregion Revised Details

                #region initial details (table 2)

                // list<initialdetails> initialdetails = new list<initialdetails>();

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        RequirementUsersByRank initial = new RequirementUsersByRank();

                        //U_ID, REQ_ID, REV_BASE_PRICE, INIT_BASE_PRICE, RN, QUANTITY, COMPANY_NAME
                        initial.UID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                        initial.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                        initial.RevBasePrice = row2["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_BASE_PRICE"]) : 0;
                        initial.InitBasePrice = row2["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["INIT_BASE_PRICE"]) : 0;
                        initial.VendTotalPrice = row2["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["VEND_TOTAL_PRICE"]) : 0;
                        initial.Rank = row2["RN"] != DBNull.Value ? Convert.ToInt32(row2["RN"]) : 0;
                        initial.Quantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                        // initial.CompanyName = row2["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row2["COMPANY_NAME"]) : string.Empty;

                        initialReport.Add(initial);
                    }
                }

                #endregion requirement items

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        ConsolidatedReport report = new ConsolidatedReport();
                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.RequirementNumber = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;
                        report.UID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                        report.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        report.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                        report.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                         report.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                        report.Closed = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        report.NoOfVendorsInvited = row["NoOfVendorsInvited"] != DBNull.Value ? Convert.ToInt32(row["NoOfVendorsInvited"]) : 0;
                        report.NoOfvendorsParticipated = row["NoOfvendorsParticipated"] != DBNull.Value ? Convert.ToInt32(row["NoOfvendorsParticipated"]) : 0;
                        report.ReqCategory = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]) : string.Empty;
                        report.RequirementCurrency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                        report.PR_ID = row["PR_ID"] != DBNull.Value ? Convert.ToString(row["PR_ID"]) : string.Empty;
                        report.PRNumbers = row["PR_NUMBERS"] != DBNull.Value ? Convert.ToString(row["PR_NUMBERS"]) : string.Empty;

                        DateTime now = DateTime.UtcNow;
                        report.DATETIME_NOW = now;
                        DateTime DATE_CB_END_TIME = Convert.ToDateTime(report.CB_END_TIME);
                        long DIFF_CB_END_TIME = Convert.ToInt64((DATE_CB_END_TIME - now).TotalSeconds);

                        if (DIFF_CB_END_TIME > 0)
                        {
                            report.CB_TIME_LEFT = DIFF_CB_END_TIME;
                        }
                        else
                        {
                            report.CB_TIME_LEFT = 0;
                            report.CB_END_TIME = now;
                        }


                        if (report.EndTime != DateTime.MaxValue && report.EndTime > now && report.StartTime < now)
                        {
                            DateTime date = Convert.ToDateTime(report.EndTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            report.TimeLeft = diff;
                            report.Closed = prmservices.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                        }
                        else if (report.StartTime != DateTime.MaxValue && report.StartTime > now)
                        {
                            DateTime date = Convert.ToDateTime(report.StartTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            report.TimeLeft = diff;
                            report.Closed = prmservices.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                        }
                        else if (report.EndTime < now)
                        {
                            report.TimeLeft = -1;
                            if ((Status == prmservices.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == prmservices.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && report.EndTime < now)
                            {
                                report.Closed = prmservices.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                                //EndNegotiation(report.RequirementID, report.CustomerID, sessionID);
                            }
                            else
                            {
                                report.Closed = Status;
                            }
                        }
                        else if (report.StartTime == DateTime.MaxValue)
                        {
                            report.TimeLeft = -1;
                            report.Closed = prmservices.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                        }
                        if (Status == prmservices.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                        {
                            report.TimeLeft = -1;
                            report.Closed = Status;
                        }


                        if (DIFF_CB_END_TIME > 0)
                        {
                            report.CB_TIME_LEFT = DIFF_CB_END_TIME;
                            report.TimeLeft = DIFF_CB_END_TIME;
                        }
                        else
                        {
                            report.CB_TIME_LEFT = 0;
                            report.CB_END_TIME = now;
                        }

                        RequirementUsersByRank InitialUser = initialReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL1 = revisedReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL2 = revisedReport.Where(a => a.Rank == 2 && a.RequirementID == report.RequirementID).FirstOrDefault();

                        #region Initial L1 START
                        if (InitialUser != null)
                        {
                            report.IL1_UID = InitialUser.UID;
                            report.IL1_RevBasePrice = InitialUser.RevBasePrice;
                            report.IL1_InitBasePrice = InitialUser.InitBasePrice;
                            report.IL1_L1InitialBasePrice = InitialUser.L1InitialBasePrice;
                            report.IL1_VendTotalPrice = InitialUser.VendTotalPrice;
                            report.IL1_Rank = InitialUser.Rank;
                            report.IL1_Quantity = InitialUser.Quantity;
                            report.IL1_CompanyName = InitialUser.CompanyName;
                            report.IL1_SelectedVendorCurrency = InitialUser.SelectedVendorCurrency;
                        }

                        #endregion Initial L1 END

                        #region Revised RL1 START
                        if (RevisedUserL1 != null)
                        {
                            report.RL1_UID = RevisedUserL1.UID;
                            report.RL1_RevBasePrice = RevisedUserL1.RevBasePrice;
                            report.RL1_InitBasePrice = RevisedUserL1.InitBasePrice;
                            report.RL1_L1InitialBasePrice = RevisedUserL1.L1InitialBasePrice;
                            report.RL1_RevVendTotalPrice = RevisedUserL1.RevVendTotalPrice;
                            report.RL1_Rank = RevisedUserL1.Rank;
                            report.RL1_Quantity = RevisedUserL1.Quantity;
                            report.RL1_CompanyName = RevisedUserL1.CompanyName;
                            report.RL1_SelectedVendorCurrency = RevisedUserL1.SelectedVendorCurrency;
                        }

                        #endregion Revised RL1 END

                        #region Revised RL2 START
                        if (RevisedUserL2 != null)
                        {
                            report.RL2_UID = RevisedUserL2.UID;
                            report.RL2_RevBasePrice = RevisedUserL2.RevBasePrice;
                            report.RL2_InitBasePrice = RevisedUserL2.InitBasePrice;
                            report.RL2_L1InitialBasePrice = RevisedUserL2.L1InitialBasePrice;
                            report.RL2_RevVendTotalPrice = RevisedUserL2.RevVendTotalPrice;
                            report.RL2_Rank = RevisedUserL2.Rank;
                            report.RL2_Quantity = RevisedUserL2.Quantity;
                            report.RL2_CompanyName = RevisedUserL2.CompanyName;
                            report.RL2_SelectedVendorCurrency = RevisedUserL2.SelectedVendorCurrency;
                            #endregion Revised RL2 END
                        }

                        report.BasePriceSavings = report.IL1_VendTotalPrice - report.RL1_RevVendTotalPrice;
                        if (report.BasePriceSavings > 0)
                        {
                            report.BasePriceSavings = Math.Round(report.BasePriceSavings, decimal_round);
                        }
                        else
                        {
                            report.BasePriceSavings = 0;
                        }

                        report.SavingsPercentage = (100 - (report.RL1_RevVendTotalPrice / report.IL1_VendTotalPrice) * 100);
                        if (report.SavingsPercentage > 0)
                        {
                            report.SavingsPercentage = Math.Round(report.SavingsPercentage, decimal_round);
                        }
                        else
                        {
                            report.SavingsPercentage = 0;
                        }

                        //report.SavingsPercentage = Math.Round(report.SavingsPercentage, decimal_round);
                        //report.RevisedUserL1 = RevisedUserL1;
                        //report.RevisedUserL2 = RevisedUserL2;

                        double factor = 1;// Utilities.GetRequirementCurrencyFactor(report.RequirementID);
                        report.Savings = Utilities.RoundValue(report.Savings / factor);
                        report.L1RevPrice = Utilities.RoundValue(report.L1RevPrice / factor);
                        report.L2RevPrice = Utilities.RoundValue(report.L2RevPrice / factor);
                        report.L1InitialBasePrice = Utilities.RoundValue(report.L1InitialBasePrice / factor);
                        report.L2InitialBasePrice = Utilities.RoundValue(report.L2InitialBasePrice / factor);
                        report.InitialL1BasePrice = Utilities.RoundValue(report.InitialL1BasePrice / factor);
                        report.IL1_RevBasePrice = Utilities.RoundValue(report.IL1_RevBasePrice / factor);
                        report.IL1_InitBasePrice = Utilities.RoundValue(report.IL1_InitBasePrice / factor);
                        report.IL1_L1InitialBasePrice = Utilities.RoundValue(report.IL1_L1InitialBasePrice / factor);
                        report.IL1_VendTotalPrice = Utilities.RoundValue(report.IL1_VendTotalPrice / factor);
                        report.RL1_RevBasePrice = Utilities.RoundValue(report.RL1_RevBasePrice / factor);
                        report.RL1_InitBasePrice = Utilities.RoundValue(report.RL1_InitBasePrice / factor);
                        report.RL1_L1InitialBasePrice = Utilities.RoundValue(report.RL1_L1InitialBasePrice / factor);
                        report.RL1_RevVendTotalPrice = Utilities.RoundValue(report.RL1_RevVendTotalPrice / factor);
                        report.RL2_RevBasePrice = Utilities.RoundValue(report.RL2_RevBasePrice / factor);
                        report.RL2_InitBasePrice = Utilities.RoundValue(report.RL2_InitBasePrice / factor);
                        report.RL2_L1InitialBasePrice = Utilities.RoundValue(report.RL2_L1InitialBasePrice / factor);
                        report.RL2_RevVendTotalPrice = Utilities.RoundValue(report.RL2_RevVendTotalPrice / factor);
                        report.BasePriceSavings = Utilities.RoundValue(report.BasePriceSavings / factor);
                        report.L1BasePrice = Utilities.RoundValue(report.L1BasePrice / factor);
                        report.L2BasePrice = Utilities.RoundValue(report.L2BasePrice / factor);

                        consolidatedReports.Add(report);
                    }

                    consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();
                }
            }
            catch (Exception ex)
            {
                ConsolidatedReport recuirementerror = new ConsolidatedReport();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public List<LogisticConsolidatedReport> GetLogisticConsolidatedReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<LogisticConsolidatedReport> consolidatedReports = new List<LogisticConsolidatedReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.Now.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.Now.ToString();
                }

                DateTime fromDate = Convert.ToDateTime(from);
                DateTime toDate = Convert.ToDateTime(to);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetLogisticConsolidatedReports", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LogisticConsolidatedReport report = new LogisticConsolidatedReport();

                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.InvoiceNumber = row["INVOICE_NUMBER"] != DBNull.Value ? Convert.ToString(row["INVOICE_NUMBER"]) : string.Empty;
                        report.InvoiceDate = row["INVOICE_DATE"] != DBNull.Value ? Convert.ToDateTime(row["INVOICE_DATE"]) : DateTime.MaxValue;
                        report.ConsigneeName = row["CONSIGNEE_NAME"] != DBNull.Value ? Convert.ToString(row["CONSIGNEE_NAME"]) : string.Empty;
                        report.Destination = row["FINAL_DESTINATION"] != DBNull.Value ? Convert.ToString(row["FINAL_DESTINATION"]) : string.Empty;
                        report.ProductName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        report.ShippingMode = row["MODE_OF_SHIPMENT"] != DBNull.Value ? Convert.ToString(row["MODE_OF_SHIPMENT"]) : string.Empty;
                        // report.QTY = row["NET_WEIGHT"] != DBNull.Value ? Convert.ToInt32(row["NET_WEIGHT"]) : 0;
                        report.ChargeableWt = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
                        report.DGFee = row["FORWORD_DG_FEE"] != DBNull.Value ? Convert.ToDouble(row["FORWORD_DG_FEE"]) : 0;
                        report.AMS = row["AMS"] != DBNull.Value ? Convert.ToString(row["AMS"]) : string.Empty;
                        report.MISC = row["MISC"] != DBNull.Value ? Convert.ToString(row["MISC"]) : string.Empty;
                        // report.MenzinesCharges = row["L2_NAME"] != DBNull.Value ? Convert.ToDouble(row["L2_NAME"]) : 0;
                        // report.ChaCharges = row["L1_INITIAL_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row["L1_INITIAL_BASE_PRICE"]) : 0;
                        report.FreightBidAmount = row["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_PRICE"]) : 0;
                        // report.GrandTotal = row["BASE_PRICE_SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["BASE_PRICE_SAVINGS"]) : 0;
                        report.NetWeight = row["NET_WEIGHT"] != DBNull.Value ? Convert.ToDouble(row["NET_WEIGHT"]) : 0;
                        report.ServiceCharges = row["SERVICE_CHARGES"] != DBNull.Value ? Convert.ToDecimal(row["SERVICE_CHARGES"]) : 0;
                        report.CustomClearance = row["CUSTOM_CLEARANCE"] != DBNull.Value ? Convert.ToDecimal(row["CUSTOM_CLEARANCE"]) : 0;
                        report.TerminalHandling = row["TERMINAL_HANDLING"] != DBNull.Value ? Convert.ToDecimal(row["TERMINAL_HANDLING"]) : 0;
                        report.DrawbackAmount = row["DRAWBACK_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["DRAWBACK_AMOUNT"]) : 0;
                        report.LoadingUnloadingCharges = row["LOADING_UNLOADING"] != DBNull.Value ? Convert.ToDouble(row["LOADING_UNLOADING"]) : 0;
                        report.WarehouseStorages = row["WAREHOUSE_STORAGES"] != DBNull.Value ? Convert.ToDouble(row["WAREHOUSE_STORAGES"]) : 0;
                        report.AdditionalMiscExp = row["ADDITIONAL_MISC_EXP"] != DBNull.Value ? Convert.ToDouble(row["ADDITIONAL_MISC_EXP"]) : 0;
                        report.NarcoticSubcharges = row["NARCOTIC_SUBCHARGES"] != DBNull.Value ? Convert.ToDouble(row["NARCOTIC_SUBCHARGES"]) : 0;
                        report.palletizeNumber = row["PALLETIZE_NUMBER"] != DBNull.Value ? Convert.ToDouble(row["PALLETIZE_NUMBER"]) : 0;
                        report.palletizeQty = row["PALLETIZE_QTY"] != DBNull.Value ? Convert.ToDouble(row["PALLETIZE_QTY"]) : 0;
                        report.QTY = row["QUANTITY"] != DBNull.Value ? Convert.ToInt32(row["QUANTITY"]) : 0;
                        report.IsPalletize = row["IS_PALLETIZE"] != DBNull.Value ? Convert.ToInt32(row["IS_PALLETIZE"]) : 0;

                        consolidatedReports.Add(report);
                    }

                    // consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();
                }
            }
            catch (Exception ex)
            {
                LogisticConsolidatedReport recuirementerror = new LogisticConsolidatedReport();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public List<ConsolidatedBasePriceReports> GetConsolidatedBasePriceReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ConsolidatedBasePriceReports> consolidatedReports = new List<ConsolidatedBasePriceReports>();
            List<RequirementUsersByRank> revisedReport = new List<RequirementUsersByRank>();
            List<RequirementUsersByRank> initialReport = new List<RequirementUsersByRank>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.UtcNow.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.UtcNow.ToString();
                }

                //DateTime fromDate = Convert.ToDateTime(from);
                //DateTime toDate = Convert.ToDateTime(to);
                sd.Add("P_FROM_DATE", from);
                sd.Add("P_TO_DATE", to);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetConsolidatedBasePriceReports", sd);


                #region Revised Details (Table 1)

                //List<RevisedDetails> revisedDetails = new List<RevisedDetails>();

                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[1].Rows)
                    {
                        RequirementUsersByRank revised = new RequirementUsersByRank();
                        revised.UID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                        revised.RequirementID = row1["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row1["REQ_ID"]) : 0;
                        revised.RevBasePrice = row1["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_BASE_PRICE"]) : 0;
                        revised.InitBasePrice = row1["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["INIT_BASE_PRICE"]) : 0;
                        revised.Rank = row1["RN"] != DBNull.Value ? Convert.ToInt32(row1["RN"]) : 0;
                        revised.Quantity = row1["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row1["QUANTITY"]) : 0;
                        revised.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;

                        revisedReport.Add(revised);
                    }
                }

                #endregion Revised Details

                #region initial details (table 2)

                // list<initialdetails> initialdetails = new list<initialdetails>();

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        RequirementUsersByRank initial = new RequirementUsersByRank();

                        //U_ID, REQ_ID, REV_BASE_PRICE, INIT_BASE_PRICE, RN, QUANTITY, COMPANY_NAME
                        initial.UID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                        initial.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                        initial.RevBasePrice = row2["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_BASE_PRICE"]) : 0;
                        initial.InitBasePrice = row2["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["INIT_BASE_PRICE"]) : 0;
                        initial.Rank = row2["RN"] != DBNull.Value ? Convert.ToInt32(row2["RN"]) : 0;
                        initial.Quantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                        // initial.CompanyName = row2["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row2["COMPANY_NAME"]) : string.Empty;

                        initialReport.Add(initial);
                    }
                }

                #endregion requirement items

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        ConsolidatedBasePriceReports report = new ConsolidatedBasePriceReports();
                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.UID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                        report.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        report.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                        // report.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                        report.Closed = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        report.NoOfVendorsInvited = row["NoOfVendorsInvited"] != DBNull.Value ? Convert.ToInt32(row["NoOfVendorsInvited"]) : 0;
                        report.NoOfvendorsParticipated = row["NoOfvendorsParticipated"] != DBNull.Value ? Convert.ToInt32(row["NoOfvendorsParticipated"]) : 0;
                        report.ReqCategory = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]) : string.Empty;


                        RequirementUsersByRank InitialUser = initialReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL1 = revisedReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL2 = revisedReport.Where(a => a.Rank == 2 && a.RequirementID == report.RequirementID).FirstOrDefault();

                        #region Initial L1 START
                        if (InitialUser != null)
                        {
                            report.IL1_UID = InitialUser.UID;
                            report.IL1_RevBasePrice = InitialUser.RevBasePrice;
                            report.IL1_InitBasePrice = InitialUser.InitBasePrice;
                            report.IL1_L1InitialBasePrice = InitialUser.L1InitialBasePrice;
                            report.IL1_Rank = InitialUser.Rank;
                            report.IL1_Quantity = InitialUser.Quantity;
                            report.IL1_CompanyName = InitialUser.CompanyName;
                        }

                        #endregion Initial L1 END

                        #region Revised RL1 START
                        if (RevisedUserL1 != null)
                        {
                            report.RL1_UID = RevisedUserL1.UID;
                            report.RL1_RevBasePrice = RevisedUserL1.RevBasePrice;
                            report.RL1_InitBasePrice = RevisedUserL1.InitBasePrice;
                            report.RL1_L1InitialBasePrice = RevisedUserL1.L1InitialBasePrice;
                            report.RL1_Rank = RevisedUserL1.Rank;
                            report.RL1_Quantity = RevisedUserL1.Quantity;
                            report.RL1_CompanyName = RevisedUserL1.CompanyName;
                        }

                        #endregion Revised RL1 END

                        #region Revised RL2 START
                        if (RevisedUserL2 != null)
                        {
                            report.RL2_UID = RevisedUserL2.UID;
                            report.RL2_RevBasePrice = RevisedUserL2.RevBasePrice;
                            report.RL2_InitBasePrice = RevisedUserL2.InitBasePrice;
                            report.RL2_L1InitialBasePrice = RevisedUserL2.L1InitialBasePrice;
                            report.RL2_Rank = RevisedUserL2.Rank;
                            report.RL2_Quantity = RevisedUserL2.Quantity;
                            report.RL2_CompanyName = RevisedUserL2.CompanyName;
                            #endregion Revised RL2 END
                        }

                        report.BasePriceSavings = report.IL1_InitBasePrice - report.RL1_RevBasePrice;
                        if (report.BasePriceSavings > 0)
                        {
                            report.BasePriceSavings = Math.Round(report.BasePriceSavings, decimal_round);
                        }
                        else
                        {
                            report.BasePriceSavings = 0;
                        }

                        report.SavingsPercentage = (100 - (report.RL1_RevBasePrice / report.IL1_InitBasePrice) * 100);
                        if (report.SavingsPercentage > 0)
                        {
                            report.SavingsPercentage = Math.Round(report.SavingsPercentage, decimal_round);
                        }
                        else
                        {
                            report.SavingsPercentage = 0;
                        }


                        //report.SavingsPercentage = Math.Round(report.SavingsPercentage, decimal_round);
                        //report.RevisedUserL1 = RevisedUserL1;
                        //report.RevisedUserL2 = RevisedUserL2;

                        consolidatedReports.Add(report);
                    }

                    consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();





                }
            }
            catch (Exception ex)
            {
                ConsolidatedBasePriceReports recuirementerror = new ConsolidatedBasePriceReports();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public string GetUserLoginTemplates(string template, string from, string to, int userID, string sessionID)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region Users Login Report

            if (template.ToUpper().Contains("USERS_LOGIN_REPORT"))
            {

                List<ActiveUsers> usersReport = prmservices.GetUsersLoginStatus(userID, from, to, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("USERS_LOGIN_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "USERS LOGIN REPORT";
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "User ID";
                wsSheet1.Cells["B" + lineNumber].Value = "User Name";
                wsSheet1.Cells["C" + lineNumber].Value = "User E-mail";
                wsSheet1.Cells["D" + lineNumber].Value = "User Phone Number";
                wsSheet1.Cells["E" + lineNumber].Value = "User Login Time";


                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;

                wsSheet1.Cells["A:E"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (var item in usersReport)
                {


                    wsSheet1.Cells["A" + lineNumber].Value = item.UserID;
                    wsSheet1.Cells["B" + lineNumber].Value = item.FirstName + ' ' + item.LastName;
                    wsSheet1.Cells["C" + lineNumber].Value = item.Email;
                    wsSheet1.Cells["D" + lineNumber].Value = item.PhoneNum;
                    wsSheet1.Cells["E" + lineNumber].Value = item.DateCreated.ToString();

                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;





                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Users Login Report



            return Convert.ToBase64String(ms.ToArray());
        }

        public string GetLogsTemplates(string template, string from, string to, int companyID, string sessionid)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region Email Logs Report

            if (template.ToUpper().Contains("EMAIL_LOGS_REPORT"))
            {

                List<EmailLogs> emailLogs = prmservices.GetEmailLogs(from, to, companyID, sessionid);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("EMAIL_LOGS_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "EMAIL LOGS REPORT";
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "REQ ID";
                wsSheet1.Cells["B" + lineNumber].Value = "REQ TYPE";
                wsSheet1.Cells["C" + lineNumber].Value = "REQ STATUS";
                wsSheet1.Cells["D" + lineNumber].Value = "MESSAGE TYPE";
                wsSheet1.Cells["E" + lineNumber].Value = "NAME";
                wsSheet1.Cells["F" + lineNumber].Value = "EMAIL";
                wsSheet1.Cells["G" + lineNumber].Value = "ATL.EMAIL";
                wsSheet1.Cells["H" + lineNumber].Value = "SUBJECT";
                wsSheet1.Cells["I" + lineNumber + ":J" + lineNumber].Merge = true;
                wsSheet1.Cells["I" + lineNumber + ":J" + lineNumber].Value = "MESSAGE";
                wsSheet1.Cells["K" + lineNumber].Value = "MESSAGE DATE & TIME";


                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Color.SetColor(Color.White);


                lineNumber++;

                wsSheet1.Cells["A:K"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (var item in emailLogs)
                {
                    if (item.ReqStatus == "UNCONFIRMED")
                    {
                        item.ReqStatus = "Quotations pending";
                    }
                    else if (item.ReqStatus == "NOTSTARTED")
                    {
                        item.ReqStatus = "Negotiation scheduled";
                    }
                    else if (item.ReqStatus == "STARTED")
                    {
                        item.ReqStatus = "Negotiation started";
                    }
                    else if (item.ReqStatus == "Negotiation Ended")
                    {
                        item.ReqStatus = "Negotiation Closed";
                    }
                    else if (item.ReqStatus == "DELETED")
                    {
                        item.ReqStatus = "Negotiation Cancelled";
                    }

                    if (item.Requirement == "VENDOR_REGISTER" || item.Requirement == "COMMUNICATIONS" || item.Requirement == "USER_REGISTER")
                    {

                        item.ReqStatus = "--";
                        item.ReqType = -1;

                    }

                    wsSheet1.Cells["A" + lineNumber].Value = item.ReqID;
                    if (item.ReqType == 0)
                    {
                        wsSheet1.Cells["B" + lineNumber].Value = "PRICE";
                    }
                    // wsSheet1.Cells["B" + lineNumber].Value = item.;
                    wsSheet1.Cells["C" + lineNumber].Value = item.ReqStatus;
                    wsSheet1.Cells["D" + lineNumber].Value = item.Requirement;
                    wsSheet1.Cells["E" + lineNumber].Value = item.FirstName + ' ' + item.LastName;
                    wsSheet1.Cells["F" + lineNumber].Value = item.Email;
                    wsSheet1.Cells["G" + lineNumber].Value = item.AltEmail;
                    wsSheet1.Cells["H" + lineNumber].Value = item.Subject;
                    wsSheet1.Cells["I" + lineNumber + ":J" + lineNumber].Merge = true;
                    wsSheet1.Cells["I" + lineNumber + ":J" + lineNumber].Value = item.Message;
                    item.MessageDate = prmservices.toLocal(item.MessageDate);
                    wsSheet1.Cells["K" + lineNumber].Value = item.MessageDate.ToString();

                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;

                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Email Logs Report


            #region SMS Logs Report

            if (template.ToUpper().Contains("SMS_LOGS_REPORT"))
            {

                List<EmailLogs> emailLogs = prmservices.GetSMSLogs(from, to, companyID, sessionid);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("SMS_LOGS_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "SMS LOGS REPORT";
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "REQ ID";
                wsSheet1.Cells["B" + lineNumber].Value = "REQ TYPE";
                wsSheet1.Cells["C" + lineNumber].Value = "REQ STATUS";
                wsSheet1.Cells["D" + lineNumber].Value = "MESSAGE TYPE";
                wsSheet1.Cells["E" + lineNumber].Value = "NAME";
                wsSheet1.Cells["F" + lineNumber].Value = "EMAIL";
                wsSheet1.Cells["G" + lineNumber].Value = "ALT.EMAIL";
                wsSheet1.Cells["H" + lineNumber].Value = "PHONE NO.";
                wsSheet1.Cells["I" + lineNumber].Value = "ALT.PHONE NO.";
                // wsSheet1.Cells["G" + lineNumber].Value = "SUBJECT";
                wsSheet1.Cells["J" + lineNumber + ":K" + lineNumber].Merge = true;
                wsSheet1.Cells["J" + lineNumber + ":K" + lineNumber].Value = "MESSAGE";
                wsSheet1.Cells["L" + lineNumber].Value = "MESSAGE DATE & TIME";


                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Font.Color.SetColor(Color.White);


                lineNumber++;

                wsSheet1.Cells["A:L"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                var item = emailLogs;

                // foreach (var item in emailLogs)
                for (int i = 0; i < item.Count; i++)
                {
                    if (item[i].ReqStatus == "UNCONFIRMED")
                    {
                        item[i].ReqStatus = "Quotations pending";
                    }
                    else if (item[i].ReqStatus == "NOTSTARTED")
                    {
                        item[i].ReqStatus = "Negotiation scheduled";
                    }
                    else if (item[i].ReqStatus == "STARTED")
                    {
                        item[i].ReqStatus = "Negotiation started";
                    }
                    else if (item[i].ReqStatus == "Negotiation Ended")
                    {
                        item[i].ReqStatus = "Negotiation Closed";
                    }
                    else if (item[i].ReqStatus == "DELETED")
                    {
                        item[i].ReqStatus = "Negotiation Cancelled";
                    }

                    wsSheet1.Cells["A" + lineNumber].Value = item[i].ReqID;
                    if (item[i].ReqType == 0)
                    {
                        wsSheet1.Cells["B" + lineNumber].Value = "PRICE";
                    }

                    wsSheet1.Cells["C" + lineNumber].Value = item[i].ReqStatus;
                    wsSheet1.Cells["D" + lineNumber].Value = item[i].Requirement;
                    wsSheet1.Cells["E" + lineNumber].Value = item[i].FirstName + ' ' + item[i].LastName;
                    wsSheet1.Cells["F" + lineNumber].Value = item[i].Email;
                    wsSheet1.Cells["G" + lineNumber].Value = item[i].AltEmail;
                    wsSheet1.Cells["H" + lineNumber].Value = item[i].PhoneNum;
                    wsSheet1.Cells["I" + lineNumber].Value = item[i].AltPhoneNum;
                    //wsSheet1.Cells["G" + lineNumber].Value = item.Subject;
                    wsSheet1.Cells["J" + lineNumber + ":K" + lineNumber].Merge = true;
                    wsSheet1.Cells["J" + lineNumber + ":K" + lineNumber].Value = item[i].Message;

                    item[i].MessageDate = prmservices.toLocal(item[i].MessageDate);
                    wsSheet1.Cells["L" + lineNumber].Value = item[i].MessageDate.ToString();


                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;

                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion SMS Logs Report


            return Convert.ToBase64String(ms.ToArray());
        }

        public List<AccountingConsolidatedReport> GetAccountingConsolidatedReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<AccountingConsolidatedReport> consolidatedReports = new List<AccountingConsolidatedReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.Now.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.Now.ToString();
                }

                DateTime fromDate = Convert.ToDateTime(from);
                DateTime toDate = Convert.ToDateTime(to);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetAccountingConsolidatedReports", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        AccountingConsolidatedReport report = new AccountingConsolidatedReport();

                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.InvoiceNumber = row["INVOICE_NUMBER"] != DBNull.Value ? Convert.ToString(row["INVOICE_NUMBER"]) : string.Empty;
                        report.ConsigneeName = row["CONSIGNEE_NAME"] != DBNull.Value ? Convert.ToString(row["CONSIGNEE_NAME"]) : string.Empty;
                        report.Destination = row["FINAL_DESTINATION"] != DBNull.Value ? Convert.ToString(row["FINAL_DESTINATION"]) : string.Empty;
                        report.ProductName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        report.ShippingMode = row["MODE_OF_SHIPMENT"] != DBNull.Value ? Convert.ToString(row["MODE_OF_SHIPMENT"]) : string.Empty;
                        report.PortOfLanding = row["PORT_OF_LANDING"] != DBNull.Value ? Convert.ToString(row["PORT_OF_LANDING"]) : string.Empty;
                        report.IsPalletize = row["IS_PALLETIZE"] != DBNull.Value ? Convert.ToInt32(row["IS_PALLETIZE"]) : 0;
                        report.FreightBidAmount = row["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_PRICE"]) : 0;
                        report.NetWeight = row["NET_WEIGHT"] != DBNull.Value ? Convert.ToDouble(row["NET_WEIGHT"]) : 0;
                        report.ServiceCharges = row["SERVICE_CHARGES"] != DBNull.Value ? Convert.ToDecimal(row["SERVICE_CHARGES"]) : 0;
                        report.CustomClearance = row["CUSTOM_CLEARANCE"] != DBNull.Value ? Convert.ToDecimal(row["CUSTOM_CLEARANCE"]) : 0;
                        report.TerminalHandling = row["TERMINAL_HANDLING"] != DBNull.Value ? Convert.ToDecimal(row["TERMINAL_HANDLING"]) : 0;
                        report.DrawbackAmount = row["DRAWBACK_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["DRAWBACK_AMOUNT"]) : 0;
                        report.LoadingUnloadingCharges = row["LOADING_UNLOADING"] != DBNull.Value ? Convert.ToDouble(row["LOADING_UNLOADING"]) : 0;
                        report.WarehouseStorages = row["WAREHOUSE_STORAGES"] != DBNull.Value ? Convert.ToDouble(row["WAREHOUSE_STORAGES"]) : 0;
                        report.AdditionalMiscExp = row["ADDITIONAL_MISC_EXP"] != DBNull.Value ? Convert.ToDouble(row["ADDITIONAL_MISC_EXP"]) : 0;
                        report.NarcoticSubcharges = row["NARCOTIC_SUBCHARGES"] != DBNull.Value ? Convert.ToDouble(row["NARCOTIC_SUBCHARGES"]) : 0;
                        report.palletizeNumber = row["PALLETIZE_NUMBER"] != DBNull.Value ? Convert.ToDouble(row["PALLETIZE_NUMBER"]) : 0;
                        report.palletizeQty = row["PALLETIZE_QTY"] != DBNull.Value ? Convert.ToDouble(row["PALLETIZE_QTY"]) : 0;
                        report.QTY = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
                        if (report.IsPalletize == 1)
                        {
                            report.Pallet = "Palletize";
                        }
                        else if (report.IsPalletize == 0)
                        {
                            report.Pallet = "Non - Palletize";
                        }

                        consolidatedReports.Add(report);
                    }

                    // consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();
                }
            }
            catch (Exception ex)
            {
                AccountingConsolidatedReport recuirementerror = new AccountingConsolidatedReport();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public string GetConsolidatedTemplates(string template, string from, string to, int userID, string sessionID)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region Accounting Consolidated Report

            if (template.ToUpper().Contains("ACCOUNTING_CONSOLIDATED_REPORT"))
            {

                List<AccountingConsolidatedReport> accountingconsolidateReport = GetAccountingConsolidatedReports(sessionID, from, to, userID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("ACCOUNTING_CONSOLIDATED_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "ACCOUNTING CONSOLIDATED REPORT";
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Req ID";
                wsSheet1.Cells["B" + lineNumber].Value = "Invoice No.";
                wsSheet1.Cells["C" + lineNumber].Value = "Posted Date";
                wsSheet1.Cells["D" + lineNumber].Value = "Consignee Name";
                wsSheet1.Cells["E" + lineNumber].Value = "Final Destination";
                wsSheet1.Cells["F" + lineNumber].Value = "Port of Destination";
                wsSheet1.Cells["G" + lineNumber].Value = "Product Name";
                wsSheet1.Cells["H" + lineNumber].Value = "Shipping Mode";
                wsSheet1.Cells["I" + lineNumber].Value = "Division";
                wsSheet1.Cells["J" + lineNumber].Value = "Net Weight";
                wsSheet1.Cells["K" + lineNumber].Value = "Grand Total";

                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;

                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (var item in accountingconsolidateReport)
                {
                    if (item.IsPalletize == 1)
                    {
                        item.Pallet = "Palletize";
                    }
                    else if (item.IsPalletize == 0)
                    {
                        item.Pallet = "Non - Palletize";
                    }

                    wsSheet1.Cells["A" + lineNumber].Value = item.RequirementID;
                    wsSheet1.Cells["B" + lineNumber].Value = item.InvoiceNumber;
                    wsSheet1.Cells["C" + lineNumber].Value = item.ReqPostedOn.ToString();
                    wsSheet1.Cells["D" + lineNumber].Value = item.ConsigneeName;
                    wsSheet1.Cells["E" + lineNumber].Value = item.Destination;
                    wsSheet1.Cells["F" + lineNumber].Value = item.PortOfLanding;
                    wsSheet1.Cells["G" + lineNumber].Value = item.ProductName;
                    wsSheet1.Cells["H" + lineNumber].Value = item.ShippingMode;
                    wsSheet1.Cells["I" + lineNumber].Value = item.Pallet;
                    wsSheet1.Cells["J" + lineNumber].Value = item.NetWeight + " " + item.QTY;

                    if (item.IsPalletize == 1)
                    {
                        item.GrandTotal = (item.FreightBidAmount + Convert.ToDouble(item.ServiceCharges) + Convert.ToDouble(item.CustomClearance) + Convert.ToDouble(item.TerminalHandling) + item.DrawbackAmount + item.LoadingUnloadingCharges +
                            item.WarehouseStorages + item.AdditionalMiscExp + item.NarcoticSubcharges + (item.palletizeQty * item.palletizeNumber));
                    }
                    else if (item.IsPalletize == 0)
                    {
                        item.GrandTotal = (item.FreightBidAmount + Convert.ToDouble(item.ServiceCharges) + Convert.ToDouble(item.CustomClearance) + Convert.ToDouble(item.TerminalHandling) + item.DrawbackAmount + item.LoadingUnloadingCharges +
                            item.WarehouseStorages + item.AdditionalMiscExp + item.NarcoticSubcharges);
                    }

                    wsSheet1.Cells["K" + lineNumber].Value = item.GrandTotal;


                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;





                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Accounting Consolidated Report

            #region Consolidated Report

            if (template.ToUpper().Contains("CONSOLIDATED_REPORT"))
            {

                List<LogisticConsolidatedReport> consolidateReport = GetLogisticConsolidatedReports(sessionID, from, to, userID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("CONSOLIDATED_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "CONSOLIDATED REPORT";
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Req ID";
                wsSheet1.Cells["B" + lineNumber].Value = "Requirement Title";
                wsSheet1.Cells["C" + lineNumber].Value = "Posted Date";
                wsSheet1.Cells["D" + lineNumber].Value = "Invoice Number";
                wsSheet1.Cells["E" + lineNumber].Value = "Invoice Date";
                wsSheet1.Cells["F" + lineNumber].Value = "Consignee Name";
                wsSheet1.Cells["G" + lineNumber].Value = "Destination";
                wsSheet1.Cells["H" + lineNumber].Value = "Product Name";
                wsSheet1.Cells["I" + lineNumber].Value = "Shipping Mode AIR/SEA";
                wsSheet1.Cells["J" + lineNumber].Value = "Quantity";
                wsSheet1.Cells["K" + lineNumber].Value = "Chargeable Wt";
                wsSheet1.Cells["L" + lineNumber].Value = "DG Fee";
                wsSheet1.Cells["M" + lineNumber].Value = "AMS";
                wsSheet1.Cells["N" + lineNumber].Value = "MISC";
                wsSheet1.Cells["O" + lineNumber].Value = "Menzines Charges";
                wsSheet1.Cells["P" + lineNumber].Value = "CHA Charges";
                wsSheet1.Cells["Q" + lineNumber].Value = "Freight/Bid Amount";
                wsSheet1.Cells["R" + lineNumber].Value = "Grand Total";

                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;

                wsSheet1.Cells["A:R"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (var item in consolidateReport)
                {
                    //if (item.IsPalletize == 1)
                    //{
                    //    item.Pallet = "Palletize";
                    //}
                    //else if (item.IsPalletize == 0)
                    //{
                    //    item.Pallet = "Non - Palletize";
                    //}

                    wsSheet1.Cells["A" + lineNumber].Value = item.RequirementID;
                    wsSheet1.Cells["B" + lineNumber].Value = item.Title;
                    wsSheet1.Cells["C" + lineNumber].Value = item.ReqPostedOn.ToString();
                    wsSheet1.Cells["D" + lineNumber].Value = item.InvoiceNumber;
                    wsSheet1.Cells["E" + lineNumber].Value = item.InvoiceDate;
                    wsSheet1.Cells["F" + lineNumber].Value = item.ConsigneeName;
                    wsSheet1.Cells["G" + lineNumber].Value = item.Destination;
                    wsSheet1.Cells["H" + lineNumber].Value = item.ProductName;
                    wsSheet1.Cells["I" + lineNumber].Value = item.ShippingMode;
                    wsSheet1.Cells["J" + lineNumber].Value = item.QTY;
                    wsSheet1.Cells["K" + lineNumber].Value = item.ChargeableWt;
                    wsSheet1.Cells["L" + lineNumber].Value = item.DGFee;
                    wsSheet1.Cells["M" + lineNumber].Value = item.AMS;
                    wsSheet1.Cells["N" + lineNumber].Value = item.MISC;
                    wsSheet1.Cells["O" + lineNumber].Value = item.MenzinesCharges;
                    wsSheet1.Cells["P" + lineNumber].Value = 2500;
                    wsSheet1.Cells["Q" + lineNumber].Value = item.FreightBidAmount;


                    if (item.IsPalletize == 1)
                    {
                        item.GrandTotal = (item.FreightBidAmount + Convert.ToDouble(item.ServiceCharges) + Convert.ToDouble(item.CustomClearance) + Convert.ToDouble(item.TerminalHandling) + item.DrawbackAmount + item.LoadingUnloadingCharges +
                            item.WarehouseStorages + item.AdditionalMiscExp + item.NarcoticSubcharges + (item.palletizeQty * item.palletizeNumber));
                    }
                    else if (item.IsPalletize == 0)
                    {
                        item.GrandTotal = (item.FreightBidAmount + Convert.ToDouble(item.ServiceCharges) + Convert.ToDouble(item.CustomClearance) + Convert.ToDouble(item.TerminalHandling) + item.DrawbackAmount + item.LoadingUnloadingCharges +
                            item.WarehouseStorages + item.AdditionalMiscExp + item.NarcoticSubcharges);
                    }

                    wsSheet1.Cells["R" + lineNumber].Value = item.GrandTotal;


                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;





                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Consolidated Report

            return Convert.ToBase64String(ms.ToArray());
        }


        public List<QCSDetails> UpdateQCSSavings(string dbname, int compid, string sessionid, string type)
        {
            List<QCSDetails> qcs = new List<QCSDetails>();
            if (string.IsNullOrEmpty(dbname))
            {
                throw new Exception("Please Provide DATABASE NAME.");
            }
            
            string query = $"select REQ_JSON,REQ_ID,QCS_ID,SAVINGS,WF_ID from qcs_details WHERE QCS_TYPE = '{type}' and COMP_ID = {compid}";
            var dt = sqlHelper.SelectQuery(query);
            CORE.DataNamesMapper<QCSDetails> mapper = new CORE.DataNamesMapper<QCSDetails>();
            qcs = mapper.Map(dt).ToList();


            foreach (QCSDetails cs in qcs) {
                var ds = sqlHelper.ExecuteQuery($"select catalogue_item_id from requirementitems WHERE REQ_ID = {cs.REQ_ID}");
                if (ds!=null && ds.Tables[0].Rows.Count > 0) {
                    List<string> ids = new List<string>();
                    foreach (DataRow row in ds.Tables[0].Rows) {
                        int productID = row["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["CATALOGUE_ITEM_ID"]) : 0;
                        ids.Add(productID.ToString());
                    }
                    cs.LPP_VALUE = prmservices.GetLPPByProductIds(String.Join(", ",ids), cs.REQ_ID.ToString(), compid, sessionid);
                }
            }
            return qcs;
        }


        public QCSDetails saveQCSSAVINGS(QCSDetails[] qcsdetails, string sessionid) {
            
            foreach (var item in qcsdetails)
            {
                string insertQuery = $@"INSERT INTO TEMP_QCS_SAVINGS (QCS_ID, REQ_ID, TOTAL_PROFIT, TOTAL_LOSS, SAVINGS, SAVINGS_IN_REQUIRED_CURRENCY, TOTAL_PROCUREMENT_VALUE, TOTAL_PROCUREMENT_VALUE_IN_REQUIRED_CURRENCY, APPROVAL_COUNT) VALUES 
                        ({item.QCS_ID}, {item.REQ_ID}, {item.TOTAL_PROFIT}, {item.TOTAL_LOSS}, {item.SAVINGS}, {item.SAVINGS_IN_REQUIRED_CURRENCY}, {item.TOTAL_PROCUREMENT_VALUE}, {item.TOTAL_PROCUREMENT_VALUE_IN_REQUIRED_CURRENCY}, {item.APPROVAL_COUNT}) ";
                sqlHelper.ExecuteQuery(insertQuery);
            }

            return null;

        }
        
        #endregion

        #region Post

        #endregion


        #region Private






        #endregion

        #region QCS

        public List<QCSDetails> GetQCSList(int uid, int reqid, string sessionid)
        {
            List<QCSDetails> details = new List<QCSDetails>();

            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $"SELECT QCS.*, RD.REQ_NUMBER FROM qcs_details QCS INNER JOIN requirementdetails RD ON  RD.REQ_ID = QCS.REQ_ID WHERE QCS.REQ_ID = {reqid} ORDER BY QCS_ID DESC";
                var dt = sqlHelper.SelectQuery(query);
                CORE.DataNamesMapper<QCSDetails> mapper = new CORE.DataNamesMapper<QCSDetails>();
                details = mapper.Map(dt).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;

        }

        public List<QCSDetails> GetQCSIDS(int reqid, string sessionid)
        {
            List<QCSDetails> details = new List<QCSDetails>();

            try
            {
                Utilities.ValidateSession(sessionid);
                string query = string.Format("SELECT * FROM qcs_details WHERE REQ_ID = {0}", reqid);
                var dt = sqlHelper.SelectQuery(query);
                CORE.DataNamesMapper<QCSDetails> mapper = new CORE.DataNamesMapper<QCSDetails>();
                details = mapper.Map(dt).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;

        }

        public QCSDetails GetQCSDetails(int qcsid, string sessionid)
        {
            QCSDetails detail = new QCSDetails();

            try
            {
                Utilities.ValidateSession(sessionid);
                string query = string.Format("SELECT * FROM qcs_details WHERE QCS_ID = {0}", qcsid);
                var dt = sqlHelper.SelectQuery(query);
                CORE.DataNamesMapper<QCSDetails> mapper = new CORE.DataNamesMapper<QCSDetails>();
                detail = mapper.Map(dt).FirstOrDefault();
            }
            catch (Exception ex)
            {
                detail.ErrorMessage = ex.Message;
                logger.Error(ex, "Error in retrieving Data");
            }

            return detail;

        }

        public QCSPRDetails GetQcsPRDetails(int reqID, string sessionid)
        {
            QCSPRDetails detail = new QCSPRDetails();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionid);
               
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_getPRDetailsByReqID", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];

                    detail.REQ_ID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    detail.RFQ_CREATOR = row["RFQ_CREATOR"] != DBNull.Value ? Convert.ToInt32(row["RFQ_CREATOR"]) : 0;
                    detail.REQ_STATUS = row["REQ_STATUS"] != DBNull.Value ? Convert.ToString(row["REQ_STATUS"]) : string.Empty;
                    detail.REQ_CURRENCY = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;

                }
                detail.VENDOR_DETAILS = new List<vendorCurrency>();

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        vendorCurrency vd = new vendorCurrency();
                        vd.U_ID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                        vd.VENDOR_NAME = row2["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row2["VENDOR_NAME"]) : string.Empty;
                        vd.VENDOR_CURRENCY = row2["VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row2["VENDOR_CURRENCY"]) : string.Empty;
                        vd.SELECTED_VENDOR_CURRENCY = row2["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row2["SELECTED_VENDOR_CURRENCY"]) : string.Empty;
                        detail.VENDOR_DETAILS.Add(vd);
                    }

                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];

                    detail.PURCHASE_GROUP = row1["PURCHASE_GROUP"] != DBNull.Value ? Convert.ToString(row1["PURCHASE_GROUP"]) : string.Empty;
                    detail.PR_NUMBER = row1["PR_NUMBER"] != DBNull.Value ? Convert.ToString(row1["PR_NUMBER"]) : string.Empty;
                }

               
            }
            catch (Exception ex)
            {
                detail.ErrorMessage = ex.Message;
                logger.Error(ex, "Error in retrieving Data");
            }

            return detail;

        }

        public Response DeleteQcs(int reqID,int qcsID, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_QCS_ID", qcsID);
                sd.Add("P_IS_ACTIVE", 0);
                DataSet ds = sqlHelper.SelectList("qcs_ActiveDeActiveQcs", sd);
            }
            catch (Exception ex)
            {
                details.ErrorMessage = ex.Message;
                logger.Error("Error in DeleteQcs "+ ex.Message);
            }
            return details;
        }

        public Response ActivateQcs(int reqID, int qcsID, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_QCS_ID", qcsID);
                sd.Add("P_IS_ACTIVE", 1);
                DataSet ds = sqlHelper.SelectList("qcs_ActiveDeActiveQcs", sd);
            }
            catch (Exception ex)
            {
                details.ErrorMessage = ex.Message;
                logger.Error(ex, "Error in ActivateQcs "+ex.Message);
            }
            return details;
        }

        public Response SaveQCSDetails(QCSDetails qcsdetails, string sessionid)
        {

            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                sd.Add("P_QCS_ID", qcsdetails.QCS_ID);
                sd.Add("P_U_ID", qcsdetails.U_ID);
                sd.Add("P_REQ_ID", qcsdetails.REQ_ID);
                sd.Add("P_QCS_CODE", qcsdetails.QCS_CODE);
                sd.Add("P_QCS_TYPE", qcsdetails.QCS_TYPE);
                sd.Add("P_PO_CODE", qcsdetails.PO_CODE);
                sd.Add("P_RECOMMENDATIONS", qcsdetails.RECOMMENDATIONS);
                sd.Add("P_UNIT_CODE", qcsdetails.UNIT_CODE);
                sd.Add("P_WF_ID", qcsdetails.WF_ID);
                sd.Add("P_REQ_JSON", qcsdetails.REQ_JSON);
                sd.Add("P_APPROVER_RANGE", qcsdetails.APPROVER_RANGE);
                sd.Add("P_IS_VALID", 1);
                sd.Add("P_SAVINGS", qcsdetails.SAVINGS);
                sd.Add("P_SAVINGS_IN_REQUIRED_CURRENCY", qcsdetails.SAVINGS_IN_REQUIRED_CURRENCY);
                sd.Add("P_TOTAL_PROCUREMENT_VALUE", qcsdetails.TOTAL_PROCUREMENT_VALUE);
                sd.Add("P_TOTAL_PROCUREMENT_VALUE_IN_REQUIRED_CURRENCY", qcsdetails.TOTAL_PROCUREMENT_VALUE_IN_REQUIRED_CURRENCY);
                sd.Add("P_APPROVERS_COUNT", qcsdetails.APPROVAL_COUNT);
                sd.Add("P_TOTAL_PROFIT", qcsdetails.TOTAL_PROFIT);
                sd.Add("P_TOTAL_LOSS", qcsdetails.TOTAL_LOSS);

                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("qcs_SaveQCSDetails", sd);
                //details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    details.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    details.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    if (qcsdetails.VENDOR_ITEM_ASSIGNMENT.Length > 0 && details.ObjectID > 0) {
                        foreach (var a in qcsdetails.VENDOR_ITEM_ASSIGNMENT) {
                            a.QCS_ID = details.ObjectID;
                        }
                        prmservices.saveQCSVendorAssignments(qcsdetails.VENDOR_ITEM_ASSIGNMENT);
                    }
                }

                if (qcsdetails.WF_ID > 0 && details.ErrorMessage == string.Empty)
                {
                    SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                    sd1.Clear();
                    sd1.Add("P_REQ_ID", qcsdetails.REQ_ID);
                    sd1.Add("P_U_ID", qcsdetails.U_ID);
                    sd1.Add("P_WF_ID", qcsdetails.WF_ID);
                    sd1.Add("P_IS_NEW_QUOTATION", 0);
                    DataSet ds = sqlHelper.SelectList("cp_QCSMarkAsComplete", sd1);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {

                    }
                }
                /*********** This is for calculation of ranks based on including taxes or not ************/
                //string query = string.Format("UPDATE requirementdetails SET REQ_INCLUSIVE_TAX = {0} WHERE REQ_ID = {1};", qcsdetails.IS_TAX_INCLUDED ? 1 : 0, qcsdetails.REQ_ID);
                //sqlHelper.ExecuteNonQuery_IUD(query);
                /*********** This is for calculation of ranks based on including taxes or not ************/

                if (details.ObjectID > 0 && qcsdetails.WF_ID > 0)
                {
                    PRMWFService pRMWF = new PRMWFService();
                    Response res2 = pRMWF.AssignWorkflow(qcsdetails.WF_ID, Convert.ToInt32(details.ObjectID), qcsdetails.U_ID, sessionid, qcsdetails.REQ_TITLE, qcsdetails.REQ_ID, qcsdetails.Attachments);
                }



            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


        public Response GetNewQuotations(QCSDetails qcsdetails, bool isNewQuotation, string selectedVendorIds, string comments, string sessionid)
        {
            Response details = new Response();

            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                int newQuot = 0;
                if (isNewQuotation == true)
                {
                    newQuot = 1;
                }

                sd.Add("P_U_ID", qcsdetails.U_ID);
                sd.Add("P_REQ_ID", qcsdetails.REQ_ID);
                sd.Add("P_WF_ID", qcsdetails.WF_ID);
                sd.Add("P_IS_NEW_QUOTATION", newQuot);
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("cp_QCSMarkAsComplete", sd);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    details.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    details.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }

                //if (!string.IsNullOrEmpty(selectedVendorIds) && string.IsNullOrEmpty(details.ErrorMessage))
                //{
                //    List<int> vendors = selectedVendorIds.Split(',').Select(v => Convert.ToInt32(v)).ToList();
                //    foreach (var vendor in vendors)
                //    {
                //        prmservices.QuatationAprovel(qcsdetails.REQ_ID, qcsdetails.U_ID, vendor, true, comments, sessionid, "quotation", 0);
                //    }
                //}
            }
            catch (Exception ex)
            {
                details.ErrorMessage = ex.Message;
            }


            return details;
        }

        #endregion QCS


        #region start OPEN PR AND OPEN PO

        public List<PRM.Core.Models.Reports.OpenPR> GetOpenPR(int compid, int plant, int purchase, string sessionid, int exclusion)
        {
            List<PRM.Core.Models.Reports.OpenPR> details = new List<PRM.Core.Models.Reports.OpenPR>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_EXCLUDE", exclusion);
                CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPR> mapper = new CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPR>();
                var dataset = sqlHelper.SelectList("sap_GetOpenPRData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                if (plant > 0)
                {
                    details = details.FindAll(d => d.PLANT == plant.ToString());
                }

                if (purchase > 0)
                {
                    details = details.FindAll(d => d.PURCHASING_GROUP == purchase.ToString());
                }


                DateTime today = DateTime.Now;
                foreach (PRM.Core.Models.Reports.OpenPR PR in details)
                {
                    if (Convert.ToDateTime(PR.REQUISITION_DATE) >= today.AddDays(-6) && Convert.ToDateTime(PR.REQUISITION_DATE) <= today)
                    {
                        PR.FILTER_STRING = "0 to 7 Days";
                    }
                    else if (Convert.ToDateTime(PR.REQUISITION_DATE) >= today.AddDays(-13) && Convert.ToDateTime(PR.REQUISITION_DATE) < today.AddDays(-6))
                    {
                        PR.FILTER_STRING = "7 to 14 Days";
                    }
                    else if (Convert.ToDateTime(PR.REQUISITION_DATE) >= today.AddDays(-20) && Convert.ToDateTime(PR.REQUISITION_DATE) < today.AddDays(-13))
                    {
                        PR.FILTER_STRING = "14 to 21 Days";
                    }
                    else if (Convert.ToDateTime(PR.REQUISITION_DATE) >= today.AddDays(-34) && Convert.ToDateTime(PR.REQUISITION_DATE) < today.AddDays(-20))
                    {
                        PR.FILTER_STRING = "21 to 35 Days";
                    }
                    else if (Convert.ToDateTime(PR.REQUISITION_DATE) >= today.AddDays(-59) && Convert.ToDateTime(PR.REQUISITION_DATE) < today.AddDays(-34))
                    {
                        PR.FILTER_STRING = "35 to 60 Days";
                    }
                    else if (Convert.ToDateTime(PR.REQUISITION_DATE) >= today.AddDays(-99) && Convert.ToDateTime(PR.REQUISITION_DATE) < today.AddDays(-59))
                    {
                        PR.FILTER_STRING = "60 to 100 Days";
                    }
                    else if (Convert.ToDateTime(PR.REQUISITION_DATE) < today.AddDays(-99))
                    {
                        PR.FILTER_STRING = "> 100";
                    }

                }



            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }

        public List<KeyValuePair> GetOpenPRPivot(int compid, int plant, int purchase, string sessionid, int exclusion)
        {

            List<KeyValuePair> privotDetails = new List<KeyValuePair>();
            List<PRM.Core.Models.Reports.OpenPR> details = new List<PRM.Core.Models.Reports.OpenPR>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_EXCLUDE", exclusion);
                CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPR> mapper = new CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPR>();
                var dataset = sqlHelper.SelectList("sap_GetOpenPRData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                if (plant > 0)
                {
                    details = details.FindAll(d => d.PLANT == plant.ToString());
                }

                if (purchase > 0)
                {
                    details = details.FindAll(d => d.PURCHASING_GROUP == purchase.ToString());
                }

                DateTime today = DateTime.Now;
                KeyValuePair keyVal1 = new KeyValuePair();
                keyVal1.Key1 = "0 to 7 Days";
                keyVal1.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) >= today.AddDays(-6) && Convert.ToDateTime(d.REQUISITION_DATE) <= today)).Count();
                privotDetails.Add(keyVal1);

                KeyValuePair keyVal2 = new KeyValuePair();
                keyVal2.Key1 = "7 to 14 Days";
                keyVal2.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) >= today.AddDays(-13) && Convert.ToDateTime(d.REQUISITION_DATE) < today.AddDays(-6))).Count();
                privotDetails.Add(keyVal2);

                KeyValuePair keyVal3 = new KeyValuePair();
                keyVal3.Key1 = "14 to 21 Days";
                keyVal3.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) >= today.AddDays(-20) && Convert.ToDateTime(d.REQUISITION_DATE) < today.AddDays(-13))).Count();
                privotDetails.Add(keyVal3);

                KeyValuePair keyVal4 = new KeyValuePair();
                keyVal4.Key1 = "21 to 35 Days";
                keyVal4.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) >= today.AddDays(-34) && Convert.ToDateTime(d.REQUISITION_DATE) < today.AddDays(-20))).Count();
                privotDetails.Add(keyVal4);

                KeyValuePair keyVal5 = new KeyValuePair();
                keyVal5.Key1 = "35 to 60 Days";
                keyVal5.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) >= today.AddDays(-59) && Convert.ToDateTime(d.REQUISITION_DATE) < today.AddDays(-34))).Count();
                privotDetails.Add(keyVal5);

                KeyValuePair keyVal6 = new KeyValuePair();
                keyVal6.Key1 = "60 to 100 Days";
                keyVal6.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) >= today.AddDays(-99) && Convert.ToDateTime(d.REQUISITION_DATE) < today.AddDays(-59))).Count();
                privotDetails.Add(keyVal6);

                KeyValuePair keyVal7 = new KeyValuePair();
                keyVal7.Key1 = "> 100";
                keyVal7.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) < today.AddDays(-99))).Count();
                privotDetails.Add(keyVal7);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return privotDetails;
        }

        public List<PRM.Core.Models.Reports.OpenPO> GetOpenPO(int compid, string pono, int plant, int purchase, string sessionid, int exclude)
        {
            List<PRM.Core.Models.Reports.OpenPO> details = new List<PRM.Core.Models.Reports.OpenPO>();
            List<PRM.Core.Models.Reports.OpenPO> deliveredData = new List<PRM.Core.Models.Reports.OpenPO>();
            int count = 0;
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_PO_NO", pono);
                sd.Add("P_EXCLUDE", exclude);
                CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPO> mapper = new CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPO>();
                var dataset = sqlHelper.SelectList("sap_GetOpenPOData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
                deliveredData = mapper.Map(dataset.Tables[1]).ToList();

                foreach (var detail in details)
                {
                    count++;

                    if (detail.IDEAL_DELIVERY_DATE != null && detail.IDEAL_DELIVERY_DATE.HasValue)
                    {
                        detail.IDEAL_DELIVERY_MONTH = detail.IDEAL_DELIVERY_DATE.Value.ToString("MMMM-yyyy");

                        DateTime CONVERTED_IDEAL_DELIVERY_DATE = getDateFormt(detail.IDEAL_DELIVERY_DATE, "MM/dd/yyyy");

                        DateTime currentDate = getDateFormt(DateTime.Now, "MM/dd/yyyy");

                        if (detail.IMPORT_LOCAL.Equals("INDIA", StringComparison.InvariantCultureIgnoreCase) && CONVERTED_IDEAL_DELIVERY_DATE < currentDate)
                        {
                            detail.STATUS = "Delivery Missed";
                        }
                        else if (detail.IMPORT_LOCAL.Equals("IMPORT", StringComparison.InvariantCultureIgnoreCase) && CONVERTED_IDEAL_DELIVERY_DATE.AddDays(7) < currentDate)
                        {
                            detail.STATUS = "Delivery Missed";
                        }

                        else
                        {
                            if (detail.IMPORT_LOCAL.ToUpper().Equals("INDIA", StringComparison.InvariantCultureIgnoreCase) && currentDate >= CONVERTED_IDEAL_DELIVERY_DATE.AddDays(-15))
                            {
                                detail.STATUS = "Delivery Alert";
                            }
                            else if (detail.IMPORT_LOCAL.ToUpper().Equals("IMPORT", StringComparison.InvariantCultureIgnoreCase) && currentDate >= CONVERTED_IDEAL_DELIVERY_DATE.AddDays(-38))
                            {
                                detail.STATUS = "Delivery Alert";
                            }
                            else
                            {
                                detail.STATUS = "Time Available";
                            }
                        }
                    }
                }

                foreach (var detail1 in deliveredData)
                {
                    if (detail1.IDEAL_DELIVERY_DATE != null && detail1.IDEAL_DELIVERY_DATE.HasValue)
                    {
                        detail1.IDEAL_DELIVERY_MONTH = detail1.IDEAL_DELIVERY_DATE.Value.ToString("MMMM-yyyy");
                    }
                    detail1.STATUS = "Delivered";
                }

                if (plant > 0)
                {
                    details = details.FindAll(d => d.PLANT == plant.ToString());
                    deliveredData = deliveredData.FindAll(d => d.PLANT == plant.ToString());
                }

                if (purchase > 0)
                {
                    details = details.FindAll(d => d.PURCHASING_GROUP == purchase.ToString());
                    deliveredData = deliveredData.FindAll(d => d.PURCHASING_GROUP == purchase.ToString());
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            details = details.OrderBy(d => d.IDEAL_DELIVERY_DATE).ToList();
            deliveredData = deliveredData.OrderBy(d => d.IDEAL_DELIVERY_DATE).ToList();
            details.AddRange(deliveredData);
            return details;
        }

        public List<KeyValuePair> GetOpenPOPivot(int compid, int plant, int purchase, string sessionid)
        {
            List<KeyValuePair> privotDetails = new List<KeyValuePair>();
            List<PRM.Core.Models.Reports.OpenPO> details = new List<PRM.Core.Models.Reports.OpenPO>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_PO_NO", string.Empty);
                CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPO> mapper = new CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPO>();
                var dataset = sqlHelper.SelectList("sap_GetOpenPOData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
                if (plant > 0)
                {
                    details = details.FindAll(d => d.PLANT == plant.ToString());
                }

                if (purchase > 0)
                {
                    details = details.FindAll(d => d.PURCHASING_GROUP == purchase.ToString());
                }

                DateTime today = DateTime.Now;
                KeyValuePair keyVal1 = new KeyValuePair();
                keyVal1.Key1 = "DELIVERY MISSED";
                keyVal1.Value1 = details.FindAll(d => Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) < today).Count();
                privotDetails.Add(keyVal1);

                KeyValuePair keyVal2 = new KeyValuePair();
                keyVal2.Key1 = "WITHIN 0 to 7 Days";
                keyVal2.Value1 = details.FindAll(d => (Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) >= today && Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) < today.AddDays(7))).Count();
                privotDetails.Add(keyVal2);

                KeyValuePair keyVal3 = new KeyValuePair();
                keyVal3.Key1 = "WITHIN 7 to 14 Days";
                keyVal3.Value1 = details.FindAll(d => (Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) >= today.AddDays(7) && Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) < today.AddDays(14))).Count();
                privotDetails.Add(keyVal3);

                KeyValuePair keyVal4 = new KeyValuePair();
                keyVal4.Key1 = "WITHIN 14 to 21 Days";
                keyVal4.Value1 = details.FindAll(d => (Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) >= today.AddDays(14) && Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) < today.AddDays(21))).Count();
                privotDetails.Add(keyVal4);

                KeyValuePair keyVal5 = new KeyValuePair();
                keyVal5.Key1 = "WITHIN 21 to 35 Days";
                keyVal5.Value1 = details.FindAll(d => (Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) >= today.AddDays(21) && Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) < today.AddDays(35))).Count();
                privotDetails.Add(keyVal5);

                KeyValuePair keyVal6 = new KeyValuePair();
                keyVal6.Key1 = "WITHIN 35 to 60 Days";
                keyVal6.Value1 = details.FindAll(d => (Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) >= today.AddDays(35) && Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) < today.AddDays(59))).Count();
                privotDetails.Add(keyVal6);

                KeyValuePair keyVal7 = new KeyValuePair();
                keyVal7.Key1 = "> 60 Days";
                keyVal7.Value1 = details.FindAll(d => (Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) > today.AddDays(60))).Count();
                privotDetails.Add(keyVal7);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return privotDetails;
        }

        public List<Response> GetOpenPOComments(string pono, string itemno, string type, string sessionid)
        {
            List<Response> details = new List<Response>();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                string query = string.Format("SELECT C.*, CONCAT(U_LNAME, ' ', U.U_FNAME) AS FULL_NAME FROM sap_openpo_comments C INNER JOIN [User] U ON U.U_ID = C.CREATED_BY " +
                                             "WHERE PURCHASING_DOCUMENT = '{0}' AND ITEM = {1} AND TYPE = '{2}' AND CATEGORY!= '-' ORDER BY CREATED DESC", pono, itemno, type);
                var ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Response response = new Response();
                        response.Message = row["COMMENTS"] != DBNull.Value ? Convert.ToString(row["COMMENTS"]) : string.Empty;
                        response.ObjectID = row["QUANTITY"] != DBNull.Value ? Convert.ToInt32(row["QUANTITY"]) : 0;
                        response.ErrorMessage = row["CREATED"] != DBNull.Value ? Convert.ToDateTime(row["CREATED"]).ToString("dd-MM-yyyy") : "";
                        response.CurrentTime = row["NEW_DELIVERY_DATE"] != DBNull.Value ? Convert.ToDateTime(row["NEW_DELIVERY_DATE"]) : DateTime.MaxValue;
                        response.RemarksType = row["TYPE"] != DBNull.Value ? Convert.ToString(row["TYPE"]) : string.Empty;
                        response.Category = row["CATEGORY"] != DBNull.Value ? Convert.ToString(row["CATEGORY"]) : string.Empty;
                        response.Status = row["STATUS"] != DBNull.Value ? Convert.ToString(row["STATUS"]) : string.Empty;
                        response.ITEMNO = row["ITEM"] != DBNull.Value ? Convert.ToString(row["ITEM"]) : string.Empty;
                        response.PURCHASEDOC = row["PURCHASING_DOCUMENT"] != DBNull.Value ? Convert.ToString(row["PURCHASING_DOCUMENT"]) : string.Empty;
                        response.UserInfo = new UserInfo();
                        response.UserInfo.FirstName = row["FULL_NAME"] != DBNull.Value ? Convert.ToString(row["FULL_NAME"]) : "";
                        details.Add(response);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }

        public List<Response> GetSapAccess(int userid, string sessionid)
        {
            List<Response> details = new List<Response>();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                string query = string.Format("SELECT * FROM SAP_Access WHERE U_ID = {0}", userid);
                var ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string accessString = row["TEAMS"] != DBNull.Value ? Convert.ToString(row["TEAMS"]) : string.Empty;
                        var accessList = accessString.Split(';');
                        foreach (var access in accessList)
                        {
                            Response response = new Response();
                            response.ErrorMessage = row["ACCESS_TYPE"] != DBNull.Value ? Convert.ToString(row["ACCESS_TYPE"]) : string.Empty;
                            if (!string.IsNullOrEmpty(access))
                            {
                                var val = access.Split(':');
                                response.Message = val[0];
                                response.ObjectID = Convert.ToInt32(val[1]);
                                details.Add(response);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving SAP Access");
            }

            return details;
        }


        public List<Response> GetAVDAccess(int userid, string sessionid)
        {
            List<Response> details = new List<Response>();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                string query = string.Format("SELECT * FROM AVD_Access WHERE U_ID = {0}", userid);
                var ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string accessString = row["TEAMS"] != DBNull.Value ? Convert.ToString(row["TEAMS"]) : string.Empty;
                        var accessList = accessString.Split(';');
                        foreach (var access in accessList)
                        {
                            Response response = new Response();
                            response.ErrorMessage = row["ACCESS_TYPE"] != DBNull.Value ? Convert.ToString(row["ACCESS_TYPE"]) : string.Empty;
                            if (!string.IsNullOrEmpty(access))
                            {
                                var val = access.Split(':');
                                response.Message = val[0];
                                response.ObjectID = Convert.ToInt32(val[1]);
                                details.Add(response);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving AVD Access");
            }

            return details;
        }



        public Response GetLastUpdatedDate(string table, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                string query = string.Format("SELECT DATE_MODIFIED FROM {0} ORDER BY DATE_MODIFIED DESC LIMIT 1", table);
                var ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    details.Message = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]).ToString("dd MMM, yyyy") : string.Empty;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving last table updated date.");
            }

            return details;
        }

        public List<PRM.Core.Models.Reports.OpenPR> GetOpenPRReport(int compid, string sessionid)
        {
            List<PRM.Core.Models.Reports.OpenPR> details = new List<PRM.Core.Models.Reports.OpenPR>();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPR> mapper = new CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPR>();
                string query = @"SELECT C.QUANTITY AS C_QUANTITY, C.CREATED AS COMMENT_CREATED,NEW_DELIVERY_DATE, C.COMMENTS, PLANT, PURCHASING_GROUP, PLANT_NAME, PURCHASER, PURCHASE_REQUISITION, ITEM_OF_REQUISITION,
                    REQUISITION_DATE, MATERIAL, SHORT_TEXT, 
                    IFNULL((select API_NAME From SAPAPIName WHERE HANA_CODE = PR.MATERIAL LIMIT 1),'-') AS API_NAME,
                    IFNULL((select LEAD_TIME From SAPMaterialMaster WHERE CODE = PR.MATERIAL LIMIT 1),'-') AS LEAD_TIME,
                    QUANTITY_REQUESTED, UNIT_OF_MEASURE, DELIVERY_DATE AS DELIV_DATE_FROM_TO,
                    GREATEST(DATE_ADD(REQUISITION_DATE, INTERVAL SAPLeadTime(PR.MATERIAL) DAY), DELIVERY_DATE) AS DELIVERY_DATE,
                    '' REMARKS, QUANTITY_ORDERED
                    FROM 
                    (SELECT * FROM (SELECT * FROM sap_openpo_comments WHERE TYPE = 'PR_SHORTAGE' 
                    ORDER BY PURCHASING_DOCUMENT DESC, ITEM DESC, CREATED DESC) AS C1 
                    GROUP BY PURCHASING_DOCUMENT, ITEM) AS C
                    INNER JOIN  sap_openpr PR ON PR.PURCHASE_REQUISITION = C.PURCHASING_DOCUMENT AND PR.ITEM_OF_REQUISITION = C.ITEM
                    INNER JOIN sapplantlookup p ON PR.PLANT = P.PLANT_CODE
                    INNER JOIN sappurchaserlookup PL ON PR.PURCHASING_GROUP = PL.PURCHASER_GRP;";

                var dataset = sqlHelper.ExecuteQuery(query);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }

        public List<PRM.Core.Models.Reports.OpenPO> GetOpenPOReport(int compid, string sessionid)
        {
            List<PRM.Core.Models.Reports.OpenPO> details = new List<PRM.Core.Models.Reports.OpenPO>();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPO> mapper = new CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPO>();
                string query = @"SELECT C.QUANTITY AS C_QUANTITY,NEW_DELIVERY_DATE, C.COMMENTS, C.CREATED AS COMMENT_CREATED, PO.PLANT, P.PLANT_NAME, PO.PURCHASING_GROUP, PL.PURCHASER, PO.PURCHASING_DOCUMENT, PO.ITEM, 
                concat(PO.PURCHASING_DOCUMENT, PO.ITEM) AS 'CONCAT', DOCUMENT_DATE AS PO_DATE, CONCAT(MONTHNAME(NOW()), '-',YEAR(NOW())) AS PO_MONTH,
                PO.NAME_OF_SUPPLIER, PO.MATERIAL, PO.SHORT_TEXT, API.API_NAME, PO.ORDER_UNIT, PO.ORDER_QUANTITY, PO.STILL_TO_BE_DELIVERED_QTY,
                PR.USER_ID, PR.MULTIPLE_PR_TEXT, PR.PR_NUMBER, PR.PR_ITM_NO, PR.PR_CREATE_DATE, PR.PO_DELV_DATE, 
                IF(IFNULL(PR.VENDORS_COUNTRY, 'India') = 'India', 'India', 'Import') AS IMPORT_LOCAL, MAT.LEAD_TIME, 
                GREATEST(DATE_ADD(IFNULL(PR.PR_CREATE_DATE, DATE('1900-01-01')), INTERVAL SAPLeadTime(PO.MATERIAL) DAY), PR.PR_DELV_DATE) AS IDEAL_DELIVERY_DATE,
                CONCAT(MONTHNAME(GREATEST(DATE_ADD(IFNULL(PR.PR_CREATE_DATE, DATE('1900-01-01')), INTERVAL SAPLeadTime(PO.MATERIAL) DAY), PR.PO_DELV_DATE)), '-', YEAR(GREATEST(DATE_ADD(IFNULL(PR.PR_CREATE_DATE, DATE('1900-01-01')), INTERVAL SAPLeadTime(PO.MATERIAL) DAY), PR.PO_DELV_DATE))) AS DELIVERY_MONTH,
                PR.PR_DELV_DATE
                FROM 
                (SELECT * FROM (SELECT * FROM sap_openpo_comments WHERE TYPE = 'SHORTAGE' 
                ORDER BY PURCHASING_DOCUMENT DESC, ITEM DESC, CREATED DESC) AS C1 
                GROUP BY PURCHASING_DOCUMENT, ITEM) AS C
                INNER JOIN sap_openpo PO ON PO.PURCHASING_DOCUMENT = C.PURCHASING_DOCUMENT AND PO.ITEM = C.ITEM
                LEFT JOIN sapplantlookup p ON PO.PLANT = P.PLANT_CODE
                LEFT JOIN sappurchaserlookup PL ON PO.PURCHASING_GROUP = PL.PURCHASER_GRP
                LEFT JOIN sapapiname API ON API.HANA_CODE = PO.MATERIAL
                LEFT JOIN sapmaterialmaster MAT ON MAT.CODE = PO.MATERIAL
                LEFT JOIN(SELECT* FROM sap_prgrn GROUP BY PO_NO, LN_ITM) PR ON PR.PO_NO = PO.PURCHASING_DOCUMENT AND PR.LN_ITM = PO.ITEM
                WHERE TYPE = 'SHORTAGE'";
                var dataset = sqlHelper.ExecuteQuery(query);
                details = mapper.Map(dataset.Tables[0]).ToList();

                foreach (var detail in details)
                {
                    if (string.IsNullOrEmpty(detail.COMMENTS))
                    {
                        detail.COMMENTS = "";
                    }

                    if (string.IsNullOrEmpty(detail.API_NAME))
                    {
                        detail.API_NAME = "";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }


        public Response SavePOComments(string pono, string itemno, double quantity, string comments, DateTime newdeliverdate, string type,
            int userid, string materialdescription, string apiname, string sitename, int compid, int plant, int purchase,
            string sessionid, PRM.Core.Models.Reports.OpenPR openpr, PRM.Core.Models.Reports.OpenPO openpo, string category, string status)
        {
            string dateFormat = "dd/MM/yyyy";
            Response response = new Response();
            try
            {

                int isValidSession = Utilities.ValidateSession(sessionid);
                string query = string.Format("INSERT INTO sap_openpo_comments (PURCHASING_DOCUMENT, ITEM, QUANTITY, COMMENTS, NEW_DELIVERY_DATE, TYPE, CREATED_BY, CREATED, MODIFIED_BY, MODIFIED,CATEGORY) VALUES " +
                    "('{0}', '{1}', {2}, '{3}', '{4}', '{5}', {6}, NOW(), {6}, NOW(),'{7}');", pono, itemno, quantity, comments, newdeliverdate.ToString("yyyy-MM-dd"), type, userid, category);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                response.ObjectID = 1;
                response.Category = category;
                // response.=status;
                UserInfo user = prmservices.GetUserNew(userid, sessionid);
                //List<OpenPR> openpr = GetOpenPR(compid, plant, purchase, sessionid);

                string body = string.Empty;
                string subject = string.Empty;
                string purchaserCode = string.Empty;

                // Please Donot Clear The Code Unless U Know What You are Doing



                //if (type == "SHORTAGE") {
                //    purchaserCode = openpo.PURCHASER;
                //    subject = "Shortage of " + materialdescription + " for " + apiname + " - Open PO ";
                //    body = prm.GenerateEmailBody("sap_po_shortage_email");
                //    body = String.Format(body, user.FirstName, user.LastName, DateTime.Now, pono, itemno,
                //        Math.Round(quantity, 0), comments, newdeliverdate.ToString(dateFormat), openpo.PLANT_NAME, openpo.PURCHASER,
                //        openpo.PURCHASING_DOCUMENT, openpo.ITEM, openpo.USER_ID, getStringDateFormt(openpo.PO_DATE, dateFormat), openpo.NAME_OF_SUPPLIER,
                //        openpo.MATERIAL, openpo.SHORT_TEXT, openpo.API_NAME, openpo.ORDER_UNIT, getStringRoundoff(openpo.ORDER_QUANTITY),
                //        getStringRoundoff(openpo.STILL_TO_BE_DELIVERED_QTY), openpo.PR_NUMBER, openpo.PR_ITM_NO, getStringDateFormt(openpo.PR_CREATE_DATE, dateFormat), getStringDateFormt(openpo.PR_DELV_DATE, dateFormat), 
                //        openpo.IMPORT_LOCAL, openpo.LEAD_TIME, getStringDateFormt(openpo.IDEAL_DELIVERY_DATE, dateFormat));
                //}
                //else if (type == "DELIVERY")
                //{
                //    purchaserCode = openpo.PURCHASER;
                //    subject = "Delay in Delivery of " + materialdescription + " for " + apiname + " at " + openpo.PLANT_NAME;
                //    body = prm.GenerateEmailBody("sap_po_delivery_email");
                //    body = String.Format(body, user.FirstName, user.LastName, DateTime.Now, pono, itemno,
                //        Math.Round(quantity, 0), comments, newdeliverdate.ToString(dateFormat), openpo.PLANT_NAME, openpo.PURCHASER, 
                //        openpo.PURCHASING_DOCUMENT, openpo.ITEM, openpo.USER_ID, getStringDateFormt(openpo.PO_DATE, dateFormat), openpo.NAME_OF_SUPPLIER, 
                //        openpo.MATERIAL, openpo.SHORT_TEXT, openpo.API_NAME, openpo.ORDER_UNIT, getStringRoundoff(openpo.ORDER_QUANTITY),
                //        getStringRoundoff(openpo.STILL_TO_BE_DELIVERED_QTY), openpo.PR_NUMBER, openpo.PR_ITM_NO, getStringDateFormt(openpo.PR_CREATE_DATE, dateFormat), getStringDateFormt(openpo.PR_DELV_DATE, dateFormat), 
                //        openpo.IMPORT_LOCAL, openpo.LEAD_TIME, getStringDateFormt(openpo.IDEAL_DELIVERY_DATE, dateFormat));
                //}
                //else if (type == "PR_SHORTAGE") {
                //    purchaserCode = openpr.PURCHASER;
                //    subject = "Shortage of " + materialdescription + " for " + apiname + " - Open PR ";
                //    body = prm.GenerateEmailBody("sap_pr_shortage_email");
                //    body = String.Format(body, user.FirstName, user.LastName, DateTime.Now, pono, itemno,
                //        Math.Round(quantity, 0), comments, newdeliverdate.ToString(dateFormat), openpr.PLANT_NAME, openpr.PURCHASER, 
                //        openpr.PURCHASE_REQUISITION, openpr.ITEM_OF_REQUISITION, getStringDateFromString(openpr.REQUISITION_DATE, dateFormat), openpr.MATERIAL, openpr.SHORT_TEXT, 
                //        openpr.API_NAME, openpr.UNIT_OF_MEASURE, getStringRoundoff(openpr.QUANTITY_REQUESTED), getStringRoundoff(openpr.QUANTITY_ORDERED), openpr.LEAD_TIME,
                //        getStringDateFromString(openpr.DELIV_DATE_FROM_TO, dateFormat), getStringDateFromString(openpr.DELIVERY_DATE, dateFormat));
                //}



                // Up to here
                if (category == "DELAY")
                {
                    purchaserCode = openpo.PURCHASER;
                    subject = "Delay in Delivery of " + materialdescription + " for " + apiname + " at " + openpo.PLANT_NAME;
                    body = prmservices.GenerateEmailBody("sap_po_delivery_email");
                    body = String.Format(body, user.FirstName, user.LastName, DateTime.Now, pono, itemno,
                        Math.Round(quantity, 0), comments, newdeliverdate.ToString(dateFormat), openpo.PLANT_NAME, openpo.PURCHASER,
                        openpo.PURCHASING_DOCUMENT, openpo.ITEM, openpo.USER_ID, getStringDateFormt(openpo.PO_DATE, dateFormat), openpo.NAME_OF_SUPPLIER,
                        openpo.MATERIAL, openpo.SHORT_TEXT, openpo.API_NAME, openpo.ORDER_UNIT, getStringRoundoff(openpo.ORDER_QUANTITY),
                        getStringRoundoff(openpo.STILL_TO_BE_DELIVERED_QTY), openpo.PR_NUMBER, openpo.PR_ITM_NO, getStringDateFormt(openpo.PR_CREATE_DATE, dateFormat), getStringDateFormt(openpo.PR_DELV_DATE, dateFormat),
                        openpo.IMPORT_LOCAL, openpo.LEAD_TIME, getStringDateFormt(openpo.IDEAL_DELIVERY_DATE, dateFormat), getDaysFromDates(getStringDateFormt(newdeliverdate, dateFormat), getStringDateFormt(openpo.IDEAL_DELIVERY_DATE, dateFormat)),//getStringDateFormt(openpo.IDEAL_DELIVERY_DATE, dateFormat)
                        category.ToLower());
                }
                else if (category == "SHORTAGE")
                {
                    purchaserCode = openpr.PURCHASER;
                    subject = "Shortage/Availability Issue of " + materialdescription + " for " + apiname + " at " + openpr.PLANT_NAME;
                    body = prmservices.GenerateEmailBody("sap_pr_shortage_email");
                    body = String.Format(body, user.FirstName, user.LastName, DateTime.Now, pono, itemno,
                        Math.Round(quantity, 0), comments, newdeliverdate.ToString(dateFormat), openpr.PLANT_NAME, openpr.PURCHASER,
                        openpr.PURCHASE_REQUISITION, openpr.ITEM_OF_REQUISITION, getStringDateFromString(openpr.REQUISITION_DATE, dateFormat), openpr.MATERIAL, openpr.SHORT_TEXT,
                        openpr.API_NAME, openpr.UNIT_OF_MEASURE, getStringRoundoff(openpr.QUANTITY_REQUESTED), getStringRoundoff(openpr.QUANTITY_ORDERED), openpr.LEAD_TIME,
                        getStringDateFromString(openpr.DELIV_DATE_FROM_TO, dateFormat), getStringDateFromString(openpr.DELIVERY_DATE, dateFormat), category.ToLower() + "/Availability Issue");
                }
                //else if (category == "OTHERS")
                //{
                //    purchaserCode = openpr.PURCHASER;
                //    subject = "Shortage of " + materialdescription + " for " + apiname + " - Open PR ";
                //    body = prm.GenerateEmailBody("sap_pr_shortage_email");
                //    body = String.Format(body, user.FirstName, user.LastName, DateTime.Now, pono, itemno,
                //        Math.Round(quantity, 0), comments, newdeliverdate.ToString(dateFormat), openpr.PLANT_NAME, openpr.PURCHASER,
                //        openpr.PURCHASE_REQUISITION, openpr.ITEM_OF_REQUISITION, getStringDateFromString(openpr.REQUISITION_DATE, dateFormat), openpr.MATERIAL, openpr.SHORT_TEXT,
                //        openpr.API_NAME, openpr.UNIT_OF_MEASURE, getStringRoundoff(openpr.QUANTITY_REQUESTED), getStringRoundoff(openpr.QUANTITY_ORDERED), openpr.LEAD_TIME,
                //        getStringDateFromString(openpr.DELIV_DATE_FROM_TO, dateFormat), getStringDateFromString(openpr.DELIVERY_DATE, dateFormat));
                //}

                string emails = user.Email;
                if (!string.IsNullOrEmpty(purchaserCode))
                {
                    string purchaserCodeEmail = string.Empty;
                    DataSet purchaserDetails = sqlHelper.ExecuteQuery(string.Format("SELECT * FROM sappurchaserlookup WHERE PURCHASER = '{0}'", purchaserCode));
                    if (purchaserDetails != null && purchaserDetails.Tables.Count > 0 && purchaserDetails.Tables[0].Rows.Count > 0)
                    {
                        if (purchaserDetails.Tables[0].Rows[0]["EMAIL"] != null && !string.IsNullOrEmpty(purchaserDetails.Tables[0].Rows[0]["EMAIL"].ToString()))
                        {
                            purchaserCodeEmail = purchaserDetails.Tables[0].Rows[0]["EMAIL"].ToString();
                        }
                    }

                    if (!string.IsNullOrEmpty(emails))
                    {
                        emails = emails + "," + purchaserCodeEmail;
                    }
                    else
                    {
                        emails = purchaserCodeEmail;
                    }
                }

                if (category == "SHORTAGE")
                {
                    string shortageEmails = ConfigurationManager.AppSettings["SHORTAGE_EMAILS"].ToString();
                    emails = string.IsNullOrEmpty(emails) ? shortageEmails : emails + "," + shortageEmails;
                }

                if (category == "DELAY")
                {
                    string shortageEmails = ConfigurationManager.AppSettings["DELAY_EMAILS"].ToString();
                    emails = string.IsNullOrEmpty(emails) ? shortageEmails : emails + "," + shortageEmails;
                }


                emails = emails.Trim(',');
                if (!string.IsNullOrEmpty(emails) && (category == "SHORTAGE" || category == "DELAY"))
                {
                    SendEmail(emails, subject, body, sessionid, null, null).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdatePoStatus(string pono, string itemno, string sessionid, string status, int userid)
        {
            //     string dateFormat = "dd/MM/yyyy";
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {

                int isValidSession = Utilities.ValidateSession(sessionid);

                sd.Add("P_PO_NO", pono);
                sd.Add("P_ITEM_NO", itemno);
                sd.Add("P_STATUS", status);
                sd.Add("P_USER", userid);
                DataSet ds = sqlHelper.SelectList("sap_UpdatePOStatus", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    //  response.ObjectID = 2;
                    //response.ErrorMessage = status;
                    //response.PR_ITM_NO = itemno;
                    //response.PR_NUMBER = pono;
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        // response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                        response.PR_NUMBER = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][0].ToString()) : string.Empty;
                        response.PR_ITM_NO = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                        response.ErrorMessage = ds.Tables[0].Rows[0][2] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][2].ToString()) : string.Empty;

                    }
                }

                //string query = string.Format("SELECT COUNT(*) FROM sap_openpo_comments" +
                //                             " WHERE PURCHASING_DOCUMENT = '{0}' AND ITEM = {1} ORDER BY CREATED DESC limit 1", pono, itemno);
                //var ds = sqlHelper.ExecuteQuery(query);
                //if (ds!=null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{

                //    // foreach (DataRow row in ds.Tables[0].Rows)ds.Tables[0].Rows
                //    //  {
                //    //  LiveBidding detail = ReportUtility.GetLiveBiddingReportObject(row, count);
                //    //details.Add(detail);
                //    //  }
                //    string query1 = string.Format("UPDATE sap_openpo_comments SET STATUS = '{2}',MODIFIED_BY = {3},MODIFIED = {4}" +
                //                             " WHERE PURCHASING_DOCUMENT = '{0}' AND ITEM = '{1}';", pono, itemno, status, userid, "NOW()");
                //    DataSet ds1 = sqlHelper.ExecuteQuery(query1);
                //    response.ObjectID = 1;

                //    response.ErrorMessage = status;
                //    if (ds != null)
                //    {
                //    }
                //    response.PR_ITM_NO = itemno;
                //    response.PR_NUMBER = pono;
                //    //      }

                //} else if (ds.Tables.Count <=0 && ds.Tables[0].Rows.Count <= 0) {

                //    string query2 = string.Format("INSERT INTO sap_openpo_comments (PURCHASING_DOCUMENT, ITEM, QUANTITY, COMMENTS, NEW_DELIVERY_DATE, TYPE, CREATED_BY, CREATED, MODIFIED_BY, MODIFIED,CATEGORY) VALUES " +
                //    "('{0}', '{1}', {2}, '{3}', '{4}', '{5}', {6}, NOW(), {6}, NOW(),'{7}');", pono, itemno,0.0000,"EMPTY COMMENT", "", "", userid, "");
                //    DataSet ds2 = sqlHelper.ExecuteQuery(query2);
                //    response.ObjectID = 2;
                //    response.ErrorMessage = status;
                //    response.PR_ITM_NO = itemno;
                //    response.PR_NUMBER = pono;

                //}

                //string query = string.Format("UPDATE sap_openpo_comments SET STATUS = '{2}',MODIFIED_BY = {3},MODIFIED = {4}" +
                //                             " WHERE PURCHASING_DOCUMENT = '{0}' AND ITEM = '{1}';", pono, itemno, status, userid, "NOW()");
                //DataSet ds = sqlHelper.ExecuteQuery(query);
                //    response.ObjectID = 1;
                //    response.ErrorMessage = status;
                //    response.PR_ITM_NO = itemno;
                //    response.PR_NUMBER = pono;

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public List<PRM.Core.Models.Reports.OpenPO> GetOpenPOShortageReport(int compid, string pono, int plant, int purchase, string sessionid, int exclude)
        {
            List<PRM.Core.Models.Reports.OpenPO> details = new List<PRM.Core.Models.Reports.OpenPO>();
            List<PRM.Core.Models.Reports.OpenPO> filterdetails = new List<PRM.Core.Models.Reports.OpenPO>();
            // int count = 0;
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_PO_NO", pono);
                sd.Add("P_EXCLUDE", 0);
                CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPO> mapper = new CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPO>();
                var dataset = sqlHelper.SelectList("sap_GetOpenPOData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                foreach (var detail in details)
                {
                    //  count++;

                    if (detail.IDEAL_DELIVERY_DATE != null && detail.IDEAL_DELIVERY_DATE.HasValue)
                    {
                        detail.IDEAL_DELIVERY_MONTH = detail.IDEAL_DELIVERY_DATE.Value.ToString("MMMM-yyyy");

                        DateTime CONVERTED_IDEAL_DELIVERY_DATE = getDateFormt(detail.IDEAL_DELIVERY_DATE, "MM/dd/yyyy");

                        DateTime currentDate = getDateFormt(DateTime.Now, "MM/dd/yyyy");

                        if (detail.IMPORT_LOCAL.Equals("INDIA", StringComparison.InvariantCultureIgnoreCase) && CONVERTED_IDEAL_DELIVERY_DATE < currentDate)
                        {
                            detail.STATUS = "Delivery Missed";
                        }
                        else if (detail.IMPORT_LOCAL.Equals("IMPORT", StringComparison.InvariantCultureIgnoreCase) && CONVERTED_IDEAL_DELIVERY_DATE.AddDays(7) < currentDate)
                        {
                            detail.STATUS = "Delivery Missed";
                        }

                        else
                        {
                            if (detail.IMPORT_LOCAL.ToUpper().Equals("INDIA", StringComparison.InvariantCultureIgnoreCase) && currentDate >= CONVERTED_IDEAL_DELIVERY_DATE.AddDays(-15))
                            {
                                detail.STATUS = "Delivery Alert";
                            }
                            else if (detail.IMPORT_LOCAL.ToUpper().Equals("IMPORT", StringComparison.InvariantCultureIgnoreCase) && currentDate >= CONVERTED_IDEAL_DELIVERY_DATE.AddDays(-38))
                            {
                                detail.STATUS = "Delivery Alert";
                            }
                            else
                            {
                                detail.STATUS = "Time Available";
                            }
                        }
                    }

                    if (detail.COMMENTS != "-" && detail.COMMENTS != "" && detail.COMMENTS != null && detail.COMMENTS != "EMPTY COMMENTS")//!= null || detail.COMMENTS != "" || detail.COMMENTS!="-".ToString()
                    {
                        //details.Add(detail);
                        filterdetails.Add(detail);
                    }
                    else
                    {

                    }

                }

                if (plant > 0)
                {
                    details = details.FindAll(d => d.PLANT == plant.ToString());
                }

                if (purchase > 0)
                {
                    details = details.FindAll(d => d.PURCHASING_GROUP == purchase.ToString());
                }



            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            details = details.OrderBy(d => d.IDEAL_DELIVERY_DATE).ToList();
            return filterdetails;
        }

        public List<PRM.Core.Models.Reports.OpenPR> GetOpenPRShortageReport(int compid, int plant, int purchase, string sessionid, int exclusion)
        {
            List<PRM.Core.Models.Reports.OpenPR> details = new List<PRM.Core.Models.Reports.OpenPR>();
            List<PRM.Core.Models.Reports.OpenPR> filterdetails = new List<PRM.Core.Models.Reports.OpenPR>();
            // int count = 0;
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionid);
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_EXCLUDE", exclusion);
                CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPR> mapper = new CORE.DataNamesMapper<PRM.Core.Models.Reports.OpenPR>();
                var dataset = sqlHelper.SelectList("sap_GetOpenPRData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                foreach (var detail in details)
                {
                    //  count++;

                    if (detail.COMMENTS != "-" && !string.IsNullOrEmpty(detail.COMMENTS) && detail.COMMENTS != "EMPTY COMMENTS")//!= null || detail.COMMENTS != "" || detail.COMMENTS!="-".ToString()
                    {
                        //details.Add(detail);
                        filterdetails.Add(detail);
                    }
                    else
                    {

                    }

                }


            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return filterdetails;
        }

        public Response ImportEntity(PRM.Core.Models.ImportEntity entity)
        {
            //throw new Exception("You are in right place.");
            string moreInfo = string.Empty;
            DataTable currentData = new DataTable();
            Response response = new Response();
            string sheetName = string.Empty;

            try
            {

                if (entity.Attachment != null)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        ms.Write(entity.Attachment, 0, entity.Attachment.Length);
                        using (ExcelPackage package = new ExcelPackage(ms))
                        {
                            currentData = Utilities.WorksheetToDataTable(package.Workbook.Worksheets["Open PR"]);
                            sheetName = package.Workbook.Worksheets[1].Name;
                        }
                    }
                }

                #region "OPEN PR"
                if (string.Compare(entity.EntityName, "OPEN PR", StringComparison.InvariantCultureIgnoreCase) == 0 && currentData.Rows.Count > 0)
                {
                    string query = "INSERT INTO `sap_openpr` (`COMP_ID`,`PLANT`,`PURCHASING_GROUP`,`PURCHASE_REQUISITION`,`ITEM_OF_REQUISITION`,`REQUISITION_DATE`," +
                        "`MATERIAL`,`SHORT_TEXT`,`UNIT_OF_MEASURE`, `QUANTITY_REQUESTED`,`TOTAL_VALUE`,`DELIV_DATE_FROM_TO`,`VENDOR`,`PURCHASE_ORDER_DATE`," +
                        "`PURCHASE_ORDER`,`DELIVERY_DATE`,`RELEASE_DATE`,`STATUSCODE`, `DELETION_INDICATOR`,`PURCHASE_ORDER_ITEM`,`SUPPLIER`,`BLOCKING_INDICATOR`," +
                        "`CLOSED`,`QUANTITY_ORDERED`,`GOODS_RECEIPT`) VALUES";
                    string queryValues = string.Empty;
                    int totalColumns = currentData.Columns.Count;
                    foreach (DataRow row in currentData.Rows)
                    {
                        queryValues = queryValues + "(";
                        for (int i = 0; i <= totalColumns - 1; i++)
                        {
                            queryValues = queryValues + "'" + row[0] + "',";
                        }

                        queryValues = queryValues.TrimEnd(',');
                        queryValues = queryValues + "),";

                        query = query + queryValues;
                        queryValues = string.Empty;
                    }

                    query = query.TrimEnd(',');

                    sqlHelper.ExecuteQuery(query);
                }
                #endregion

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message + moreInfo;
            }

            return response;
        }

        public void SendDailyReport(int compid, string type, string sessionid)
        {
            List<PRM.Core.Models.Reports.OpenPO> details = new List<PRM.Core.Models.Reports.OpenPO>();
            //Dictionary<string, string> purchasers = new Dictionary<string, string>();
            List<OwnEntity> purchasers = new List<OwnEntity>();
            try
            {
                string dateFormat = "dd/MM/yyyy";
                string subject = string.Empty;
                string days = string.Empty;
                List<PRM.Core.Models.Reports.OpenPO> list = GetOpenPO(compid, string.Empty, 0, 0, sessionid, 0);
                var dt = sqlHelper.SelectQuery("select * from sappurchaserlookup sapprlp" +
                                               " inner join sap_access sapacc on sapacc.TEAMS like CONCAT('%', sapprlp.PURCHASER, ':', sapprlp.PURCHASER_GRP, '%')" +
                                               " inner join user u on u.U_ID = sapacc.U_ID" +
                                               " where sapacc.TEAMS not like '%ALL%'");
                // var dt = sqlHelper.SelectQuery("select * from sappurchaserlookup");
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        //string purchaser = row["PURCHASER"].ToString();
                        //string email = row["U_EMAIL"].ToString();
                        //purchasers.Add(purchaser, email);
                        OwnEntity oe = new OwnEntity();
                        oe.purchaser = row["PURCHASER"].ToString(); // KEY
                        oe.email = row["U_EMAIL"].ToString(); // VALUE
                        purchasers.Add(oe);

                    }
                }
                if (type.Equals("LOCAL", StringComparison.InvariantCultureIgnoreCase))
                {
                    list = list.FindAll(l => l.IDEAL_DELIVERY_DATE.Value.Date > DateTime.Now.Date && (l.IDEAL_DELIVERY_DATE.Value.Date - DateTime.Now.Date).Days == 15);
                    list = list.FindAll(l => l.IMPORT_LOCAL.Equals("INDIA", StringComparison.InvariantCultureIgnoreCase)).ToList();
                    days = "15";
                    //subject = "Domestic Shipment : <15 days from Ideal Delivery date";
                    subject = "Domestic Shipments due for delivery on Idea Delivery Date";
                }

                if (type.Equals("IMPORT", StringComparison.InvariantCultureIgnoreCase))
                {
                    list = list.FindAll(l => l.IDEAL_DELIVERY_DATE.Value.Date > DateTime.Now.Date && (l.IDEAL_DELIVERY_DATE.Value.Date - DateTime.Now.Date).Days == 45);
                    list = list.FindAll(l => l.IMPORT_LOCAL.Equals("IMPORT", StringComparison.InvariantCultureIgnoreCase)).ToList();
                    days = "45";
                    //subject = "Import Shipment : <45 days from Ideal Delivery Date";
                    subject = "Import Shipments due for delivery on Ideal Delivery Date";
                }


                if (purchasers != null && purchasers.Count > 0)
                {
                    //foreach (KeyValuePair<string, string> entry in purchasers)
                    foreach (OwnEntity oe in purchasers)
                    {
                        var purchaserItems = list.FindAll(l => l.PURCHASER.Equals(oe.purchaser, StringComparison.InvariantCultureIgnoreCase)).ToList();
                        if (purchaserItems != null && purchaserItems.Count > 0)
                        {
                            string emailContent = prmservices.GenerateEmailBody("SapdailyReportemail");
                            string bodyTemplate = prmservices.GenerateEmailBody("SapdailyReport_BODY");
                            string body = string.Empty;
                            emailContent = string.Format(emailContent, type, days);
                            foreach (var openpo in purchaserItems)
                            {
                                body = body + string.Format(bodyTemplate, openpo.PLANT_NAME, openpo.PURCHASER,
                                openpo.PURCHASING_DOCUMENT, openpo.ITEM, openpo.USER_ID, openpo.NAME_OF_SUPPLIER, openpo.MATERIAL, openpo.SHORT_TEXT,
                                openpo.API_NAME, openpo.ORDER_UNIT, getStringRoundoff(openpo.ORDER_QUANTITY),
                                getStringRoundoff(openpo.STILL_TO_BE_DELIVERED_QTY), getStringDateFormt(openpo.PR_CREATE_DATE, dateFormat), getStringDateFormt(openpo.PO_DATE, dateFormat),
                                getStringDateFormt(openpo.PR_DELV_DATE, dateFormat), openpo.IMPORT_LOCAL, openpo.LEAD_TIME, getStringDateFormt(openpo.IDEAL_DELIVERY_DATE, dateFormat));
                            }

                            emailContent = emailContent.Replace("%%BODY%%", body);
                            emailContent = emailContent.Replace("%%BODY%%", body).Replace("USER_NAME.", "");

                            string emails = string.Empty;
                            if (!string.IsNullOrEmpty(oe.email))
                            {
                                dt = sqlHelper.SelectQuery("SELECT UD.U_ID, U.U_EMAIL FROM sappurchaserlookup SL INNER JOIN companydepartments CD ON SL.PURCHASER  = CD.DEPT_CODE INNER JOIN userdepartments UD ON UD.DEPT_ID = CD.DEPT_ID INNER JOIN [User] U ON U.U_ID = UD.U_ID WHERE UD.IS_VALID = 1 AND SL.PURCHASER = '" + oe.email + "'");
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        if (row["U_EMAIL"] != null && !string.IsNullOrEmpty(row["U_EMAIL"].ToString()))
                                        {
                                            emails = emails + row["U_EMAIL"].ToString() + ",";
                                        }
                                    }
                                }

                                emails = emails.Trim(',');
                                emails = emails + "," + oe.email;
                            }

                            emails = emails.Trim(',');
                            if (!string.IsNullOrEmpty(emails))
                            {
                                SendEmail(oe.email, subject, emailContent, null, null, null).ConfigureAwait(false);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }
        }

        private DateTime getDateFormt(DateTime? datetime, string format)
        {

            DateTime response = Convert.ToDateTime(datetime);

            if (datetime != null)
            {
                response = Convert.ToDateTime(Convert.ToDateTime(datetime).ToString(format));
            }

            return response;

        }

        private string getStringDateFormt(DateTime? datetime, string format)
        {

            string response = string.Empty;

            if (datetime != null)
            {
                response = Convert.ToDateTime(datetime).ToString(format);
            }

            return response;

        }

        private string getStringDateFromString(string datetime, string format)
        {

            string response = string.Empty;

            if (datetime != null && datetime != "" && !string.IsNullOrEmpty(datetime))
            {
                response = Convert.ToDateTime(datetime).ToString(format);
            }

            return response;

        }

        private string getStringRoundoff(string number)
        {

            string response = number;

            int val = 0;

            if (number != null && number != "" && !string.IsNullOrEmpty(number))
            {
                if (Int32.TryParse(number, out val))
                {
                    response = Convert.ToInt32(number).ToString();
                }
                else if (number.Contains("."))
                {
                    response = number.Split('.')[0];
                }
            }

            return response;

        }

        private Double getDaysFromDates(string datetime1, string datetime2)
        {

            Double response = 0;

            DateTime d1 = DateTime.ParseExact(datetime1, "dd/MM/yyyy", null);
            DateTime d2 = DateTime.ParseExact(datetime2, "dd/MM/yyyy", null);

            //(d1.Date - d2.Date).Days;

            if (d1 != null && d1 != null)
            {
                response = (d1 - d2).TotalDays;
            }

            return response;

        }


        public async Task SendEmail(string To, string Subject, string Body, string sessionID = null, Attachment attachment = null, List<Attachment> ListAttachment = null, int reqID = 0)
        {

            try
            {
                //logger.Debug("UserName:{0}-------------------------------124124", ListAttachment.Count);
                Communication communication = new Communication();
                communication = Utilities.GetCommunicationData("EMAIL_FROM", sessionID, reqID: reqID);
                if (communication.Company.SendEmails != 0 || string.IsNullOrEmpty(sessionID))
                {

                    Body = Body.Replace("COMPANY_LOGO_ID", communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
                    Body = Body.Replace("COMPANY_NAME", !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : "PRM360");
                    Body = Body.Replace("USER_NAME", !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");
                    Body = Body.Replace("COMPANY_ADDRESS", !string.IsNullOrEmpty(communication.CompanyAddress) ? communication.CompanyAddress : "");
                    Body = Utilities.replaceDomain(Body);
                    Subject = Utilities.replaceDomain(Subject);

                    SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString());
                    System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
                    smtpClient.Credentials = credentials;
                    smtpClient.EnableSsl = false;
                    MailMessage mail = new MailMessage();
                    //mail.From = new MailAddress(GetFromAddress(communication.User.Email), GetFromDisplayName(communication.User.Email));
                    mail.From = new MailAddress("no-reply@prm360.com", "PRM360 Notifications");
                    mail.BodyEncoding = System.Text.Encoding.ASCII;
                    if (attachment != null)
                    {
                        mail.Attachments.Add(attachment);
                    }

                    if (ListAttachment != null)
                    {
                        logger.Debug("UserName:{0}   ---- 2nd log", ListAttachment.Count);
                        foreach (Attachment singleAttachment in ListAttachment)
                        {
                            logger.Debug("{0}   ---- 3rd log", singleAttachment);
                            if (singleAttachment != null)
                            {
                                logger.Debug("{0}   ---- 4th log", singleAttachment);
                                mail.Attachments.Add(singleAttachment);
                                logger.Debug("{0}   ---- 5th log", "ASD");
                            }
                        }
                    }

                    List<string> ToAddresses = To.Split(',').ToList<string>();
                    foreach (string address in ToAddresses)
                    {
                        if (!string.IsNullOrEmpty(address))
                        {
                            mail.To.Add(new MailAddress(address));
                        }
                    }

                    mail.Subject = Subject;
                    mail.IsBodyHtml = true;
                    mail.Body = Body;
                    smtpClient.Send(mail);
                    // await smtpClient.SendMailAsync(mail);
                    //logger.Info("asdasd>>"smtpClient.Send(mail));
                    Response response = new Response();
                    response.ObjectID = 1;

                }
            }
            catch (Exception ex)
            {
                Response response = new Response();
                response.ErrorMessage = ex.Message;
            }
        }

        #endregion OPEN PR AND OPEN PO

        public CArrayKeyValue[] GetCompanySavingStats(int compid, DateTime fromDate, DateTime toDate, string sessionid)
        {
            List<CArrayKeyValue> list = new List<CArrayKeyValue>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_COMP_ID", compid);
            sd.Add("P_FROM_DATE", fromDate);
            sd.Add("P_TO_DATE", toDate);

            DataSet ds = sqlHelper.SelectList("cp_GetCompanySavingStats", sd);
            if (ds != null && ds.Tables.Count >= 0)
            {
                if (ds.Tables.Count > 0)
                {
                    //Category Savings
                    CArrayKeyValue categorySavings = new CArrayKeyValue();
                    categorySavings.Name = "CATEGORY";
                    categorySavings.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in ds.Tables[0].AsEnumerable())
                    {
                        var key = row["CATEGORY"] != DBNull.Value ? Convert.ToString(row["CATEGORY"]) : string.Empty;
                        var savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDecimal(row["SAVINGS"]) : 0;
                        var spend = row["SPEND"] != DBNull.Value ? Convert.ToDecimal(row["SPEND"]) : 0;
                        if (!string.IsNullOrEmpty(key))
                        {
                            categorySavings.ArrayPair.Add(new KeyValuePair()
                            {
                                Key1 = key,
                                DecimalVal = savings,
                                DecimalVal1 = spend
                            });
                        }
                    }

                    list.Add(categorySavings);
                }

                if (ds.Tables.Count > 1)
                {
                    //Monthly Savings
                    CArrayKeyValue categorySavings = new CArrayKeyValue();
                    categorySavings.Name = "MONTHLY";
                    categorySavings.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in ds.Tables[1].AsEnumerable())
                    {
                        var key = row["MONTH_NAME"] != DBNull.Value ? Convert.ToString(row["MONTH_NAME"]) : string.Empty;
                        var year = row["YEAR1"] != DBNull.Value ? Convert.ToString(row["YEAR1"]) : string.Empty;
                        var savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDecimal(row["SAVINGS"]) : 0;
                        var spend = row["SPEND"] != DBNull.Value ? Convert.ToDecimal(row["SPEND"]) : 0;
                        if (!string.IsNullOrEmpty(key))
                        {
                            categorySavings.ArrayPair.Add(new KeyValuePair()
                            {
                                Key1 = key,
                                DecimalVal = savings,
                                DecimalVal1 = spend,
                                Year = year
                            });
                        }
                    }

                    list.Add(categorySavings);
                }

                if (ds.Tables.Count > 2)
                {
                    //Monthly Savings
                    CArrayKeyValue categorySavings = new CArrayKeyValue();
                    categorySavings.Name = "DEPARTMENT";
                    categorySavings.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in ds.Tables[2].AsEnumerable())
                    {
                        var key = row["DEPARTMENT"] != DBNull.Value ? Convert.ToString(row["DEPARTMENT"]) : string.Empty;
                        var savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDecimal(row["SAVINGS"]) : 0;
                        var spend = row["SPEND"] != DBNull.Value ? Convert.ToDecimal(row["SPEND"]) : 0;
                        if (!string.IsNullOrEmpty(key))
                        {
                            categorySavings.ArrayPair.Add(new KeyValuePair()
                            {
                                Key1 = key,
                                DecimalVal = savings,
                                DecimalVal1 = spend
                            });
                        }
                    }

                    list.Add(categorySavings);
                }

                if (ds.Tables.Count > 3)
                {
                    //Monthly Savings
                    CArrayKeyValue categorySavings = new CArrayKeyValue();
                    categorySavings.Name = "PLANT_NAME";
                    categorySavings.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in ds.Tables[3].AsEnumerable())
                    {
                        var key = row["PLANT_NAME"] != DBNull.Value ? Convert.ToString(row["PLANT_NAME"]) : string.Empty;
                        var savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDecimal(row["SAVINGS"]) : 0;
                        var spend = row["SPEND"] != DBNull.Value ? Convert.ToDecimal(row["SPEND"]) : 0;
                        if (!string.IsNullOrEmpty(key))
                        {
                            categorySavings.ArrayPair.Add(new KeyValuePair()
                            {
                                Key1 = key,
                                DecimalVal = savings,
                                DecimalVal1 = spend
                            });
                        }
                    }

                    list.Add(categorySavings);
                }
            }

            return list.ToArray();
        }

        public List<RfqtoQcsReport> GetRfqtoQcsReport(int userid, string sessionid, int compID, string material,
            string companyName, string buyer, string fromdate, string todate, int PageSize = 0, int NumberOfRecords = 0)
        {
            List<RfqtoQcsReport> details = new List<RfqtoQcsReport>();
            List<AwardedVendors> details1 = new List<AwardedVendors>();
            List<AwardedVendors> details2 = new List<AwardedVendors>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userid);
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_MATERIAL", material);
                sd.Add("P_COMP_NAME", companyName);
                sd.Add("P_BUYER", buyer);
                sd.Add("P_PAGE", PageSize);
                sd.Add("P_PAGE_SIZE", NumberOfRecords);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);

                CORE.DataNamesMapper<RfqtoQcsReport> mapper = new CORE.DataNamesMapper<RfqtoQcsReport>();
                CORE.DataNamesMapper<AwardedVendors> mapper1 = new CORE.DataNamesMapper<AwardedVendors>();
                CORE.DataNamesMapper<AwardedVendors> mapper2 = new CORE.DataNamesMapper<AwardedVendors>();
                var dataset = sqlHelper.SelectList("rp_GetRfqQcsApprovalReport", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
                details1 = mapper1.Map(dataset.Tables[1]).ToList();
                details2 = mapper2.Map(dataset.Tables[2]).ToList();

                foreach (RfqtoQcsReport rfq in details)
                {
                    var awardedVendors = details1.Where(a => a.REQ_ID == rfq.REQ_ID && a.QCS_ID == rfq.QCS_ID).ToList();
                    var budgetRows = details2.Where(a => a.REQ_ID == rfq.REQ_ID && a.QCS_ID == rfq.QCS_ID).ToList();
                    
                    if (budgetRows != null && budgetRows.Count > 0) {

                        foreach (var item in awardedVendors) {
                            var budgetAvaialble = budgetRows.Where(a => a.REQ_ID == item.REQ_ID && a.QCS_ID == item.QCS_ID && a.ITEM_ID == item.ITEM_ID).FirstOrDefault();
                            if (budgetAvaialble != null)
                            {
                               item.BUDGET = budgetAvaialble.BUDGET;
                            }
                        }
                    }

                    if (awardedVendors!=null && awardedVendors.Count > 0) {
                        
                        var vendorAssignedLandedCost = from a in awardedVendors
                                                       group a by a.VENDOR_ID into g
                                                       select new 
                                                       { 
                                                           LandedCost = g.Key, 
                                                           ASSIGNED_QTY_LANDED_COST = g.FirstOrDefault().ASSIGNED_QTY_LANDED_COST, 
                                                           AWARDED_VENDORS = g.FirstOrDefault().AWARDED_VENDOR,
                                                           AWARDED_VENDOR_NAME = g.FirstOrDefault().AWARDED_VENDOR_NAME
                                                       };
                        var actualBudget = vendorAssignedLandedCost.Sum(a=>a.ASSIGNED_QTY_LANDED_COST);
                        var budget = awardedVendors.Sum(a => a.QTY_DISTRIBUTED * a.BUDGET);
                        rfq.BUDGET = Math.Round(budget, decimal_round);
                        rfq.ACTUAL_BUDGET = Math.Round(actualBudget, decimal_round);
                        rfq.TOTAL_PROFIT_GAIN = rfq.BUDGET > 0 ? (rfq.BUDGET - rfq.ACTUAL_BUDGET) : 0;
                        rfq.AWARDED_VENDOR = string.Join(",", vendorAssignedLandedCost.Select(a=>a.AWARDED_VENDORS).ToArray());
                        rfq.AWARDED_VENDOR_NAME = string.Join(",", vendorAssignedLandedCost.Select(a => a.AWARDED_VENDOR_NAME).ToArray());
                    }

                    var rfqItemsCount = dataset.Tables[3].Select("REQ_ID = " + Convert.ToInt32(rfq.REQ_ID)).FirstOrDefault();
                    if (rfqItemsCount != null)
                    {
                        var items = rfqItemsCount["ITEMS_COUNT"] != DBNull.Value ? Convert.ToInt32(rfqItemsCount["ITEMS_COUNT"]) : 0;
                        rfq.ITEMS_COUNT = items;
                    }

                    var vendorsCount = dataset.Tables[4].Select("REQ_ID = " + Convert.ToInt32(rfq.REQ_ID)).FirstOrDefault();
                    if (vendorsCount != null)
                    {
                        var totalVendors = vendorsCount["TOTAL_VENDORS"] != DBNull.Value ? Convert.ToInt32(vendorsCount["TOTAL_VENDORS"]) : 0;
                        var totalParticipatedVendors = vendorsCount["TOTAL_PARTICIPATED_VENDORS"] != DBNull.Value ? Convert.ToInt32(vendorsCount["TOTAL_PARTICIPATED_VENDORS"]) : 0;
                        rfq.TOTAL_VENDORS = totalVendors;
                        rfq.TOTAL_PARTICIPATED_VENDORS = totalParticipatedVendors;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }

            return details;
        }

        public List<PRFieldMapping> GetFilterValues(int compID,string reqid, string sessionID, string fromdate, string todate)
        {
            List<PRFieldMapping> details = new List<PRFieldMapping>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_REQ_ID", reqid);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                CORE.DataNamesMapper<PRFieldMapping> mapper = new CORE.DataNamesMapper<PRFieldMapping>();
                var dataset = sqlHelper.SelectList("rp_GetRfqQcsApprovalReportFilterValues", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }

            return details;
        }

    }
}
