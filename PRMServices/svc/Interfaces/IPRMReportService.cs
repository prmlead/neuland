﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMReportService
    {
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getlivebiddingreport?reqid={reqID}&count={count}&sessionid={sessionID}")]
        LiveBidding[] GetLiveBiddingReport(int reqID, int count, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getitemwisereport?reqid={reqID}&sessionid={sessionID}")]
        ItemWiseReport[] GetItemWiseReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "deliverytimelinereport?reqid={reqID}&sessionid={sessionID}")]
        DeliveryTimeLine[] GetDeliveryTimeLineReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "paymenttermsreport?reqid={reqID}&sessionid={sessionID}")]
        DeliveryTimeLine[] GetPaymentTermsReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdetails?reqid={reqID}&sessionid={sessionID}")]
        ReportsRequirement GetReqDetails(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "gettemplates?template={template}&compid={compID}&userid={userID}&reqid={reqID}&from={fromDate}&to={toDate}&templateid={templateid}&sessionid={sessionID}")]
        string GetTemplates(string template, int compID, int userID, int reqID, string fromDate, string toDate, int templateid, string sessionID);

        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateQCSSavings?dbname={dbname}&compid={compid}&sessionid={sessionid}&type={type}")]
        //List<QCSDetails> UpdateQCSSavings(string dbname, int compid, string sessionid, string type);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqreportforexcel?reqid={reqID}&sessionid={sessionID}")]
        ExcelRequirement GetReqReportForExcel(int reqID, string sessionID);

        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getReqItemWiseVendors?reqid={reqID}&sessionid={sessionID}")]
        //MACRequirement GetReqItemWiseVendors(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedreports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<ConsolidatedReport> GetConsolidatedReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getLogisticConsolidatedreports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<LogisticConsolidatedReport> GetLogisticConsolidatedReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAccountingConsolidatedreports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<AccountingConsolidatedReport> GetAccountingConsolidatedReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedtemplates?template={template}&from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        string GetConsolidatedTemplates(string template, string from, string to, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedbasepricereports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<ConsolidatedBasePriceReports> GetConsolidatedBasePriceReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetUserLoginTemplates?template={template}&from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        string GetUserLoginTemplates(string template, string from, string to, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetLogsTemplates?template={template}&from={from}&to={to}&companyID={companyID}&sessionid={sessionID}")]
        string GetLogsTemplates(string template, string from, string to, int companyID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanysavingstats?compid={compid}&fromDate={fromDate}&toDate={toDate}&sessionid={sessionid}")]
        CArrayKeyValue[] GetCompanySavingStats(int compid, DateTime fromDate, DateTime toDate, string sessionid);

        #region QCS

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getqcslist?uid={uid}&reqid={reqid}&sessionid={sessionid}")]
        List<QCSDetails> GetQCSList(int uid, int reqid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetQCSIDS?reqid={reqid}&sessionid={sessionid}")]
        List<QCSDetails> GetQCSIDS(int reqid, string sessionid);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "qcsdetails?qcsid={qcsid}&sessionid={sessionid}")]
        QCSDetails GetQCSDetails(int qcsid, string sessionid);

        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //ResponseFormat = WebMessageFormat.Json,
        //RequestFormat = WebMessageFormat.Json,
        //UriTemplate = "saveQCSSAVINGS")]
        //QCSDetails saveQCSSAVINGS(QCSDetails[] qcsdetails, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetQcsPRDetails?reqID={reqID}&sessionid={sessionid}")]
        QCSPRDetails GetQcsPRDetails(int reqID, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "saveqcsdetails")]
        Response SaveQCSDetails(QCSDetails qcsdetails, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "GetNewQuotations")]
        Response GetNewQuotations(QCSDetails qcsdetails, bool isNewQuotation, string selectedVendorIds, string comments, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "DeleteQcs")]
        Response DeleteQcs(int reqID, int qcsID, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "ActivateQcs")]
        Response ActivateQcs(int reqID, int qcsID, string sessionid);

        #endregion QCS




        #region OPEN PR AND OPEN PO

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openpr?compid={compid}&plant={plant}&purchase={purchase}&sessionid={sessionid}&exclusion={exclusion}")]
        List<PRM.Core.Models.Reports.OpenPR> GetOpenPR(int compid, int plant, int purchase, string sessionid, int exclusion);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openprpivot?compid={compid}&plant={plant}&purchase={purchase}&sessionid={sessionid}&exclusion={exclusion}")]
        List<KeyValuePair> GetOpenPRPivot(int compid, int plant, int purchase, string sessionid, int exclusion);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openpo?compid={compid}&pono={pono}&plant={plant}&purchase={purchase}&sessionid={sessionid}&exclude={exclude}")]
        List<PRM.Core.Models.Reports.OpenPO> GetOpenPO(int compid, string pono, int plant, int purchase, string sessionid, int exclude);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openpopivot?compid={compid}&plant={plant}&purchase={purchase}&sessionid={sessionid}")]
        List<KeyValuePair> GetOpenPOPivot(int compid, int plant, int purchase, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openpocomments?pono={pono}&itemno={itemno}&type={type}&sessionid={sessionid}")]
        List<Response> GetOpenPOComments(string pono, string itemno, string type, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "sapaccess?userid={userid}&sessionid={sessionid}")]
        List<Response> GetSapAccess(int userid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "avdaccess?userid={userid}&sessionid={sessionid}")]
        List<Response> GetAVDAccess(int userid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "lastupdatedate?table={table}&sessionid={sessionid}")]
        Response GetLastUpdatedDate(string table, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openporeport?compid={compid}&sessionid={sessionid}")]
        List<PRM.Core.Models.Reports.OpenPO> GetOpenPOReport(int compid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openprreport?compid={compid}&sessionid={sessionid}")]
        List<PRM.Core.Models.Reports.OpenPR> GetOpenPRReport(int compid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "senddailyreport?compid={compid}&type={type}&sessionid={sessionid}")]
        void SendDailyReport(int compid, string type, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getopenposhortagereport?compid={compid}&pono={pono}&plant={plant}&purchase={purchase}&sessionid={sessionid}&exclude={exclude}")]
        List<PRM.Core.Models.Reports.OpenPO> GetOpenPOShortageReport(int compid, string pono, int plant, int purchase, string sessionid, int exclude);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getopenprshortagereport?compid={compid}&plant={plant}&purchase={purchase}&sessionid={sessionid}&exclusion={exclusion}")]
        List<PRM.Core.Models.Reports.OpenPR> GetOpenPRShortageReport(int compid, int plant, int purchase, string sessionid, int exclusion);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savepocomments")]
        Response SavePOComments(string pono, string itemno, double quantity, string comments,
            DateTime newdeliverdate, string type, int userid, string materialdescription, string apiname, string sitename,
            int compid, int plant, int purchase, string sessionid, PRM.Core.Models.Reports.OpenPR openpr, PRM.Core.Models.Reports.OpenPO openpo, string category, string status);


        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatepostatus")]
        Response UpdatePoStatus(string pono, string itemno, string sessionid, string status, int userid);


        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "importentity")]
        Response ImportEntity(PRM.Core.Models.ImportEntity entity);

        #endregion OPEN PR AND OPEN PO



        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrfqtoqcsreport?userid={userid}&sessionid={sessionid}&compID={compID}&material={material}&companyName={companyName}&buyer={buyer}" +
            "&fromdate={fromdate}&todate={todate}&PageSize={PageSize}&NumberOfRecords={NumberOfRecords}")]
        List<RfqtoQcsReport> GetRfqtoQcsReport(int userid, string sessionid, int compID, string material,
            string companyName, string buyer, string fromdate, string todate, int PageSize = 0, int NumberOfRecords = 0);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getFilterValues?compid={compID}&reqid={reqid}&sessionid={sessionID}&fromdate={fromdate}&todate={todate}")]
        List<PRFieldMapping> GetFilterValues(int compID,string reqid, string sessionID, string fromdate, string todate);

    }
}
