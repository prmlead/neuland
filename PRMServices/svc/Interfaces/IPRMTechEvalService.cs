﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMTechEvalService
    {
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getquestionnaire?evalid={evalID}&loadquestions={GetQuestions}&sessionid={sessionID}")]
        Questionnaire GetQuestionnaire(int evalID, int GetQuestions, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getresponses?evalid={evalID}&userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        List<Answer> GetResponses(int userID, int evalID, int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getevalreport?evalid={evalID}&userid={userID}&sessionid={sessionID}")]
        TechEvalReport GetEvalReport(int userID, int evalID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getquestionnairelist?userid={userID}&evalid={evalID}&sessionid={sessionID}")]
        List<Questionnaire> GetQuestionnaireList(int userID, int evalID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "gettechevalution?evalid={evalID}&reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        List<TechEval> GetTechEvalution(int evalID, int reqID, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorsfortecheval?evalid={evalID}&sessionid={sessionID}")]
        List<VendorDetails> GetVendorsForTechEval(int evalID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "gettechevalforrequirement?reqid={reqID}&loadquestions={GetQuestions}&sessionid={sessionID}")]
        Questionnaire GetTechEvalForRequirement(int reqID, int GetQuestions, string sessionID);


        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveresponse")]
        Response SaveResponse(List<Answer> answers);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savequestion")]
        Response SaveQuestion(Question question);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savequestionnaire")]
        Response SaveQuestionnaire(Questionnaire questionnaire);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "assignquestionnaire")]
        Response AssignQuestionnaire(Questionnaire questionnaire);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendquestionnairetovendors")]
        Response SendQuestionnaireToVendors(int evalID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savetechevaluation")]
        Response SaveTechEvaluation(TechEval techeval);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deletequestion")]
        Response DeleteQuestion(Question question);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "importentity")]
        Response ImportEntity(ImportEntity entity);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savevendorforquestionnaire")]
        Response SaveVendorForQuestionnaire(int evalID, List<VendorDetails> vendors, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "deletequestionnaire")]
        Response DeleteQuestionnaire(int evalID, string sessionID);
    }
}
