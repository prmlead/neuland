﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Data;
using System.Reflection;
using PRMServices.Models.Catalog;

using MySql.Data.MySqlClient;
using System.Web;

namespace PRMServices
{
    
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMCatalogServiceDummy : IPRMCatalogServiceDummy
    {
        #region Attribute calls
        public List<AttributeModel> GetAttribute(int CompanyId, string sessionId)
         {
            List<AttributeModel> Prop = new List<AttributeModel>();
            DataSet ds = CatalogUtility.GetResultSet(sessionId, "cm_getattributes", new List<string>() { "P_COMP_ID " }, new List<object>() { CompanyId });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    AttributeModel property = new AttributeModel();
                    try
                    {
                        CatalogUtility.SetItemFromRow(property, dr);
                    }
                    catch (Exception ex)
                    {
                        property = new AttributeModel();
                        property.ErrorMessage = ex.Message;
                    }
                    Prop.Add(property);

                }
            }
            return Prop;
        }

        public CatalogResponse SaveAttribute(AttributeModel property, string sessionId)
        {
            CatalogResponse catalogResponse = new CatalogResponse();
            
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(CatalogUtility.GetConnectionString()))
                {
                    if (cmd.Connection.State != ConnectionState.Open)
                    {
                        cmd.Connection.Open();
                    }
                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "cm_saveattribute";

                    cmd.Parameters.Add(new MySqlParameter("P_USER_ID", property.U_Id));
                    cmd.Parameters.Add(new MySqlParameter("P_COMP_ID", property.CompanyId));
                    cmd.Parameters.Add(new MySqlParameter("P_ATTR_ID", property.AttrId));
                    cmd.Parameters.Add(new MySqlParameter("P_PROP_NAME", property.AttrName));
                    cmd.Parameters.Add(new MySqlParameter("P_PROP_DESC", property.AttrDesc));
                    cmd.Parameters.Add(new MySqlParameter("P_PROP_DEFAULT_VALUE", property.AttrDefaultValue));
                    cmd.Parameters.Add(new MySqlParameter("P_PROP_DATATYPE", property.AttrDataType));
                    cmd.Parameters.Add(new MySqlParameter("(1024),P_PROP_OPTIONS", property.AttrOptions));
                    cmd.Parameters.Add(new MySqlParameter("P_ISVALID", property.IsValid));

                    MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    myDA.Fill(ds); 

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    }
                    catalogResponse.SessionID = sessionId;

                    cmd.Connection.Close();
                }
            }
            

            return catalogResponse;
        }

        #endregion Attribute calls



    }
}
