﻿using PRMServices.Models;
using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel.Activation;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMCommunication : IPRMCommunication
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        private PRMServices prmServices = new PRMServices();

        public List<UserDetails> GetCatalogueVendors(string catalogue, string sessionID)
        {
            List<UserDetails> ListUser = new List<UserDetails>();
            try
            {
                string query = @"SELECT U.U_ID, V.U_FNAME, V.U_LNAME, V.U_PHONE, V.U_EMAIL, 
                V.COMP_NAME AS COMPANY_NAME, UD.V_CATEGORY,
	            V.IS_VALID, V.REG_STATUS, V.REG_REASON, V.REG_COMMENTS, V.ADDITIONAL_INFO,
	            V.REG_SCORE, UD.MY_CATALOG_FILE
                FROM [user] U 
                INNER JOIN company C ON U.U_ID = C.CREATED_BY
                INNER JOIN userdata UD ON U.U_ID  = UD. U_ID
                INNER JOIN (SELECT distinct V.* FROM cm_vendorcategory VC INNER JOIN Vendors V ON V.U_ID = VC.VendorId AND V.IS_PRIMARY = 1 AND VC.CategoryId IN ({0})) V ON V.U_ID = U.U_ID
                INNER JOIN userroles UR ON U.U_ID = UR.U_ID
                WHERE
                UD.IS_VALID = 1 AND V.COMP_NAME != 'PRICE_CAP' AND UR.U_TYPE = 'VENDOR' ORDER BY U.DATE_CREATED desc";
                query = string.Format(query, catalogue);
                int isValidSession = Utilities.ValidateSession(sessionID);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        UserDetails User = new UserDetails();
                        User.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : -1;
                        User.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        User.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                        User.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        User.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        User.Category = row["V_CATEGORY"] != DBNull.Value ? Convert.ToString(row["V_CATEGORY"]) : string.Empty;
                        User.IsValid = row["IS_VALID"] != DBNull.Value ? (Convert.ToInt32(row["IS_VALID"]) == 1 ? true : false) : false;
                        User.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        ListUser.Add(User);
                    }
                }
            }
            catch (Exception ex)
            {
                UserDetails User = new UserDetails();
                User.ErrorMessage = ex.Message;
                ListUser.Add(User);
            }

            return ListUser;
        }

        public Response SendCommunication(List<UserDetails> vendors, string subject, string body, bool sendSMS, bool sendEmail, string sessionID)
        {
            Response response = new Response();

            try
            {
                if(vendors!=null && vendors.Count > 0 && (sendSMS || sendEmail))
                {
                    foreach (var vendor in vendors)
                    {
                        try
                        {
                            if (sendSMS)
                            {
                                string tempBody = body;
                                tempBody = tempBody.Replace("<br />", Environment.NewLine);
                                string sms = subject + Environment.NewLine + tempBody;
                                Utilities.SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum, sms, 0, vendor.UserID, "COMMUNICATIONS", null).ConfigureAwait(false);
                            }

                            if (sendEmail)
                            {
                                Utilities.SendEmail(vendor.Email, subject, body, 0, vendor.UserID, "COMMUNICATIONS", sessionID).ConfigureAwait(false);
                            }

                            response.ObjectID++;
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }                
            }
            catch(Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
    }
}