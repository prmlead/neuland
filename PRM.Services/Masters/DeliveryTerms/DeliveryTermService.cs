﻿using Dapper;
using PRM.Core.Domain.Acl;
using PRM.Core.Domain.Masters.DeliveryTerms;
using PRM.Core.Pagers;
using PRM.Data;
using PRM.Services.Acl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Services.Masters.DeliveryTerms
{
   public class DeliveryTermService  : IDeliveryTermService
    {
        public DeliveryTermService(IRepository<DeliveryTerm> deliveryTermRep,
      IDbProvider dbProvider, 
      IAclMappingService aclMappingService)
        {
            _deliveryTermRep = deliveryTermRep;
            _dbProvider = dbProvider;
            _aclMappingService = aclMappingService;

        }

        private readonly IRepository<DeliveryTerm> _deliveryTermRep;
        private readonly IDbProvider _dbProvider;
        private readonly IAclMappingService _aclMappingService;

        public void DeleteDeliveryTerm(DeliveryTerm entity)
        {
            _deliveryTermRep.Delete(entity);
        }

        public DeliveryTerm GetDeliveryTermById(int id)
        {

            var entity = _deliveryTermRep.GetById(id);

            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "DeliveryTerm", EntityId = entity.Id });
            if (entity.LimitedToDepartment)
                entity.AllowedDepartments = mapper.Where(e => e.Type == (int)AclMappingType.Department).Select(e => e.Value).ToArray();

            if (entity.LimitedToProject)
                entity.AllowedProjects = mapper.Where(e => e.Type == (int)AclMappingType.Project).Select(e => e.Value).ToArray();

            if (entity.LimitedToUser)
                entity.AllowedUsers = mapper.Where(e => e.Type == (int)AclMappingType.User).Select(e => e.Value).ToArray();

            return entity;
        }

        public IPagedList<DeliveryTerm> GetDeliveryTermList(Pager pager,int companyId)
        {
            using (var con = _dbProvider.Connection())
            {
                var query = "SELECT SQL_CALC_FOUND_ROWS  * FROM master_deliveryterm";

                List<String> WhereColumns = new List<string>();

                WhereColumns.Add(string.Format(" Company_Id = {0} ", companyId));
                WhereColumns.Add(string.Format(" Deleted = {0}", 0));

                if (!string.IsNullOrEmpty(pager.Criteria.SearchTerm))
                {
                    WhereColumns.Add(string.Format(" Tttle Like %{0}%", pager.Criteria.SearchTerm));
                    WhereColumns.Add(string.Format(" Terms Like %{0}%", pager.Criteria.SearchTerm));


                }

                if (pager.Criteria.Filter.Any())
                {

                }

                if (WhereColumns.Any())
                    query = query + string.Format(" WHERE {0}", string.Join(" AND ", WhereColumns));

                query = query + " ORDER BY CreatedOnUtc DESC ";

                if (pager.PageIndex >= 0 && pager.PageSize > 0)
                {
                    query = query + string.Format(" LIMIT {0},{1} ", pager.PageIndex * pager.PageSize, pager.PageSize);
                }

                query += " ; SELECT FOUND_ROWS()";

                var multi = con.QueryMultiple(query);

                var lst = multi.Read<DeliveryTerm>().ToList();
                int total = multi.Read<int>().SingleOrDefault();

                return new PagedList<DeliveryTerm>(lst, pager.PageIndex, pager.PageSize, total);
            }
        }

        public IList<DeliveryTerm> GetDeliveryTermList(object filter)
        {
            return _deliveryTermRep.GetList(filter).ToList();
        }

        public void InsertDeliveryTerm(DeliveryTerm entity)
        {
          var id =  _deliveryTermRep.Insert(entity);

            entity.Id = id;
            InsertMapping(entity);
        }

        public void UpdateDeliveryTerm(DeliveryTerm entity)
        {
            _deliveryTermRep.Update(entity);

            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "DeliveryTerm", EntityId = entity.Id });

            foreach (var map in mapper)
                _aclMappingService.DeleteAclMapping(map);

            InsertMapping(entity);
        }


        private void InsertMapping(DeliveryTerm entity)
        {
            if (entity.LimitedToDepartment)
            {
                _aclMappingService.InsertDepartmentMapping(entity.Company_Id, "DeliveryTerm", entity.Id, entity.AllowedDepartments);
            }

            if (entity.LimitedToProject)
            {
                _aclMappingService.InsertProjectMapping(entity.Company_Id, "DeliveryTerm", entity.Id, entity.AllowedProjects);

            }

            if (entity.LimitedToUser)
            {
                _aclMappingService.InsertUserMapping(entity.Company_Id, "DeliveryTerm", entity.Id, entity.AllowedUsers);

            }
        }
    }
}
