﻿using PRM.Core.Domain.Requirments;
using PRM.Core.Pagers;
using System.Collections.Generic;

namespace PRM.Services.Requirements
{
    public interface IRequirementTemplateService
    {
        void InsertRequirementTemplate(RequirementTemplate entity);

        void UpdateRequirementTemplate(RequirementTemplate entity);

        void DeleteRequirementTemplate(RequirementTemplate entity);

        RequirementTemplate GetRequirementTemplateById(int id);

        IList<RequirementTemplate> GetRequirementTemplateList(object filter);

        IPagedList<RequirementTemplate> GetRequirementTemplateList(Pager pager, int companyId);

    }
}