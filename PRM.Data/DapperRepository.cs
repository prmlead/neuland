﻿using Dapper;
using PRM.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Data
{
   public class DapperRepository<T>  : IRepository<T> where T : class
    {

        public DapperRepository(IDbProvider dbProvider)
        {
            _dbProvider = dbProvider;
        }

        private readonly IDbProvider _dbProvider;

        public int Insert(T entity)
        {
            using (var con = _dbProvider.Connection())
            using(var trans = con.BeginTransaction())
            {
                try
                {
                   var id = con.Insert(entity, trans);
                    trans.Commit();
                    return Convert.ToInt32(id);
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    return 0;
                }
            }
        }

        public void Update(T entity)
        {
            using (var con = _dbProvider.Connection())
            using (var trans = con.BeginTransaction())
            {
                try
                {
                    con.Update(entity, trans);
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                }
            }
        }

        public void Delete(T entity)
        {
            using (var con = _dbProvider.Connection())
            using (var trans = con.BeginTransaction())
            {
                try
                {
                    con.Delete(entity, trans);
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                }
            }
        }

        public IEnumerable<T> GetAll()
        {
            using (var con = _dbProvider.Connection())
            {
                try
                {
                   return con.GetList<T>();
                }
                catch (Exception ex)
                {
                    return new List<T>();
                }
            }
        }

        public IEnumerable<T> GetList(object filter)
        {
            using (var con = _dbProvider.Connection())
            {
                try
                {
                    return con.GetList<T>(filter);
                }
                catch (Exception ex)
                {
                    return new List<T>();
                }
            }
        }

        public T GetById(int Id)
        {
            using (var con = _dbProvider.Connection())
            {
                try
                {
                    return con.Get<T>(Id);
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public void InsertMany(IList<T> lstEntity)
        {
            using (var con = _dbProvider.Connection())
            using (var trans = con.BeginTransaction())
            {
                try
                {
                    lstEntity.ToList().ForEach(e =>
                    {
                       con.Insert(e, trans);
                    });
                    trans.Commit();

                }
                catch (Exception ex)
                {
                    trans.Rollback();
                }
            }
        }
    }
}
