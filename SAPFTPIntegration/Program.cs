﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Configuration;
using System.Data;
using PRMServices.SQLHelper;
using System.Data.SqlClient;

namespace SAPFTPIntegration
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static char csvDelimiter = ';';
        private static string _archiveFolder = ConfigurationManager.AppSettings["ArchiveFolder"].ToString();
        private static string _errorFolder = ConfigurationManager.AppSettings["ErrorFolder"].ToString();
        private static string _stageFolder = ConfigurationManager.AppSettings["StageFolder"].ToString();
        private static string _processFolder = ConfigurationManager.AppSettings["ProcessFolder"].ToString();
        private static string SAPFTPPRFilePattern = "PRM_SAP_PR";
        private static string SAPFTPVEndorFilePattern = "PRM_SAP_VEND";

        static void Main(string[] args)
        {
            string jobType = ConfigurationManager.AppSettings["JOB_TYPE"].ToString();
            logger.Info("JOB_TYPE:" + jobType);

            NetworkCredential credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserId"], ConfigurationManager.AppSettings["UserPwd"]);
            string url = ConfigurationManager.AppSettings["Host"];

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "PR_JOB")
            {
                DownloadFtpDirectory(url + "/PR_Data/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPPRFilePattern);
                ProcessPRFiles();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "VENDOR_JOB")
            {
                DownloadFtpDirectory(url + "/PR_Data/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPVEndorFilePattern);
                ProcessVendorFiles();
            }
        }

        private static void ProcessPRFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPPRFilePattern))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.ToLower().Replace(".xls", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.ToLower().Replace(".xls", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.ToLower().Replace(".xls", ".csv");

                        try
                        {
                            List<string> columns = "PLANT,PLANT_CODE,PURCHASE_GROUP_CODE,PURCHASE_GROUP_NAME,REQUESITION_DATE,PR_RELEASE_DATE,PR_CHANGE_DATE,PR_NUMBER,REQUISITIONER_NAME,REQUISITIONER_EMAIL,ITEM_OF_REQUESITION,MATERIAL_GROUP_CODE,MATERIAL_GROUP_DESC,MATERIAL_CODE,MATERIAL_TYPE,MATERIAL_HSN_CODE,MATERIAL_DESCRIPTION,SHORT_TEXT,ITEM_TEXT,UOM,QTY_REQUIRED,MATERIAL_DELIVERY_DATE,LEAD_TIME,PR_TYPE,PR_TYPE_DESC,PR_NOTE,LOEKZ,ERDAT,OTHER_INFO1".Split(',').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"PR records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    string query = @"INSERT INTO [dbo].[SAP_PR_DETAILS] ([JOB_ID], [PLANT], [PLANT_CODE], [PURCHASE_GROUP_CODE], [PURCHASE_GROUP_NAME], [REQUESITION_DATE], [PR_RELEASE_DATE], 
                                    [PR_CHANGE_DATE], [PR_NUMBER], [REQUISITIONER_NAME], [REQUISITIONER_EMAIL], [ITEM_OF_REQUESITION], [MATERIAL_GROUP_CODE], [MATERIAL_GROUP_DESC], [MATERIAL_CODE], [MATERIAL_TYPE], 
                                    [MATERIAL_HSN_CODE], [MATERIAL_DESCRIPTION], [SHORT_TEXT], [ITEM_TEXT], [UOM], [QTY_REQUIRED], [MATERIAL_DELIVERY_DATE], [LEAD_TIME], [PR_TYPE], [PR_TYPE_DESC], [PR_NOTE], 
                                    [LOEKZ], [ERDAT], [DATE_CREATED], [OTHER_INFO1]) VALUES ";
                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;
                                        newRow["PLANT"] = GetDataRowVaue(row, "PLANT");
                                        newRow["PLANT_CODE"] = GetDataRowVaue(row, "PLANT_NAME");
                                        newRow["PURCHASE_GROUP_CODE"] = GetDataRowVaue(row, "PURCHASE_GROUP");
                                        newRow["PURCHASE_GROUP_NAME"] = GetDataRowVaue(row, "PURCHASE_GROUP_NAME");
                                        newRow["REQUESITION_DATE"] = GetDataRowVaue(row, "REQUESITION_DATE");
                                        newRow["PR_RELEASE_DATE"] = GetDataRowVaue(row, "PR_RELEASED_DATE");
                                        newRow["PR_CHANGE_DATE"] = GetDataRowVaue(row, "PR_CHANGE_DATE");
                                        newRow["PR_NUMBER"] = GetDataRowVaue(row, "PR_NUMBER");
                                        newRow["REQUISITIONER_NAME"] = GetDataRowVaue(row, "REQUISITIONER_NAME");
                                        newRow["REQUISITIONER_EMAIL"] = GetDataRowVaue(row, "REQUISITIONER_EMAIL");
                                        newRow["ITEM_OF_REQUESITION"] = GetDataRowVaue(row, "REQUISITION_ITEM");
                                        newRow["MATERIAL_GROUP_CODE"] = GetDataRowVaue(row, "MATERIAL_GROUP_CODE");
                                        newRow["MATERIAL_GROUP_DESC"] = GetDataRowVaue(row, "MATERIAL_GROUP");
                                        newRow["MATERIAL_CODE"] = GetDataRowVaue(row, "MATERIAL_CODE");
                                        newRow["MATERIAL_TYPE"] = GetDataRowVaue(row, "MATERIAL_TYPE");
                                        newRow["MATERIAL_HSN_CODE"] = GetDataRowVaue(row, "HSN_CODE");
                                        newRow["MATERIAL_DESCRIPTION"] = GetDataRowVaue(row, "MATERIAL_DESCRIPTION");
                                        newRow["SHORT_TEXT"] = GetDataRowVaue(row, "SHORT_TEXT");
                                        newRow["ITEM_TEXT"] = GetDataRowVaue(row, "ITEM_TEXT");
                                        newRow["UOM"] = GetDataRowVaue(row, "UOM");
                                        newRow["QTY_REQUIRED"] = GetDataRowVaue(row, "QTY_REQUIRED")?.Replace(",", "");
                                        newRow["MATERIAL_DELIVERY_DATE"] = GetDataRowVaue(row, "MATERIAL_DELIVERY_DATE");
                                        newRow["LEAD_TIME"] = GetDataRowVaue(row, "LEAD_TIME");
                                        newRow["PR_TYPE"] = GetDataRowVaue(row, "PR_TYPE");
                                        newRow["PR_TYPE_DESC"] = GetDataRowVaue(row, "PR_DESC");
                                        newRow["PR_NOTE"] = GetDataRowVaue(row, "DELIVERY_LOCATION");
                                        newRow["LOEKZ"] = GetDataRowVaue(row, "DELETION_IND");
                                        newRow["ERDAT"] = GetDataRowVaue(row, "PR_DATE");
                                        newRow["OTHER_INFO1"] = fileName;
                                        newRow["DATE_CREATED"] = DateTime.UtcNow;

                                        query += $@"('{guid}',";
                                        query += $@"'{GetDataRowVaue(row, "PLANT")}',";
                                        query += $@"'{GetDataRowVaue(row, "PLANT_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "PURCHASE_GROUP")}',";
                                        query += $@"'{GetDataRowVaue(row, "PURCHASE_GROUP_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "REQUESITION_DATE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_RELEASED_DATE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_CHANGE_DATE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_NUMBER")}',";
                                        query += $@"'{GetDataRowVaue(row, "REQUISITIONER_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "REQUISITIONER_EMAIL")}',";
                                        query += $@"'{GetDataRowVaue(row, "REQUISITION_ITEM")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_GROUP_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_GROUP")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_TYPE")}',";
                                        query += $@"'{GetDataRowVaue(row, "HSN_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_DESCRIPTION")}',";
                                        query += $@"'{GetDataRowVaue(row, "SHORT_TEXT")}',";
                                        query += $@"'{GetDataRowVaue(row, "ITEM_TEXT")}',";
                                        query += $@"'{GetDataRowVaue(row, "UOM")}',";
                                        query += $@"'{GetDataRowVaue(row, "QTY_REQUIRED").Replace(",", "")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_DELIVERY_DATE")}',";
                                        query += $@"'{GetDataRowVaue(row, "LEAD_TIME")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_TYPE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_DESC")}',";
                                        query += $@"'{GetDataRowVaue(row, "DELIVERY_LOCATION")}',";
                                        query += $@"'{GetDataRowVaue(row, "DELETION_IND")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_DATE")}',";
                                        query += $@"GETUTCDATE(),";
                                        query += $@"'{fileName}'),";

                                        tempDT.Rows.Add(newRow);
                                    }

                                    query = query.TrimEnd(',');
                                    //bizClass.ExecuteNonQuery_IUD(query);
                                    logger.Debug("END GetPRDetails QUERY: " + query);
                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_PR_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_pr_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }
        private static void ProcessVendorFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPVEndorFilePattern))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv");

                        try
                        {
                            List<string> columns = "NAMEV,NAME1,SMTP_ADDR,TELF1,NAME2,WAERS,LIFNR,MATNR,STCD3,VEN_ALT_FIR,VEN_ALT_LAS,SMTP_ADDR1,TEL_NUMBER,CITY1,ERDAT,UTIME,TYPE_CODE,TYPE_DESC,BANK_ACCOUNT_NO,IFSC_CODE,BANK_NAME,PAN_NUMBER,OTHER_INFO1".Split(',').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"Vendor records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    string query = @"INSERT INTO [dbo].[SAP_VENDOR_DETAILS] ([JOB_ID], [NAMEV], [NAME1], [SMTP_ADDR], [TELF1], [NAME2], [WAERS], [LIFNR], [MATNR], [STCD3], 
                                    [VEN_ALT_FIR], [VEN_ALT_LAS], [SMTP_ADDR1], [TEL_NUMBER], [CITY1], [ERDAT], [UTIME], 
                                    TYPE_CODE,  [TYPE_DESC], BANK_ACCOUNT_NO,IFSC_CODE, BANK_NAME,PAN_NUMBER,
                                    [OTHER_INFO1], [DATE_CREATED]) VALUES ";

                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;
                                        newRow["NAMEV"] = GetDataRowVaue(row, "FIRST_NAME");
                                        newRow["NAME1"] = GetDataRowVaue(row, "LAST_NAME");
                                        newRow["SMTP_ADDR"] = GetDataRowVaue(row, "EMAIL");
                                        newRow["TELF1"] = GetDataRowVaue(row, "PHONE");
                                        newRow["NAME2"] = GetDataRowVaue(row, "COMPANY_NAME");
                                        newRow["WAERS"] = GetDataRowVaue(row, "CURRENCY");
                                        newRow["LIFNR"] = GetDataRowVaue(row, "ERP_CODE");
                                        newRow["MATNR"] = GetDataRowVaue(row, "MATERIAL_CODE");
                                        newRow["STCD3"] = GetDataRowVaue(row, "GST_NUMBER");
                                        newRow["VEN_ALT_FIR"] = GetDataRowVaue(row, "ALT_FIRST_NAME");
                                        newRow["VEN_ALT_LAS"] = GetDataRowVaue(row, "ALT_LAST_NAME");
                                        newRow["SMTP_ADDR1"] = GetDataRowVaue(row, "ALTEMAIL");
                                        newRow["TEL_NUMBER"] = GetDataRowVaue(row, "ALTPHONE_NO");
                                        newRow["CITY1"] = GetDataRowVaue(row, "LOCATION");
                                        newRow["ERDAT"] = GetDataRowVaue(row, "REG_SINCE");
                                        newRow["UTIME"] = GetDataRowVaue(row, "REG_SINCE");
                                        newRow["TYPE_CODE"] = GetDataRowVaue(row, "TYPE_CODE");
                                        newRow["TYPE_DESC"] = GetDataRowVaue(row, "TYPE_DESC");
                                        newRow["BANK_ACCOUNT_NO"] = GetDataRowVaue(row, "BANK_ACCOUNT_NO");
                                        newRow["IFSC_CODE"] = GetDataRowVaue(row, "IFSC_CODE");
                                        newRow["BANK_NAME"] = GetDataRowVaue(row, "BANK_NAME");
                                        newRow["PAN_NUMBER"] = GetDataRowVaue(row, "PAN_NUMBER");
                                        newRow["OTHER_INFO1"] = fileName;
                                        newRow["DATE_CREATED"] = DateTime.UtcNow;

                                        query += $@"('{guid}',";
                                        query += $@"'{GetDataRowVaue(row, "FIRST_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "LAST_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "EMAIL")}',";
                                        query += $@"'{GetDataRowVaue(row, "PHONE")}',";
                                        query += $@"'{GetDataRowVaue(row, "COMPANY_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "CURRENCY")}',";
                                        query += $@"'{GetDataRowVaue(row, "ERP_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "GST_NUMBER")}',";
                                        query += $@"'{GetDataRowVaue(row, "ALT_FIRST_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "ALT_LAST_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "ALTEMAIL")}',";
                                        query += $@"'{GetDataRowVaue(row, "ALTPHONE_NO")}',";
                                        query += $@"'{GetDataRowVaue(row, "LOCACTION")}',";
                                        query += $@"'{GetDataRowVaue(row, "REG_SINCE")}',";
                                        query += $@"'{GetDataRowVaue(row, "REG_SINCE")}',";
                                        query += $@"'{GetDataRowVaue(row, "TYPE_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "TYPE_DESC")}',";
                                        query += $@"'{GetDataRowVaue(row, "BANK_ACCOUNT_NO")}',";
                                        query += $@"'{GetDataRowVaue(row, "IFSC_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "BANK_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "PAN_NUMBER")}',";
                                        query += $@"'{fileName}',";
                                        query += $@"GETUTCDATE()),";

                                        tempDT.Rows.Add(newRow);
                                    }

                                    query = query.TrimEnd(',');
                                    //bizClass.ExecuteNonQuery_IUD(query);
                                    logger.Debug("END GetVENDORDetails QUERY: " + query);
                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_VENDOR_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_vendor_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }

        private static void DownloadFtpDirectory(string url, NetworkCredential credentials, string localPath, string jobType)
        {
            FtpWebRequest listRequest = (FtpWebRequest)WebRequest.Create(url);
            listRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            listRequest.Credentials = credentials;

            List<string> lines = new List<string>();

            using (FtpWebResponse listResponse = (FtpWebResponse)listRequest.GetResponse())
            using (Stream listStream = listResponse.GetResponseStream())
            using (StreamReader listReader = new StreamReader(listStream))
            {
                while (!listReader.EndOfStream)
                {
                    lines.Add(listReader.ReadLine());
                }
            }

            foreach (string line in lines)
            {
                try
                {
                    string[] tokens = line.Split(new[] { ' ' }, 9, StringSplitOptions.RemoveEmptyEntries);
                    string name = tokens[3];

                    string localFilePath = Path.Combine(localPath, name);
                    string fileUrl = url + "/" + name;

                    if (tokens[2] == "<DIR>" && false)
                    {
                        if (!Directory.Exists(localFilePath))
                        {
                            Directory.CreateDirectory(localFilePath);
                        }

                        DownloadFtpDirectory(fileUrl + "/", credentials, localFilePath, jobType);
                    }
                    else if (tokens[2] != "<DIR>" && !string.IsNullOrWhiteSpace(name) && name.ToLower().Contains(".csv"))
                    {
                        logger.Info("FILEURL:" + fileUrl);
                        if (fileUrl.ToUpper().Contains(jobType))
                        {
                            FtpWebRequest downloadRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                            downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                            downloadRequest.Credentials = credentials;
                            using (FtpWebResponse downloadResponse = (FtpWebResponse)downloadRequest.GetResponse())
                            using (Stream sourceStream = downloadResponse.GetResponseStream())
                            using (Stream targetStream = File.Create(localFilePath))
                            {
                                byte[] buffer = new byte[10240];
                                int read;
                                while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    targetStream.Write(buffer, 0, read);
                                }
                            }

                            FtpWebRequest fileMoveRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                            fileMoveRequest.UseBinary = true;
                            fileMoveRequest.Method = WebRequestMethods.Ftp.Rename;
                            fileMoveRequest.Credentials = credentials;
                            fileMoveRequest.RenameTo = $"{ConfigurationManager.AppSettings["FTPArchiveFolder"]}{name}";
                            var response = (FtpWebResponse)fileMoveRequest.GetResponse();
                            bool Success = response.StatusCode == FtpStatusCode.CommandOK || response.StatusCode == FtpStatusCode.FileActionOK;
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error($"DOWNLOAD FTP FILE: {line} with ERROR:{ex.Message}, DETAILS: {ex.StackTrace}");
                }
            }
        }

        private static string GetDataRowVaue(DataRow row, string columnName)
        {
            if (row.Table.Columns.Contains(columnName))
            {
                return row[columnName] != DBNull.Value ? row[columnName].ToString() : string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
