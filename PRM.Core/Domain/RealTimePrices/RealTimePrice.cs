﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.RealTimePrices
{
    [Table("realtimeprice")]
    public class RealTimePrice : BaseEntity
    {


        public int TypeId { get; set; }

        public string  Product { get; set; }

        public string Category { get; set; }



        public string TradingMode { get; set; }


        public string Region { get; set; }

        public string Market { get; set; }

        public decimal Price { get; set; }

        public string Unit { get; set; }

        public string Currency { get; set; }

        public decimal Change { get; set; }

        public DateTime? PriceDate { get; set; }

    }
}
