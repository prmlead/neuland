﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.Logistics
{
    public class LogisticQuotationReport
    {
       public int RequirementId { get; set; }

        public int QuotationId { get; set; }


        public int ItemId { get; set; }

        public int VendorId { get; set; }

        public string VendorCompanyName { get; set; }

        public string ProductId { get; set; }

        public string ProductNo { get; set; }

        public string Airlines { get; set; }

        public string Routing { get; set; }

        public string Transit { get; set; }

        public decimal Tariff { get; set; }

        public decimal OfferedRate { get; set; }

        public int OfferedRateType { get; set; }

        public decimal RevisedOfferedRate { get; set; }

          public int RevisedOfferedRateType { get; set; }

        public decimal Fsc { get; set; }

        public int FscType { get; set; }

        public decimal Ssc { get; set; }

        public int SscType { get; set; }

        public decimal Misc { get; set; }

        public int MiscType { get; set; }

        public decimal OtherCharges { get; set; }

        public int OtherChargesType { get; set; }
        public decimal RevisedOtherCharges { get; set; }

        public int RevisedOtherChargesType { get; set; }
        public decimal Xray { get; set; }

        public int XrayType { get; set; }
        public decimal Ams { get; set; }

        public int AmsType { get; set; }
        public decimal AwbFee { get; set; }

        public int AwbFeeType { get; set; }

        public decimal DgFee { get; set; }

        public int DgFeeType { get; set; }
        public decimal ForwordDgFee { get; set; }

        public int ForwordDgFeeType { get; set; }


        public decimal CustomClearance { get; set; }

        public int CustomClearanceType { get; set; }

        public decimal ServiceCharges { get; set; }

        public int ServiceChargesType { get; set; }

        public decimal TerminalHandling { get; set; }

        public int TerminalHandlingType { get; set; }


        public double ProductQuantity { get; set; }

        public decimal Total
        {
            get;set;
        }

        public string RequirmentStatus { get; set; }

        public decimal RevTotal { get; set; }

        public int IsQuotationRejected { get; set; }


        public string VendorRemark { get; set; }


        public string Title { get; set; }

        public string InvoiceNumber { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public string ConsigneeName { get; set; }

        public double netWeight { get; set; }

        public string natureOfGoods { get; set; }

        public string finalDestination { get; set; }

        public string productIDorName { get; set; }

        public double palletizeNumber { get; set; }

        public double palletizeQty { get; set; }

        public int Ispalletize { get; set; }

        public string AirwayBillNumber { get; set; }

        public string ShippingBillNumber { get; set; }

        public string LicenseNumber { get; set; }

        public string CustomerName { get; set; }

        public double DrawbackAmount { get; set; }

        public double WarehouseStorages { get; set; }

        public double LoadingUnloadingCharges { get; set; }

        public double AdditionalMiscExp { get; set; }

        public double NarcoticSubcharges { get; set; }

    }
}
