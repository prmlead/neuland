﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRM.Core.Models
{
    [DataContract]
    public class ResponseEval
    {
        string eMessage = string.Empty;
        [DataMember(Name = "errorMessage")]
        public string ErrorMessage
        {
            get
            {
                return this.eMessage;
            }
            set
            {
                this.eMessage = value;
            }
        }

        string sessionId = string.Empty;
        [DataMember(Name = "sessionID")]
        public string SessionID
        {
            get
            {
                return this.sessionId;
            }
            set
            {
                this.sessionId = value;
            }
        }

        [DataMember(Name = "createdBy")]
        public int CreatedBy
        {
            get;
            set;
        }

        [DataMember(Name = "modifiedBy")]
        public int ModifiedBY
        {
            get;
            set;
        }
    }
}