﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class UserInfo : Entity
    {
        [DataMember(Name = "birthday")]
        public DateTime? Birthday { get; set; }

        [DataMember(Name = "userType")]
        public string UserType { get; set; }

        [DataMember(Name = "institution")]
        public string Institution { get; set; }

        [DataMember(Name = "addressLine1")]
        public string AddressLine1 { get; set; }

        [DataMember(Name = "addressLine2")]
        public string AddressLine2 { get; set; }

        [DataMember(Name = "addressLine3")]
        public string AddressLine3 { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "state")]
        public string State { get; set; }

        [DataMember(Name = "country")]
        public string Country { get; set; }

        [DataMember(Name = "zipCode")]
        public string ZipCode { get; set; }

        [DataMember(Name = "addressPhoneNum")]
        public string AddressPhoneNum { get; set; }

        [DataMember(Name = "extension1")]
        public string Extension1 { get; set; }

        [DataMember(Name = "extension2")]
        public string Extension2 { get; set; }

        [DataMember(Name = "userData1")]
        public string UserData1 { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "userID")]
        public string UserID { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "phoneNum")]
        public string PhoneNum { get; set; }

        [DataMember(Name = "category")]
        public string Category { get; set; }

        [DataMember(Name = "isOTPVerified")]
        public int isOTPVerified { get; set; }

        [DataMember(Name = "isEmailOTPVerified")]
        public int isEmailOTPVerified { get; set; }

        [DataMember(Name = "credentialsVerified")]
        public int CredentialsVerified { get; set; }

        [DataMember(Name = "registrationScore")]
        public int RegistrationScore { get; set; }

        [DataMember(Name = "isSuperUser")]
        public bool IsSuperUser { get; set; }

        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "isNegotiationRunning")]
        public int IsNegotiationRunning { get; set; }

        [DataMember(Name = "profileFileName")]
        public string ProfileFileName { get; set; }

        [DataMember(Name = "profileFileUrl")]
        public string ProfileFileUrl { get; set; }

        [DataMember(Name = "phoneID")]
        public string PhoneID { get; set; }

        [DataMember(Name = "phoneOS")]
        public string PhoneOS { get; set; }

        [DataMember(Name = "currency")]
        public string Currency { get; set; }

        [DataMember(Name = "companyId")]
        public int CompanyId { get; set; }

        [DataMember(Name = "altEmail")]
        public string AltEmail { get; set; }

        [DataMember(Name = "altPhoneNum")]
        public string AltPhoneNum { get; set; }

        [DataMember(Name = "address")]
        public string Address { get; set; }

        [DataMember(Name = "isPrimary")]
        public int IsPrimary { get; set; }


        string requestUserRole = string.Empty;
        [DataMember(Name = "requestUserRole")]
        public string RequestUserRole
        {
            get
            {
                if (!string.IsNullOrEmpty(requestUserRole))
                {
                    return requestUserRole;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    requestUserRole = value;
                }
            }
        }

    }
}