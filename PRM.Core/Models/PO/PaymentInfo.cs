﻿
using System;
using System.Runtime.Serialization;
namespace PRM.Core.Models
{
    [DataContract]
    public class PaymentInfo : ResponseAudit
    {

        [DataMember(Name = "pdID")]
        public int PDID { get; set; }

        [DataMember(Name = "poID")]
        public int POID { get; set; }

        string paymentCode = string.Empty;
        [DataMember(Name = "paymentCode")]
        public string PaymentCode
        {
            get
            {
                if (!string.IsNullOrEmpty(paymentCode))
                { return paymentCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { paymentCode = value; }
            }
        }

        [DataMember(Name = "paymentAmount")]
        public decimal PaymentAmount { get; set; }        



        DateTime paymentDate = DateTime.MaxValue;
        [DataMember(Name = "paymentDate")]
        public DateTime? PaymentDate
        {
            get
            {
                return paymentDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    paymentDate = value.Value;
                }
            }
        }
        
        string paymentComments = string.Empty;
        [DataMember(Name = "paymentComments")]
        public string PaymentComments
        {
            get
            {
                if (!string.IsNullOrEmpty(paymentComments))
                { return paymentComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { paymentComments = value; }
            }
        }

        string paymentMode = string.Empty;
        [DataMember(Name = "paymentMode")]
        public string PaymentMode
        {
            get
            {
                if (!string.IsNullOrEmpty(paymentMode))
                { return paymentMode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { paymentMode = value; }
            }
        }

        string transactionID = string.Empty;
        [DataMember(Name = "transactionID")]
        public string TransactionID
        {
            get
            {
                if (!string.IsNullOrEmpty(transactionID))
                { return transactionID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { transactionID = value; }
            }
        }


        string ackComments = string.Empty;
        [DataMember(Name = "ackComments")]
        public string AckComments
        {
            get
            {
                if (!string.IsNullOrEmpty(ackComments))
                { return ackComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { ackComments = value; }
            }
        }

        

    }
}