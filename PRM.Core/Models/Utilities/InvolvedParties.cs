﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class InvolvedParties : Entity
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "userIDList")]
        public int[] UserIDList { get; set; }

        [DataMember(Name = "customerID")]
        public int CustomerID { get; set; }

        [DataMember(Name = "superUserID")]
        public int SuperUserID { get; set; }

        [DataMember(Name="methodName")]
        public string MethodName { get; set; }

        [DataMember(Name = "userRunngPriceList")]
        public double[] UserRunngPriceList { get; set; }

        [DataMember(Name = "vendorName")]
        public string[] VendorName { get; set; }

        [DataMember(Name = "callerID")]
        public int CallerID { get; set; }

        [DataMember(Name = "custCompID")]
        public int CustCompID { get; set; }
    }
}