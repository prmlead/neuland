﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRM.Core.Models
{
    [DataContract]
    public class MaterialReceived : Entity
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }        

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "materialReceivedReceiptNo")]
        public string MaterialReceivedReceiptNo { get; set; }

        [DataMember(Name = "materialReceivedFrom")]
        public string MaterialReceivedFrom { get; set; }

        [DataMember(Name = "materialReceivedMode")]
        public string MaterialReceivedMode { get; set; }

        [DataMember(Name = "materialReceivedThrough")]
        public string MaterialReceivedThrough { get; set; }

        [DataMember(Name = "materialReceivedDate")]
        public DateTime MaterialReceivedDate { get; set; }

        [DataMember(Name = "materialReceivedLink")]
        public string MaterialReceivedLink { get; set; }

        [DataMember(Name = "comments")]
        public string Comments { get; set; }

        [DataMember(Name = "attachment")]
        public byte[] Attachment { get; set; }

        [DataMember(Name = "attachmentName")]
        public string AttachmentName { get; set; }

        [DataMember(Name = "requirementPO")]
        public RequirementPO RequirementPO { get; set; }

        [DataMember(Name = "reqMaterialDetails")]
        public ReqMaterialDetails ReqMaterialDetails { get; set; }

        

        
    }
}