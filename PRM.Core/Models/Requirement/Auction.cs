﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace PRM.Core.Models
{
    [DataContract]
    public class Auction : Entity
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "custFirstName")]
        public string CustFirstName { get; set; }

        [DataMember(Name = "custLastName")]
        public string CustLastName { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "initialPrice")]
        public string InitialPrice { get; set; }

        [DataMember(Name = "finalPrice")]
        public string FinalPrice { get; set; }
    }
}